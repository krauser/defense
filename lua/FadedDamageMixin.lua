
local damageMixinDoDamage = DamageMixin.DoDamage
function DamageMixin:DoDamage(damage, target, point, direction, surface, altMode, showtracer)
   local attacker = nil
   -- Get the attacker
   if self:GetParent() and self:GetParent():isa("Player") then
      attacker = self:GetParent()
   elseif HasMixin(self, "Owner") and self:GetOwner() and self:GetOwner():isa("Player") then
      attacker = self:GetOwner()
   end

   -- Bonus to kill easier "obs" alone spot power faster, but not the one in main
   if (target and target:isa("PowerPoint")) then
      if (#GetEntitiesForTeamWithinRange("InfantryPortal", 1, target:GetOrigin(), 15) == 0
             and #GetEntitiesForTeamWithinRange("PrototypeLab", 1, target:GetOrigin(), 15) == 0
             and #GetEntitiesForTeamWithinRange("ArmsLab", 1, target:GetOrigin(), 15) == 0
             and #GetEntitiesForTeamWithinRange("PhaseGate", 1, target:GetOrigin(), 15) == 0
             and #GetEntitiesForTeamWithinRange("Armory", 1, target:GetOrigin(), 15) == 0
             and #GetEntitiesForTeamWithinRange("Observatory", 1, target:GetOrigin(), 15) > 0
      )
      then
         damage = damage * 280
      end
   end

   -- Players are not affected by dmg scale
   local is_xeno = attacker and attacker:GetActiveWeapon() and attacker:GetActiveWeapon():isa("XenocideLeap")
   local isPlayedByPlayer = (attacker and not DefIsVirtual(attacker) and attacker:isa("Player"))
   if (target and damage and attacker and not (attacker:isa("Hydra") or attacker:isa("Whip"))) then
      if (not is_xeno and attacker:GetTeamNumber() ~= 1) then
         if (not isPlayedByPlayer) then
            damage = damage * getAlienDamageScalar()
         else
            -- Vanilla dmgs are just too strong
            if (attacker:isa("Onos")) then
               if (target:isa("Exo")) then
                  damage = damage / 3 -- Onos players are really too op against exos
               else
                  damage = damage / 2.2
               end

            elseif (attacker:isa("Lerk")) then
               damage = damage / 1.5
            elseif (attacker:isa("Gorge")) then
               damage = damage / 1.2
            else
               damage = damage / 2
            end

            if (target and not target:isa("Player")) then
               if (not (target:isa("PhaseGate") or target:isa("Sentry") or target:isa("Mine"))) then
                  damage = damage / 1.25 -- Prevent building from getting destroyed (unfair)
                  -- if (target.ActivateNanoShield) then
                  --    target:ActivateNanoShield()
                  -- end
               end
            end

            if (not target:isa("Sentry")) then
               if (HasMixin(target, "GhostStructure") and (target:GetIsGhostStructure() or not target:GetIsPowered()))
               then
                  damage = 0.5
                  if (target.ActivateNanoShield) then
                     target:ActivateNanoShield()
                  end
               end
            end
         end
      end
   end
   return damageMixinDoDamage(self, damage, target, point, direction, surface, altMode, showtracer)
end
