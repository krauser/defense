-- forsaken mod
Script.Load("lua/Weapons/Projectile.lua")
Script.Load("lua/Mixins/ModelMixin.lua")
Script.Load("lua/TeamMixin.lua")
Script.Load("lua/DamageMixin.lua")
Script.Load("lua/VortexAbleMixin.lua")

class 'Flare' (Projectile)

Flare.kMapName = "flare"
Flare.kModelName = PrecacheAsset("models/marine/rifle/rifle_grenade.model")

local kMinLifeTime = 5

-- prevents collision with friendly players in range to spawnpoint
Flare.kDisableCollisionRange = 10

local networkVars = { }

AddMixinNetworkVars(BaseModelMixin, networkVars)
AddMixinNetworkVars(ModelMixin, networkVars)
AddMixinNetworkVars(TeamMixin, networkVars)
AddMixinNetworkVars(VortexAbleMixin, networkVars)

function fire_flare_grenade(player)

   if (Server and player) then
      local viewAngles = player:GetViewAngles()
      local viewCoords = viewAngles:GetCoords()

      --  Make sure start point isn't on the other side of a wall or object
      local startPoint = player:GetEyePos() - (viewCoords.zAxis * 0.2)
      local trace = Shared.TraceRay(startPoint, startPoint + viewCoords.zAxis * 25,
                    CollisionRep.Default, PhysicsMask.Bullets, EntityFilterOne(player))

      --  make sure the grenades flies to the crosshairs target
      local grenadeStartPoint = player:GetEyePos() + viewCoords.zAxis * 0.65 - viewCoords.xAxis * 0.35 - viewCoords.yAxis * 0.25

      --  if we would hit something use the trace endpoint, otherwise use the players view direction (for long range shots)
      local grenadeDirection = ConditionalValue(trace.fraction ~= 1, trace.endPoint - grenadeStartPoint, viewCoords.zAxis)
      grenadeDirection:Normalize()

      local grenade = CreateEntity(Flare.kMapName, grenadeStartPoint, player:GetTeamNumber())

      --  Inherit player velocity?
      local startVelocity = grenadeDirection

      if GetIsVortexed(player) then
     startVelocity = startVelocity * 15
      else
     startVelocity = startVelocity * 20
      end

      startVelocity.y = startVelocity.y + 3

      local angles = Angles(0,0,0)
      angles.yaw = GetYawFromVector(grenadeDirection)
      angles.pitch = GetPitchFromVector(grenadeDirection)
      grenade:SetAngles(angles)

      grenade:Setup(player, startVelocity, true)

      -- self.lastFiredGrenadeId = grenade:GetId()

      if GetIsVortexed(player) then
     grenade:SetVortexDuration(player.remainingVortexDuration)
      end
   end
end

local function __arcShot(self, x, y)
   local endPoint = self:GetOrigin() + Vector(x, 0, y)
   local startPoint = self:GetOrigin() + Vector(0, 20, 0)
   GetEffectManager():TriggerEffects("arc_hit_primary", {effecthostcoords = Coords.GetTranslation(endPoint)})

   local hitEntities = GetEntitiesWithinRange("Player", endPoint, ARC.kSplashRadius)
   RadiusDamage(hitEntities, endPoint, 5, 50, self, true)

   if (Client) then
      local tracerVelocity = GetNormalizedVector(endPoint - startPoint) * kTracerSpeed
      local tracer = BuildTracer(startPoint, endPoint,
                                 tracerVelocity,
                                 kRailgunTracerEffectName,
                                 kRailgunTracerResidueEffectName)
      table.insert(Client.tracersList, tracer)
   end
   for index, target in ipairs(hitEntities) do

      if HasMixin(target, "Effects") then
         target:TriggerEffects("arc_hit_secondary")
      end
   end
end

local function _recurStrike(self)
   self:TriggerEffects("arc_firing")
   -- player:TriggerEffects("railgun_attack")

   for x = -7, 7 do
      if ((x % 2) == 0) then
         __arcShot(self, x, 0)
      end
   end
   for y = -7, 7 do
      if ((y % 2) == 0) then
         __arcShot(self, 0, y)
      end
   end
   return false
end

-- local coord = self:GetOrigin()
-- if coord then

   --    self:TriggerEffects("arc_firing")
   --    -- Play big hit sound at origin

   --    -- don't pass triggering entity so the sound / cinematic will always be relevant for everyone
   --    GetEffectManager():TriggerEffects("arc_hit_primary", {effecthostcoords = Coords.GetTranslation(coord)})

   --    -- Faded: Added here
   --    local hitEntities = {}
   --    local _hitEntities = GetEntitiesWithinRange("Alien", coord, ARC.kSplashRadius)
   --    -- Shared:FadedMessage("Entities hit: " .. tostring(#hitEntities))

   --    -- Do damage to every target in range
   --    local max_hit_per_shot = 5
   --    for i = 1, (math.min(max_hit_per_shot, #_hitEntities)) do
   --       if (not _hitEntities[i].client) then -- Do not kill players
   --          table.insert(hitEntities, _hitEntities[i])
   --       end
   --    end

   --    -- Kill all babblers too in range (can't survive that anyway)
   --    local babblers_hit = GetEntitiesWithinRange("Babbler", coord, ARC.kSplashRadius)
   --    for _, b in ipairs(babblers_hit) do
   --       b:Kill()
   --    end

   --    RadiusDamage(hitEntities, coord, ARC.kSplashRadius, ARC.kAttackDamage, self, true)
   --    -- Play hit effect on each
   --    for index, target in ipairs(hitEntities) do

   --       if HasMixin(target, "Effects") then
   --          target:TriggerEffects("arc_hit_secondary")
   --       end
   --    end

   -- end
-- end

local function startAirStrike(self)
   local player = self:GetOwner()
   if (player) then
      _recurStrike(self)
   end
end

function Flare:OnCreate()

    Projectile.OnCreate(self)

    InitMixin(self, BaseModelMixin)
    InitMixin(self, ModelMixin)
    InitMixin(self, TeamMixin)
    InitMixin(self, DamageMixin)
    InitMixin(self, VortexAbleMixin)

    -- don't start our lifetime from here, start it from the first actual tick the grenade exists.
    self:SetNextThink(0.01)
    self.endOfLife = nil

    self.creationTime = Shared.GetTime()
    if Client then
       self.lightColorNormal = Color(1, 0.3, 0.2)
       self.lightColorTwinkle = Color(self.lightColorNormal.r, self.lightColorNormal.g, self.lightColorNormal.b)

       self.light = Client.CreateRenderLight()
       self.light:SetType( RenderLight.Type_Point )
       self.light:SetColor( Color(0.72, 0.7, 0.7) )        -- cf twinkle effect
       self.light:SetIntensity( kFadedFlareIntensity )
       self.light:SetRadius( kFadedFlareRadius )

       self.light:SetAtmosphericDensity(1)
       self.light:SetCastsShadows(false)
       self.light:SetSpecular(false)

       self.light:SetIsVisible(true)

       self.lightTimer = nil
       self.lightTwinkleValue = -1
    end
    if (Server and Shared.GetCheatsEnabled()) then
       Entity.AddTimedCallback(self, startAirStrike, 2)
    end
end

if Client then


   function Flare:OnUpdateRender()
      Projectile:OnUpdateRender()
      if self.light ~= nil then
         local fade_out_delay = 10
         if ((self.creationTime + kFlareLifeTime) - fade_out_delay <= Shared.GetTime())
         then
            local intensity = 0
            intensity = (self.creationTime + kFlareLifeTime) - (Shared.GetTime() + 1)
            intensity = ((intensity * 100) / (fade_out_delay)) / 100
            self.light:SetIntensity(kFadedFlareIntensity * intensity)
            -- self.light:SetIntensity(FlickerLight(self.light:GetIntensity() / 10))
            -- self.lightTimer = Shared.GetTime()
         end
         self.light:SetCoords(
            Coords.GetLookIn(
               self:GetOrigin() + Vector(0, 1.1, 0),
               self:GetCoords().zAxis))
         self.lightTimer = Shared.GetTime()
      end

   end

   function Flare:OnDestroy()
      if self.light ~= nil then
         Client.DestroyRenderLight(self.light)
         self.light = nil
      end
   end
end

function Flare:GetProjectileModel()
   return Flare.kModelName
end
function Flare:GetDeathIconIndex()
   return kDeathMessageIcon.Grenade
end

function Flare:GetDamageType()
    return kGrenadeLauncherGrenadeDamageType
end

if Server then

    function Flare:ProcessHit(targetHit, surface)
        if self:GetVelocity():GetLength() > 2 then
            self:TriggerEffects("grenade_bounce")
        end
    end

    -- Blow up after a time
    function Flare:OnThink()

       -- Grenades are created in predict movement, so in order to get the correct
       -- lifetime, we start counting our lifetime from the first OnThink rather than when
       -- we were created
       if not self.endOfLife then
          self.endOfLife = Shared.GetTime() + kFlareLifeTime
       end

       local delta = self.endOfLife - Shared.GetTime()
       if delta > 0 then
          self:SetNextThink(delta)
       else
          self:Detonate(nil)
       end

    end

    function Flare:Detonate(targetHit)
       DestroyEntity(self)
    end

    function Flare:PrepareToBeWhackedBy(whacker)

       self.whackerId = whacker:GetId()

       -- It is possible that the endOfLife isn't set yet.
       if not self.endOfLife then
          self.endOfLife = 0
       end

       -- Prolong thinktime a bit to give it time to get out of range
       self.endOfLife = math.max(self.endOfLife, Shared.GetTime() + 2)
       self.prepTime = Shared.GetTime()

    end

    function Flare:GetWhacker()
       return self.whackerId and Shared.GetEntity(self.whackerId)
    end

    function Flare:IsWhacked()
       return self.whacked == true
    end

    function Flare:Whack(velocity)

       -- whack the grenade back where it came from.
       self.physicsBody:SetCoords(self:GetCoords())
       self:SetVelocity(velocity)

       self.whacked = true

    end

    function Flare:GetCanDetonate()
       if self.creationTime then
          return self.creationTime + kMinLifeTime < Shared.GetTime()
       end
       return false
    end

    function Flare:SetVelocity(velocity)

       Projectile.SetVelocity(self, velocity)

       if Flare.kDisableCollisionRange > 0 then

          if self.physicsBody and not self.collisionDisabled then

             -- exclude all nearby friendly players from collision
             for index, player in ipairs(GetEntitiesForTeamWithinRange("Player", self:GetTeamNumber(), self:GetOrigin(), Flare.kDisableCollisionRange)) do

                if player:GetController() then
                   Shared.SetPhysicsObjectCollisionsEnabled(self.physicsBody, player:GetController(), false)
                end

             end

             self.collisionDisabled = true

          end

       end

    end

end

Shared.LinkClassToMap("Flare", Flare.kMapName, networkVars)
