
local function moonlightOnCreate(self)
   if (Client.GetLocalPlayer() == self and Shared.GetMapName() == "ns2_kf_farm") then
      self.moonlight = Client.CreateRenderLight()
      self.moonlight:SetIsVisible(true)

      self.moonlight:SetType(RenderLight.Type_Point)
      self.moonlight:SetColor(Color(.15, .15, 1))
      self.moonlight:SetIntensity(0.1)
      self.moonlight:SetRadius(100)
      self.moonlight:SetAtmosphericDensity(0)
      self.moonlight:SetIsVisible(true)
      self.moonlight:SetSpecular(true)
   end
end

local function moonlightOnUpdateRender(self)
   local coords = self:GetCoords()
   local moon_coords = coords
   moon_coords.origin = moon_coords.origin + Vector(22, 30, -25)
   if (self.moonlight) then
      self.moonlight:SetCoords(moon_coords)
   end
end

if (Client) then
   local playerOnCreate = Player.OnCreate
   function Player:OnCreate()
      if (playerOnCreate) then
         playerOnCreate(self)
      end
      moonlightOnCreate(self)
   end
   local playerOnUpdateRender = Player.OnUpdateRender
   function Player:OnUpdateRender()
      if (playerOnUpdateRender) then
         playerOnUpdateRender(self)
      end
      moonlightOnUpdateRender(self)
   end

   local playerOnDestroy = Player.OnDestroy
   function Player:OnDestroy()
      if self.moonlight ~= nil then
         Client.DestroyRenderLight(self.moonlight)
         self.moonlight = nil
      end
      if (playerOnDestroy) then
         playerOnDestroy(self)
      end
   end




   local spectatorOnCreate = Spectator.OnCreate
   function Spectator:OnCreate()
      if (spectatorOnCreate) then
         spectatorOnCreate(self)
      end
      moonlightOnCreate(self)
   end
   local spectatorOnUpdateRender = Spectator.OnUpdateRender
   function Spectator:OnUpdateRender()
      if (spectatorOnUpdateRender) then
         spectatorOnUpdateRender(self)
      end
      moonlightOnUpdateRender(self)
   end

   local spectatorOnDestroy = Spectator.OnDestroy
   function Spectator:OnDestroy()
      if self.moonlight ~= nil then
         Client.DestroyRenderLight(self.moonlight)
         self.moonlight = nil
      end
      if (spectatorOnDestroy) then
         spectatorOnDestroy(self)
      end
   end
end
