
if (Server) then
    local kVerticalOffset = 0.3
    local kBabblerSpawnPoints =
    {
          Vector(0.3, kVerticalOffset, 0.3),
          Vector(-0.3, kVerticalOffset, -0.3),
          Vector(0, kVerticalOffset, 0.3),
          Vector(0, kVerticalOffset, -0.3),
          Vector(0.3, kVerticalOffset, 0),
          Vector(-0.3, kVerticalOffset, 0),
    }

   function BabblerEgg:OnConstructionComplete()

      -- Disables also collision.
      self:SetModel(nil)
      self:TriggerEffects("babbler_hatch")

      -- local owner = self:GetOwner()

      for i = 1, kNumBabblersPerEgg do

         local babbler = CreateEntity(Babbler.kMapName, self:GetOrigin() + kBabblerSpawnPoints[i], self:GetTeamNumber())
         -- babbler:SetOwner(owner)
         babbler:SetSilenced(self.silenced)

         -- if owner and owner:isa("Gorge") then
         --    babbler:SetVariant(owner:GetVariant())
         -- end

         -- babbler:SetMoveType( kBabblerMoveType.Cling, owner, owner:GetOrigin(), true )

         table.insert(self.trackingBabblerId, babbler:GetId())

      end

   end
end

-- if (Server) then
--    function BabblerEgg:OnConstructionComplete()

--       -- Disables also collision.
--       self:SetModel(nil)
--       self:TriggerEffects("babbler_hatch")

--       -- Do nothing during trader wave or if nothing is left to spawn
--       if (isTraderWave or getUnspawnedLifeform(Skulk.kMapName) < kNumBabblersPerEgg) then
--          return
--       end


--       --local owner = self:GetOwner()

--       for i = 1, kNumBabblersPerEgg do

--          spawnAlienCreature(Skulk.kMapName, self:GetOrigin())
--          lifeformSpawned(Skulk.kMapName)
--          -- local babbler = CreateEntity(Babbler.kMapName, self:GetOrigin() + kBabblerSpawnPoints[i], self:GetTeamNumber())

--          -- -- Faded No need for an owner or tracking ids
--          -- babbler:SetOwner(owner)
--          -- babbler:SetSilenced(self.silenced)

--          -- if owner and owner:isa("Gorge") then
--          --    babbler:SetVariant(owner:GetVariant())
--          -- end

--          -- babbler:SetMoveType( kBabblerMoveType.Cling, owner, owner:GetOrigin(), true )

--          -- table.insert(self.trackingBabblerId, babbler:GetId())

--       end

--    end
-- end
