-- ======= Copyright (c) 2003-2011, Unknown Worlds Entertainment, Inc. All rights reserved. =======
--
-- lua\Weapons\Marine\LaySentry.lua
--
--    Created by:   Simon Hiller (andante09@gmx.de)
--    Update & documentation: Jean-Baptiste (jeanbaptiste.laurent.pro@gmail.com)
--
-- ========= For more information, visit us at http://www.unknownworlds.com =====================

-- In order to use, copy & past the template and replace all the _EXWEAP_ by your class (ex: Exosuit/Sentry/etc)
--
--  --> (Diff between each version) Example (for a _EXWEAP_) <--
--  class 'Lay_EXWEAP_' (Weapon)
--  Lay_EXWEAP_.kMapName = "Lay_EXWEAP_"
-- --
--  function Lay_EXWEAP_:GetIsValidRecipient(recipient)
--          local Lay_EXWEAP_ = recipient:GetWeapon(Lay_EXWEAP_.kMa...
--          return Lay_EXWEAP_ == nil
--  function Lay_EXWEAP_:GetDropStructureId()
--      return kTechId._EXWEAP_
--  function Lay_EXWEAP_:GetSuffixName()
--      return "_EXWEAP_" -- Warning: First letter is in lower case
--  function LaySentry:GetDropClassName()
--      return "_EXWEAP_"
--  function Lay_EXWEAP_:GetDropMapName()
--      return _EXWEAP_.kMapName
--  function Lay_EXWEAP_:GetHUDSlot()
--      return 9 -- Where you want this item to be
--  function Lay_EXWEAP_:OnTag(tagName)
--      PROFILE("Lay_EXWEAP_:OnTag")
--  function Lay_EXWEAP_:OnUpdateAnimationInput(modelMixin)
--      PROFILE("Lay_EXWEAP_:OnUpdateAnimationInput")
--      function Lay_EXWEAP_:OnProcessIntermediate(input)
-- --


Script.Load("lua/Weapons/Weapon.lua")
Script.Load("lua/PickupableWeaponMixin.lua")

class 'LayFade' (Weapon)

LayFade.kMapName = "layfade"

local kDropModelName = PrecacheAsset("models/marine/mine/mine_pile.model")
local kHeldModelName = PrecacheAsset("models/marine/welder/welder.model") --PrecacheAsset("models/marine/mine/mine_3p.model")

local kViewModelName = PrecacheAsset("models/marine/welder/welder_view.model") --PrecacheAsset("models/marine/mine/mine_view.model")
local kAnimationGraph = PrecacheAsset("models/marine/welder/welder_view.animation_graph") --PrecacheAsset("models/marine/mine/mine_view.animation_graph")

local kPlacementDistance = 40
-- 5 is cluster grenades
-- 4 are mines
-- 3 is welder
local kHUDSlot = 4

local networkVars =
   {
      minesLeft = string.format("integer (0 to %d)", 5),
      droppingMine = "boolean",
      kHUDSlot = "integer",
      oldWeaponMapName = "string (32)",
      oldWeaponAmmo = "integer",
   }

local function GetFreeSlot(player)
   if (player and player:isa("Marine")) then
      if (player:GetWeaponInHUDSlot(1) == nil) then return (1) end
      if (player:GetWeaponInHUDSlot(2) == nil) then return (2) end
      if (player:GetWeaponInHUDSlot(3) == nil) then return (3) end
      if (player:GetWeaponInHUDSlot(4) == nil) then return (4) end
      if (player:GetWeaponInHUDSlot(5) == nil) then return (5) end
   end
   return (4)
end

function LayFade:OnCreate()

   Weapon.OnCreate(self)

   -- InitMixin(self, PickupableWeaponMixin)

   self.minesLeft = 5
   self.droppingMine = false
   -- self:SetHUDSlot(GetFreeSlot(self:GetParent()))
   self:SetHUDSlot(kHUDSlot)
end

function LayFade:OnInitialized()

   Weapon.OnInitialized(self)

   self:SetModel(kHeldModelName)

end

function LayFade:GetIsValidRecipient(recipient)

   -- if (self:GetParent() == nil and recipient
   --    and not GetIsVortexed(recipient) and recipient:isa("Marine")
   --    and GetFreeSlot(recipient) > 0)
   --  then

   --      local LayFade = recipient:GetWeapon(LayFade.kMapName)
   --      return LayFade == nil

   --  end

   return false

end

function LayFade:GetDropStructureId()
   return kTechId.Fade
end

function LayFade:GetMinesLeft()
   return self.minesLeft
end

-- function LayFade:GetReplacementWeaponMapName()
--    return Axe.kMapName
-- end

function LayFade:GetViewModelName()
   return kViewModelName
end

function LayFade:GetAnimationGraphName()
   return kAnimationGraph
end

function LayFade:GetSuffixName()
   return "fade"
end

function LayFade:GetDropClassName()
   return "Fade"
end

function LayFade:GetDropMapName()
   return Fade.kMapName
end

function LayFade:SetHUDSlot(slot)
   self.kHUDSlot = slot or kHUDSlot
   -- self.kHUDSlot = slot or kHUDSlot
end

function LayFade:GetHUDSlot()
   -- local player = self:GetParent()

   -- if (player) then -- pick first unused slot
   --    local free_slot = GetFreeSlot(player)
   --    if (free_slot > 0) then
   --       self.kHUDSlot = free_slot
   --       return (free_slot)
   --    end
   -- end
   return self.kHUDSlot
   -- If all are full, we have client error (do not know
   -- how to fix them)
end

function LayFade:OnTag(tagName)

   PROFILE("LayFade:OnTag")

   ClipWeapon.OnTag(self, tagName)

   if tagName == "mine" then

      local player = self:GetParent()
      if player then

         self:PerformPrimaryAttack(player)

         if self.minesLeft == 0 then

            self:OnHolster(player)
            player:RemoveWeapon(self)
            player:SwitchWeapon(1)

            if Server then
               DestroyEntity(self)
            end

         end

      end

      self.droppingMine = false

   end

end

function LayFade:OnPrimaryAttackEnd(player)
   self.droppingMine = false
end

function LayFade:GetIsDroppable()
   return true
end

function LayFade:OnPrimaryAttack(player)

   -- Ensure the current location is valid for placement.
   if not player:GetPrimaryAttackLastFrame() then

      local showGhost, coords, valid = self:GetPositionForStructure(player)
      if valid then

         if self.minesLeft > 0 then
            self.droppingMine = true
            self:PerformPrimaryAttack(player)
            self:OnHolster(player)
            -- player:RemoveWeapon(self)
            -- player:SwitchWeapon(3)

            -- self.minesLeft = self.minesLeft - 1
            -- if Server then
            --    DestroyEntity(self)
            -- end
         else
            self.droppingMine = false

            if Client then
               player:TriggerInvalidSound()
            end

         end

      else

         self.droppingMine = false

         if Client then
            player:TriggerInvalidSound()
         end

      end

   end

end


local function DropStructure(self, player)

   if Server then
      player:ClearOrders() -- Remove obj order

      local showGhost, coords, valid = self:GetPositionForStructure(player)
      if valid then

         -- Create mine.
         local mine = nil--CreateEntity(self:GetDropMapName(), Vector(0, 0, 0), 2)
         -- mine = CreateEntity(self:GetDropMapName(), coords.origin, 2)

         mine = self
         local o = GetLocationAroundFor(coords.origin,kTechId.Armory,5)
         if o and mine then
            -- min:SetOrigin(coords.origin)
            -- Check for space
            mine = player:Replace(self:GetDropMapName())
            mine:SetOrigin(o)
            mine:SetCameraDistance(0)

            lifeformSpawned(self:GetDropMapName())
            -- mine:SetOwner(player)
            if mine:SpaceClearForEntity(coords.origin) then

               local angles = Angles()
               angles:BuildFromCoords(coords)
               mine:SetAngles(angles)

               player:TriggerEffects("create_" .. self:GetSuffixName())


               -- Jackpot.
               return true, mine

            else

               player:TriggerInvalidSound()
               -- DestroyEntity(mine)

            end

         else
            player:TriggerInvalidSound()
         end

      else

         if not valid then
            player:TriggerInvalidSound()
         end

      end

   elseif Client then
      return true
   end

   return false

end

function LayFade:Refill(amount)
   self.minesLeft = amount
end

function LayFade:PerformPrimaryAttack(player)

   local success = true

   if self.minesLeft > 0 then

      player:TriggerEffects("start_create_" .. self:GetSuffixName())

      local viewAngles = player:GetViewAngles()
      local viewCoords = viewAngles:GetCoords()
      local entity = nil

      success, entity = DropStructure(self, player)

      if success then
         self.minesLeft = 5
         -- if (entity ~= nil) then
         --    entity:SetConstructionComplete()
         -- end
      end

   end

   return success

end

function LayFade:OnHolster(player, previousWeaponMapName)

   Weapon.OnHolster(self, player, previousWeaponMapName)

   self.droppingMine = false

end

function LayFade:OnDraw(player, previousWeaponMapName)

   Weapon.OnDraw(self, player, previousWeaponMapName)

   -- Attach weapon to parent's hand
   self:SetAttachPoint(Weapon.kHumanAttachPoint)

   self.droppingMine = false

   self:SetModel(kHeldModelName)

end

function LayFade:Dropped(prevOwner)

   -- -- Print("Weapon drop request")
   -- if (prevOwner) then
   --    FadedSpawnBuilding(kTechId.Sentry, Sentry.kMapName,
   --                       prevOwner:GetOrigin(), Vector(0, -0.6, 0), 5)
   --    if Server then
   --       DestroyEntity(self)
   --    end
   -- end
   -- Weapon.Dropped(self, prevOwner)

   --self:SetModel(kDropModelName)

end

-- Given a gorge player's position and view angles, return a position and orientation
-- for structure. Used to preview placement via a ghost structure and then to create it.
-- Also returns bool if it's a valid position or not.
function LayFade:GetPositionForStructure(player)

   local isPositionValid = false
   local foundPositionInRange = false
   local structPosition = nil

   local origin = player:GetEyePos() + player:GetViewAngles():GetCoords().zAxis * kPlacementDistance

   -- Trace short distance in front
   local trace = Shared.TraceRay(player:GetEyePos(), origin, CollisionRep.Default, PhysicsMask.AllButPCsAndRagdolls, EntityFilterTwo(player, self))

   local displayOrigin = trace.endPoint

   -- If we hit nothing, trace down to place on ground
   if trace.fraction == 1 then

      origin = player:GetEyePos() + player:GetViewAngles():GetCoords().zAxis * kPlacementDistance
      trace = Shared.TraceRay(origin, origin - Vector(0, kPlacementDistance, 0), CollisionRep.Default, PhysicsMask.AllButPCsAndRagdolls, EntityFilterTwo(player, self))

   end


   -- If it hits something, position on this surface (must be the world or another structure)
   if trace.fraction < 1 then

      foundPositionInRange = true

      if trace.entity == nil then
         isPositionValid = true
      elseif not trace.entity:isa("ScriptActor") and not trace.entity:isa("Clog") and not trace.entity:isa("Web") then
         isPositionValid = true
      end

      displayOrigin = trace.endPoint - player:GetViewAngles():GetCoords().zAxis * 0.3

      -- Don't allow dropped structures to go too close to techpoints and resource nozzles
      -- if GetPointBlocksAttachEntities(displayOrigin) then
      --    isPositionValid = false
      -- end

      -- Don't allow placing above or below us and don't draw either
      local structureFacing = player:GetViewAngles():GetCoords().zAxis

      if math.abs(Math.DotProduct(trace.normal, structureFacing)) > 0.9 then
         structureFacing = trace.normal:GetPerpendicular()
      end

      -- if trace.normal:DotProduct(Vector(0, 1, 0)) < .5 then
      --    isPositionValid = false
      -- end

      -- Coords.GetLookIn will prioritize the direction when constructing the coords,
      -- so make sure the facing direction is perpendicular to the normal so we get
      -- the correct y-axis.
      local perp = Math.CrossProduct(trace.normal, structureFacing)
      structureFacing = Math.CrossProduct(perp, trace.normal)

      structPosition = Coords.GetLookIn(displayOrigin, structureFacing, trace.normal)
   end

   if (isPositionValid) then
      isPositionValid = canAlienFieldComSpawnAlien(self, player, structPosition.origin)
   end
   return foundPositionInRange, structPosition, isPositionValid

end

function LayFade:GetGhostModelName()
   return LookupTechData(self:GetDropStructureId(), kTechDataModel)
end

function LayFade:OnUpdateAnimationInput(modelMixin)

   PROFILE("LayFade:OnUpdateAnimationInput")

   modelMixin:SetAnimationInput("activity", ConditionalValue(self.droppingMine, "primary", "none"))

end

if Client then

   function LayFade:OnProcessIntermediate(input)

      local player = self:GetParent()

      if player then

         self.showGhost, self.ghostCoords, self.placementValid = self:GetPositionForStructure(player)
         self.showGhost = self.showGhost and self.minesLeft > 0

      end

   end

end

function LayFade:GetWeight()
   return (0)
end

function LayFade:GetShowGhostModel()
   return self.showGhost
end

function LayFade:GetGhostModelCoords()
   return self.ghostCoords
end

function LayFade:GetIsPlacementValid()
   return self.placementValid
end

-- Annexes changes

-- GetTexCoordsForTechId(kTechId.Welder)
gTechIdPosition[kTechId.LayFade] = kDeathMessageIcon.Welder

-- local originalMarineGetPlayerStatusDesc
-- originalMarineGetPlayerStatusDesc
--    = Class_ReplaceMethod("Marine", "GetPlayerStatusDesc",
--                          function(self)
--                             local weapon = self:GetWeaponInHUDSlot(6)
--                             if (weapon) and self:GetIsAlive() then
--                                if (weapon:isa("LayFade")) then
--                                   return "Battery"--kPlayerStatus.FadedBattery
--                                end
--                             end
--                             return originalMarineGetPlayerStatusDesc(self)
--                          end
--                         )


Shared.LinkClassToMap("LayFade", LayFade.kMapName, networkVars)
