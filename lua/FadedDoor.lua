-- Script.Load("lua/LiveMixin.lua")


-- local networkVars = {
-- }

-- AddMixinNetworkVars(LiveMixin, networkVars)


-- local doorOnCreate = Door.OnCreate
-- function Door:OnCreate()
--    doorOnCreate(self)
--    InitMixin(self, LiveMixin)
-- end

-- function Door:GetShowHealthFor(player)
--    return true
-- end
-- function Door:GetReceivesStructuralDamage()
--     return true
-- end

-- function Door:OnTakeDamage()
--    Shared:FadedMessage("It huuurts\n")
-- end

-- Shared.LinkClassToMap("Door", Door.kMapName, networkVars)

function getAliveDoorAround(ent)
   local doors = {}

   if (ent) then
      local all_doors = GetEntitiesForTeamWithinRange("Door", 1,
                                                      ent:GetOrigin(),
                                                      3)
      for _, d in ipairs(all_doors) do
         if (d and d:GetIsAlive() and d:GetState() ~= Door.kState.Open) then
            table.insert(doors, d)
         end
      end
   end
   return doors
end
