-- ===================== Faded Mod =====================
--
-- lua\FadedGamerules.lua
--
--    Created by: Rio (rio@myrio.de)
--    Update by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

local locale = LibCache:GetLibrary("LibLocales-1.0")
local strformat = string.format
local floor = math.floor
local random = math.random
local tinsert = table.insert

local alive_marine = {}

local kFadedTotalRespawn = 5
-- List of spectate fade player
spectateFadedPlayers = {}

-- List of players already auto added once
babblerPlayersAutoJoin = {}
-- List of players in the babbler player list
babblerPlayers = {}
babblerNextSpawnWave = 0

-- Next veteran marine (random on alive player)
-- Note: I use table.insert() / table.remove() for this table
fadedNextVeteranPlayersName = {}
fadedVeteranPlayersName = {}

-- Player who kill fade or winning faded alive
fadedNextPlayersName = {}
-- Faded of the current round (for selection chance)
fadedPlayersName = {}
fadedMarinesMainBase = ""
fadedMarinesMainBaseCoord = Vector(0, 0, 0)

-- fadedNextPlayerName = nil
-- fadedPlayerName = nil

local fadedPregameTenSecMessage = false
local fadedPregameFiveSecMessage = false
local fadedModLastTimeTwoPlayersToStartMessage = nil

local fadedMapTime = 0
local startingBatteryDropped = false

local cycle = MapCycle_GetMapCycle()
local mod_loaded_time = Shared.GetTime()
local first_start_in_msg_refresh = Shared.GetTime()

if (Server) then
   -- Checks if the faded is dead
   -- Does not count hallucination
   function NS2Gamerules:CheckIfFadedIsDead()
      for _, faded in pairs(self.team2:GetPlayers()) do
         if (faded ~= nil and faded:isa("Player") and faded:GetIsAlive()
                and not faded:isa("Skulk") -- Do not count babbler players
                and faded.isHallucination ~= true)
         then
            return (false)
         end
      end
      return (true)
   end

   -- Checks if the marines are dead
   function NS2Gamerules:CheckIfMarinesAreDead()
      for _, marine in pairs(self.team1:GetPlayers()) do
         if (marine ~= nil and (marine:isa("Player") and not marine:isa("Skulk"))
             and marine:GetIsAlive()) then
            return (false)
         end
      end
      return (true)
   end

   -- Only allow players to join the marine and random team
   function NS2Gamerules:GetCanJoinTeamNumber(teamNumber)
      return true //(teamNumber ~= 2)
   end

   local joinTeam = NS2Gamerules.JoinTeam
   function NS2Gamerules:JoinTeam(player, newTeamNumber, force)

      -- if (newTeamNumber == 2 and force ~= true) then
      --    player:FadedMessage("Can't join the alien team (bots only)")
      --    return false
      -- end

      local alien_scale = 3
      local alien_slot_left = (self.team1:GetNumPlayers() / alien_scale) - self.team2:GetNumPlayers()
      if (newTeamNumber == 2) then
         if (alien_slot_left >= 1 or self.team2:GetNumPlayers() == 0) then
            force = true -- force respawn
            -- newTeamNumber = 1
         else
            player:FadedMessage("Team full (limited to 1 alien player for " .. tostring(alien_scale) .. " marines)")
            return false
         end
      end

      if (getWaveNb() <= 1 or isTraderWave() or player:GetDeaths() == 0) then
         force = true -- Respawn by force on wave 1
      end
      -- If in ready room, remove spectatefade and babbler
      if (newTeamNumber == kTeamReadyRoom) then
         SpectateFadedChatCommand(player, true)
         BabblerPlayerChatCommand(player, false, true)
         -- player = player:Replace(Marine.kMapName, 1, false, player:GetOrigin())
      end

      if (babblerPlayers[player:GetName()] == nil) then
         babblerPlayers[player:GetName()] = kFadedTotalRespawn
         -- joinTeam(self, player, 1, force)
         -- player = player:Replace(Marine.kMapName, 1, false, player:GetOrigin())
      end
      return joinTeam(self, player, newTeamNumber, force)
   end

   -- Allow friendly fire to help the Faded a bit
   function NS2Gamerules:GetFriendlyFire() return kFadedModFriendlyFireEnabled end
   function GetFriendlyFire()
      return kFadedModFriendlyFireEnabled
   end

   -- Display the Faded health and armor left when the round
   function NS2Gamerules:printFadedHealthArmorLeft()
      for _, faded in ipairs(self.team2:GetPlayers()) do
         if (faded and faded:GetIsAlive()
                and not faded:isa("Skulk")
             and faded.isHallucination ~= true) then
            local hp = string.format("%.0f", faded:GetHealth())
            local ap = string.format("%.0f", faded:GetArmor())
            Shared:FadedMessage("Faded '" .. faded:GetName() .. "'"
                                   .. " has " .. hp .. "/" .. ap
                                   .. " (hp/ap)")
         end
      end
   end

   -- Define the game ending rules
   function NS2Gamerules:CheckGameEnd()
      -- Print("Faded NS2Gamerules:CheckGameEnd()")
      if (self:GetGameStarted() and self.timeGameEnded == nil
          and Shared.FadedMessage) then
         if (self.timeLastGameEndCheck == nil
                or (Shared.GetTime() > self.timeLastGameEndCheck + 1))
         then

            local team1Players = self.team1:GetNumPlayers()
            local team2Players = self.team2:GetNumPlayers()

            local ip_powered = 0
            for _, ip in ipairs(GetEntities("InfantryPortal")) do
               if (ip and ip:GetIsPowered() and not ip:GetIsGhostStructure()) then
                  ip_powered = ip_powered + 1
               end
            end

            self.timeLastGameEndCheck = Shared.GetTime()
            -- local team1Players = self.team1:GetNumPlayers()

            if (team1Players == 0) then
               -- Shared:FadedMessage(locale:ResolveString("FADED_NO_MARINES_LEFT"))
               self:EndGame(self.team2)
               -- elseif (team2Players == 0) then
               --    Shared:FadedMessage(locale:ResolveString("FADED_NO_FADED_LEFT"))
               --    self:EndGame(self.team1)
            elseif (self:CheckIfMarinesAreDead() == true and ip_powered == 0) then
               -- Shared:FadedMessage(locale:ResolveString("FADED_VICTORY"))

               -- If the Faded wins, the player has a higher chance to be the Faded next round
               self:EndGame(self.team2)
               -- elseif (FadedRoundTimer:GetIsRoundTimeOver()) then
               --    Shared:FadedMessage(locale:ResolveString("FADED_ROUND_TIME_OVER"))
               --    self:SaveEndRoundAliveMarine()
               --    self:EndGame(self.team1)
               --    --self:DrawGame()
            elseif (areAllWavesFinished()) then
               self:EndGame(self.team1)
               -- elseif (#GetEntitiesForTeam("Hive", 2) == 0 or not GetEntitiesForTeam("Hive", 2)[1]:GetIsAlive()) then
               --    self:EndGame(self.team1)
            end
            self.timeLastGameEndCheck = Shared.GetTime()
         end
      end
   end

   function NS2Gamerules:SpawnAlienBaseBuilding(main_base_coords)
      -- local rand_it = math.random(1, #alien_spawn_points)
      local coord = nil
      local main_base_coord = main_base_coords[1]:GetOrigin()
      -- local main_base_coord = alien_spawn_points[rand_it]:GetOrigin()
      local tech_id = {kTechId.Hive, kTechId.Crag, kTechId.Whip, kTechId.Whip, kTechId.Whip, kTechId.Hydra, kTechId.Hydra, kTechId.Hydra, kTechId.Hydra, kTechId.Hydra, kTechId.Hydra, kTechId.Hydra, kTechId.Hydra, kTechId.Hydra, kTechId.Hydra, kTechId.Hydra, kTechId.Hydra, kTechId.Hydra, kTechId.Hydra, kTechId.Shell, kTechId.Shell}

      coord = GetLocationAroundFor(main_base_coord, kTechId.Armory, 10)
      for i, b in ipairs({Hive.kMapName, Crag.kMapName, Whip.kMapName, Whip.kMapName, Whip.kMapName, Hydra.kMapName, Hydra.kMapName, Hydra.kMapName, Hydra.kMapName, Hydra.kMapName, Hydra.kMapName, Hydra.kMapName, Hydra.kMapName, Hydra.kMapName, Hydra.kMapName, Hydra.kMapName, Hydra.kMapName, Hydra.kMapName, Hydra.kMapName, Shell.kMapName, Shell.kMapName})
      do
         coord = main_base_coord
         for j = 0, 30 do
            coord = GetLocationAroundFor(coord, tech_id[i], 10)
            if (coord) then
               local trace = Shared.TraceRay(coord, coord - Vector(0, 10, 0), CollisionRep.Default, PhysicsMask.AllButPCsAndRagdolls, EntityFilterAll())
               coord = trace.endPoint
               if (tech_id[i] == kTechId.Hive) then
                  FadedSpawnBuilding(tech_id[i], b, coord, Vector(0, 3.5, 0), 10, true, 2)
               else
                  FadedSpawnBuilding(tech_id[i], b, coord, Vector(0, 0, 0), 10, true, 2)
               end
               break
            end
            coord = main_base_coord + Vector(0, j*0.1, j*0.1)
         end
      end
      for i = 1, 4 do
         local orig = GetLocationAroundFor(main_base_coord, kTechId.Clog, 50)
         if (orig) then
            orig = orig - Vector(0, 0.6, 0)
            if (orig) then
               local clog = CreateEntity(Clog.kMapName, orig, 2)
               if (clog) then
                  clog:createClogWhip(8)
               end
            end
         end
      end
      -- for index, entity in ientitylist(Shared.GetEntitiesWithClassname("Clog"))
      -- do
      --    if (math.random() < 0.2) then
      --       local h = CreateEntity(Clog.kMapName, entity:GetOrigin(), 2)
      --       if (h and h.SetConstructionComplete) then
      --          h.clogDelta = entity:GetId()
      --          h:createClogWhip(5)
      --          h:SetConstructionComplete()
      --       end
      --    end
      -- end
   end

   function ConstructMiniMarineBase(orig)
      if (orig) then
         FadedSpawnBuilding(kTechId.Armory, Armory.kMapName, orig, Vector(0, -0.6, 0))
         FadedSpawnBuilding(kTechId.PhaseGate, PhaseGate.kMapName, orig, Vector(0, -0.6, 0))
         FadedSpawnBuilding(kTechId.Observatory, Observatory.kMapName, orig, Vector(0, -0.6, 0))
      end
   end

   -- Spawn a set of structure at the marine main base
   -- for the objectif mode.
   function NS2Gamerules:SpawnMainBaseBuilding(marine_spawn_points)
      local struct = nil
      -- Spawn a building at the valid places, coord_fix is a patch
      -- to fix the building beeing placed in the air
      ----------

      local it = 1
      local main_base_coord = nil

      -- local rand_it = math.random(1, #marine_spawn_points)
      for _, marine_spawn_point in ipairs(marine_spawn_points) do
         main_base_coord = marine_spawn_point:GetOrigin()
         fadedMarinesMainBase = marine_spawn_point:GetLocationName()
         fadedMarinesMainBaseCoord = main_base_coord
         -- local main_base_coord = marine_spawn_points[1]:GetOrigin()



         ----------------------------------------
         local nb_marine_alive = 0
         local bonus_IP = 0
         for _, marine in ipairs(GetGamerules():GetTeam1():GetPlayers()) do
            if (marine and marine:GetIsAlive()) then
               nb_marine_alive = nb_marine_alive + 1
            end
         end

         if (nb_marine_alive >= 10) then
            bonus_IP = math.floor((nb_marine_alive - 10) / 5)
         end

         -- local nb_exo = Clamp(nb_marine_alive / 5, 1, 10)
         -- for nb_drop = 1, nb_exo do
         --    local max_range = 5
         --    local extents = GetExtents(kTechId.Armory)
         --    local entity = nil

         --    -- Team.RespawnPlayer(player:GetTeam(), player, orig, Angles(0, 0, 0)
         --    local spawnPoint = GetRandomBuildPosition(kTechId.Exo,
         --                                              main_base_coord,
         --                                              max_range)
         --    if (spawnPoint) then
         --       entity = CreateEntity(Exosuit.kMapName, main_base_coord, 1)
         --    end
         -- end

         ----------------------------------------

         --Shared:FadedMessage("Armslabs dropped")
         if (Shared.GetMapName() ~= "ns2_def_troopers") then
            FadedSpawnBuilding(kTechId.Armory, Armory.kMapName, main_base_coord, Vector(0, -0.6, 0))
            FadedSpawnBuilding(kTechId.PrototypeLab, PrototypeLab.kMapName, main_base_coord, Vector(0, -0.6, 0))
            FadedSpawnBuilding(kTechId.ArmsLab, ArmsLab.kMapName, main_base_coord, Vector(0, -0.6, 0))
            FadedSpawnBuilding(kTechId.InfantryPortal, InfantryPortal.kMapName, main_base_coord, Vector(0, -0.6, 0))
            if (bonus_IP > 0) then
               FadedSpawnBuilding(kTechId.InfantryPortal, InfantryPortal.kMapName, main_base_coord, Vector(0, -0.6, 0))
               bonus_IP = bonus_IP - 1
            end

            FadedSpawnBuilding(kTechId.Observatory, Observatory.kMapName, main_base_coord, Vector(0, -0.6, 0))


         else
            local struct = nil


            -- for i = 0, 100 do -- kf_farm location are upstairs, help ns2 a bit to place them
            --    struct = CreateEntity(InfantryPortal.kMapName, main_base_coord + Vector(0, i/50, 0), 1)
            --    if (struct) then break end
            -- end
            -- if (struct) then
            --    SetRandomOrientation(struct)
            --    if (struct.SetConstructionComplete) then
            --       struct:SetConstructionComplete()
            --    end
            -- else
            FadedSpawnBuilding(kTechId.InfantryPortal, InfantryPortal.kMapName, main_base_coord, Vector(0, -0.6, 0))
            -- end

         end

         -- if (#marine_spawn_points > 1) then
         if (Shared.GetMapName() ~= "ns2_def_troopers") then
            FadedSpawnBuilding(kTechId.PhaseGate, PhaseGate.kMapName, main_base_coord, Vector(0, -0.6, 0))
         end

         -- if (Shared.GetMapName() == "ns2_kf_farm") then
         --    FadedSpawnBuilding(kTechId.Armory, Armory.kMapName, main_base_coord, Vector(0, -0.6, 0))
         -- end











         -- end

         -- for i = 0, 4 do
         --    FadedSpawnBuilding(kTechId.LayMines, LayMines.kMapName, main_base_coord, Vector(0, -0.6, 0))
         -- end


         -- FadedSpawnBuilding(kTechId.SentryBattery, SentryBattery.kMapName, main_base_coord, Vector(0, -0.6, 0))
      end
      local team1Players = self.team1:GetPlayers()
      for _, player in ipairs(team1Players) do
         SpawnSinglePlayer(player, main_base_coord)
      end

      if (Shared.GetMapName() ~= "ns2_kf_farm" and Shared.GetMapName() ~= "ns2_def_troopers") then

         local pp = {}
         for _, entity in ientitylist(Shared.GetEntitiesWithClassname("PowerPoint")) do
            if (#GetEntitiesForTeamWithinRange("Armory", 1, entity:GetOrigin(), 40) == 0) then
               table.insert(pp, entity)
            end
         end

         -- One armory per power
         for i = 1, math.min(3, #pp) do
            local it = nil
            local orig = nil

            for e = 1, 30 do
               if (it == nil or #GetEntitiesForTeamWithinRange("Armory", 1, orig, 50) > 0) then
                  it = math.random(1, #pp)
                  orig = pp[it]:GetOrigin()
               end
            end

            if (it and orig) then
               ConstructMiniMarineBase(orig)
               table.remove(pp, it)
            end
         end
      end

   end -- endfct

   local function _get_rand_location(loc_available)
      return loc_available[math.random(1, #loc_available)]
   end

   local location_per_map = nil
   function _initLocationPerMap()
      -- if (location_per_map == nil) then
      location_per_map = {}

      location_per_map["ns2_biodome"] = _get_rand_location({"Falls"})
      location_per_map["ns2_caged"] = _get_rand_location({"Central Processing"})
      location_per_map["ns2_derelict"] = _get_rand_location({"Plaza", "Administration"})
      location_per_map["ns2_descent"] = _get_rand_location({"Hydroanalysis"})
      -- location_per_map["ns2_docking"] = _get_rand_location({"Locker"})
      -- location_per_map["ns2_tram"] = _get_rand_location({"Hub"})
      location_per_map["ns2_eclipse"] = _get_rand_location({"T-Junction"})
      location_per_map["ns2_kodiak"] = _get_rand_location({"Central Biosphere", "Upper Rapids"})
      -- location_per_map["ns2_mineshaft"] = _get_rand_location({"Central Drilling"})
      -- location_per_map["ns2_refinery"] = _get_rand_location({"Containment"})--({"Lava Falls"})
      location_per_map["ns2_summit"] = _get_rand_location({"Crossroads", "Crevice"})
      location_per_map["ns2_veil"] = _get_rand_location({""})
      location_per_map["ns2_dark"] = _get_rand_location({"Mountain Pass", "Mineshaft"})
      location_per_map["ns2_veil_five"] = _get_rand_location({"Monorail", "System Waypointing"})
      location_per_map["ns2_mvm_fray"] = _get_rand_location({"C-2", "C-1"})

      --end
   end

   -- Return fade_spawn_coord, {marine_spawn1, ...}
   function NS2Gamerules:GetBothTeamSpawnPoint()
      local _alien_spawn = {}
      local alien_spawn = {}
      local _marine_spawn = {}
      local marine_spawn = {}
      local rt = {}

      _initLocationPerMap()
      -- for i = 0, 10 do
      --    -- local nb_tp = #GetEntities("TechPoint")
      --    if (string.find(Shared.GetMapName(), "_xs_")) then
      --       nb_TP = 1
      --    else
      --       nb_TP = 9999
      --    end
      -- end

      nb_TP = math.min(1, self.team1:GetNumPlayers() / 5) + 1 -- Max 2 bases
      if (Shared.GetMapName() == "ns2_kf_farm") then
         nb_TP = nb_TP + 1
      end

      if (location_per_map[Shared.GetMapName()]) then

         for index, entity in ientitylist(Shared.GetEntitiesWithClassname("TechPoint")) do
            if (location_per_map[Shared.GetMapName()] == entity:GetLocationName()) then
               table.insert(marine_spawn, entity)
            else
               table.insert(_marine_spawn, entity)
            end
         end
         -- Sometime there are no TP, check TP too
         if (#marine_spawn == 0) then
            for _, entname in ipairs({"ResourcePoint", "PowerPoint"}) do
               for index, entity in ientitylist(Shared.GetEntitiesWithClassname(entname)) do
                  if (location_per_map[Shared.GetMapName()] == entity:GetLocationName()) then
                     if (#marine_spawn == 0 or math.random() > 0.5) then
                        marine_spawn = {}
                        table.insert(marine_spawn, entity)
                     end
                  end
               end
            end
         end
      else
         for index, entity in ientitylist(Shared.GetEntitiesWithClassname("TechPoint")) do
            table.insert(_marine_spawn, entity)
         end
      end

      nb_TP = nb_TP - #marine_spawn
      for i = 1, nb_TP do
         if (#_marine_spawn > 0) then
            local rand_spawn = math.random(1, #_marine_spawn)
            table.insert(marine_spawn, _marine_spawn[rand_spawn])
            _marine_spawn[rand_spawn] = nil
         end
      end

      -- for index, entity in ientitylist(Shared.GetEntitiesWithClassname("TechPoint")) do
      --    if (#marine_spawn == 0) then
      --       table.insert(marine_spawn, entity)
      --    end
      --    break
      -- end

      -- for index, entity in ientitylist(Shared.GetEntitiesWithClassname("ResourcePoint")) do
      --    local id = FindNearestEntityId("TechPoint", entity:GetOrigin())
      --    local nearest_tp = Shared.GetEntity(id)
      --    if (nearest_tp and entity:GetOrigin():GetDistanceTo(nearest_tp:GetOrigin()) > 35) then
      -- table.insert(_alien_spawn, getRandomRT())
      --    end
      -- end

      -- for index, entity in ientitylist(Shared.GetEntitiesWithClassname("ResourcePoint")) do
      --    table.insert(_alien_spawn, entity) -- Force the insert
      -- end
      -- for index, entity in ientitylist(Shared.GetEntitiesWithClassname("TechPoint")) do
      --    table.insert(_alien_spawn, entity) -- Force the insert
      -- end

      _alien_spawn = getRtPoints()

      --_alien_spawn = sortByProximityReverse(marine_spawn[1], _alien_spawn)
      local farest = 1
      for i, s in ipairs(_alien_spawn) do
         -- if (_alien_spawn[farest]:GetOrigin():GetDistanceTo(marine_spawn[1]:GetOrigin())
         --     < _alien_spawn[i]:GetOrigin():GetDistanceTo(marine_spawn[1]:GetOrigin())) then
         if (getPathDist(_alien_spawn[farest]:GetOrigin(), marine_spawn[1]:GetOrigin())
             < getPathDist(_alien_spawn[i]:GetOrigin(), marine_spawn[1]:GetOrigin())) then
            farest = i
         end
      end
      alien_spawn = {_alien_spawn[farest]}
      return alien_spawn, marine_spawn
   end

   -- Check if there is room for at least a building (so we are sure)
   function GetLocationAroundFor(orig, tech_id, pmax_range)
      local spawnPoint = nil
      local max_range = 15
      local extents = GetExtents(kTechId.Armory)

      if (pmax_range) then
         max_range = pmax_range
      end

      orig = orig + Vector(0, 0.2, 0) -- A bit above
      -- Team.RespawnPlayer(player:GetTeam(), player, orig, Angles(0, 0, 0)
      for i = 0, 10 do
         spawnPoint = GetRandomBuildPosition(tech_id, orig, max_range)
         --spawnPoint = GetLocationAroundFor(orig, kTechId.Armory, 10)
         if (spawnPoint) then
            break
         end
         orig = orig + Vector(math.random()-0.5, 0, math.random()-0.5)
      end
      return spawnPoint
   end

   function SpawnSinglePlayer(player, orig)
      local spawnPoint = nil
      for i = 0, 10 do
         spawnPoint = GetRandomBuildPosition(kTechId.Armory,
                                             orig,
                                             15)
         --spawnPoint = GetLocationAroundFor(orig, kTechId.Armory, 10)
         if (spawnPoint) then
            break
         end
         if (math.random() < 0.5) then
            orig = orig + Vector(0, 0.1, 0)
         else
            orig = orig + Vector(0, 0, 0.1)
         end
      end
      if spawnPoint then
         player:SetOrigin(spawnPoint)
      else
         player:SetOrigin(orig + Vector(0, 0.6, 0))--Team.RespawnPlayer(player:GetTeam(), player, orig, Angles(0, 0, 0))
      end
   end

   -- Spawn at one point, standart system
   -- -- Return spawn point not used by the faded
   function NS2Gamerules:FadedSpawnAtOnePoint(fade_spawn,
                                              marine_spawn_points,
                                              team1Players,
                                              team2Players)
      -- -- Print("Faded NS2Gamerules:FadedSpawnAtOnePoint()")
      -- For safety, spawn Fade on a free spawn point
      for _, faded in ipairs(team2Players) do
         -- Team.RespawnPlayer(faded:GetTeam(), faded, fade_spawn:GetOrigin(),
         --             Angles(0, 0, 0))
         SpawnSinglePlayer(faded, fade_spawn:GetOrigin())
      end
      for _, player in ipairs(team1Players) do
         local new_coord = marine_spawn_points[1]:GetOrigin()
         -- Team.RespawnPlayer(player:GetTeam(), player, new_coord, Angles(0, 0, 0))
         SpawnSinglePlayer(player, new_coord)
      end
   end

   -- Dispatch marine around the map
   function NS2Gamerules:FadedSpawnMarineAroundMap(marine_spawn_points)
      -- Print("Faded NS2Gamerules:FadedSpawnMarineAroundMap()")
      local team1Players = self.team1:GetPlayers()

      -- marine_spawn_points = GetEntities(CommandStation.kMapName)
      if (#marine_spawn_points > 0) then
         local i = math.random(1, #marine_spawn_points)
         for _, player in ipairs(team1Players)
         do
            if (player and marine_spawn_points[i]) then
               SpawnSinglePlayer(player, marine_spawn_points[i]:GetOrigin())
            end
            i = math.max((i + 1) % (#marine_spawn_points+1), 1)
            -- Team.RespawnPlayer(player:GetTeam(), player, new_coord, Angles(0, 0, 0))
         end
      end


   end

   -- Define the rules for the game start
   function NS2Gamerules:CheckGameStart()
      -- Print("Faded NS2Gamerules:CheckGameStart()")
      local team1Players = self.team1:GetNumPlayers()
      local team2Players = self.team2:GetNumPlayers()

      local first_round_time = mod_loaded_time + kFadedNextMapWaitingTime
      if (Shared.GetTime() < first_round_time)
      then
         if (first_start_in_msg_refresh < Shared.GetTime())
         then
            local start_in = string.format("%.0f", first_round_time - Shared.GetTime())
            first_start_in_msg_refresh = first_start_in_msg_refresh + kFadedNextMapMessageRefresh
            Shared:FadedMessage(
               string.format("First round starting in %ds", start_in))
         end
         return
      end

      if (team1Players + team2Players == 1
          and Shared.GetTime() - (fadedModLastTimeTwoPlayersToStartMessage or 0) > kFadedModTwoPlayerToStartMessage) then
         -- if (Shared.FadedMessage) then
         --    Shared:FadedMessage(locale:ResolveString("FADED_GAME_NEEDS_TWO_PLAYERS"))
         -- end
         fadedModLastTimeTwoPlayersToStartMessage = Shared.GetTime()
         return
      end

      if self:GetGameState() == kGameState.NotStarted then
         if (team1Players > 0 or (team1Players > 0 and team2Players > 0))
         then
            self:SetGameState(kGameState.PreGame)
         end
      elseif self:GetGameState() == kGameState.PreGame then
         if (team1Players + team2Players == 0) then
            self:SetGameState(kGameState.NotStarted)
         end
      end
   end


   function NS2Gamerules:SaveEndRoundAliveMarine()
      -- Print("Faded NS2Gamerules:SaveEndRoundAliveMarine()")
      -- Get alive marine
      alive_marine = {}
      for _, marine in pairs(self.team1:GetPlayers()) do
         if (marine ~= nil and marine:GetIsAlive())
         then
            table.insert(alive_marine, marine:GetName())
         end
      end
   end

   -- If marine win, choose N veteran among the alive marines
   function NS2Gamerules:ChooseVeteranPlayers()
      -- Print("Faded NS2Gamerules:ChooseVeteranPlayers()")
      local cpt = 0
      local rand_nb = 0
      local max_veteran = kMarineNb / kFadedVeteranScale

      if (self.team1:GetNumPlayers() < 1) then return end
      -- If a veteran is alive, he keep his veteran
      -- If the veteran is the next Faded, skip
      for it, veteran_name in ipairs(alive_marine) do
         if (fadedVeteranPlayersName[veteran_name] == true
             and fadedPlayersName[alive_marine[rand_nb]] ~= true) then
            fadedNextVeteranPlayersName[alive_marine[it]] = true
            table.remove(alive_marine, it)
            cpt = cpt + 1
         end
      end

      -- Choose random veteran marine among survivor
      -- If the marine is the Faded or a Veteran, choose a new one
      while (cpt < max_veteran and table.getn(alive_marine) > 0) do
         rand_nb = math.random(1, table.getn(alive_marine))
         if (fadedPlayersName[alive_marine[rand_nb]] ~= true
                and not fadedNextVeteranPlayersName[alive_marine[rand_nb]])
         then
            fadedNextVeteranPlayersName[alive_marine[rand_nb]] = true
            cpt = cpt + 1
         end
         table.remove(alive_marine, rand_nb)
         -- end
      end
      alive_marine = {}
   end

   function NS2Gamerules:ChooseFadedPlayers()
      -- Print("Faded NS2Gamerules:ChooseFadedPlayers()")
      -- Choose a random player as Faded
      local cpt = 0
      local _marine_nb = GetGamerules():GetTeam1():GetNumPlayers()
      local new_faded_list = {}
      -- Print("NS2Gamerules:ChooseFaded()")
      while (_marine_nb > 0 and (cpt * kFadedAddFadeMarineScale) < _marine_nb) do
         local player = nil
         -- Retrieve the next player selected, if any
         for nextFadedName, value in pairs(fadedNextPlayersName) do
            if (value == true) then
               player = self:GetSingleFaded(nextFadedName)
               fadedNextPlayersName[nextFadedName] = nil
               break
            end
         end
         -- No more next faded at this point, reset
         if (player == nil) then
            fadedPlayersName = {}
         end
         -- If we choose the same player, choose again
         for i = 0, 10 do
            if (player == nil or new_faded_list[player:GetName()]) then
               player = self:GetSingleFaded(nil)
               break
            end
         end
         -- Add player to the list of new faded
         if (player) then
            new_faded_list[player:GetName()] = true
         end
         cpt = cpt + 1
      end
      fadedPlayersName = {} -- Reset
      fadedPlayersName = new_faded_list
      fadedNextPlayersName = {}
      -- Print("NS2Gamerules:ChooseFaded() END")
   end

   ------------

   -- TODO:
   -- * perf leak (order ?)
   -- * Alien gameplay
   -- * Command pour spawn les aliens (ancien com alien)

   function NS2Gamerules:RemoveBuilding()
      -- Print("Faded NS2Gamerules:RemoveBuilding()")
      -- Since farm as TP for upstair, remove the model (so downstrairs it does not looks ugly)
      if (Shared.GetMapName() == "ns2_kf_farm") then
         for index, entity in ientitylist(Shared.GetEntitiesWithClassname("TechPoint")) do
            entity:SetModel(nil)
            --DestroyEntity(entity)
         end
      end

      if (Shared.GetMapName() ~= "ns2_kf_farm" and Shared.GetMapName() ~= "ns2_def_troopers") then
         -- Turn off the lights by removing all powernode
         -- local base_found = false
         -- local IPs = GetEntities("InfantryPortal")

         -- local PPs = {}
         -- for index, pp in ientitylist(Shared.GetEntitiesWithClassname("PowerPoint")) do
         --    table.insert(PPs, pp)
         -- end

         -- for _, ip in ipairs(IPs) do
         --    Shared.SortEntitiesByDistance(ip:GetOrigin(), PPs)
         -- end

         for index, pp in ientitylist(Shared.GetEntitiesWithClassname("PowerPoint")) do
            -- base_found = false
            -- for _, ip in ipairs(IPs) do
            --    if (pp and ip and ip:GetLocationName() == pp:GetLocationName()) then
            --       base_found = true
            --       break
            --    end
            -- end
            if (#GetEntitiesForTeamWithinRange("InfantryPortal", 1, pp:GetOrigin(), 40) == 0) then
               pp:Kill()
            end
         end
      end
   end

   -- Print the mod version
   function NS2Gamerules:PrintModVersion()
      if (kFadedModVersion:lower():find("development")) then
         Shared:FadedMessage("Warning! This is a development version! It's for testing purpose only!")
      elseif (kFadedModVersion:lower():find("alpha")) then
         Shared:FadedMessage("Warning! This is an alpha version, which means it's not finished yet!")
      elseif (kFadedModVersion:lower():find("beta")) then
         Shared:FadedMessage("This mod is in beta. Feel free to leave feedback on the Steam workshop page.")
      end
   end

   -- Some info messages
   function NS2Gamerules:PrintInfoMessageAtRoundStart()
      -- Print("Faded NS2Gamerules:PrintInfoMessageAtRoundStart()")

      -- -- No need to tell the marines who is the Faded, check the scoreboard !
      -- for faded_name in pairs(fadedPlayersName) do
      --      self:GetTeam1():FadedMessage(strformat(locale:ResolveString("FADED_PLAYER_IS_NOW_THE_FADED"), faded_name))
      -- end

      if (kFadedModFriendlyFireEnabled) then
         self:GetTeam1():FadedMessage(locale:ResolveString("FADED_MARINE_GAME_STARTED_2"))
      end
      self:GetTeam1():FadedMessage(locale:ResolveString("FADED_MARINE_GAME_STARTED_1"))
   end

   local playerScores = {}
   function NS2Gamerules:SaveScore(allPlayers)
      -- Print("Faded NS2Gamerules:SaveScore()")
      playerScores = {}
      for _, fromPlayer in ientitylist(allPlayers) do
         local score = 0
         if HasMixin(fromPlayer, "Scoring") then
            score = fromPlayer:GetScore()
         end

         local playerScore = {
            Score = score,
            Kills = fromPlayer:GetKills(),
            Deaths = fromPlayer:GetDeaths()
         }

         local clientIndex = fromPlayer:GetClientIndex()
         playerScores[clientIndex] = playerScore
      end
   end

   function NS2Gamerules:RestoreScore(allPlayers)
      -- Print("Faded NS2Gamerules:RestoreScore()")
      for _, fromPlayer in ientitylist(allPlayers) do
         local clientIndex = fromPlayer:GetClientIndex()

         if HasMixin(fromPlayer, "Scoring") and playerScores[clientIndex] then
            -- fromPlayer.score = playerScores[clientIndex].Score
            fromPlayer:AddScore(playerScores[clientIndex].Score)
         end
         if (playerScores[clientIndex]) then
            fromPlayer.kills = playerScores[clientIndex].Kills
            fromPlayer.deaths = playerScores[clientIndex].Deaths
         end
      end
   end

   --------------

   function NS2Gamerules:SwitchFadedToMarineTeam()
      -- Print("Faded NS2Gamerules:SwitchFadedToMarineTeam()")
      -- Switch the faded to the marine team
      if (self.team2:GetNumPlayers() > 0) then
         -- for i, playerId in ipairs(self.team2.playerIds) do
         --    local alienPlayer = Shared.GetEntity(playerId)
         --    if (alienPlayer) then
         --       self:JoinTeam(alienPlayer, 1, false)
         --    end
         -- end
         local faded_list = self.team2:GetPlayers()
         for _, faded in pairs(faded_list) do
            self:JoinTeam(faded, 1, false)
         end
      end
   end

   -- Move marine into a Veteran marine
   function NS2Gamerules:MoveMarineToVeteran()
      -- Print("Faded NS2Gamerules:MoveMarineToVeteran()")
      local player = nil
      fadedVeteranPlayersName = {}
      if (self.team1:GetNumPlayers() <= 3) then
         for _, marine in pairs(self.team1:GetPlayers()) do
            fadedNextVeteranPlayersName[marine:GetName()] = true
         end
      end
      for k, v in pairs(fadedNextVeteranPlayersName) do
         player = Shared:GetPlayerByName(k)
         -- if (player and player:isa("Marine")) then
         --    Print("Replace marine to veteran player: " .. player:GetName())
         -- end
         if (player and player:isa("Marine")) then
            fadedVeteranPlayersName[player:GetName()] = true
            local list_msg =
               {
                  "FADED_VETERAN_INF0_1",
                  "FADED_VETERAN_INF0_2",
                  "FADED_VETERAN_INF0_3",
                  "FADED_VETERAN_INF0_4",
               }
            VeteranMarine:UpgradeMarine(player)
            -- -- Disable messages for Veteran (it spam the chat just too much)
            -- for k, v in pairs(list_msg) do
            --    player:FadedMessage(locale:ResolveString(v))
            -- end
            -- Shared:FadedMessage(strformat(locale:ResolveString("FADED_PLAYER_IS_NOW_A_VETERAN"), player:GetName()))
         end
      end
      -- Safety reset
      fadedNextVeteranPlayersName = {}
      alive_marine = {}
   end

   -- local function updateSpectatorBabblerIndicator(self)
   --    local table_ref = {}
   --    for _, name in ipairs(babblerPlayers) do
   --       table_ref[name] = true
   --    end
   --    for _, str in ipairs({"Spectator"}) do
   --       for _, spectator in ientitylist(Shared.GetEntitiesWithClassname(str)) do
   --          if (spectator and spectator.GetName
   --                 and table_ref[spectator:GetName()]
   --              and fadedPlayersName[spectator:GetName()] ~= true) then
   --             local fmtstr = locale:ResolveString("FADED_BABBLERPLAYER_RESPAWN_IN")
   --             local delay = babblerNextSpawnWave - Shared.GetTime()
   --             delay = string.format("%.1f", delay)
   --             local final_msg = string.format(fmtstr, delay)
   --             spectator:FadedMessage(final_msg)
   --          end
   --       end
   --    end
   --    Entity.AddTimedCallback(self, updateSpectatorBabblerIndicator, kFadedBabblerPlayerRespawnMsg)
   -- end

   local _babblerPlayerNextSpawnLocation = {}
   local function triggerBabblersPlayerRespawn(self)
      if (#_babblerPlayerNextSpawnLocation > 0) then
         local spawn_loc = _babblerPlayerNextSpawnLocation[1]
         table.remove(_babblerPlayerNextSpawnLocation, 1)
         GetGamerules():spawnBabblerPlayers(spawn_loc, 1)
         Entity.AddTimedCallback(self, triggerBabblersPlayerRespawn, 0.4)
      end
   end

   local function _getDistFromNeareastMarine(rt)
      if (rt) then
         local id = FindNearestEntityId("Marine", rt:GetOrigin())
         local nearest_marine = Shared.GetEntity(id)
         if (nearest_marine) then
            return (rt:GetOrigin():GetDistanceTo(nearest_marine:GetOrigin()))
         end
      end
      return (0)
   end

   local function sortSpawnByMarineProximity(rt_point)
      local i = 1
      local sorted_rt_point = {}
      local added = false
      for _, rt in ipairs(rt_point) do
         i = 0
         local m1 = _getDistFromNeareastMarine(rt)
         for it, rt2 in ipairs(sorted_rt_point) do
            local m2 = _getDistFromNeareastMarine(rt2)
            if (m1 < m2) then
               table.insert(sorted_rt_point, it, rt)
               added = true
               break
            end
         end
         if (added == false) then
            table.insert(sorted_rt_point, #sorted_rt_point + 1, rt)
         end
      end
      -- for it, rt in ipairs(sorted_rt_point) do
      --    Shared:FadedMessage("Nearest marine from rt n'" .. tostring(it)
      --                           .. " is at " .. _getDistFromNeareastMarine(rt) .. "m")
      -- end
      return (sorted_rt_point)
   end

   local function defaultBabblersPlayerRespawn(self)

      -- only spawn again if we are not currently spawning entities
      if (#_babblerPlayerNextSpawnLocation == 0) then
         local rt_point = {}
         local l_rt_places = {
            Shared.GetEntitiesWithClassname("TechPoint"),
            Shared.GetEntitiesWithClassname("ResourcePoint"),
            -- Shared.GetEntitiesWithClassname("Ragdoll")
         }
         for _, rt_places in ipairs(l_rt_places) do
            for index, entity in ientitylist(rt_places) do
               local marines = nil
               marines = GetEntitiesForTeamWithinRange("Marine", 1,
                                                       entity:GetOrigin(),
                                                       kFadedMinDistFromMarineToRespw)
               if (#marines == 0)
               then
                  table.insert(rt_point, entity)
                  -- Shared:FadedMessage("Insert done")
               else
                  -- Shared:FadedMessage("Marine around: " .. tostring(#marines))
               end
            end
         end
         local i = 0
         rt_point = sortSpawnByMarineProximity(rt_point)
         local rt_nb = 0--math.random(1, table.getn(rt_point))
         if (#rt_point > 0) then
            while (i < #babblerPlayers) do
               rt_nb = ((rt_nb + 1) % #rt_point) + 1
               table.insert(_babblerPlayerNextSpawnLocation,
                            rt_point[rt_nb]:GetOrigin())
               -- GetGamerules():spawnBabblerPlayers(rt_point[rt_nb]:GetOrigin(), 1)
               i = i + 3
            end
            triggerBabblersPlayerRespawn(self)
         end
      end

      local fade_nb = 0
      for _ in pairs(fadedPlayersName) do
         fade_nb = fade_nb + 1
      end
      local skulk_nb = self.team2:GetNumPlayers() - fade_nb
      skulk_nb = Clamp(skulk_nb, 0, kFadedMaxBabblerPlayerSpawnDelay)
      -- babblerNextSpawnWave = Shared.GetTime() + kFadedBabblerPlayerSpawnInterval
      local wave_time = 1-- + skulk_nb * kFadedDelayOnSpawnWavePerBabbler
      babblerNextSpawnWave = Shared.GetTime() + wave_time

      ---
      local proto = GetEntitiesForTeam("PrototypeLab", 1)
      -- if (kFadedInstantBabblerSpawnWithProto
      --     and #proto > 0 and proto[1]:GetIsPowered() == true) then
      --    Entity.AddTimedCallback(self, defaultBabblersPlayerRespawn, 5)
      -- else
      --    Entity.AddTimedCallback(self, defaultBabblersPlayerRespawn, wave_time)
      -- end

   end

   local function movePlayerToFadedSpectator(self)
      local player = nil
      for player_name, v in pairs(spectateFadedPlayers) do
         player = Shared:GetPlayerByName(player_name)
         if (v and player and player:GetIsAlive() == false
                and fadedPlayersName[player:GetName()] ~= true
             and player:GetTeamNumber() ~= kSpectatorIndex) then
            self:JoinTeam(player, kSpectatorIndex)
         end
      end
      Entity.AddTimedCallback(self, movePlayerToFadedSpectator, 3)
   end


   local function botsGlobalBrain(self)
      return (true)
   end

   local function respawnAlienPlayers(self)
      local function _MoveToAlienTeamAndReplace(player)
         if (player and player:GetTeamNumber() == 2 and not DefIsVirtual(player) and not player:GetIsAlive())
         then
            if (HasMixin(player, "Order")) then
               player:ClearOrders()
            end
            player:AddResources(100)
            changeToAlienFieldCom(player,
                                  GetLocationAroundFor(
                                     getRandomRTAroundMarine():GetOrigin(),
                                     kTechId.Armory,
                                     5))
         end
      end
      Server.ForAllPlayers(_MoveToAlienTeamAndReplace)
      return true
   end

   local function updateBotsOrders(self)
      for _, name in ipairs({"Alien", "Drifter"}) do
         for _, alien in ipairs(GetEntitiesForTeam(name, 2)) do
            if (alien and alien:GetIsAlive() and DefIsVirtual(alien)) then
               if (not self:isa("Drifter")) then
                  AlienUpdateOrderCallback(alien)
               end
               AlienKillAfterTimeout(alien)
            end
         end
      end
   end

   local function updateMarineBotsOrders(self)
      for _, marine in ipairs(GetEntitiesForTeam("Marine", 1)) do
         if (marine and marine:GetIsAlive() and DefIsVirtual(marine)) then
            MarineBotUpdateOrderCallback(marine)
         end
      end
      return true
   end

   local InitCallbackDone = false
   function NS2Gamerules:InitCallback()
      if (InitCallbackDone == false) then
         Entity.AddTimedCallback(self, defaultBabblersPlayerRespawn, 3)
         -- Entity.AddTimedCallback(self, updateSpectatorBabblerIndicator, kFadedBabblerPlayerRespawnMsg)
         Entity.AddTimedCallback(self, movePlayerToFadedSpectator, 3)

         Entity.AddTimedCallback(self, handleWaves, 1)
         Entity.AddTimedCallback(self, respawnAlienPlayers, 2)
         -- Entity.AddTimedCallback(self, SatanPunishMarines, 1)
         Entity.AddTimedCallback(self, updateBotsOrders, 1)
         -- Entity.AddTimedCallback(self, updateMarineBotsOrders, 0.2)
         -- Entity.AddTimedCallback(self, SpawnSentryBatteryOnMap, kFadedBatterySpawnDelay)
         InitCallbackDone = true
         babblerNextSpawnWave = Shared.GetTime() + 3
      end
   end

   local kFadedLastMarineEvent = false
   local lastEpiqueMusic = nil
   local music = {
      PrecacheAsset("sound/NS2.fev/music/main_menu"),
      PrecacheAsset("sound/NS2.fev/ambient/docking/docking_music"),
      -- PrecacheAsset("sound/NS2.fev/ambient/spooky_music_summit"),
      -- PrecacheAsset("sound/NS2.fev/common/explore")
   }

   local function buildAllPowerNode()
      local all_powerPoint = Shared.GetEntitiesWithClassname("PowerPoint")
      for index, powerPoint in ientitylist(all_powerPoint) do
         if (powerPoint) then
            if (powerPoint:GetPowerState() == PowerPoint.kPowerState.unsocketed) then
               powerPoint:SocketPowerNode()
            end
            powerPoint:SetConstructionComplete()
         end
      end
   end

   local resetGame = NS2Gamerules.ResetGame
   function NS2Gamerules:ResetGame()

      -- Print("Faded NS2Gamerules:ResetGame()")
      -- Disable auto team balance
      -- See  FadedRagdoll.lua
      -- if (self:GetGameStarted()) then
      --      self:SpawnSentryBatteryOnMap(true)
      -- end

      -- clearAllClogsWhip()
      clearAllDeadDoorEffects()

      startingBatteryDropped = false
      kFadedLastMarineEvent = false
      Server.SetConfigSetting("auto_team_balance", nil)

      -- Move all previous alien/Faded spectator to marine team
      local function _MoveToMarineTeam(player)
         -- Do not move spectator and admin in RR into game
         local isPlaying = player:GetTeamNumber() == 2 or player:GetTeamNumber() == 1
         local isFadedSpectator = spectateFadedPlayers[player:GetName()]

         if (player and player.GetName) then
            babblerPlayers[player:GetName()] = kFadedTotalRespawn
         end

         -- if (player and (isPlaying or isFadedSpectator))
         -- then
         --    --player:SetCameraDistance(0)
         --    self:JoinTeam(player, 1, true)

         --    -- if (not babblerPlayersAutoJoin[player:GetName()])
         --    -- then -- auto add once in the babbler player list
         --    babblerPlayersAutoJoin[player:GetName()] = true
         --    BabblerPlayerChatCommand(player, true)
         --    -- end
         -- end
      end
      Server.ForAllPlayers(_MoveToMarineTeam)

      -- for i, _ in ipairs(babblerPlayers) do
      --    babblerPlayers[i] = 5 -- RESPAWN / LIFE
      -- end


      local allPlayers = Shared.GetEntitiesWithClassname("Player")
      -- self:SaveScore(allPlayers)
      resetGame(self)
      buildAllPowerNode()
      -- self:RestoreScore(allPlayers)

      -- Marine count and faded nb count
      kMarineNb = GetGamerules():GetTeam1():GetNumPlayers()
      if (kMarineNb < 0) then kMarineNb = 0 end
      if (kMarineNb <= 0) then
         Print("Faded error: Try to reset a game with no marine")
         return
      end
      self:InitCallback()

      kPlayerHallucinationNumFraction = kMarineNb

      -- Count fade number for the next round
      -- kFadeNb = 0
      -- while ((kFadeNb * kFadedAddFadeMarineScale) < kMarineNb) do
      --    kFadeNb = kFadeNb + 1
      -- end

      -- Choose Faded
      -- self:ChooseFadedPlayers()

      -- local function _updateFadedList(player)
      --      for _, faded in ipairs(fadedPlayersName) do
      --         Server.SendNetworkMessage(player, "AddScoreboardFadeList",
      --                       { name = faded:GetName()}, true)
      --      end
      -- end
      -- local function _resetFadedScoreBoardList(player)
      --      Server.SendNetworkMessage(player, "ResetFadedScoreboardList",
      --                    {}, true)
      -- end

      -- -- Check if we have at least 1 Faded
      -- Server.ForAllPlayers(_resetFadedScoreBoardList)
      -- Server.ForAllPlayers(_updateFadedList)
      -- for faded in pairs(fadedPlayersName) do
      --    -- Move selected Faded to alien team

      --    local function _MoveToAlienTeam(player)
      --       if (fadedPlayersName[player:GetName()] == true
      --           and self.team2:GetNumPlayers() <= kFadeNb) then
      --          player:SetCameraDistance(0)
      --          self:JoinTeam(player, 2, true)
      --       end
      --    end
      --    Server.ForAllPlayers(_MoveToAlienTeam)

      --    -- Convert alien to Fade
      --    local function _SpawnFaded(player)
      --       if (player:GetTeamNumber() == 2) then
      --          kFadedSpawnFadeHallucination = false
      --          -- local p = player:Replace(Marine.kMapName) -- Fake marine test
      --          -- if (math.random(1, 2) == 1) then
      --          --       p:GiveItem(Flamethrower.kMapName)
      --          -- else
      --          --       p:GiveItem(Shotgun.kMapName)
      --          -- end
      --          -- player:Replace(Skulk.kMapName)
      --          player:Replace(Fade.kMapName)
      --          DestroyEntity(player)
      --          kFadedSpawnFadeHallucination = true
      --       end
      --    end
      --    Server.ForAllPlayers(_SpawnFaded)

      --    -- Create veteran among alive marine
      --    -- self:ChooseVeteranPlayers()
      --    -- self:MoveMarineToVeteran()
      --    -- Some info messages
      --    self:PrintInfoMessageAtRoundStart()
      --    self:PrintModVersion()
      --    break
      -- end

      -- Spawn player around the map
      local alien_spawn_points, marine_spawn_points = self:GetBothTeamSpawnPoint()
      self:SpawnMainBaseBuilding(marine_spawn_points)
      alien_spawn_points, marine_spawn_points = self:GetBothTeamSpawnPoint()
      --self:SpawnAlienBaseBuilding(alien_spawn_points)--getRandomRT():GetOrigin())


      -- self:FadedSpawnMarineAroundMap(marine_spawn_points)

      -- Reset the mod timer
      -- FadedRoundTimer:Reset()
      -- Init Timer GUI for all player with the new time
      for _, fromPlayer in ientitylist(allPlayers) do
         -- Server.SendNetworkMessage(fromPlayer, "RoundTime",
         --                           {time = kFadedModRoundTimerInSecs},
         --                           true)

         -- Server.SendNetworkMessage(fromPlayer, "PlayClientPrivateMusic",
         --                           {music = "sound/NS2.fev/ambient/docking/docking_music"},
         --                           true)
         Server.SendNetworkMessage(fromPlayer, "kMarineNb_kFadeNB",
                                   {kMarineNb = kMarineNb, kFadeNb = kFadeNb},
                                   true)
      end


      local function _MoveToAlienTeamAndReplace(player)
         if (player:GetTeamNumber() == 2) then
            changeToAlienFieldCom(player)
         end
      end
      Server.ForAllPlayers(_MoveToAlienTeamAndReplace)

      self:RemoveBuilding()


      -- local alientechtree = GetTechTree(kTeam2Index)
      -- alientechtree:GetTechNode(kTechId.BileBomb):SetResearched(true)
      -- alientechtree:GetTechNode(kTechId.Umbra):SetResearched(true)
      -- alientechtree:GetTechNode(kTechId.Spores):SetResearched(true)
      -- alientechtree:GetTechNode(kTechId.Stomp):SetResearched(true)

      self:SetMaxBots(0, false)
      resetWaves()
      -- if (#GetEntities("TechPoint") <= 2) then
      self:SetAllTech(true)
      -- end
   end

   function NS2Gamerules:SetMaxBots(newMax, com)
      self.botTeamController:SetMaxBots(0, false) --newMax, com)
      self.botTeamController:UpdateBots()
   end

   -- Return the Ids of players in a marine team without the last fade
   -- Note: Player who type "nofade" are removed
   function NS2Gamerules:GetPossiblePlayerIdsForTeam()
      local playerIds = {}
      for _, player in ipairs(self.team1:GetPlayers()) do
         local playerId = player:GetId()
         local clientIndex = player:GetClientIndex()

         -- Exclude Faded player
         if (not self.noFaded[clientIndex]
                and fadedNextPlayersName[player:GetName()] ~= true
             and fadedPlayersName[player:GetName()] ~= true) then
            tinsert(playerIds, playerId)
         end
      end

      return playerIds
   end

   -- Return the Ids of players in marine team
   function NS2Gamerules:GetPossiblePlayerIds()
      -- Print("Faded NS2Gamerules:GetPossiblePlayerIds()")
      local playerTeam = self:GetPossiblePlayerIdsForTeam()
      return playerTeam
   end

   -- Faded selection
   -- The variable "fadedNextPlayer" contain the name of the player
   -- who won the round, which can be:
   -- * The Faded
   -- * The marine who kill the Faded.
   -- The selection is divided in 2 part:
   -- 1/ Check if the winner can be the faded
   -- ** (according to "faded_selection_chance")
   -- 2/ If not, pick a random player
   function NS2Gamerules:GetSingleFaded(fadedNextPlayerName)
      -- Print("Faded NS2Gamerules:GetSingleFaded()")
      local player = nil
      -- Print("NS2Gamerules:GetSingleFaded()")
      if (not FadedMod.nextFaded) then
         -- Use a random number to check if the winner can be the Faded
         -- Random return a float, range: (min:0.0, max:0,9)
         if (fadedNextPlayerName ~= nil) then
            local randomNumber = math.random(0, 100)
            if (fadedPlayersName[fadedNextPlayerName] == nil) then
               if (randomNumber <= kFadedModFadedSelectionChance)
               then -- If we just kill a fade
                  -- Print("Choice 80: " .. tostring(fadedNextPlayerName))
                  player = Shared:GetPlayerByName(fadedNextPlayerName)
               end
            else
               if (randomNumber <= kFadedModFadedNextSelectionChance)
               then -- Chance to be fade again
                  -- Print("Choice 60: " .. tostring(fadedNextPlayerName))
                  player = Shared:GetPlayerByName(fadedNextPlayerName)
               end
            end
         end

         -- Remove winner if they have typed nofaded
         if (player and self.noFaded[player:GetClientIndex()]) then
            player = nil
         end
         -- If not choose a random player
         -- Remove the fadedNextPlayer from the random choice
         -- so he get exactly FadedModFadedSelectionChance chance
         local player_list = self:GetPossiblePlayerIds()
         if (not player and player_list and #player_list >= 1)
         then
            if (Shared.GetRandomPlayer) then
               player = Shared:GetRandomPlayer(player_list)
            end
            -- Print("Random choice: " .. player:GetName())
         end
      end
      -- Print("Faded NS2Gamerules:GetSingleFaded() END")
      return (player)
   end

   -- Reset the game on round end.
   function NS2Gamerules:UpdateToReadyRoom()
      -- Print("Faded NS2Gamerules:UpdateToReadyRoom()")
      local state = self:GetGameState()
      if (state == kGameState.Team1Won or state == kGameState.Team2Won or state == kGameState.Draw) then
         -- Check if we need to change the map
         if (fadedMapTime >= (cycle.time * 60) - 10 and fadedMapTime >= 0) then
            local function _MoveToRR(player)
               self:JoinTeam(player, kTeamReadyRoom)
            end
            Server.ForAllPlayers(_MoveToRR)
            -- self.timeToCycleMap = Shared.GetTime() + kFadedModTimeTillMapChange
            fadedMapTime = -1
         elseif (fadedMapTime > 0) then
            if (self.timeSinceGameStateChanged >= kFadedModTimeTillNewRound) then
               -- Reset the game
               self:ResetGame()

               -- See if there are enough players, otherwise stop the game
               local team1Players = self.team1:GetNumPlayers()
               local team2Players = self.team2:GetNumPlayers()

               if (team1Players + team2Players <= 1) then
                  -- Set game state to not started
                  self:SetGameState(kGameState.NotStarted)
                  self.countdownTime = 6
                  self.lastCountdownPlayed = nil
                  self:EndGame(self.team1)
               else
                  -- Set game state to countdown
                  self:SetGameState(kGameState.Countdown)
                  self.countdownTime = 6
                  self.lastCountdownPlayed = nil
               end
            end
         end
      end
      -- Print("Faded NS2Gamerules:UpdateToReadyRoom() END")
   end

   -- Display a timer on pre game
   local updatePregame = NS2Gamerules.UpdatePregame
   function NS2Gamerules:UpdatePregame(timePassed)
      -- Print("Faded NS2Gamerules:UpdatePregame()")
      if (self:GetGameState() == kGameState.PreGame) then
         local preGameTime = kFadedModPregameLength

         if (self.timeSinceGameStateChanged > preGameTime) then
            fadedPregameTenSecMessage = false
            fadedPregameFiveSecMessage = false
         else
            if (fadedPregameFiveSecMessage == false and floor(preGameTime - self.timeSinceGameStateChanged) == 5) then
               Shared:FadedMessage(strformat(locale:ResolveString("FADED_GAME_STARTS_IN"), 5))
               fadedPregameFiveSecMessage = true
               fadedPregameTenSecMessage = true
            elseif (fadedPregameTenSecMessage == false and floor(preGameTime - self.timeSinceGameStateChanged) == 10) then
               Shared:FadedMessage(strformat(locale:ResolveString("FADED_GAME_STARTS_IN"), 10))
               fadedPregameTenSecMessage = true
            end
         end
      end

      updatePregame(self, timePassed)
      -- Print("Faded NS2Gamerules:UpdatePregame() END")
   end

   -- Send to each player the new name
   function NS2Gamerules:networkBroadCastName(marine,
                                              newform_name, corpse_name)

      -- for _, marine in pairs(self.team1:GetPlayers()) do
      -- Print("Sending to " .. marine:GetName())
      Server.SendNetworkMessage(
         marine, "setFakeMarineName",
         {
            fakeMarineRealName = newform_name,
            fakeMarineName = corpse_name,
         }, true)
   end

   function NS2Gamerules:networkBroadCastFakeMarineName(marine)
      for _, fakeMarine in pairs(self.team2:GetPlayers()) do
         if (fakeMarine:isa("Marine") and fakeMarine:GetIsAlive()) then
            self:networkBroadCastName(marine,
                                      fakeMarine:GetName(),
                                      fakeMarine:GetFakeMarineName())
         end
      end
   end

   -- Update rate of message sent over the network to the client
   local kFadedNetworkSend = 0
   local kFadedNetworkSendRate = 0.5

   function NS2Gamerules:TriggerAntiCamp(player)
      -- no anticamp when cheats are one
      if (kFadedAntiCampEnable == false or Shared.GetCheatsEnabled()) then return end
      if (self:GetGameStarted()) then
         local babbler = GetEntitiesForTeam("Babbler", 2)
         -- limit of babbler to avoid server crash
         if (#babbler < kMaxBabblerOnMap) then
            local cpt = 0
            player:FadedMessage(locale:ResolveString("FADED_ANTI_CAMP_WARNING"))
            while (cpt < kFadedNumBabblersPerEgg) do
               local egg = CreateEntity(BabblerEgg.kMapName, player:GetOrigin(), 2)
               if (egg) then
                  egg:SetCamperInfo(player)
               end
               cpt = cpt + kNumBabblersPerEgg
            end
            self:spawnBabblerPlayers(player:GetOrigin(), kFadedNumBabblersPerEgg)
         end
      end
   end

   -- Anti camp system
   -- If a marine stay in the same N meters during kCampMaxTime time,
   -- a number of bablers egs spawn. If he continue to stay on this zone,
   -- babler will then poped out every M seconds
   local bablerAntiCamp_time_delay = 0
   function NS2Gamerules:BabblerAntiCamp(timePassed)
      if (kFadedAntiCampEnable == false or Shared.GetCheatsEnabled()) then return end
      bablerAntiCamp_time_delay = bablerAntiCamp_time_delay + timePassed
      if (bablerAntiCamp_time_delay > 1) then
         for _, marine in pairs(self.team1:GetPlayers()) do
            -- local marines_building = GetEntitiesWithMixinWithinRange(
            --    "PowerConsumer", marine:GetOrigin(), 15)
            if (marine:GetIsAlive() and marine:isa("Marine")) then
               local origin = marine:GetOrigin()
               local distance = origin:GetDistanceTo(marine.kFadedLastCampPosition)
               if (distance < 0) then
                  distance = distance * -1
               end
               if (distance <= kAntiCampRadius) then
                  marine.kFadedCampDuration = marine.kFadedCampDuration + bablerAntiCamp_time_delay
               else
                  marine.kFadedLastCampPosition = marine:GetOrigin()
                  marine.kFadedCampDuration = 0
               end
               if (marine.kFadedCampDuration >= kCampMaxTime) then
                  -- Only substract 10s
                  marine.kFadedCampDuration = kCampMaxTime - 10
                  self:TriggerAntiCamp(marine)
               end
               -- Add shadow design to babbler
               for _, babbler in ipairs(GetEntitiesForTeam("Babbler", 2)) do
                  babbler:SetVariant(kGorgeVariant.shadow)
               end
            end
         end

         bablerAntiCamp_time_delay = bablerAntiCamp_time_delay - 1
      end
      -- Force egs construction
      for _, egs in ipairs(GetEntitiesForTeam("BabblerEgg", 2)) do
         egs:Construct(timePassed)
      end
   end

   -- -- Send a chat message to fade to teach how to use corpse
   -- function NS2Gamerules:fadeDisguiseMessageHelp(timePassed)
   --    for _, faded in ipairs(GetEntitiesForTeam("Fade", 2)) do
   --      if (faded.kFadedCorpseMessageHelp) then
   --         local corpse_around = 0
   --         corpse_around = GetEntitiesWithinRange("Marine", faded:GetOrigin(), 10)
   --         for _, m in ipairs(corpse_around) do
   --            if (m:GetIsAlive() == false) then
   --           -- local msg = m:GetClient().SubstituteBindStrings("Press BIND_Drop to use your Faded back")
   --           -- Print(msg)

   --           -- faded:FadedMessage("Press 'Use'(E) on a corpse to disguise your self")
   --           -- faded:FadedMessage("Or Press 'Request Medpack'(A) to eat the corpse an regen health.")

   --           faded.kFadedCorpseMessageHelp = false
   --           break
   --            end
   --         end
   --      end
   --    end
   -- end

   -- local _BurnBodiesTimeInterval = 0
   -- function NS2Gamerules:burnBodiesOrder(timePassed)
   --    _BurnBodiesTimeInterval = _BurnBodiesTimeInterval + timePassed
   --    if (_BurnBodiesTimeInterval > 10) then
   --      _BurnBodiesTimeInterval = 0
   --      for _, marine in pairs(self.team1:GetPlayers()) do
   --         local active_weap = marine:GetActiveWeapon()
   --         if (marine:GetIsAlive() and active_weap
   --            and active_weap:GetMapName() == Flamethrower.kMapName) then
   --            -- marine:GiveOrder()
   --            -- marine:GiveOrder(kTechId.Attack, newTarget:GetId(), nil)
   --            marine:GiveOrder(kTechId.Attack, newTarget:GetId(), nil)
   --         end
   --      end
   --    end
   -- end


   function NS2Gamerules:_test()

      local numPlayers = 0
      local numRookies = 0

      -- Player may have been deleted this tick, so check id to make sure player count is correct)
      for index, player in ipairs(GetEntities("Entity")) do

         if (player ~= nil and player:GetClientIndex() == -1) then
            numPlayers = numPlayers + 1
            Print("EntityName == " .. EntityToString(player))
         end
      end

      if (numPlayers > 0) then
         Print("Num RAgdol == " .. tostring(numPlayers))
      end
   end


   -- coord: where to spawn them
   -- nb: how many slot max
   function NS2Gamerules:spawnBabblerPlayers(coord, nb)
      local player = nil
      local it_player_respawn = {}
      for it, player_name in ipairs(babblerPlayers) do
         if (nb <= 0) then
            break
         end
         player = Shared:GetPlayerByName(player_name)
         -- Shared:FadedMessage("Respawning " .. player_name
         --             .. " nb = " .. tostring(nb)
         --             .. " v = " .. tostring(v)
         --             .. " isnull = " .. tostring(player)
         --             .. " is alive = " .. tostring(player:GetIsAlive()))
         if (nb > 0 and player and player:GetIsAlive() == false) then
            -- Shared:FadedMessage("Can respawn")
            local success = true
            if (fadedPlayersName[player:GetName()] ~= true) then
               -- if (player:GetTeamNumber() ~= 2) then
               --    success, player = self:JoinTeam(player, 2, true)
               --    -- else -- simple respawn
               --    --       if (player.Reset) then
               --    --          player:Reset()
               --    --          if player.OnReset then
               --    --         player:OnReset()
               --    --          end
               --    --       end
               -- end

               if (success and player) then
                  local new_player = nil
                  local randx = (math.random(0, 1) / 2) * (-1 * math.random(0, 1))
                  local randy = (math.random(0, 1) / 2) * (-1 * math.random(0, 1))
                  local new_coord = coord + Vector(randx, 2, randy)


                  if (babblerPlayers[player:GetName()] > 0) then
                     babblerPlayers[player:GetName()] = babblerPlayers[player:GetName()] - 1

                     player:FadedMessage("Respawn left: " .. tostring(babblerPlayers[player:GetName()]))

                     new_player = player:Replace(Marine.kMapName, 1, false, new_coord)

                     if (new_player and string.find(new_player:GetName(),
                                                    "[BOT] ",
                                                    1, true))
                     then
                        local weap =
                           {
                              Rifle.kMapName,
                              Shotgun.kMapName,
                              GrenadeLauncher.kMapName,
                              Flamethrower.kMapName,
                           }
                        local equip =
                           {
                              Welder.kMapName,
                           }

                        local choosen_weap = weap[math.random(1, #weap)]
                        local choosen_equip = equip[math.random(1, #equip)]

                        new_player:GiveItem(choosen_weap)
                        -- new_player:GiveItem(choosen_equip)
                     end
                  else -- Players out of respawn go to babbler
                     new_player = player:Replace(Skulk.kMapName, 2, false, new_coord)
                  end

                  if (new_player) then
                     -- SpawnSinglePlayer(new_player, new_coord)
                     nb = nb - 1
                     table.insert(it_player_respawn, it)
                     if (new_player:isa("Marine")) then
                        Server.SendNetworkMessage(new_player, "SendSelectEquipmentMessage", {}, true)
                     else
                        spawnBabblerSquad(new_player)
                     end

                  end
                  -- if (new_player) then
                  --    new_player:SetOrigin(coord)
                  -- end
               end
            end
         end
      end
      for _, it in ipairs(it_player_respawn) do
         -- Put at the end of the list to rotate
         table.insert(babblerPlayers, babblerPlayers[it])
         table.remove(babblerPlayers, it)
      end
      return #it_player_respawn
   end

   function SpawnSentryBatteryOnMap(self, force, nocallback)
      if (kFadedDisableObjectivMod) then return end
      if (force == true or self:GetGameStarted()) then
         local rt_point = {}
         local rt_places = Shared.GetEntitiesWithClassname("ResourcePoint")
         for index, entity in ientitylist(rt_places) do
            table.insert(rt_point, entity)
         end
         local i = 0
         while (i < 10 and table.getn(rt_point) > 1) do -- 10 try for the battery spawn
            local rand_rt = math.random(1, table.getn(rt_point))
            local st = nil
            local force_coord = true

            i = i + 1
            -- No sentry battery in main base
            if (rt_point[rand_rt]:GetLocationName() ~= fadedMarinesMainBase) then
               st = FadedSpawnBuilding(kTechId.SentryBattery,
                                       SentryBattery.kMapName,
                                       rt_point[rand_rt]:GetOrigin(),
                                       Vector(0, 0, 0), 5, force_coord)
            end

            if (st) then
               Shared:FadedMessage(string.format(
                                      locale:ResolveString("FADED_BATTERY_SPAWN"),
                                      st:GetLocationName()))
               break
            end
         end
      end
      if (nocallback ~= true) then
         Entity.AddTimedCallback(self, SpawnSentryBatteryOnMap, kFadedBatterySpawnDelay)
      end
   end

   local randomHintMessage_delay = 0
   -- local hint_nb_displayed = 0
   local hint_messages =
      {
         "Press 'buy' to purchase a weapon at round start",
         "You can buy sentries at the prototype lab",
         "Shotgun alt fire throws a flare",
         "GL alt fire throw a pulse grenade",
         "The bonewall worm is easier to kill with grenades",
         "Some gorges are able to create tunnels bots can take",
         "Hydras, whips and clogs can grow on infestation",
         "The railgun does an AoE ARC shot if fully charged",
         "Blue dot on the map are ammo and med packs",
         "As a medic, the GL throws healing grenades",
         "As a medic, wounded marines have an icon above them",
         "Doors can be locked down and welded",
         -- "Suiciding yourself will full hp/ap will give you a candy",
         "You can clip grenades to a wall and detonate them remotly",
         "The MG shoot explosive bullets, good against lerks",
         -- "Sacrifying a full HP virgin rookie will kill all the aliens once",
         "Aliens only attack powered building",
         "Grenades can be attached to walls and detonated by hand later",
         "As an alien, gorges will heal you if you are wounded",
         "Gorges heal wounded aliens, take them down first",
         "Gorges can build hives on places far from marines",
         "Hives are hard to kill, but will give you a lot of resources",
         "Killing a hive will kill all the aliens, and end the current wave"
      }

   local hint_messages_tmp = {}
   function NS2Gamerules:randomHintMessage(timePassed)
      if (kFadedHintEnable == true) then
         randomHintMessage_delay = randomHintMessage_delay + timePassed
         if (randomHintMessage_delay > 90) then
            randomHintMessage_delay = 0
            if (table.getn(hint_messages_tmp) == 0) then
               for k, v in ipairs(hint_messages) do
                  hint_messages_tmp[k] = v
               end
            end
            -- hint_nb_displayed = hint_nb_displayed + 1
            -- if (hint_messages[hint_nb_displayed] == nil) then
            --    hint_nb_displayed = 1
            -- end
            local msg_nb = math.random(1, table.getn(hint_messages_tmp))
            Shared:FadedHintMessage(hint_messages_tmp[msg_nb])
            table.remove(hint_messages_tmp, msg_nb)
         end
      end
   end

   -- Hook into the update .function, so we can update our mod
   -- local babblerModelSizeUpTime = 0
   local onUpdate = NS2Gamerules.OnUpdate

   local moveToAlienTeamDeadMarines_refresh = 0
   local function moveToAlienTeamDeadMarines(timePassed)
      moveToAlienTeamDeadMarines_refresh = moveToAlienTeamDeadMarines_refresh + timePassed
      if (moveToAlienTeamDeadMarines_refresh > 5) then
         for _, marine in ipairs(GetGamerules():GetTeam1():GetPlayers()) do
            if (marine and not marine:GetIsAlive()) then
               if (marine:GetDeaths() == 0) then -- New player
                  GetGamerules():JoinTeam(marine, 1, true)

                  local marines = GetEntitiesForTeam("Marine", 1)

                  for i = 1, 10 do
                     if (#marines > 0) then
                        -- Spawn around an other marine who is alive
                        local rand_marine = marines[math.random(1, #marines)]
                        if (rand_marine:GetIsAlive()) then
                           local new_orig = GetLocationAroundFor(rand_marine:GetOrigin(), kTechId.Marine, 5)
                           local m = marine:Replace(Marine.kMapName, 1, false, new_orig)
                           GetGamerules():JoinTeam(marine, 1, true)
                           -- m:SetDesiredCameraDistance(0)
                           -- m:SetCameraDistance(0)
                           break
                        end
                     end
                  end
               else
                  GetGamerules():JoinTeam(marine, 2, true)
               end
            end
         end

      end
   end

   function NS2Gamerules:GetWarmUpPlayerLimit()
        return 0 -- No warmup
   end

   -- local clog_refresh = 0
   function NS2Gamerules:OnUpdate(timePassed)
      onUpdate(self, timePassed)
      -- clog_refresh = clog_refresh + timePassed
      -- if (clog_refresh > 0.07) then
      --    clog_refresh = 0
         refreshAllClogWhipPos()
      -- end
      self:randomHintMessage(timePassed)
      -- self:fadeDisguiseMessageHelp()
      if (fadedMapTime >= 0) then
         fadedMapTime = fadedMapTime + timePassed
      end
      --moveToAlienTeamDeadMarines(timePassed)

      --handleWaves(timePassed)


      -- if (babblerModelSizeUpTime < Shared.GetTime()) then
      --    babblerModelSizeUpTime = Shared.GetTime() + 2

      --    local allPlayers = Shared.GetEntitiesWithClassname("Player")
      --    for _, babblerPlayer in ipairs(GetEntities("Skulk")) do
      --       if (babblerPlayer and babblerPlayer:GetIsAlive()
      --           and babblerPlayer.modelSize ~= 1) then
      --          for _, toPlayer in ientitylist(allPlayers) do
      --             Server.SendNetworkMessage(toPlayer, "ChangeModelSize",
      --                                       {modelSize = babblerPlayer.modelSize,
      --                                        id = babblerPlayer:GetId()},
      --                                       true)
      --          end
      --       end
      --    end
      -- end

      -- self:burnBodiesOrder(timePassed)
   end

   function NS2Gamerules:GetAllTech()
      return true--#GetEntities("TechPoint") <= 2
   end

   -- Welcome message!
   function NS2Gamerules:OnClientConnect(client)
      Gamerules.OnClientConnect(self, client)

      local player = client:GetControllingPlayer()

      if (player and player.FadedMessage) then
         -- player:FadedMessage(locale:ResolveString("FADED_WELCOME_MESSAGE_1"))
         -- player:FadedMessage(locale:ResolveString("FADED_WELCOME_MESSAGE_2"))
         -- player:FadedMessage(locale:ResolveString("FADED_WELCOME_MESSAGE_3"))
         -- player:FadedMessage(locale:ResolveString("FADED_WELCOME_MESSAGE_4"))
         -- player:FadedMessage(locale:ResolveString("FADED_WELCOME_MESSAGE_5"))

         -- Server.SendNetworkMessage(player, "RoundTime", { time = kFadedModRoundTimerInSecs }, true)
         player:FadedMessage("Server is dedicated ? " .. tostring(Server.IsDedicated()))
         Server.SendNetworkMessage(player, "ConfigModification",
                                   {
                                      kFadedNoCustomFlashlight = kFadedNoCustomFlashlight,
                                      kFadedRespawnedEnable = kFadedRespawnedEnable,
                                      kFadedGLCheatEnable = kFadedGLCheatEnable,
                                      kFadedHSGCheatEnable = kFadedHSGCheatEnable,
                                   }, true)
         -- send log to each client, so they known what have been changed
         if (all_change_string_log) then
            for _, str in ipairs(all_change_string_log) do
               Server.SendNetworkMessage(player, "ConfigModificationLog",
                                         {
                                            str = str,
                                         }, true)

            end
         -- else
         --    Shared:FadedMessage("Warning: all_change_string_log is null")
         end
      end
   end

   -- Dont't let people spawn if they go to the ready room and back in game just after the game started
   function NS2Gamerules:GetCanSpawnImmediately()
      local game_state = self:GetGameState()
      if ((game_state ~= kGameState.Countdown and not self:GetGameStarted())
          or Shared.GetCheatsEnabled()) then
         return (true)
      else
         return (false)
      end
   end

   -- function NS2Gamerules:GetCanPlayerHearPlayer(listenerPlayer, speakerPlayer)
   --    -- if (listenerPlayer and speakerPlayer) then
   --    --    if (listenerPlayer:GetTeamNumber() == speakerPlayer:GetTeamNumber()) then
   --    --       return (true)
   --    --       -- If alien player want to use local, do not forward to marines
   --    --    elseif (speakerPlayer:GetTeamNumber() == 2
   --    --               and listenerPlayer:GetTeamNumber() == 1
   --    --               and speakerPlayer.IsUsingTeamVocalChat ~= nil
   --    --               and speakerPlayer:IsUsingTeamVocalChat() == true)
   --    --    then
   --    --       return (false)
   --    --    end
   --    -- end
   --    -- Print("Alltalk mode")
   --    return kFadedModAllTalkEnabled
   -- end
end
