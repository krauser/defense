function Shotgun:GetPrimaryMinFireDelay()
    return kShotgunFireRate * 0.5
end

function Shotgun:GetHUDSlot()
   return 2
end

function Shotgun:GetMaxClips()
   return 3
end

local function LoadBullet(self)

   if self.ammo > 0 and self.clip < self:GetClipSize() then

        self.clip = self.clip + 1
        self.ammo = self.ammo - 1

    end

end


function Shotgun:GetHasSecondary(player)
   return (true)
end

local flareAttackRate = 0.5
function Shotgun:OnSecondaryAttack(player)
   ClipWeapon.OnSecondaryAttack(self, player)
   if (self.secondaryAttacking == nil) then
      self.secondaryAttacking = Shared.GetTime()
   end
   if (self.lastAltAttack == nil) then
      self.lastAltAttack = Shared.GetTime()
   end

   if (self.secondaryAttacking) then
      if (kFadedFlareEnable
          and self.lastAltAttack + flareAttackRate <= Shared.GetTime()) then
         if (self:GetClip() >= 1) then
            self.lastAltAttack = Shared.GetTime()
            fire_flare_grenade(player)
            if not player:GetDarwinMode() then
               self:SetClip(self:GetClip() - 1)
            end
            self:TriggerEffects("grenadelauncher_attack")
            -- self:TriggerEffects("shotgun_attack")
         end
      end
   end
end

-- if (Server) then
--    local playerGiveItem = Player.GiveItem
--    function Player:GiveItem(itemMapName, setActive, suppressError)
--       if (itemMapName == Shotgun.kMapName and self.IsMedic and self:IsMedic()) then
--          self:FadedMessage("A medic can't use a shotgun")
--          return nil
--       else
--          return playerGiveItem(self, itemMapName, setActive, suppressError)
--       end
--    end
-- end

-- --
-- function Shotgun:GetPrimaryMinFireDelay(player)
--    return 0.2 -- /second
-- end

-- TODO: regarder du coté de "function Marine:OnUpdateAnimationInput(modelMixin)"
-- Pour accélérer le reload ou le rate of fire plus proprement
local shotgunOnTag = Shotgun.OnTag
function Shotgun:OnTag(tagName)
   shotgunOnTag(self, tagName)

   -- if (tagName == "load_shell") then
   --    LoadBullet(self) -- Reload an extra bullet to make it playable
   -- end



   -- PROFILE("Shotgun:OnTag")

   --  local continueReloading = false
   --  if self:GetIsReloading() and tagName == "reload_end" then

   --      continueReloading = true
   --      self.reloading = false

   --  end

   --  ClipWeapon.OnTag(self, tagName)

   --  if tagName == "load_shell" then
   --     LoadBullet(self)
   --  elseif tagName == "reload_shotgun_start" then
   --     self:TriggerEffects("shotgun_reload_start")
   --  elseif tagName == "reload_shotgun_shell" then
   --     self:TriggerEffects("shotgun_reload_shell")
   --  elseif tagName == "reload_shotgun_end" then
   --     self:TriggerEffects("shotgun_reload_end")
   --  end

   --  if continueReloading then

   --     local player = self:GetParent()
   --      if player then
   --         player:Reload()
   --      end

   --  end
end

-------------------------------------------------------

local kBulletSize = 0.016
-- The greater the more precise
local kSpreadDistance = 0.95
local kStartOffset = 0
local kSpreadVectors =
{
      GetNormalizedVector(Vector(-0.01, 0.01, kSpreadDistance)),
      GetNormalizedVector(Vector(-0.02, 0.02, kSpreadDistance)),

      GetNormalizedVector(Vector(-0.35, 0, kSpreadDistance)),
      GetNormalizedVector(Vector(0.35, 0, kSpreadDistance)),
      GetNormalizedVector(Vector(0, -0.35, kSpreadDistance)),
      GetNormalizedVector(Vector(0, 0.35, kSpreadDistance)),

      GetNormalizedVector(Vector(-0.45, 0.45, kSpreadDistance)),
      GetNormalizedVector(Vector(0.45, 0.45, kSpreadDistance)),
      GetNormalizedVector(Vector(0.45, -0.45, kSpreadDistance)),
      GetNormalizedVector(Vector(-0.45, -0.45, kSpreadDistance)),


      GetNormalizedVector(Vector(-0.8, -0.8, kSpreadDistance)),
      GetNormalizedVector(Vector(-0.8, 0.8, kSpreadDistance)),
      GetNormalizedVector(Vector(0.8, 0.8, kSpreadDistance)),
      GetNormalizedVector(Vector(0.8, -0.8, kSpreadDistance)),

      GetNormalizedVector(Vector(-1, 0, kSpreadDistance)),
      GetNormalizedVector(Vector(1, 0, kSpreadDistance)),
      GetNormalizedVector(Vector(0, -1, kSpreadDistance)),
      GetNormalizedVector(Vector(0, 1, kSpreadDistance)),

      -- Faded 4 bullets added
      GetNormalizedVector(Vector(-1.5, 0, kSpreadDistance)),
      GetNormalizedVector(Vector(1.5, 0, kSpreadDistance)),
      GetNormalizedVector(Vector(0, 1.5, kSpreadDistance)),
      GetNormalizedVector(Vector(0, -1.5, kSpreadDistance)),
}


local function PushTarget(player)
   if (Server and player:isa("Player")) then
      player:SetVelocity(player:GetVelocity() + (player:GetKnockBack()))
   end
end

function Alien:SetKnockBack(velocity)
   self.kFadedKnockback = velocity
end
function Alien:GetKnockBack()
   return (self.kFadedKnockback)
end

function Player:SetKnockBack(velocity)
   self.kFadedKnockback = velocity
end
function Player:GetKnockBack()
   return (self.kFadedKnockback)
end

local function HeavyShotgunShootFunction(self, player, numberBullets)

   local viewAngles = player:GetViewAngles()
   viewAngles.roll = NetworkRandom() * math.pi * 2

   local shootCoords = viewAngles:GetCoords()

   -- Filter ourself out of the trace so that we don't hit ourselves.
   local filter = EntityFilterTwo(player, self)
   local range = self:GetRange()

   if GetIsVortexed(player) then
      range = 5
   end

   local startPoint = player:GetEyePos()

   self:TriggerEffects("heavyshotgun_attack_sound")
   self:TriggerEffects("heavyshotgun_attack")

   local hits = 0
   for bullet = 1, numberBullets do

      bullet = 1
      if not kSpreadVectors[bullet] then
         break
      end

      local newSpreadDirection = nil
      newSpreadDirection = kSpreadVectors[bullet] + GetNormalizedVector(Vector((math.random() - 0.5) * 0.7, (math.random() - 0.5) * 0.7, kSpreadDistance))

      local spreadDirection = shootCoords:TransformVector(newSpreadDirection)

      local endPoint = startPoint + spreadDirection * range
      -- Randomize spread
      local spreadX = kSpreadVectors[bullet].x * (math.random() - 0.5)
      local spreadY = kSpreadVectors[bullet].y * (math.random() - 0.5)

      startPoint = player:GetEyePos() + shootCoords.xAxis * spreadX * kStartOffset + shootCoords.yAxis * spreadY * kStartOffset

      local targets, trace, hitPoints = GetBulletTargets(startPoint, endPoint, spreadDirection, kBulletSize, filter)

      -- if (Server and hitPoint[1]) then
      --    local direction = spreadDirection + trace.normal
      --    _CreateClusterFragment(self, player, hitPoint[1], 0, direction)
      -- end


      local damage = 0

      local direction = (trace.endPoint - startPoint):GetUnit()
      local hitOffset = direction * kHitEffectOffset
      local impactPoint = trace.endPoint - hitOffset
      local effectFrequency = self:GetTracerEffectFrequency()
      local showTracer = bullet % effectFrequency == 0

      local numTargets = #targets

      -- If we hit nothing, only display a part of the bullets shot
      -- to avoid a mini freeze client side due to having too much to render
      if numTargets == 0 and math.random(0, 100) <= 40 then
         self:ApplyBulletGameplayEffects(player, nil, impactPoint, direction, 0, trace.surface, showTracer)
      end

      if Client and showTracer then
         TriggerFirstPersonTracer(self, impactPoint)
      end

      for i = 1, numTargets do

         local target = targets[i]
         local hitPoint = hitPoints[i]

         if (target:isa("Player")) then -- Push target
            local viewCoords = player:GetViewCoords()
            local direction = viewCoords.zAxis
            local base_vect = ((direction * kFadedHeavyShotgunKnockBack)) / 4.3
            target:SetKnockBack(base_vect)
            target:AddTimedCallback(PushTarget, 0)

         end

         self:ApplyBulletGameplayEffects(player, target, hitPoint - hitOffset, direction, kFadedHeavyShotgunDamage, "", showTracer and i == numTargets)

         local client = Server and player:GetClient() or Client
         if not Shared.GetIsRunningPrediction() and client.hitRegEnabled then
            RegisterHitEvent(player, bullet, startPoint, trace, damage)
         end

      end

   end


   -- TEST_EVENT("Shotgun primary attack")
end

-- Allow alternative fire
-- function Shotgun:GetHasSecondary(player)
--    return false
-- end

-- function Shotgun:OnSecondaryAttack(player)
--    ClipWeapon.OnSecondaryAttack(self, player)
--    if (self.lastHeavyShot == nil) then
--       self.lastHeavyShot = 0
--    end
--    if (self.secondaryAttacking and self:GetClip() >= 6 and self.lastHeavyShot + 0.5 < Shared.GetTime())
--    then
--       self.lastHeavyShot = Shared.GetTime()
--       self:TriggerEffects("heavyshotgun_attack_sound")
--       HeavyShotgunShootFunction(self, player, kFadedHeavyShotgunPelletPerShot)
--       -- self:ShootFunction(player, half_pellet)
--       -- self:OnPrimaryAttack(player)
--       if (not player:GetDarwinMode()) then
--          self:SetClip(0)
--       end
--    end
-- end

