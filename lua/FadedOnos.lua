
function Onos:GetBaseArmor()
    return kOnosArmor * getAlienHealthScalar()
end

function Onos:GetBaseHealth()
   return kOnosHealth * getAlienHealthScalar()
end


local onosGetMaxSpeed = Onos.GetMaxSpeed
function Onos:GetMaxSpeed(possible)
   local speed = onosGetMaxSpeed(self, possible) * getAlienSpeedScalar()

   speed = AlienGetSpeedBonusIfFar(self, speed)
   if (self.electrified) then
      speed = speed / kFadedPulseSlowDownFactor
   end
   return (speed)
end


