
------------------------------------------
--  Base class for bot brains
------------------------------------------

Script.Load("lua/bots/BotUtils.lua")
Script.Load("lua/bots/BotDebug.lua")

gBotDebug:AddBoolean("debugall", false)

------------------------------------------
--  Globals
------------------------------------------

class 'PlayerBrain'

function PlayerBrain:Initialize()

    self.lastAction = nil

end

function PlayerBrain:GetShouldDebug(bot)

    -- //----------------------------------------
    -- --  This code is for Player-types, commanders should override this
    -- //----------------------------------------
    -- -- If commander-selected, turn debug on
    -- local isSelected = bot:GetPlayer():GetIsSelected( kMarineTeamType ) or bot:GetPlayer():GetIsSelected( kAlienTeamType )

    -- if isSelected and gDebugSelectedBots then
    --     return true
    -- elseif self.targettedForDebug then
    --     return true
    -- else
        return false
    -- end

end


local function getPathDist(a, b)
   local points = PointArray()

   Pathing.GetPathPoints(a:GetOrigin(), b:GetOrigin(), points)
   return #points
end

-- Faded
local function sortByPathProximity(alien, targets)
   local i = 1
   local sorted_targets = {}
   local added = false
      -- local points = PointArray()
      -- local dist = 0.7

   for _, target in ipairs(targets)
   do
      i = 0
      local t1 = getPathDist(alien, target)--alien:GetOrigin():GetDistanceTo(target:GetOrigin())
      for it, target2 in ipairs(sorted_targets) do
         local t2 = getPathDist(alien, target2)--alien:GetOrigin():GetDistanceTo(target2:GetOrigin())
         if (t1 < t2) then
            table.insert(sorted_targets, it, target)
            added = true
            break
         end
      end
      if (added == false) then
         table.insert(sorted_targets, #sorted_targets + 1, target)
      end
   end
   -- for it, rt in ipairs(sorted_rt_point) do
   --    Shared:FadedMessage("Nearest marine from rt n'" .. tostring(it)
   --                           .. " is at " .. _getDistFromNeareastMarine(rt) .. "m")
   -- end
   return (sorted_targets)
end

-- FIXME TOO
local function updateRefreshData(currentBot)
   if (not currentBot.nextDataRefresh) then
      currentBot.isNearbyDoor = false
      currentBot.isMarinesAround = false

      currentBot.nextDataRefresh = Shared.GetTime() + 1
   else
      if (currentBot.nextDataRefresh < Shared.GetTime()) then
         currentBot.nextDataRefresh = Shared.GetTime() + 1

         if (currentBot.defenseAggressiveAI) then
            local marines = GetEntitiesForTeamWithinRange("Player", 1,
                                                          currentBot:GetOrigin(),
                                                          currentBot.defenseAggressiveAIDistance)

            currentBot.isNearbyDoor = (#getAliveDoorAround(currentBot) > 0)
            for _, marine in ipairs(marines) do
               if (not GetWallBetween(marine:GetModelOrigin(),
                                      currentBot:GetModelOrigin(),
                                      currentBot.defenseAggressiveAIDistance))
               then
                  currentBot.isMarinesAround = true
                  break
               end
            end
         end
      end
   end
end

function PlayerBrain:DefenseModUpdate(bot, move)
    if not bot:GetPlayer():isa( self:GetExpectedPlayerClass() )
    or bot:GetPlayer():GetTeamNumber() ~= self:GetExpectedTeamNumber() then
        return
    end


    local bestAction = nil

    local currentBot = bot:GetPlayer()
    local currentOrder = bot:GetPlayerOrder()
    -- updateRefreshData(currentBot)

    local isSelected = currentBot.GetIsSelected and currentBot:GetIsSelected( kAlienTeamType )

    if (not isSelected and bot:GetTeamNumber() == 2) then
       -- lerks needs to fly high, with the opti they are just like jumping on the ground ...
       if (not bot:isa("Lerk") and not currentBot:GetIsSighted())
       then
          if ((bot and (bot.last_refresh == nil or bot.refresh_rate == nil
                           or Shared.GetTime()-bot.last_refresh > bot.refresh_rate)))
          then
             bot.last_refresh = Shared.GetTime()
             -- More aggressive
             if (-- currentBot.isMarinesAround
                 (currentOrder and currentOrder:GetType() == kTechId.Attack)
                 or currentBot.isNearbyDoor) then
                bot.refresh_rate = 0
             else
                bot.refresh_rate = 1

                -- if (currentBot.client) then
                --    currentBot:SetName("Test name")
                --    self.client:GetControllingPlayer():OnClientUpdated(currentBot.client)
                -- end

             end
          else
             return
          end
       end
    end

    self.teamBrain = GetTeamBrain( bot:GetPlayer():GetTeamNumber() )

    -- Prepare senses before action-evals use it
    assert( self:GetSenses() ~= nil )
    self:GetSenses():OnBeginFrame(bot)

    for actionNum, actionEval in ipairs( self:GetActions() ) do

        self:GetSenses():ResetDebugTrace()

        local action = actionEval(bot, self)
        assert( action.weight ~= nil )

        if self.debug then
            DebugPrint("weight(%s) = %0.2f. trace = %s",
                    action.name, action.weight, self:GetSenses():GetDebugTrace())
        end

        if bestAction == nil or action.weight > bestAction.weight then
            bestAction = action
        end
    end

    if bestAction ~= nil then
        if self.debug then
            DebugPrint("-- chose action: " .. bestAction.name)
        end

        bestAction.perform(move)
        self.lastAction = bestAction

        if self.debug or gBotDebug:Get("debugall") then
            Shared.DebugColor( 0, 1, 0, 1 )
            Shared.DebugText( bestAction.name, bot:GetPlayer():GetEyePos()+Vector(-1,0,0), 0.0 )
        end
    end
    return true
end

function PlayerBrain:Update(bot, move)
   return self:DefenseModUpdate(bot, move)
end

-- local hHallucinationOnInitialized = Hallucination.OnInitialized
-- function Hallucination:OnInitialized()

--    hHallucinationOnInitialized(self)
--    self:SetUpdates(false)

-- end

-- local hSkulkOnInitialized = Skulk.OnInitialized
-- function Skulk:OnInitialized()

--    hSkulkOnInitialized(self)
--    self:SetUpdates(false)

-- end

-- local hAlienOnInitialized = Alien.OnInitialized
-- function Alien:OnInitialized()

--    hAlienOnInitialized(self)
--    self:SetUpdates(false)

-- end
