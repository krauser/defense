
Script.Load("lua/bots/CommonActions.lua")
Script.Load("lua/bots/BrainSenses.lua")

local kUpgrades = {
    -- kTechId.Carapace,
    -- kTechId.Regeneration,

    -- kTechId.Phantom,
    -- kTechId.Aura,

    -- kTechId.Celerity,
    -- kTechId.Adrenaline,
}

------------------------------------------
--  More urgent == should really attack it ASAP
------------------------------------------
local function GetAttackUrgency(bot, mem)

   return 0.0
    -- -- See if we know whether if it is alive or not
    -- local ent = Shared.GetEntity(mem.entId)
    -- if not HasMixin(ent, "Live") or not ent:GetIsAlive() then
    --     return 0.0
    -- end

    -- local botPos = bot:GetPlayer():GetOrigin()
    -- local targetPos = ent:GetOrigin()
    -- local distance = botPos:GetDistance(targetPos)

    -- -- if mem.btype == kMinimapBlipType.PowerPoint then
    -- --     local powerPoint = ent
    -- --     if powerPoint ~= nil and powerPoint:GetIsSocketed() then
    -- --         return 0.55
    -- --     else
    -- --         return 0
    -- --     end
    -- -- end

    -- local immediateThreats = {
    --     [kMinimapBlipType.Door] = true,
    --     [kMinimapBlipType.Sentry] = true,
    --     -- [kMinimapBlipType.Marine] = true,
    --     -- [kMinimapBlipType.JetpackMarine] = true,
    --     -- [kMinimapBlipType.Exo] = true,
    -- }

    -- if distance < 15 and immediateThreats[mem.btype] then
    --     -- Attack the nearest immediate threat (urgency will be 1.1 - 2)
    --     return 1 + 1 / math.max(distance, 1)
    -- end

    -- -- No immediate threat - load balance!
    -- local numOthers = bot.brain.teamBrain:GetNumAssignedTo( mem,
    --         function(otherId)
    --             if otherId ~= bot:GetPlayer():GetId() then
    --                 return true
    --             end
    --             return false
    --         end)

    -- --Other urgencies do not rank anything here higher than 1!
    -- local urgencies = {
    --    [kMinimapBlipType.Marine] =              numOthers >= 1 and 0.3 or 0.75,
    --    [kMinimapBlipType.JetpackMarine] =       numOthers >= 1 and 0.3 or 0.75,
    --    [kMinimapBlipType.Exo] =                 numOthers >= 1 and 0.3 or 0.75,

    --     [kMinimapBlipType.ARC] =                numOthers >= 2 and 0.4 or 0.9,
    --     [kMinimapBlipType.CommandStation] =     numOthers >= 4 and 0.3 or 0.75,
    --     [kMinimapBlipType.PhaseGate] =          numOthers >= 2 and 0.2 or 0.9,
    --     [kMinimapBlipType.Observatory] =        numOthers >= 2 and 0.2 or 0.8,
    --     [kMinimapBlipType.Extractor] =          numOthers >= 2 and 0.2 or 0.7,
    --     [kMinimapBlipType.InfantryPortal] =     numOthers >= 2 and 0.2 or 0.6,
    --     [kMinimapBlipType.PrototypeLab] =       numOthers >= 1 and 0.2 or 0.55,
    --     [kMinimapBlipType.Armory] =             numOthers >= 2 and 0.2 or 0.5,
    --     [kMinimapBlipType.RoboticsFactory] =    numOthers >= 2 and 0.2 or 0.5,
    --     [kMinimapBlipType.ArmsLab] =            numOthers >= 3 and 0.2 or 0.6,
    --     [kMinimapBlipType.MAC] =                numOthers >= 1 and 0.2 or 0.4,
    -- }

    -- if urgencies[ mem.btype ] ~= nil then
    --     return urgencies[ mem.btype ]
    -- end

    -- return 0.0

end

local function xeno_detonation(player)
   if (player and player:GetIsAlive()) then
      local self = player:GetActiveWeapon()
      player:TriggerEffects("xenocide", {effecthostcoords = Coords.GetTranslation(player:GetOrigin())})

      local hitEntities = GetEntitiesWithMixinWithinRange("Live", player:GetOrigin(), kXenocideRange)
      RadiusDamage(hitEntities, player:GetOrigin(), kXenocideRange, kXenocideDamage, self)

      player:SetBypassRagdoll(true)

      player:Kill()
   end
   return false
end

local function PerformAttackEntity( eyePos, bestTarget, bot, brain, move )

    assert( bestTarget )

    local marinePos = bestTarget:GetOrigin()

    local doFire = false
    bot:GetMotion():SetDesiredMoveTarget( marinePos )

    local distance = eyePos:GetDistance(marinePos)
    if distance < 2.5 then
        doFire = true
    end

    local leap_added = false
    local force_view_target = false
    if (defHasAbility(kTechId.Leap) and bestTarget:isa("JetpackMarine"))
    then
       -- local y_dist = GetNormalizedVectorXY(bot:GetOrigin()):GetDistanceTo(GetNormalizedVectorXY(bestTarget:GetOrigin()))
       -- local x_dist = GetNormalizedVectorXZ(bot:GetOrigin()):GetDistanceTo(GetNormalizedVectorXZ(bestTarget:GetOrigin()))

       if ((bot.defenseLastLeap == nil or bot.defenseLastLeap < Shared.GetTime())) then
          -- Marine is flying above (leap toward him)
          if (not GetWallBetween(bot:GetOrigin(), bestTarget:GetOrigin())
              and bot:GetOrigin():GetDistanceTo(bestTarget:GetOrigin()) > 3) then
             move.commands = AddMoveCommand( move.commands, Move.SecondaryAttack )
             bot.defenseLastLeap = Shared.GetTime() + math.random() * 3
             leap_added = true
          end
       end
       bot:GetMotion():SetDesiredViewTarget( bestTarget:GetEngagementPoint())
       force_view_target = true
    end

    if (doFire and bot.def_xeno_skulk == true and bestTarget:isa("Player")) then
       if ((bot.defenseLastXenoChk == nil or bot.defenseLastXenoChk + 1 < Shared.GetTime())) then
          bot.defenseLastXenoChk = Shared.GetTime()

          if (defHasAbility(kTechId.Xenocide)
                 and (#GetEntitiesForTeamWithinRange("Player", 1, bot:GetOrigin(), 6) >= 2
                      or getNbPlayerAliveForTeam1() == 1)) then
             if bot.xenociding ~= true then
                bot:GetPlayer():SetActiveWeapon(XenocideLeap.kMapName, true)
             end
             move.commands = AddMoveCommand( move.commands, Move.SecondaryAttack )
             move.commands = AddMoveCommand( move.commands, Move.PrimaryAttack )
             Entity.AddTimedCallback(bot, xeno_detonation, 2)
          end
       end
    end

    if doFire then
        local target = bestTarget:GetEngagementPoint()

        if bestTarget:isa("Player") then
             -- Attacking a player
             target = target + Vector( math.random(), math.random(), math.random() ) * 0.3
            if bot:GetPlayer():GetIsOnGround() and bestTarget:isa("Player") then
                move.commands = AddMoveCommand( move.commands, Move.Jump )
            end
        else
            -- Attacking a structure
            if GetDistanceToTouch(eyePos, bestTarget) < 1 then
                -- Stop running at the structure when close enough
                bot:GetMotion():SetDesiredMoveTarget(nil)
            end
        end

        if (leap_added == false) then
           bot:GetMotion():SetDesiredViewTarget( target )
           move.commands = AddMoveCommand( move.commands, Move.PrimaryAttack )
        end


        -- move.commands = AddMoveCommand( move.commands, Move.SecondaryAttack )
    else
       if (not force_view_target) then
          bot:GetMotion():SetDesiredViewTarget( nil )
       end

        -- Occasionally jump
        if math.random() < 0.1 and bot:GetPlayer():GetIsOnGround() then
            move.commands = AddMoveCommand( move.commands, Move.Jump )
            if distance < 15 then
                -- When approaching, try to jump sideways
                bot.timeOfJump = Shared.GetTime()
                bot.jumpOffset = nil
            end
        end
    end

    if bot.timeOfJump ~= nil and Shared.GetTime() - bot.timeOfJump < 0.5 then

        if bot.jumpOffset == nil then

            local botToTarget = GetNormalizedVectorXZ(marinePos - eyePos)
            local sideVector = botToTarget:CrossProduct(Vector(0, 1, 0))
            if math.random() < 0.5 then
                bot.jumpOffset = botToTarget + sideVector
            else
                bot.jumpOffset = botToTarget - sideVector
            end
            if (not force_view_target) then
               bot:GetMotion():SetDesiredViewTarget( bestTarget:GetEngagementPoint() )
            end

            -- bot.jumpOffset = botToTarget + botToTarget:CrossProduct(Vector(0, 0, 1))
            -- move.commands = AddMoveCommand( move.commands, Move.SecondaryAttack )
            -- move.commands = AddMoveCommand( move.commands, Move.SecondaryAttack )
            -- move.commands = AddMoveCommand( move.commands, Move.SecondaryAttack )
        end


        bot:GetMotion():SetDesiredMoveDirection( bot.jumpOffset )

    end

end

local function PerformAttack( eyePos, mem, bot, brain, move )

   if (mem) then

      local target = Shared.GetEntity(mem.entId)

      if target ~= nil then

         PerformAttackEntity( eyePos, target, bot, brain, move )

      else

         -- mem is too far to be relevant, so move towards it
         bot:GetMotion():SetDesiredViewTarget(nil)
         bot:GetMotion():SetDesiredMoveTarget(mem.lastSeenPos)

      end

      brain.teamBrain:AssignBotToMemory(bot, mem)
   end
end

------------------------------------------
--  Each want function should return the fuzzy weight,
-- along with a closure to perform the action
-- The order they are listed matters - actions near the beginning of the list get priority.
------------------------------------------
kSkulkBrainActions =
{

    ------------------------------------------
    --
    ------------------------------------------
    -- function(bot, brain)
    --     return { name = "debug idle", weight = 0.001,
    --             perform = function(move)
    --                 bot:GetMotion():SetDesiredMoveTarget(nil)
    --                 -- there is nothing obvious to do.. figure something out
    --                 -- like go to the marines, or defend
    --             end }
    -- end,

    ------------------------------------------
    --
    ------------------------------------------
    -- CreateExploreAction( 0.01, function(pos, targetPos, bot, brain, move)
    --             bot:GetMotion():SetDesiredMoveTarget(targetPos)
    --             bot:GetMotion():SetDesiredViewTarget(nil)
    --             end ),

    ------------------------------------------
    --
    ------------------------------------------
    -- function(bot, brain)
    --     local name = "evolve"

    --     local weight = 0.0
    --     local player = bot:GetPlayer()
    --     local s = brain:GetSenses()

    --     local distanceToNearestThreat = s:Get("nearestThreat").distance
    --     local desiredUpgrades = {}

    --     if player:GetIsAllowedToBuy() and
    --        (distanceToNearestThreat == nil or distanceToNearestThreat > 15) and
    --        (player.GetIsInCombat == nil or not player:GetIsInCombat()) then

    --         -- Safe enough to try to evolve

    --         local existingUpgrades = player:GetUpgrades()

    --         for i = 1, #kUpgrades do
    --             local techId = kUpgrades[i]
    --             local techNode = player:GetTechTree():GetTechNode(techId)

    --             local isAvailable = false
    --             if techNode ~= nil then
    --                 isAvailable = techNode:GetAvailable(player, techId, false)
    --             end

    --             if not player:GetHasUpgrade(techId) and isAvailable and
    --                GetIsUpgradeAllowed(player, techId, existingUpgrades) and
    --                GetIsUpgradeAllowed(player, techId, desiredUpgrades) then
    --                 table.insert(desiredUpgrades, techId)
    --             end
    --         end

    --         if  #desiredUpgrades > 0 then
    --             weight = 100.0
    --         end
    --     end

    --     return { name = name, weight = weight,
    --         perform = function(move)
    --             player:ProcessBuyAction( desiredUpgrades )
    --         end }

    -- end,

    --
    --Save hives under attack
    --
    -- function(bot, brain)
    --     local skulk = bot:GetPlayer()
    --     local teamNumber = skulk:GetTeamNumber()

    --     local hiveUnderAttack
    --     bot.hiveprotector = bot.hiveprotector or math.random()
    --     if bot.hiveprotector > 0.5 then
    --         for _, hive in ipairs(GetEntitiesForTeam("Hive", teamNumber)) do
    --             if hive:GetHealthScalar() <= 0.4 then
    --                 hiveUnderAttack = hive
    --                 break
    --             end
    --         end
    --     end

    --     local weight = hiveUnderAttack and 1.1 or 0
    --     local name = "hiveunderattack"

    --     return { name = name, weight = weight,
    --         perform = function(move)
    --             bot:GetMotion():SetDesiredMoveTarget(hiveUnderAttack and hiveUnderAttack:GetOrigin())
    --             bot:GetMotion():SetDesiredViewTarget(nil)
    --         end }

    -- end,

    ------------------------------------------
    --
    ------------------------------------------
    function(bot, brain)
        local name = "attack"
        local skulk = bot:GetPlayer()
        local eyePos = skulk:GetEyePos()

        local memories = GetTeamMemories(skulk:GetTeamNumber())
        local bestUrgency, bestMem = GetMaxTableEntry( memories,
                function( mem )
                    return GetAttackUrgency( bot, mem )
                end)

        local weapon = skulk:GetActiveWeapon()
        local canAttack = weapon ~= nil and weapon:isa("BiteLeap")

        local weight = 0.0

        if canAttack and bestMem ~= nil then

            local dist = 0.0
            if Shared.GetEntity(bestMem.entId) ~= nil then
                dist = GetDistanceToTouch( eyePos, Shared.GetEntity(bestMem.entId) )
            else
                dist = eyePos:GetDistance( bestMem.lastSeenPos )
            end

            weight = EvalLPF( dist, {
                    { 0.0, EvalLPF( bestUrgency, {
                        { 0.0, 0.0 },
                        { 10.0, 25.0 }
                        })},
                    { 10.0, EvalLPF( bestUrgency, {
                            { 0.0, 0.0 },
                            { 10.0, 5.0 }
                            })},
                    { 100.0, 0.0 } })
        end

        return { name = name, weight = weight,
            perform = function(move)
                PerformAttack( eyePos, bestMem, bot, brain, move )
            end }
    end,

    ------------------------------------------
    --
    ------------------------------------------
    -- function(bot, brain)
    --     local name = "pheromone"

    --     local skulk = bot:GetPlayer()
    --     local eyePos = skulk:GetEyePos()

    --     local pheromones = EntityListToTable(Shared.GetEntitiesWithClassname("Pheromone"))
    --     local bestPheromoneLocation = nil
    --     local bestValue = 0

    --     for p = 1, #pheromones do

    --         local currentPheromone = pheromones[p]
    --         if currentPheromone then
    --             local techId = currentPheromone:GetType()

    --             if techId == kTechId.ExpandingMarker or techId == kTechId.ThreatMarker then

    --                 local location = currentPheromone:GetOrigin()
    --                 local locationOnMesh = Pathing.GetClosestPoint(location)
    --                 local distanceFromMesh = location:GetDistance(locationOnMesh)

    --                 if distanceFromMesh > 0.001 and distanceFromMesh < 2 then

    --                     local distance = eyePos:GetDistance(location)

    --                     if currentPheromone.visitedBy == nil then
    --                         currentPheromone.visitedBy = {}
    --                     end

    --                     if not currentPheromone.visitedBy[bot] then

    --                         if distance < 5 then
    --                             currentPheromone.visitedBy[bot] = true
    --                         else

    --                             -- Value goes from 5 to 10
    --                             local value = 5.0 + 5.0 / math.max(distance, 1.0) - #(currentPheromone.visitedBy)

    --                             if value > bestValue then
    --                                 bestPheromoneLocation = locationOnMesh
    --                                 bestValue = value
    --                             end

    --                         end

    --                     end

    --                 end

    --             end

    --         end

    --     end

    --     local weight = EvalLPF( bestValue, {
    --         { 0.0, 0.0 },
    --         { 10.0, 1.0 }
    --         })

    --     return { name = name, weight = weight,
    --         perform = function(move)
    --             bot:GetMotion():SetDesiredMoveTarget(bestPheromoneLocation)
    --             bot:GetMotion():SetDesiredViewTarget(nil)
    --         end }
    -- end,

    ------------------------------------------
    --
    ------------------------------------------
    function(bot, brain)
        local name = "order"

        local skulk = bot:GetPlayer()
        local order = bot:GetPlayerOrder()

        local weight = 0.0
        if order ~= nil then
            weight = 10.0
        end

        return { name = name, weight = weight,
            perform = function(move)
                if order then

                    local target = Shared.GetEntity(order:GetParam())

                    if target ~= nil and order:GetType() == kTechId.Attack then

                        PerformAttackEntity( skulk:GetEyePos(), target, bot, brain, move )

                    else

                        if brain.debug then
                            DebugPrint("unknown order type: %s", ToString(order:GetType()) )
                        end

                        bot:GetMotion():SetDesiredMoveTarget( order:GetLocation() )
                        bot:GetMotion():SetDesiredViewTarget( nil )

                    end
                end
            end }
    end,

}

------------------------------------------
--
------------------------------------------
function CreateSkulkBrainSenses()

    local s = BrainSenses()
    s:Initialize()

    s:Add("allThreats", function(db)
            local player = db.bot:GetPlayer()
            local team = player:GetTeamNumber()
            local memories = GetTeamMemories( team )
            return FilterTableEntries( memories,
                function( mem )
                    local ent = Shared.GetEntity( mem.entId )

                    if ent:isa("Player") or ent:isa("Sentry") or ent:isa("InfantryPortal") then
                        local isAlive = HasMixin(ent, "Live") and ent:GetIsAlive()
                        local isEnemy = HasMixin(ent, "Team") and ent:GetTeamNumber() ~= team
                        return isAlive and isEnemy
                    else
                        return false
                    end
                end)
        end)

    s:Add("nearestThreat", function(db)
            local allThreats = db:Get("allThreats")
            local player = db.bot:GetPlayer()
            local playerPos = player:GetOrigin()

            local distance, nearestThreat = GetMinTableEntry( allThreats,
                function( mem )
                    local origin = mem.origin
                    if origin == nil then
                        origin = Shared.GetEntity(mem.entId):GetOrigin()
                    end
                    return playerPos:GetDistance(origin)
                end)

            return {distance = distance, memory = nearestThreat}
        end)

    return s
end
