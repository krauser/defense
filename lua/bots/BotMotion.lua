-- ======= Copyright (c) 2003-2013, Unknown Worlds Entertainment, Inc. All rights reserved. =======
--
-- Created by Steven An (steve@unknownworlds.com)
--
-- This class takes high-level motion-intents as input (ie. "I want to move here" or "I want to go in this direction")
-- and translates them into controller-inputs, ie. mouse direction and button presses.
--
-- ==============================================================================================

local botPathInit = false

local function _ParsePathingSettings()

    -- override this setting - anything larger causes huge perf spikes in pathing
    -- local maxOptionTileSize = 36
    -- if settings.option_tile_size > maxOptionTileSize then
    --    Print("Warning: Overriding map's pathing_settings.option_tile_size from %d to %d", settings.option_tile_size, maxOptionTileSize);
    --     settings.option_tile_size = maxOptionTileSize
    -- end

    -- gPathingOptions[Pathing.Option_CellSize]         = 0.26         -- Grid cell size
    -- gPathingOptions[Pathing.Option_CellHeight]       = 0.40         -- Grid cell height
    -- gPathingOptions[Pathing.Option_AgentHeight]      = 2.0          -- Minimum height where the agent can still walk
    -- gPathingOptions[Pathing.Option_AgentRadius]      = 0.6          -- Radius of the agent in cells
    -- gPathingOptions[Pathing.Option_AgentMaxClimb]    = 0.9          -- Maximum height between grid cells the agent can climb
    -- gPathingOptions[Pathing.Option_AgentMaxSlope]    = 45.0         -- Maximum walkable slope angle in degrees.
    -- gPathingOptions[Pathing.Option_RegionMinSize]    = 8            -- Regions whose area is smaller than this threshold will be removed.
    -- gPathingOptions[Pathing.Option_RegionMergeSize]  = 20           -- Regions whose area is smaller than this threshold will be merged
    -- gPathingOptions[Pathing.Option_EdgeMaxLen]       = 12.0         -- Maximum contour edge length
    -- gPathingOptions[Pathing.Option_EdgeMaxError]     = 1.3          -- Maximum distance error from contour to cells
    -- gPathingOptions[Pathing.Option_VertsPerPoly]     = 6.0          -- Max number of vertices per polygon
    -- gPathingOptions[Pathing.Option_DetailSampleDist] = 6.0          -- Detail mesh sample spacing.
    -- gPathingOptions[Pathing.Option_DetailSampleMaxError] = 1.0      -- Detail mesh simplification max sample error.
    -- gPathingOptions[Pathing.Option_TileSize]         = 16           -- Width and Height of a tile



    -- SetPathingOption(Pathing.Option_CellSize, settings.option_cell_size)
    -- SetPathingOption(Pathing.Option_CellHeight, settings.option_cell_height)
    SetPathingOption(Pathing.Option_AgentHeight, 100)
    SetPathingOption(Pathing.Option_AgentRadius, 0.5)
    SetPathingOption(Pathing.Option_AgentMaxClimb, 10)
    SetPathingOption(Pathing.Option_AgentMaxSlope, 360)

    SetPathingOption(Pathing.Option_RegionMinSize, 4)
    SetPathingOption(Pathing.Option_RegionMergeSize, 4)
    -- SetPathingOption(Pathing.Option_EdgeMaxLen, settings.option_edge_max_len)
    -- SetPathingOption(Pathing.Option_EdgeMaxError, settings.option_edge_max_error)
    -- SetPathingOption(Pathing.Option_VertsPerPoly, settings.option_verts_per_poly)
    -- SetPathingOption(Pathing.Option_DetailSampleDist, settings.option_detail_sample_dist)
    -- SetPathingOption(Pathing.Option_DetailSampleMaxError, settings.option_detail_sample_max_error)
    SetPathingOption(Pathing.Option_TileSize, 1)

end

local function _InitializePathing()

   if (botPathInit == false) then
      _ParsePathingSettings()
      Pathing.BuildMesh()

      botPathInit = true
   end

end



local function _SmoothPathPoints(points, dst, maxPoints, maxDistance)

   local i = 1
   local a = 0.15 -- alpha for color
   local new_points = {}

   local numPoints = #points
   local point1 = points[i]
   local point2 = points[i]
   while (i + 3 < numPoints and i < maxPoints) do

      point1 = points[i]
      point2 = points[i + 3]

      if (point1 and point2) then
         -- If the distance between two points is large, add intermediate points

         local delta = point2 - point1
         local distance = delta:GetLength()
         local numNewPoints = math.floor(distance / maxDistance)

         for j = 1, numNewPoints do

            local f = j / numNewPoints
            local newPoint = point1 + delta * f
            -- if table.find(points, newPoint) == nil then
            table.insert(new_points, newPoint)
            -- end
         end
         -- DebugWireSphere(points[i], 0.06,
         --                 kFadedHuntPathDuration,
         --                 0, 0.8, 0, a) -- r, g, b, a
      end
      i = i + 3
   end

   local delta = point2 - dst
   local distance = delta:GetLength()
   local numNewPoints = math.floor(distance / maxDistance)

   for j = 1, numNewPoints do

      local f = j / numNewPoints
      local newPoint = point1 + delta * f
      -- if table.find(points, newPoint) == nil then
      table.insert(new_points, newPoint)
      -- end
   end

   return new_points
end

local last_call = Shared.GetTime()
local function _InternalGeneratePath(src, dst, doSmooth, smoothDist, maxSplitPoints, allowFlying)

   if not smoothDist then
      smoothDist = 0.5
   end

   if not maxSplitPoints then
      maxSplitPoints = 2
   end

   local points = PointArray()

   -- Query the pathing system for the path to the dst
   -- if fails then fallback to the old system
   local isReachable = Pathing.GetPathPoints(src, dst, points)

   -- if (Server and last_call + 1 < Shared.GetTime() and src) then
   --    last_call = Shared.GetTime()
   --    for _, player in ipairs(GetEntitiesForTeam("Player", 1)) do
   --       Server.SendNetworkMessage(player, "OnFinishHunting",
   --                                 {
   --                                    location = src
   --                                 }, true)
   --    end
   -- end

    if #points ~= 0-- and isReachable
    then
       if (doSmooth) then
          points = _SmoothPathPoints( points, dst, 100, 0.7)
       end
       return points
    end

    return points

end

------------------------------------------
--  Expensive pathing call
------------------------------------------
local function GetOptimalMoveDirection( bot, from, to )

   -- _InitializePathing()

   -- local nextPathPoint = nil
   -- pathPoints = _InternalGeneratePath(from, to, true)
   -- if (pathPoints and #pathPoints > 1) then
   --    nextPathPoint = pathPoints[1]
   -- end

   local i = 1
   -- Dist of one point: 0.5
   local min_point_to_cache = 60
   local path_max_point = 30 -- A lot less 'long' GetPath done (close are a lot quicker)
   if (false and bot and bot.def_cache_path and #bot.def_cache_path > 5)
   then
      local path_checkpoint = bot.def_cache_path[#bot.def_cache_path]
      pathPoints = _InternalGeneratePath(from, path_checkpoint, false)

      if (pathPoints and #pathPoints > 0) then
         bot.def_cache_path = {}
         while (i <= #pathPoints and i < path_max_point) do
            table.insert(bot.def_cache_path, pathPoints[i])
            i = i + 1
         end
      end

      -- local nto = bot.def_cache_path[5]
      -- local nto_next = bot.def_cache_path[6]
      -- if (GetNormalizedVectorXZ(from):GetDistanceTo(GetNormalizedVectorXZ(nto)) < 5)
      -- then
      --    table.remove(bot.def_cache_path, 1)
      --    bot.def_next_direction = (nto - from):GetUnit()
      -- if (bot:isa("Lerk")) then
      --    Shared:FadedMessage("Reusing checkpoint :" .. tostring(pathPoints[1]:GetDistanceTo(pathPoints[2])))
      -- end
      -- end

   else

      -- bot.def_cache_path = {}
      local refresh_rate = 0
      if bot:isa("Skulk") then
         refresh_rate = 0.35
      elseif bot:isa("Gorge") then
         refresh_rate = 0.45
      elseif bot:isa("Lerk") then
         refresh_rate = 0.20
      elseif bot:isa("Fade") then
         refresh_rate = 0.3
      elseif bot:isa("Onos") then
         refresh_rate = 0.3
      end

      if (bot.def_last_path_gen == nil or bot.def_last_path_gen + refresh_rate < Shared.GetTime()) then
         bot.def_last_path_gen = Shared.GetTime()

         pathPoints = _InternalGeneratePath(from, to, false)
         -- Shared:FadedMessage("Regen call")
      else
         -- Shared:FadedMessage("Optimized call")
         return bot.def_last_direction
      end

      -- if (pathPoints and #pathPoints > min_point_to_cache) then
      --    while (i <= #pathPoints and i < path_max_point) do
      --       table.insert(bot.def_cache_path, pathPoints[i])
      --       i = i + 1
      --    end
      -- end
   end

   if (pathPoints and #pathPoints > 1) then
      bot.def_last_direction = (pathPoints[1] - from):GetUnit()
   else
      -- sensible fallback
      -- DebugPrint("Could not find path from %s to %s", ToString(from), ToString(to))
      bot.def_last_direction = (to-from):GetUnit()
   end
   --TODO: faire un pathfinding global (pour tout les bots proche, faire un unique pathfinding)
   local ret = bot.def_last_direction
   if (#pathPoints < 35) then
      bot.def_last_path_gen = nil
      bot.def_last_direction = nil
   end
   return ret
end

------------------------------------------
--  Provides an interface for higher level logic to specify desired motion.
--  The actual bot classes use this to compute move.move, move.yaw/pitch. Also, jump.
------------------------------------------

class "BotMotion"

function BotMotion:Initialize(player)

    self.currMoveDir = Vector(0,0,0)
    self.currViewDir = Vector(1,0,0)
    self.lastMovedPos = player:GetOrigin()

end

function BotMotion:ComputeLongTermTarget(player)

    local kTargetOffset = 1

    if self.desiredMoveDirection ~= nil then

        local toPoint = player:GetOrigin() + self.desiredMoveDirection * kTargetOffset
        return toPoint

    elseif self.desiredMoveTarget ~= nil then

        return self.desiredMoveTarget

    else

        return nil

    end
end

------------------------------------------
--
------------------------------------------
function BotMotion:OnGenerateMove(player)

    local currentPos = player:GetOrigin()
    local eyePos = player:GetEyePos()
    local doJump = false

    local delta = currentPos - self.lastMovedPos

    //----------------------------------------
    --  Update ground motion
    //----------------------------------------

    local moveTargetPos = self:ComputeLongTermTarget(player)

    if moveTargetPos ~= nil and not player:isa("Embryo") then

        local distToTarget = currentPos:GetDistance(moveTargetPos)

        if distToTarget <= 0.01 then

            -- Basically arrived, stay here
            self.currMoveDir = Vector(0,0,0)

        else

            -- Path to the target position
            -- But for perf and for hysteresis control, only change direction about every 10th of a second
            local updateMoveDir = math.random() < 0.11

            if updateMoveDir then

                -- If we have not actually moved much since last frame, then maybe pathing is failing us
                -- So for now, move in a random direction for a bit and jump
                if delta:GetLength() < 1e-2 then

                    //Print("stuck! spazzing out")
                    self.currMoveDir = GetRandomDirXZ()
                    doJump = true

                elseif distToTarget <= 2.0
                   or (player:isa("Alien") and player:GetPlayerOrder() and player:GetPlayerOrder():GetType() == kTechId.Attack)
                then

                    -- Optimization: If we are close enough to target, just shoot straight for it.
                    -- We assume that things like lava pits will be reasonably large so this shortcut will
                    -- not cause bots to fall in
                    -- NOTE NOTE STEVETEMP TODO: We should add a visiblity check here. Otherwise, units will try to go through walls
                    self.currMoveDir = (moveTargetPos - currentPos):GetUnit()

                else

                    -- We are pretty far - do the expensive pathing call
                    self.currMoveDir = GetOptimalMoveDirection( player, currentPos, moveTargetPos )

                end

            end

            self.lastMovedPos = currentPos
        end


    else

        -- Did not want to move anywhere - stay still
        self.currMoveDir = Vector(0,0,0)

    end

    //----------------------------------------
    --  View direction
    //----------------------------------------

    if self.desiredViewTarget ~= nil then

        -- Look at target
        self.currViewDir = (self.desiredViewTarget - eyePos):GetUnit()

    elseif self.currMoveDir:GetLength() > 1e-4 then

        -- Look in move dir
        self.currViewDir = self.currMoveDir
        self.currViewDir.y = 0.0  -- pathing points are slightly above ground, which leads to funny looking-up
        self.currViewDir = self.currViewDir:GetUnit()

    else
        -- leave it alone
    end

    return self.currViewDir, self.currMoveDir, doJump

end

------------------------------------------
--  Higher-level logic interface
------------------------------------------
function BotMotion:SetDesiredMoveTarget(toPoint)

    -- Mutually exclusive
    self:SetDesiredMoveDirection(nil)

    if not VectorsApproxEqual( toPoint, self.desiredMoveTarget, 1e-4 ) then
        self.desiredMoveTarget = toPoint
    end

end

------------------------------------------
--  Higher-level logic interface
------------------------------------------
-- Note: while a move direction is set, it overrides a target set by SetDesiredMoveTarget
function BotMotion:SetDesiredMoveDirection(direction)

    if not VectorsApproxEqual( direction, self.desiredMoveDirection, 1e-4 ) then
        self.desiredMoveDirection = direction
    end

end

------------------------------------------
--  Higher-level logic interface
--  Set to nil to clear view target
------------------------------------------
function BotMotion:SetDesiredViewTarget(target)

    self.desiredViewTarget = target

end
