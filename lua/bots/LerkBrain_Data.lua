
Script.Load("lua/bots/CommonActions.lua")
Script.Load("lua/bots/BrainSenses.lua")

local kUpgrades = {
    -- kTechId.Carapace,
    -- kTechId.Regeneration,

    -- kTechId.Phantom,
    -- kTechId.Aura,

    -- kTechId.Celerity,
    -- kTechId.Adrenaline,
}

------------------------------------------
--  More urgent == should really attack it ASAP
------------------------------------------
local function GetAttackUrgency(bot, mem)

   return 0.0

    -- -- See if we know whether if it is alive or not
    -- local ent = Shared.GetEntity(mem.entId)
    -- if not HasMixin(ent, "Live") or not ent:GetIsAlive() then
    --     return 0.0
    -- end

    -- local botPos = bot:GetPlayer():GetOrigin()
    -- local targetPos = ent:GetOrigin()
    -- local distance = botPos:GetDistance(targetPos)

    -- if mem.btype == kMinimapBlipType.PowerPoint then
    --     local powerPoint = ent
    --     if powerPoint ~= nil and powerPoint:GetIsSocketed() then
    --         return 0.65
    --     else
    --         return 0
    --     end
    -- end

    -- local immediateThreats = {

    --     [kMinimapBlipType.Marine] = true,
    --     [kMinimapBlipType.JetpackMarine] = true,
    --     [kMinimapBlipType.Exo] = true,
    --     [kMinimapBlipType.Sentry] = true
    -- }

    -- if distance < 10 and immediateThreats[mem.btype] then
    --     -- Attack the nearest immediate threat (urgency will be 1.1 - 2)
    --     return 1 + 1 / math.max(distance, 1)
    -- end

    -- -- No immediate threat - load balance!
    -- local numOthers = bot.brain.teamBrain:GetNumAssignedTo( mem,
    --         function(otherId)
    --             if otherId ~= bot:GetPlayer():GetId() then
    --                 return true
    --             end
    --             return false
    --         end)

    -- local urgencies = {
    --     -- Active threats
    --     [kMinimapBlipType.Marine] =             numOthers >= 4 and 0.6 or 1,
    --     [kMinimapBlipType.JetpackMarine] =      numOthers >= 4 and 0.7 or 1.1,
    --     [kMinimapBlipType.Exo] =                numOthers >= 6 and 0.8 or 1.2,
    --     [kMinimapBlipType.Sentry] =             numOthers >= 3 and 0.5 or 0.95,

    --     -- Structures
    --     [kMinimapBlipType.ARC] =                numOthers >= 4 and 0.4 or 0.9,
    --     [kMinimapBlipType.CommandStation] =     numOthers >= 8 and 0.3 or 0.85,
    --     [kMinimapBlipType.PhaseGate] =          numOthers >= 4 and 0.2 or 0.8,
    --     [kMinimapBlipType.Observatory] =        numOthers >= 3 and 0.2 or 0.75,
    --     [kMinimapBlipType.Extractor] =          numOthers >= 3 and 0.2 or 0.7,
    --     [kMinimapBlipType.InfantryPortal] =     numOthers >= 3 and 0.2 or 0.6,
    --     [kMinimapBlipType.PrototypeLab] =       numOthers >= 3 and 0.2 or 0.55,
    --     [kMinimapBlipType.Armory] =             numOthers >= 3 and 0.2 or 0.5,
    --     [kMinimapBlipType.RoboticsFactory] =    numOthers >= 3 and 0.2 or 0.5,
    --     [kMinimapBlipType.ArmsLab] =            numOthers >= 3 and 0.2 or 0.5,
    --     [kMinimapBlipType.MAC] =                numOthers >= 2 and 0.2 or 0.4,
    -- }

    -- if urgencies[ mem.btype ] ~= nil then
    --     return urgencies[ mem.btype ]
    -- end

    -- return 0.0

end

function IsOutside(ent, pmaxDist)
   local maxDist = 30

   if (pmaxDist) then
      maxDist = pmaxDist
   end

   local origin = ent:GetOrigin()
   local trace = Shared.TraceRay(origin, origin + Vector(0, maxDist, 0), CollisionRep.Default, PhysicsMask.AllButPCsAndRagdolls, EntityFilterAll())

   -- If it hits something
   local heigh = nil

   if (trace.endPoint) then
      heigh = origin:GetDistanceTo(trace.endPoint)
   else
      heigh = maxDist
   end

   if trace.fraction < 1 then
      return false, origin:GetDistanceTo(trace.endPoint)
   end

   -- Double check
   return not GetWallBetween(ent:GetOrigin(), ent:GetOrigin() + Vector(0, maxDist, 0)), heigh
end

function getFlyHeigh(ent)
   local origin = ent:GetOrigin()
   local trace = Shared.TraceRay(origin, origin - Vector(0, 300, 0), CollisionRep.Default, PhysicsMask.AllButPCsAndRagdolls, EntityFilterAll())

   -- If it hits something, position on this surface (must be the world or another structure)
   if trace.fraction < 1 then
      return origin:GetDistanceTo(trace.endPoint)
   end
   return 0
end

local function __doPoisonBite( eyePos, bestTarget, bot, brain, move )
   bot:GetPlayer():SetActiveWeapon(LerkBite.kMapName, true)
   move.commands = AddMoveCommand( move.commands, Move.PrimaryAttack )
end

local function __doSporeOrUmbra( eyePos, bestTarget, bot, brain, move )
   if (bot:GetPlayer().lastSporeSent == nil) then
      bot:GetPlayer().nextSporeSent = 0
   end
   -- Change each Ns of weapon
   if (bot:GetPlayer().nextSporeSent < Shared.GetTime())
   then
      bot.defensePreventAttack = nil
      bot:GetPlayer().nextSporeSent = Shared.GetTime() + math.random(1, 4)
      if (math.random() < 0.07) then -- Spores
         bot:GetPlayer().DistMoveToUse = Move.PrimaryAttack
      else -- Spike
         bot:GetPlayer().DistMoveToUse = Move.SecondaryAttack
         if (math.random() < 0.7) then
            bot.defensePreventAttack = true
         end
      end
   end
   if (math.random(1, 3) == 1) then
      bot:GetPlayer():SetActiveWeapon(LerkUmbra.kMapName, true)
   else
      bot:GetPlayer():SetActiveWeapon(Spores.kMapName, true)
   end

   if (bot.defensePreventAttack ~= true) then
      move.commands = AddMoveCommand( move.commands, bot:GetPlayer().DistMoveToUse )
   end
end

local function _doAttack( eyePos, bestTarget, bot, brain, move )

   local distance = eyePos:GetDistance(bestTarget:GetOrigin())

   bot:GetMotion():SetDesiredViewTarget( bestTarget:GetEngagementPoint() )

   if (distance > 3 and defHasAbility(kTechId.Spores) and bot.def_has_range_attack == true)
   then
      __doSporeOrUmbra( eyePos, bestTarget, bot, brain, move )
   else
      __doPoisonBite( eyePos, bestTarget, bot, brain, move )
   end

   -- Attacking a structure
   if GetDistanceToTouch(eyePos, bestTarget) < 8 then
      -- Stop running at the structure when close enough
      bot:GetMotion():SetDesiredMoveTarget(nil)
   end
end

----------------

local function _doFlyHighThread( eyePos, bestTarget, bot, brain, move )
   local minFlyHeigh = 16
   local maxFlyHeigh = 30
   local minTimeInSky = 2
   local maxTimeInSky = 5
   local skyAttackDuration = 10

   local engagementPoint = bestTarget:GetEngagementPoint()

   local marinePos = bestTarget:GetOrigin()
   local ground_dist = GetNormalizedVectorXZ(bot:GetOrigin())
   local currentflyHeigh = getFlyHeigh(bot)
   local _, ceilingHeigh = IsOutside(bestTarget, maxFlyHeigh)
   -- local bothOutside = IsOutside(bot) and IsOutside(bestTarget)

   if (bot.def_max_fly_heigh_percent == nil) then -- safety
      bot.def_max_fly_heigh_percent = 1
   end

   maxFlyHeigh = Clamp(1, maxFlyHeigh, ceilingHeigh * (80/100) * bot.def_max_fly_heigh_percent)
   minFlyHeigh = maxFlyHeigh * (50/100)

   local y_fly_heigh = marinePos.y + minFlyHeigh
   local highPosBot = GetNormalizedVectorXZ(bot:GetOrigin()) + Vector(0, y_fly_heigh, 0)
   local highPosTarget = GetNormalizedVectorXZ(marinePos) + Vector(0, y_fly_heigh, 0)
   local OutAndVisible = (not GetWallBetween(highPosTarget, highPosBot)
                             and not GetWallBetween(bot:GetOrigin(), highPosBot))

   -- local new
   -- local OutAndVisible = true and not GetWallBetween(bot:GetEyePos()
   --                                                      + Vector(0, minFlyHeigh - currentflyHeigh, 0),
   --                                                   marinePos)

   -- local visible = GetWallBetween(bot:GetEyePos(), marinePos) == false
   -- local OutAndVisible = bothOutside and visible

   bot.defenseSkyThread = nil
   ground_dist = ground_dist:GetDistanceTo(GetNormalizedVectorXZ(marinePos))
   if (ceilingHeigh > 5) then
      if (OutAndVisible) then

         if (bot.defenseLerkSkyAttack == nil and ground_dist < 60)
         then
            bot.defenseLerkSkyAttack = Shared.GetTime() + math.random(minTimeInSky, maxTimeInSky)
         end

         if (bot.defenseLerkSkyAttack and bot.defenseLerkSkyAttack < Shared.GetTime())
         then
            if (bot.defenseLerkSkyAttack + skyAttackDuration < Shared.GetTime()) then -- go high again
               bot.defenseLerkSkyAttack = nil
            else
               -- Do nothing, we are byting the marine
               return nil
            end
         else -- Fly high, just a thread
            bot.defenseFlyHeight = minFlyHeigh + math.random(0, maxFlyHeigh - minFlyHeigh)
            bot:GetMotion():SetDesiredViewTarget( highPosTarget)--engagementPoint + Vector(0, bot.defenseFlyHeight, 0))
            -- Shared:FadedMessage("Regular fly high")
            bot.defenseSkyThread = true
         end
      else
         bot.defenseLerkSkyAttack = nil
      end

   end

   if (OutAndVisible and ground_dist > 20) then
      bot.defenseSkyThread = true
   end
   return bot.defenseSkyThread
end

local function _doEvadeBullets( eyePos, bestTarget, bot, brain, move )

   local marinePos = bestTarget:GetModelOrigin() -- Aim at the head

   if (bestTarget:isa("Player")) then -- Go behind the player
      marinePos = marinePos + bestTarget:GetViewCoords().zAxis * -0.2
   end

   -- Make lerk movement more like an insect, move randomly a bit everywhere
   if ((bot:GetPlayer().sighted and math.random() < 0.52) or math.random() < 0.1) then

      move.commands = AddMoveCommand( move.commands, Move.Jump )

      -- When approaching, try to jump sideways
      bot.timeOfJump = Shared.GetTime()
      bot.jumpOffset = nil

   end

   -- If not jump for too long, force one
   if not bot:GetPlayer():GetIsOnGround() then
      if bot.timeOfJump and bot.timeOfJump + 0.15 < Shared.GetTime() then
         move.commands = AddMoveCommand( move.commands, Move.Jump )
      end
   end

   -- Change the view direction and position to evade bullets even better
   if bot.timeOfJump ~= nil and Shared.GetTime() - bot.timeOfJump < 0.3 then

      if bot.jumpOffset == nil then


         local botToTarget = GetNormalizedVectorXZ(marinePos - eyePos)
         local sideVector = nil
         if (math.random() < 0.5) then
            sideVector = botToTarget:CrossProduct(Vector(0, 1 + math.random(), 0))
         else
            sideVector = botToTarget:CrossProduct(Vector(1 + math.random(), 0, 0))
         end
         if math.random() < 0.72 then -- Choose between up/down or Right/Left (more chances to go up)
            bot.jumpOffset = botToTarget + sideVector
         else
            bot.jumpOffset = botToTarget - sideVector
         end
         -- bot:GetMotion():SetDesiredViewTarget( bestTarget:GetEngagementPoint())

      end

      bot:GetMotion():SetDesiredMoveDirection( bot.jumpOffset )
   end

end

local function PerformAttackEntity( eyePos, bestTarget, bot, brain, move )

   assert( bestTarget )

   local doFire = false
   local marinePos = bestTarget:GetOrigin()
   local distance = eyePos:GetDistance(marinePos)
   local do_fly_high = nil
   local defenseSkyThread = nil

   -- bot:GetMotion():SetDesiredMoveTarget( marinePos )
   -- bot:GetMotion():SetDesiredMoveDirection( (marinePos - bot:GetOrigin()):GetUnit() )
   do_fly_high = _doFlyHighThread( eyePos, bestTarget, bot, brain, move )
   defenseSkyThread = bot.defenseSkyThread
   if distance < 18 and GetBotCanSeeTarget( bot:GetPlayer(), bestTarget ) then
      if (not defenseSkyThread) then  -- Do not shoot if in the air (not aiming at target)
         doFire = true
      end
   end

   if (do_fly_high ~= true and doFire) then
      _doAttack( eyePos, bestTarget, bot, brain, move )
      -- bot:GetMotion():SetDesiredViewTarget( bestTarget:GetModelOrigin())
   end

   _doEvadeBullets( eyePos, bestTarget, bot, brain, move )

   if (do_fly_high) then
      -- This allow lerks to keep flying high when far instead of eating dirt all the way
      if (bot:GetOrigin().y < marinePos.y + 5) then
         if (bot:GetOrigin():GetDistanceTo(marinePos) > 25) then
            bot:GetMotion():SetDesiredViewTarget( bot:GetOrigin() + Vector(0, 100, 0) )
         else
            bot:GetMotion():SetDesiredViewTarget( marinePos + Vector(0, 5, 0) )
         end
      end
   end

end

local function PerformAttack( eyePos, mem, bot, brain, move )

    assert( mem )

    local target = Shared.GetEntity(mem.entId)

    if target ~= nil then

        PerformAttackEntity( eyePos, target, bot, brain, move )

    else

        -- mem is too far to be relevant, so move towards it
        bot:GetMotion():SetDesiredViewTarget(nil)
        bot:GetMotion():SetDesiredMoveTarget(mem.lastSeenPos)

    end

    brain.teamBrain:AssignBotToMemory(bot, mem)
end

------------------------------------------
--  Each want function should return the fuzzy weight,
-- along with a closure to perform the action
-- The order they are listed matters - actions near the beginning of the list get priority.
------------------------------------------
kLerkBrainActions =
{

    //----------------------------------------
    //
    //----------------------------------------
    function(bot, brain)
        return { name = "debug idle", weight = 0.001,
                perform = function(move)
                    bot:GetMotion():SetDesiredMoveTarget(nil)
                    -- there is nothing obvious to do.. figure something out
                    -- like go to the marines, or defend
                end }
    end,

    //----------------------------------------
    //
    //----------------------------------------
    -- CreateExploreAction( 0.01, function(pos, targetPos, bot, brain, move)

    --             if math.random() < 0.1 and bot:GetPlayer():GetIsOnGround() then

    --                 move.commands = AddMoveCommand( move.commands, Move.Jump )

    --                 -- When approaching, try to jump sideways
    --                 bot.timeOfJump = Shared.GetTime()
    --                 bot.jumpOffset = nil

    --             end

    --             if not bot:GetPlayer():GetIsOnGround() and bot.timeOfJump and bot.timeOfJump + 0.3 < Shared.GetTime() then
    --                 move.commands = AddMoveCommand( move.commands, Move.Jump )

    --                 if bot.timeOfJump + 3 > Shared.GetTime() then
    --                     bot.timeOfJump = Shared.GetTime()
    --                 end

    --             end

    --             bot:GetMotion():SetDesiredMoveTarget(targetPos)
    --             bot:GetMotion():SetDesiredViewTarget(nil)
    --             end ),

    //----------------------------------------
    //
    //----------------------------------------
    -- function(bot, brain)
    --     local name = "evolve"

    --     local weight = 0.0
    --     local player = bot:GetPlayer()
    --     local s = brain:GetSenses()

    --     local distanceToNearestThreat = s:Get("nearestThreat").distance
    --     local desiredUpgrades = {}

    --     if player:GetIsAllowedToBuy() and
    --        (distanceToNearestThreat == nil or distanceToNearestThreat > 15) and
    --        (player.GetIsInCombat == nil or not player:GetIsInCombat()) then

    --         -- Safe enough to try to evolve

    --         local existingUpgrades = player:GetUpgrades()

    --         for i = 1, #kUpgrades do
    --             local techId = kUpgrades[i]
    --             local techNode = player:GetTechTree():GetTechNode(techId)

    --             local isAvailable = false
    --             if techNode ~= nil then
    --                 isAvailable = techNode:GetAvailable(player, techId, false)
    --             end

    --             if not player:GetHasUpgrade(techId) and isAvailable and
    --                GetIsUpgradeAllowed(player, techId, existingUpgrades) and
    --                GetIsUpgradeAllowed(player, techId, desiredUpgrades) then
    --                 table.insert(desiredUpgrades, techId)
    --             end
    --         end

    --         if  #desiredUpgrades > 0 then
    --             weight = 100.0
    --         end
    --     end

    --     return { name = name, weight = weight,
    --         perform = function(move)
    --             player:ProcessBuyAction( desiredUpgrades )
    --         end }

    -- end,

    //----------------------------------------
    //
    //----------------------------------------
    function(bot, brain)
        local name = "attack"
        local skulk = bot:GetPlayer()
        local eyePos = skulk:GetEyePos()

        local memories = GetTeamMemories(skulk:GetTeamNumber())
        local bestUrgency, bestMem = GetMaxTableEntry( memories,
                function( mem )
                    return GetAttackUrgency( bot, mem )
                end)

        local weapon = skulk:GetActiveWeapon()
        local canAttack = weapon ~= nil and weapon:isa("LerkBite")

        local weight = 0.0

        if canAttack and bestMem ~= nil then

            local dist = 0.0
            if Shared.GetEntity(bestMem.entId) ~= nil then
                dist = GetDistanceToTouch( eyePos, Shared.GetEntity(bestMem.entId) )
            else
                dist = eyePos:GetDistance( bestMem.lastSeenPos )
            end

            weight = EvalLPF( dist, {
                    { 0.0, EvalLPF( bestUrgency, {
                        { 0.0, 0.0 },
                        { 10.0, 25.0 }
                        })},
                    { 10.0, EvalLPF( bestUrgency, {
                            { 0.0, 0.0 },
                            { 10.0, 5.0 }
                            })},
                    { 100.0, 0.0 } })
        end

        return { name = name, weight = weight,
            perform = function(move)
                PerformAttack( eyePos, bestMem, bot, brain, move )
            end }
    end,

    //----------------------------------------
    //
    //----------------------------------------
    -- function(bot, brain)
    --     local name = "pheromone"

    --     local skulk = bot:GetPlayer()
    --     local eyePos = skulk:GetEyePos()

    --     local pheromones = EntityListToTable(Shared.GetEntitiesWithClassname("Pheromone"))
    --     local bestPheromoneLocation = nil
    --     local bestValue = 0

    --     for p = 1, #pheromones do

    --         local currentPheromone = pheromones[p]
    --         if currentPheromone then
    --             local techId = currentPheromone:GetType()

    --             if techId == kTechId.ExpandingMarker or techId == kTechId.ThreatMarker then

    --                 local location = currentPheromone:GetOrigin()
    --                 local locationOnMesh = Pathing.GetClosestPoint(location)
    --                 local distanceFromMesh = location:GetDistance(locationOnMesh)

    --                 if distanceFromMesh > 0.001 and distanceFromMesh < 2 then

    --                     local distance = eyePos:GetDistance(location)

    --                     if currentPheromone.visitedBy == nil then
    --                         currentPheromone.visitedBy = {}
    --                     end

    --                     if not currentPheromone.visitedBy[bot] then

    --                         if distance < 5 then
    --                             currentPheromone.visitedBy[bot] = true
    --                         else

    --                             -- Value goes from 5 to 10
    --                             local value = 5.0 + 5.0 / math.max(distance, 1.0) - #(currentPheromone.visitedBy)

    --                             if value > bestValue then
    --                                 bestPheromoneLocation = locationOnMesh
    --                                 bestValue = value
    --                             end

    --                         end

    --                     end

    --                 end

    --             end

    --         end

    --     end

    --     local weight = EvalLPF( bestValue, {
    --         { 0.0, 0.0 },
    --         { 10.0, 1.0 }
    --         })

    --     return { name = name, weight = weight,
    --         perform = function(move)
    --             bot:GetMotion():SetDesiredMoveTarget(bestPheromoneLocation)
    --             bot:GetMotion():SetDesiredViewTarget(nil)
    --         end }
    -- end,

    //----------------------------------------
    //
    //----------------------------------------
    function(bot, brain)
        local name = "order"

        local skulk = bot:GetPlayer()
        local order = bot:GetPlayerOrder()

        local weight = 0.0
        if order ~= nil then
            weight = 10.0
        end

        return { name = name, weight = weight,
            perform = function(move)
                if order then

                    local target = Shared.GetEntity(order:GetParam())

                    if target ~= nil and order:GetType() == kTechId.Attack then

                        PerformAttackEntity( skulk:GetEyePos(), target, bot, brain, move )

                    else

                        if brain.debug then
                            DebugPrint("unknown order type: %s", ToString(order:GetType()) )
                        end

                        bot:GetMotion():SetDesiredMoveTarget( order:GetLocation() )
                        bot:GetMotion():SetDesiredViewTarget( nil )

                        if math.random() < 0.1 and bot:GetPlayer():GetIsOnGround() then

                            move.commands = AddMoveCommand( move.commands, Move.Jump )

                            -- When approaching, try to jump sideways
                            bot.timeOfJump = Shared.GetTime()
                            bot.jumpOffset = nil

                        end

                        if not bot:GetPlayer():GetIsOnGround() and bot.timeOfJump and bot.timeOfJump + 0.3 < Shared.GetTime() then
                            move.commands = AddMoveCommand( move.commands, Move.Jump )

                            if bot.timeOfJump + 3 > Shared.GetTime() then
                                bot.timeOfJump = Shared.GetTime()
                            end

                        end

                    end
                end
            end }
    end,

}

------------------------------------------
--
------------------------------------------
function CreateLerkBrainSenses()

    local s = BrainSenses()
    s:Initialize()

    s:Add("allThreats", function(db)
            local player = db.bot:GetPlayer()
            local team = player:GetTeamNumber()
            local memories = GetTeamMemories( team )
            return FilterTableEntries( memories,
                function( mem )
                    local ent = Shared.GetEntity( mem.entId )

                    if ent:isa("Player") or ent:isa("Sentry") then
                        local isAlive = HasMixin(ent, "Live") and ent:GetIsAlive()
                        local isEnemy = HasMixin(ent, "Team") and ent:GetTeamNumber() ~= team
                        return isAlive and isEnemy
                    else
                        return false
                    end
                end)
        end)

    s:Add("nearestThreat", function(db)
            local allThreats = db:Get("allThreats")
            local player = db.bot:GetPlayer()
            local playerPos = player:GetOrigin()

            local distance, nearestThreat = GetMinTableEntry( allThreats,
                function( mem )
                    local origin = mem.origin
                    if origin == nil then
                        origin = Shared.GetEntity(mem.entId):GetOrigin()
                    end
                    return playerPos:GetDistance(origin)
                end)

            return {distance = distance, memory = nearestThreat}
        end)

    return s
end
