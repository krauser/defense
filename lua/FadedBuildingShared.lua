-- ===================== Faded Mod =====================
--
-- lua\FadedBuildingShared.lua
--
--    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- This files is used to stored shared function between all objective mod building
--
-- ===================== Faded Mod =====================

local locale = LibCache:GetLibrary("LibLocales-1.0")
local strformat = string.format

if (Server) then

   function FadedSpawnBuilding(techId, mapName, orig, coord_fix, max_range, force, team)
      if (max_range == nil) then
         max_range = 10
      end

      local extents = GetExtents(techId)
      local struct = nil
      local spawnPoint = nil
      if (force ~= true) then
         local i = 0
         while (i < 42 and not spawnPoint) do
            spawnPoint = GetRandomBuildPosition(techId, orig, max_range / 2)
            if (not spawnPoint) then
               spawnPoint = GetRandomBuildPosition(techId, orig, max_range)
            end

            -- Prevent building from spawning behind a wall or in the next room
            if (spawnPoint and orig and GetWallBetween(spawnPoint, orig)) then
               spawnPoint = nil
            end

            orig = orig + Vector(math.random() -0.5, math.random() / 2, math.random() -0.5) -- so the coord are not "exactly" the same
            i = i + 1
         end
      else
         spawnPoint = orig
      end

      if spawnPoint then
         spawnPoint = spawnPoint + coord_fix
         if (team == nil) then
            team = 1
         end
         struct = CreateEntity(mapName, spawnPoint, team)
         if (struct) then
            SetRandomOrientation(struct)
            if (struct.SetConstructionComplete) then
               struct:SetConstructionComplete()
            end
         end
      end
      return struct
   end

end
