
function BoneWall:GetResetsPathing()
   return false
end

function BoneWall:GetLifeSpan()
   return 2.1
end

local boneWallOnInitialized = BoneWall.OnInitialized
function BoneWall:OnInitialized() --

   CommanderAbility.OnInitialized(self)

   self.spawnPoint = self:GetOrigin()
   self:SetModel(BoneWall.kModelName, kAnimationGraph)

   if Server then
      -- self:TriggerEffects("bone_wall_burst")
      local team = self:GetTeam()
      if team then
         local level = math.max(0, team:GetBioMassLevel() - 1)
         local newMaxHealth = (kBoneWallHealth + level * kBoneWallHealthPerBioMass) * getAlienHealthScalar()
         if newMaxHealth ~= self.maxHealth  then
            self:SetMaxHealth(newMaxHealth)
            self:SetHealth(self.maxHealth)
         end
      end
   end

   -- Make the structure kinematic so that the player will collide with it.
   self:SetPhysicsType(PhysicsType.Kinematic)
   ------------------

   -- boneWallOnInitialized(self)
   for _, m in ipairs(GetEntitiesForTeamWithinRange("Player", 1, self:GetOrigin(), 2)) do
      if (not m:GetDarwinMode()) then
         m:DeductHealth(4)
      end
   end
end

function Web:GetSendDeathMessage()
   return false
end

local move_speed = 0.17
local boneWallOnUpdate = BoneWall.OnUpdate
function BoneWall:OnUpdate(deltaTime)
   boneWallOnUpdate(self, deltaTime)
   if (Server) then
      if (self.defenseSpawnLeft == nil) then
         self.defenseSpawnLeft = (1 / move_speed) * 60*2 -- Xs bonewall
      end

      if (self.nextSoundIn == nil) then
         self.nextSoundIn = -1
      end
      if (self.nextBoneWall == nil and self:GetCreationTime() + move_speed < Shared.GetTime()) then
         local nearest = nil
         local marines = GetEntitiesForTeam("Player", 1)
         Shared.SortEntitiesByDistance(self:GetOrigin(), marines)
         if (#marines) then
            nearest = marines[1]
         end
         -- local id = FindNearestEntityId("Marine", self:GetOrigin())
         -- local nearest = Shared.GetEntity(id)
         if (nearest) then
            local next_orig = getPath(self:GetOrigin(), nearest:GetOrigin(), true)
            -- If no position and almost out of time, force spawn around or elsewhere
            if ((self:GetCreationTime() + self:GetLifeSpan() / 0.6) < Shared.GetTime()) then
               if (#next_orig == 0) then
                  new_orig = {GetLocationAroundFor(self:GetOrigin(), kTechId.Clog, 4)}
               end
               if (#next_orig == 0) then
                  next_orig = {getRandomRTAroundMarine():GetOrigin() + Vector(0, 0.2, 0)}
               end
            end

            if (#next_orig >= 1 and self.defenseSpawnLeft > 0) then
               self.nextBoneWall = CreateEntity(BoneWall.kMapName, next_orig[1] + Vector(0, -1, 0), 2)
               if (self.nextBoneWall) then
                  AlienSpawnInfest(self.nextBoneWall)

                  self.nextBoneWall:SetHealth(self:GetHealth())
                  self.nextBoneWall:SetArmor(self:GetArmor())

                  self.nextBoneWall.defenseSpawnLeft = self.defenseSpawnLeft - 1
                  self.nextBoneWall.previousBoneWall = self
                  if (self.nextSoundIn <= 0) then
                     local web = CreateEntity(Web.kMapName, self:GetOrigin(), 2)
                     if (web) then web:Kill() end
                     self:TriggerEffects("bone_wall_burst")
                     self.nextBoneWall.nextSoundIn = 4
                  else
                     self.nextBoneWall.nextSoundIn = self.nextSoundIn - 1
                  end
                  --    self:TriggerEffects("web_destroy", { effecthostcoords = coords })
               end
            end
         end

      end
   end
end

local boneWallOnKill = BoneWall.OnKill
function BoneWall:OnKill(attacker, doer, point, direction)

   if (math.random() < 0.27) then -- Consume a lot of CPU if the boneworm is killed a lot on the tail
      self:TriggerEffects("death")
   end

   -- boneWallOnKill(self, attacker, doer, point, direction)
   -- if (self.previousBoneWall) then
   --    self.previousBoneWall:Kill()
   -- end
   if (self.nextBoneWall) then
      if (self.nextBoneWall.nextBoneWall) then
         self.nextBoneWall.nextBoneWall:DeductHealth(self:GetMaxHealth() / 7)
      end
      self.nextBoneWall:DeductHealth(self:GetMaxHealth() / 4)
   end

   DestroyEntity(self)
end


function BoneWall:OnAdjustModelCoords(modelCoords)
   local scale = 0.21 -- self.modelsize
   local coords = modelCoords
   coords.xAxis = coords.xAxis * scale -- Largeur
   coords.yAxis = coords.yAxis * scale -- Hauteur
   coords.zAxis = coords.zAxis * scale -- Profondeur
   return coords
end
