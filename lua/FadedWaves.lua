

local init = false
local wave_nb = 1
local wave_hp_scalar = 1
local all_waves_finished = false

local all_wave_pressure = 2
local all_wave_max_to_next = 0

-- Place lifeform used by player in priority first on the list
defense_waves_config =
   {
      {hp_scalar = 0.2, max_to_next = all_wave_max_to_next, skulk_nb = 5, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 0.5, sleep_time = 0, aliens =
          {
             { mapName = Gorge.kMapName, nb = 1 },
             --             { mapName = BoneWall.kMapName, nb = 0 },
             -- { mapName = Lerk.kMapName, nb = 2 },
          }
      },
      -- {hp_scalar = 0.3, max_to_next = all_wave_max_to_next, skulk_nb = 10, babbler_nb = 0, pressure = all_wave_pressure,
      --  dmg = 0.5, sleep_time = 0, aliens =
      --     {
      --        { mapName = Gorge.kMapName, nb = 1 },
      --        -- { mapName = Lerk.kMapName, nb = 2 },
      --     }
      -- },
      {hp_scalar = 0.4, max_to_next = all_wave_max_to_next, skulk_nb = 15, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 0.5, sleep_time = 0, aliens =
          {
             { mapName = Gorge.kMapName, nb = 3 },
             -- { mapName = BoneWall.kMapName, nb = 1 },
             -- { mapName = Lerk.kMapName, nb = 2 },
          }
      },
      {hp_scalar = 0.5, max_to_next = all_wave_max_to_next, skulk_nb = 20, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 0.7, sleep_time = 10, aliens =
          {
             { mapName = Gorge.kMapName, nb = 1 },
             { mapName = Lerk.kMapName, nb = 2 },
             { mapName = BoneWall.kMapName, nb = 1 },
          }
      },
      {hp_scalar = 0.7, max_to_next = all_wave_max_to_next, skulk_nb = 25, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 0.7, sleep_time = 20, aliens =
          {
             { mapName = Gorge.kMapName, nb = 1 },
          }
      },
      {hp_scalar = 0.9, max_to_next = all_wave_max_to_next, skulk_nb = 30, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 0.7, sleep_time = 20, aliens =
          {
             { mapName = Fade.kMapName, nb = 1 },
             { mapName = BoneWall.kMapName, nb = 1 },
          }
      },
      {hp_scalar = 1, max_to_next = all_wave_max_to_next, skulk_nb = 30, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 0.7, sleep_time = 20, aliens =
          {
             { mapName = Gorge.kMapName, nb = 1 },
          }
      },
      {hp_scalar = 1.1, max_to_next = all_wave_max_to_next, skulk_nb = 32, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 0.8, sleep_time = 20, aliens =
          {
             { mapName = Gorge.kMapName, nb = 1 },
          }
      },
      {hp_scalar = 2, max_to_next = 1000, skulk_nb = 0, babbler_nb = 0, pressure = 0,
       dmg = 1.3, sleep_time = 0, aliens =  {{ mapName = Onos.kMapName, nb = 1 }}},
      {hp_scalar = 1.1, max_to_next = all_wave_max_to_next, skulk_nb = 32, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 0.8, sleep_time = 20, aliens =
          {
             { mapName = Gorge.kMapName, nb = 1 },
             { mapName = BoneWall.kMapName, nb = 1 },
          }
      },
      {hp_scalar = 1.1, max_to_next = all_wave_max_to_next, skulk_nb = 32, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 0.8, sleep_time = 20, aliens =
          {
             { mapName = Gorge.kMapName, nb = 1 },
             { mapName = Fade.kMapName, nb = 1 },
          }
      },
      {hp_scalar = 1.1, max_to_next = all_wave_max_to_next, skulk_nb = 35, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 0.8, sleep_time = 20, aliens =
          {
             { mapName = Onos.kMapName, nb = 2 },
             { mapName = BoneWall.kMapName, nb = 1 },
          }
      },
      {hp_scalar = 2, max_to_next = 1000, skulk_nb = 0, babbler_nb = 0, pressure = 0,
       dmg = 1.3, sleep_time = 0, aliens =  {{ mapName = Onos.kMapName, nb = 1 }}},
      {hp_scalar = 1, max_to_next = all_wave_max_to_next, skulk_nb = 35, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 1, sleep_time = 20, aliens =
          {
             { mapName = Fade.kMapName, nb = 2 },
             -- { mapName = Lerk.kMapName, nb = 5 },
          }
      },
      {hp_scalar = 2, max_to_next = 1000, skulk_nb = 0, babbler_nb = 0, pressure = 0,
       dmg = 1.3, sleep_time = 0, aliens =  {{ mapName = Onos.kMapName, nb = 1 }}},
      {hp_scalar = 1, max_to_next = all_wave_max_to_next, skulk_nb = 40, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 1, sleep_time = 30, aliens =
          {
             { mapName = Fade.kMapName, nb = 1 },
             { mapName = Onos.kMapName, nb = 1 },
             { mapName = BoneWall.kMapName, nb = 1 },
             -- { mapName = Lerk.kMapName, nb = 5 },
          }
      },
      {hp_scalar = 2, max_to_next = 1000, skulk_nb = 0, babbler_nb = 0, pressure = 0,
       dmg = 1.4, sleep_time = 0, aliens =  {{ mapName = Onos.kMapName, nb = 1 }}},
      {hp_scalar = 1, max_to_next = all_wave_max_to_next, skulk_nb = 40, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 1.1, sleep_time = 35, aliens =
          {
             { mapName = Gorge.kMapName, nb = 1 },
             { mapName = Fade.kMapName, nb = 1 },
             -- { mapName = Lerk.kMapName, nb = 7 },
          }
      },
      {hp_scalar = 2, max_to_next = 1000, skulk_nb = 0, babbler_nb = 0, pressure = 0,
       dmg = 1.3, sleep_time = 0, aliens =  {{ mapName = Onos.kMapName, nb = 1 }}},
      {hp_scalar = 1.2, max_to_next = all_wave_max_to_next, skulk_nb = 40, babbler_nb = 0, pressure = 0,
       dmg = 1.20, sleep_time = 40, aliens =
          {
             { mapName = Fade.kMapName, nb = 2 },
          }
      },
      {hp_scalar = 2.4, max_to_next = 10000, skulk_nb = 0, babbler_nb = 0, pressure = 0,
       extra_msg = "BOSS 1/3: Onoses", dmg = 1.3, sleep_time = 0, aliens =
          {
             { mapName = Onos.kMapName, nb = 7 },
          }
      },
      {hp_scalar = 1.2, max_to_next = all_wave_max_to_next, skulk_nb = 20, babbler_nb = 0, pressure = 0,
       dmg = 1.20, sleep_time = 0, aliens =
          {
             { mapName = Gorge.kMapName, nb = 3 },
          }
      },
      {hp_scalar = 2.2, max_to_next = all_wave_max_to_next, skulk_nb = 6, babbler_nb = 0, pressure = 0,
       extra_msg = "BOSS 2/3: Lerks", dmg = 1.3, sleep_time = 0, aliens =
          {
             { mapName = Lerk.kMapName, nb = 20 },
             { mapName = Fade.kMapName, nb = 1 },
          }
      },
      {hp_scalar = 2.2, max_to_next = all_wave_max_to_next, skulk_nb = 40, babbler_nb = 0, pressure = all_wave_pressure,
       extra_msg = "BOSS 3/3: Last wave", dmg = 2, sleep_time = 5, aliens =
          {
             { mapName = Onos.kMapName, nb = 1 },
          }
      },
      {hp_scalar = 2.2, max_to_next = all_wave_max_to_next, skulk_nb = 50, babbler_nb = 0, pressure = all_wave_pressure,
       dmg = 2, sleep_time = 5, aliens =
          {
             { mapName = Onos.kMapName, nb = 1 },
          }
      },
   }


function waveTriggerPressure()
   for _, i in ipairs(defense_waves_config) do
      i.pressure = i.pressure == 2 and 0 or 2
   end
end

local force_next = false
function forceNextWave(player)
   force_next = true
end
function forceNextSquad(player)
   force_next = true
end

function resetWaves()
   init = true
   wave_nb = 0
   wave_hp_scalar = 1
   all_waves_finished = false
end

function getWaveNb()
   return wave_nb
end
function isTraderWave()
   return false
end

function areAllWavesFinished()
   -- if (wave_nb == #defense_waves_config) then
   --    return true
   -- end
   -- return false
   return all_waves_finished
end


function respawnAlienPlayers()
   local count = 0
   for _, p in ipairs(GetGamerules():GetTeam2():GetPlayers()) do
      if (p and (not p:GetIsAlive()) and p.client) then
         local rt = getRandomRT()
         local spawnPoint = GetLocationAroundFor(rt:GetOrigin(), kTechId.Onos, 10)
         local orig = rt:GetOrigin()
         if (spawnPoint) then
            orig = spawnPoint
         end
         local ent = p:Replace(Skulk.kMapName, 2, false, orig)
         -- ent:SetCameraDistance(0)
         count = count + 1
      end
   end
   return (count)
end

function getAlienHealthScalar()
   if (wave_nb == 0) then
      return 1
   end
   return getScalar(defense_waves_config[wave_nb].hp_scalar, 10)
end

function getAlienDamageScalar()
   if (wave_nb == 0) then
      return 1
   end
   return getScalar(defense_waves_config[wave_nb].dmg, 10)
end

function getMaxNbSkulk()
   if (wave_nb == 0) then
      return 1
   end
   local nb_marines_alive = 0
   for _, marine in pairs(GetGamerules():GetTeam1():GetPlayers()) do
      if (marine ~= nil and (marine:isa("Player"))) then
         nb_marines_alive = nb_marines_alive + 1
      end
   end

   local scalar = Clamp(nb_marines_alive / 8, 0.3, 1)
   local max_skulk_nb = defense_waves_config[wave_nb].skulk_nb * scalar
   return (math.floor(max_skulk_nb))
end

function spawnMissingSkulk()
   if (wave_nb > 0)
   then
      respawnAlienPlayers()

      local nb_skulk = 0
      local max_skulk_nb = getMaxNbSkulk()
      for _, ent_name in ipairs({"Skulk", "Babbler", "Lerk"}) do
         for _, entity in ipairs(GetEntitiesForTeam(ent_name, 2)) do
            if (entity and entity:GetIsAlive() and not entity.client) then
               nb_skulk = nb_skulk + 1
            end
         end
      end

      local all_high_lifeform_nb = 0
      for _, ent_name in ipairs({"Gorge", "Fade", "Onos"}) do
         for _, entity in ipairs(GetEntitiesForTeam(ent_name, 2)) do
            if (entity and entity:GetIsAlive() and not entity.client) then
               all_high_lifeform_nb = all_high_lifeform_nb + 1
            end
         end
      end

-- TODO: u =n truc qui fait un champ de C4
-- Weldable door
-- Retirer le FF sur la porte
      -- Mines: Insta kill (comme l'arc)
      -- Cycle jour nuit
      if (nb_skulk == 0 or (defense_waves_config[wave_nb].pressure == 2 and all_high_lifeform_nb > 0))
      then
         if (nb_skulk == 0 or nb_skulk + (max_skulk_nb / 4) < max_skulk_nb) then
         -- for i =  do
            -- if (math.random(1, 100) < 8) then
            --    spawnAlienCreature(Lerk.kMapName)
            -- else
               --spawnAlienCreature(Babbler.kMapName)
            local cont = spawnAlienCreature(Contamination.kMapName)
            if (cont) then
               cont.maxSpawn = math.min(max_skulk_nb - nb_skulk, 5)
            end
            -- end
         end
      end

      -- local nb_babbler = #GetEntitiesForTeam("Babbler", 2)
      -- local max_babbler_nb = defense_waves_config[wave_nb].babbler_nb
      -- local marines = GetEntitiesForTeam("Player", 1)
      -- for i = nb_babbler, max_babbler_nb do
      --    local bab = spawnAlienCreature(Babbler.kMapName)
      --    if (bab and #marines > 0) then
      --       local m = marines[math.random(1, #marines)]
      --       bab.defenseTargetId = m:GetId()
      --       bab:SetMoveType(kBabblerMoveType.Attack, m, m:GetOrigin())
      --    end
      -- end
   end
end

local wave_check_delay = 0
local sleep_time = 0
function handleWaves(timePassed)
   if (not GetGamerules():GetGameStarted()) then
      return
   end

   if (init == false) then
      resetWaves()
   end

   wave_check_delay = wave_check_delay + timePassed
   -- for _, entity in ipairs(GetEntitiesForTeam("Babbler", 2)) do
   --    if (entity and entity:GetIsAlive() and not entity.client) then
   --       defenseBabblerUpdate(entity, timePassed)
   --    end
   -- end
   if (wave_check_delay > 3) then
      wave_check_delay = 0

      local nb_alien = 0
      local all_bot = {}
      local pressure = wave_nb > 0 and defense_waves_config[wave_nb].pressure or 0
      -- if (pressure == 2) then
      --    all_bot = {
      --       GetEntitiesForTeam("Gorge", 2),
      --       GetEntitiesForTeam("Fade", 2),
      --       GetEntitiesForTeam("Onos", 2)
      --    }
      -- else

      if (wave_nb == #defense_waves_config or pressure == 0) then
         all_bot = {
            GetEntitiesForTeam("Skulk", 2),
            GetEntitiesForTeam("Babbler", 2),
            GetEntitiesForTeam("Lerk", 2),
            GetEntitiesForTeam("Gorge", 2),
            GetEntitiesForTeam("Fade", 2),
            GetEntitiesForTeam("Onos", 2)
         }
      else
         all_bot = {
            -- GetEntitiesForTeam("Skulk", 2),
            -- GetEntitiesForTeam("Babbler", 2),
            -- GetEntitiesForTeam("Lerk", 2),
            GetEntitiesForTeam("Gorge", 2),
            GetEntitiesForTeam("Fade", 2),
            GetEntitiesForTeam("Onos", 2)
         }
      end

      -- Do not count players
      for _, aliens in ipairs(all_bot) do
         for _, alien in ipairs(aliens) do
            if (alien and alien:GetIsAlive() and not alien.client) then
               nb_alien = nb_alien + 1
            end
         end
      end

         -- + #GetEntitiesForTeam("Skulk", 2)
         -- + #GetEntitiesForTeam("Lerk", 2)
      if (wave_nb == 0 or force_next == true
             or (defense_waves_config[wave_nb].max_to_next >= nb_alien))
      then
         -- Shared:FadedMessage("Next wave in " .. math.floor(sleep_time) .. "s")
         -- sleep_time = sleep_time - 1
         -- if (sleep_time > 0) then
         --    return
         -- end

         force_next = false
         if (wave_nb == #defense_waves_config) then
            all_waves_finished = true
         end

         wave_nb = Clamp(wave_nb + 1, 1, #defense_waves_config)
         wave = defense_waves_config[wave_nb]

         local total_alien = getMaxNbSkulk()--defense_waves_config[wave_nb].skulk_nb

         for _, alien_conf in ipairs(wave.aliens) do
            for i = 1, alien_conf.nb do
               spawnAlienCreature(alien_conf.mapName)
               total_alien = total_alien + 1
            end
         end
         if (defense_waves_config[wave_nb].extra_msg ~= nil) then
            Shared:FadedMessage("Wave: n'" .. wave_nb .. "/" .. #defense_waves_config
                                   .. " -> "
                                   .. defense_waves_config[wave_nb].extra_msg)
         else
            Shared:FadedMessage("Wave: n'" .. wave_nb .. "/" .. #defense_waves_config
                                   .. " (Aliens = " .. total_alien .. ")")
         end
         spawnMissingSkulk()
      end
      if (pressure > 0) then
         spawnMissingSkulk()
      end
   end
end

------------------


-- local function OnMouseDown(_, button, doubleClick)

--     local isCommander, player = GetLocalPlayerIsACommander()
--     if not isCommander or CommanderUI_GetMouseIsOverUI() then
--         return
--     end

--     local mousePos = MouseTracker_GetCursorPos()

--     mousePressed[button] = true
--     mouseButtonDownAtPoint[button] = Vector(mousePos.x, mousePos.y, 0)

--     local evalButtonDown = kMouseActions.ButtonDown[button]
--     if evalButtonDown then
--         evalButtonDown(player, mousePos.x, mousePos.y)
--     end

--     -- Evaluate if there are any world actions right now for this mouse input.
--     if doubleClick then

--         local evalDoubleClick = kMouseActions.DoubleClick[button]
--         if evalDoubleClick then
--             evalDoubleClick(player, mousePos.x, mousePos.y)
--         end

--     end


--     -- If there are no world actions, forward along to the Commander UI code.

-- end

-- local function OnMouseUp(_, button)

--     local isCommander, player = GetLocalPlayerIsACommander()
--     if not isCommander then
--         return
--     end

--     local mousePos = MouseTracker_GetCursorPos()

--     mousePressed[button] = false

--     -- Evaluate if there are any world actions right now for this mouse input.
--     local evalButtonUp = kMouseActions.ButtonUp[button]
--     if evalButtonUp then
--         evalButtonUp(player, mousePos.x, mousePos.y)
--     end

--     -- If there are no world actions, forward along to the Commander UI code.

-- end

-- local listener = { OnMouseDown = OnMouseDown, OnMouseUp = OnMouseUp}
-- MouseTracker_ListenToButtons(listener)

-- if (Client) then
--    function Commander:ClientOnMouseRelease(mouseButton, x, y)

--       local displayConfirmationEffect = false

--       local normalizedPickRay = CreatePickRay(self, x, y)
--       if mouseButton == 1 then

--          if self.currentTechId ~= kTechId.None then
--             self:SetCurrentTech(kTechId.None)
--             Shared:FadedMessage("Click simple")
--             local trace = GetCommanderPickTarget(self, normalizedPickRay, false, false)
--          else

--             local trace = GetCommanderPickTarget(self, normalizedPickRay, false, false)
--             self:SendTargetedAction(kTechId.Default, normalizedPickRay, nil, trace.entity)
--             displayConfirmationEffect = true

--          end

--       end

--       if displayConfirmationEffect then

--          local trace = GetCommanderPickTarget(self, normalizedPickRay)
--          self:TriggerEffects("issue_order", { effecthostcoords = Coords.GetTranslation(trace.endPoint) } )

--       end

--    end
-- end
