-- ===================== Faded Mod =====================
--
-- lua\FadedNetworkMessages_Server.lua
--
--    Created by: Rio (rio@myrio.de)
--    Update by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

-- Script.Load("lua/TechData.lua")

local floor = math.floor
local locale = LibCache:GetLibrary("LibLocales-1.0")

local function OnMessageSelectEquipment(client, message)
   -- Print("Faded OnMessageSelectEquipment()")
   local player = client:GetControllingPlayer()

   local gameTime = GetGamerules():GetGameStartTime()
   if gameTime ~= 0 then
      gameTime = floor(Shared.GetTime()) - gameTime
   end

   if (getWaveNb() > 1) then
      player:FadedMessage("Your weapon selection has been saved for the next round.")
      return
   end

   -- player.buy_weap_done = true


   -- Denied weapon equipement if the marine is dead
   if (player and player:GetIsAlive()) then
      local equipment = message.Equipment or kTechId.Welder
      local equipmentMapName = LookupTechData(equipment, kTechDataMapName)

      player:DestroyWeapons()
      if (player.RemoveJetpack) then
         player = player:RemoveJetpack()
      end

      player:GiveItem(Axe.kMapName)
      player:GiveItem(Rifle.kMapName)
      --player:GiveItem(Pistol.kMapName)
      -- player:GiveItem(Shotgun.kMapName)
      player:GiveItem(Builder.kMapName)
      player:GiveItem(LaySentryBattery.kMapName)

      local allSentry = Shared.GetEntitiesWithClassname("Sentry")
      for _, s in ientitylist(allSentry) do
         if s and s:isa("Sentry") and s:GetOwner() and s:GetOwner():GetId() == player:GetId()
         then
            s:Kill()
         end
      end


      -- player:GiveItem(StabBlink.kMapName)
      -- player:GiveItem(MACVeteran.kMapName)

      -- Give the marine the selected weapon and equipment.
      player:ClearHunterAbility()
      player:ClearMedicAbility()
      if equipment == kTechId.Jetpack then
         player:GiveJetpack()
      elseif (equipment == kTechId.Scan) then -- hunter
         player:PromoteToHunter()
      elseif (equipment == kTechId.MedPack) then -- healer
         player:PromoteToMedic()
      else
         if player:GiveItem(equipmentMapName) then
            Shared.PlayWorldSound(nil, Marine.kGunPickupSound, nil, player:GetOrigin())
         end
      end

      -- If the player gets a jetpack, we need to get the new player entitiy
      player = client:GetControllingPlayer()

      local weapon = message.Weapon or kTechId.Rifle
      local weaponMapName = LookupTechData(weapon, kTechDataMapName)

      if (equipment == kTechId.MedPack) then -- healer
         --player:RemoveWeapon(Shotgun.kMapName)
         -- player:GiveItem(Pistol.kMapName)
         player:GiveItem(Shotgun.kMapName)
         player:GiveItem(GrenadeLauncher.kMapName)
      elseif (weaponMapName == HeavyShotgun.kMapName) then -- HS
         weaponMapName = Shotgun.kMapName
         if player:GiveItem(weaponMapName) then
            Shared.PlayWorldSound(nil, Marine.kGunPickupSound, nil, player:GetOrigin())
            local hsg = player:GetWeaponInHUDSlot(1)
            convertToHeavyShotgun(hsg)
            player.kFadedConvertToHeavyShotgun = true
         end
      else
         if (weaponMapName ~= Shotgun.kMapName) then
            player:GiveItem(Shotgun.kMapName)
         end
         player.kFadedConvertToHeavyShotgun = false
         if player:GiveItem(weaponMapName) then
            Shared.PlayWorldSound(nil, Marine.kGunPickupSound, nil, player:GetOrigin())
         end
      end

   end
   -- Print("Faded OnMessageSelectEquipment() END")
end

function Marine:RemoveJetpack()
   if (not self:isa("JetpackMarine")) then return self end

   local activeWeapon = self:GetActiveWeapon()
   local activeWeaponMapName = nil
   local health = self:GetHealth()

   if activeWeapon ~= nil then
      activeWeaponMapName = activeWeapon:GetMapName()
   end

   local marine = self:Replace(Marine.kMapName, self:GetTeamNumber(), true, Vector(self:GetOrigin()))

   marine:SetActiveWeapon(activeWeaponMapName)
   marine:SetHealth(health)
   return marine
end

-- Effect of the eating process Server side
local function OnMessageTriggerEating(client, message)
   local player = client:GetControllingPlayer()
   local used_corpse = nil
   local dead_marine = GetEntities("Ragdoll")
   -- Destroy the corpse used
   for _, corpse in ipairs(dead_marine) do
      if (corpse:GetPlayerName() == message.corpse_name) then
         used_corpse = corpse
         break
      end
   end
   if (used_corpse and player and player:isa("Fade")) then
      local sound = "sound/NS2.fev/alien/structures/death_large"
      -- player:DevourMarineCorpse()
      player:ResetEatingFraction()
      player:FadedMessage(string.format(locale:ResolveString("FADED_EAT_CORPSE"), tostring(used_corpse:GetPlayerName())))
      player:AddHealth(kFadedEatingRegen, nil, false, nil, nil)
      player:SetArmor(player:GetArmor() + kFadedEatingRegenArmor)
      used_corpse:TimeUp()
      Shared.PlayPrivateSound(player,sound, nil, 1.0, player:GetOrigin())
   end
end

-- Trigger transformation (Fade --> marine / Marine --> Fade)
local function OnMessageTriggerTransform(client, message)
   local player = client:GetControllingPlayer()
   if (player) then
      player.kFadedTransformation = true
      player.kFadedCorpseUsedName = message.corpse_name
   end
end

local function OnMessageTriggerReviveFaded(client, message)
   local player = client:GetControllingPlayer()
   local used_corpse = nil
   local dead_faded = GetEntities("Ragdoll")
   -- Destroy the corpse used
   for _, corpse in ipairs(dead_faded) do
      if (corpse:GetPlayerName() == message.corpse_name) then
         used_corpse = corpse
         break
      end
   end

   if (used_corpse and player and player:isa("Fade")) then
      local sound = "sound/NS2.fev/alien/fade/spawn"
      local player_to_revive = Shared:GetPlayerByName(used_corpse:GetPlayerName())
      if (player_to_revive) then
         player:ResetEatingFraction()

         local success = nil
         local new_faded = nil
         if (player_to_revive.Reset) then
            player_to_revive:Reset()
            if player_to_revive.OnReset then
               player_to_revive:OnReset()
            end
         end
         -- end
         -- success, new_faded = player:GetTeam():ReplaceRespawnPlayer(player_to_revive, player:GetOrigin())
         -- if (success) then
         new_faded = player_to_revive:Replace(Skulk.kMapName, player:GetTeamNumber(), false, player:GetOrigin()) -- Make it alive again
         -- end
         if (new_faded) then
            kFadedSpawnFadeHallucination = false
            new_faded = new_faded:Replace(Fade.kMapName, player:GetTeamNumber(), false, player:GetOrigin()) -- Fix on respawn bug (swipe missing)
            kFadedSpawnFadeHallucination = true
            player:AddScore(kFadedReviveScore)
         end

         if (new_faded) then
            new_faded:SetActiveWeapon(SwipeBlink.kMapName)
            SpawnSinglePlayer(new_faded, player:GetOrigin()) -- Spawn near the other faded
            new_faded:SetHealth(kFadedRespawnedFadeHealth)
            new_faded:SetEnergy(kFadedRespawnedFadeEnergy)
            new_faded:SetArmor(0)
            player:SetHealth(player:GetHealth() - kFadedRespawnedFadeHealthCost)
            used_corpse:TimeUp()
            Shared.PlayPrivateSound(player, sound, nil, 1.0, player:GetOrigin())
         end
      end
   end
end

-- Remove a corpse burn client side
local function OnMessageTriggerBurnCorpse(client, message)
   local dead_marine = GetEntities("Ragdoll")
   -- Destroy the corpse used
   for _, corpse in ipairs(dead_marine) do
      if (corpse:GetPlayerName() == message.corpse_name) then
         corpse:TimeUp()
         break
      end
   end
end

local function clearOrder(player)
   if (Server and player:isa("Player")) then
      player:ClearOrders()
   end
end

local function OnMessageTriggerHunting(client, message)
   local player = client:GetControllingPlayer()
   local entity = {}

   if (player and player:GetIsAlive() and player:isa("Marine")) then
      for _, team in ipairs({GetGamerules():GetTeam2():GetPlayers(), GetGamerules():GetTeam1():GetPlayers()}) do
         for _, alien in pairs(team) do
            -- Fake marines are not detected with the hunter
            if (alien and alien:GetIsAlive()-- and alien:isa("Fade")
                and alien.forPlayer == nil and player ~= alien) then -- Do not trace hallucination
               if (#entity > 0
                      and alien:GetOrigin():GetDistance(player:GetOrigin()) <
                   entity[1]:GetOrigin():GetDistance(player:GetOrigin())) then
                  table.insert(entity, 1, alien)
               else
                  table.insert(entity, alien)
               end
            end
         end
      end
      local nearest_faded = entity[1]

      -- local entId = FindNearestEntityId("Alien", player:GetOrigin())
      -- local nearest_faded = Shared.GetEntity(entId)
      if (nearest_faded ~= nil) then
         Server.SendNetworkMessage(player, "OnFinishHunting",
                                   {
                                      location
                                         = nearest_faded:GetOrigin() + Vector(0, 0.7, 0)
                                   }, true)
      end
   end
end

-- Send a message to the client with the local key binding
local function OnChatCallBack(client, msg)
   local player = client:GetControllingPlayer()

   if (player) then
      if (msg.local_key ~= nil) then
         player:FadedMessage(
            string.format(locale:ResolveString(msg.msg), msg.local_key))
      else
         player:FadedMessage(msg.msg)
      end
   end
end

local function impulseRagdoll()
end

local function OnMessageTriggerDropRagdoll(client, msg)
   local ragdoll = nil
   local player = client:GetControllingPlayer()

   if (player and player:isa("Fade") and player:GetIsAlive()
       and msg and player:GetWeaponInHUDSlot(1)) then
      local nb, ragdoll = SpecialWeaponHandling(player:GetWeaponInHUDSlot(1), player, _CreateRagdoll, 4)
      if (nb == 0 or ragdoll == nil) then
         ragdoll = CreateRagdoll(player)
      end
      ragdoll:SetModel(msg.modelName, msg.graphName)
      ragdoll.marineRagdoll = msg.marineRagdoll
      ragdoll.fadedRagdoll = msg.fadedRagdoll
      ragdoll.playerName = msg.playerName
   end
end

local function OnMessageTriggerTakeRagdoll(client, msg)
   local ragdoll = nil
   local battery = nil
   local player = client:GetControllingPlayer()

   if (player and msg) then
      local ragdoll = Shared.GetEntity(msg.ragdoll_id)
      if (msg.battery_id) then
         battery = Shared.GetEntity(msg.battery_id)
      end
      if (ragdoll) then DestroyEntity(ragdoll)  end
      if (battery) then DestroyEntity(battery)  end
   end
end

local function OnMessageTriggerImpulseRagdoll(client, msg)
   local player = client:GetControllingPlayer()

   if (player) then
      local dead_marine = GetEntities("Ragdoll")
      -- Destroy the corpse used
      for _, corpse in ipairs(dead_marine) do
         if (corpse:GetPlayerName() == msg.name) then
            -- Shared:FadedMessage("Old Ragdoll coord server side at "
            --             .. "x: " .. corpse:GetOrigin().x
            --             .. "y: " .. corpse:GetOrigin().y
            --             .. "z: " .. corpse:GetOrigin().z
            -- )
            corpse:SetCoords(player:GetCoords())
            corpse:SetOrigin(player:GetOrigin())
            corpse:SetVelocity(msg.impulse)
            local new_ragdoll = CreateRagdoll(corpse)
            if (new_ragdoll) then
               DestroyEntity(corpse)
            end
            -- corpse:GetPhysicsModel():AddImpulse(msg.impulse_start_coord, msg.impulse * 10)
            break
         end
      end
      -- for _, marine in pairs(GetGamerules():GetTeam1():GetPlayers()) do
      --      if (marine) then
      --         Server.SendNetworkMessage(marine, "ImpulseRagdoll",
      --                       {
      --                      ragdoll_coord = msg.ragdoll_coord,
      --                      impulse_start_coord = msg.impulse_start_coord,
      --                      impulse = msg.impulse
      --                       }, true)
      --      end
      -- end
   end
end

local function OnMessageTriggerShadowStep(client, msg)
   local player = client:GetControllingPlayer()

   if (player and player:isa("Fade") and player:GetIsAlive()) then
      player:FadedTriggerShadowStep(msg.direction)
   end
end


-- Hack to make custom weapons buyable
local defense_techs = {
   kTechId.NapalmGrenade,
   kTechId.HealGrenade,
   kTechId.LaySentryBattery,
   kTechId.LayFlameMines,
   kTechId.Scan,
}
local defense_techs_mapname = {
   NapalmGrenadeThrower.kMapName,
   HealGrenadeThrower.kMapName,
   LaySentryBattery.kMapName,
   LayFlameMines.kMapName,
}

-- local _macAssoc = {}

-- local MACOnUpdate = MAC.OnUpdate
-- function MAC:OnUpdate(deltaTime)
--    if _macAssoc[self:GetId()] then
--       Shared:FadedMessage("a")
--       local m = Shared:GetPlayerByName(_macAssoc[self:GetId()])
--       -- local m = Shared.GetEntity(self.defenseMarineId)
--       if (m and m:GetIsAlive()) then
--          Shared:FadedMessage("b")
--          local o = kTechId.Defend
--          if (self:GetOrigin():GetDistanceTo(m:GetOrigin()) > 5) then
--             o = kTechId.Move
--          end
--          self:GiveOrder(o, m:GetId(), m:GetOrigin(), nil, false, false)
--       else
--          _macAssoc[self:GetId()] = nil
--       end
--    end
--    MACOnUpdate(self, deltaTime)
-- end

local function OnMessageBuy(client, buyMessage)
   local player = client:GetControllingPlayer()

   if player and player:GetIsAllowedToBuy() then

      local cost = 0
      local validBuyIds = {}
      local TechIds = ParseBuyMessage(buyMessage)

      for i, TechId in ipairs(TechIds) do
         if ((TechId == kTechId.LayMines or TechId == kTechId.LayFlameMines)
                and player:GetWeapon(LaySentryBattery.kMapName)
                and GetCostForTech(kTechId.LayMines) <= player:GetResources())
         then
            player:FadedMessage("Slot 4 already used by the sentry")
            return
         end

         if (TechId == kTechId.DualMinigunExosuit or TechId == kTechId.DualRailgunExosuit)
         then
            local nb_marines = GetGamerules():GetTeam1():GetNumPlayers()
            local nb_exos = #GetEntitiesForTeam("Exo", 1) + #GetEntitiesForTeam("Exosuit", 1)

            if (nb_exos >= (nb_marines / 5)) then
               player:FadedMessage("Exos limit reached (one for '5' players)")
               return
            end
         end
      end

      if (player:ProcessBuyAction(TechIds) ~= true)
      then
         if (#TechIds == 1) then
            -- cost = GetCostForTech(t)
            -- if (TechIds[1] == kTechId.MAC and cost <= player:GetResources()) then
            -- local spawnPoint = GetRandomBuildPosition(kTechId.MAC,
            --                                           player:GetOrigin(),
            --                                           15)
            -- if spawnPoint then
            --    entity = CreateEntity(MAC.kMapName, spawnPoint, player:GetTeamNumber())
            --    if (entity) then
            --       _macAssoc[entity:GetId()] = player:GetName()
            --       -- entity:GiveOrder(kTechId.Defend, player:GetId(),
            --       --                  player:GetOrigin(), nil, false, false)
            --       -- entity.defenseMarineName = player:GetName()
            --       player:AddResources(-cost)
            --    end
            -- end

            -- else
            for i, t in ipairs(defense_techs)
            do
               cost = GetCostForTech(t)
               if (TechIds[1] == t and cost <= player:GetResources()) then
                  player:AddResources(-cost)
                  player:GiveItem(defense_techs_mapname[i])
               end
            end
            -- end
         end
      end
   end
end

Server.HookNetworkMessage("Buy", OnMessageBuy)
Server.HookNetworkMessage("SelectEquipment", OnMessageSelectEquipment)
Server.HookNetworkMessage("OnChatCallBack", OnChatCallBack)
-- Server.HookNetworkMessage("IsFadeEating", OnSetFadedEating)
Server.HookNetworkMessage("OnTriggerEating", OnMessageTriggerEating)
Server.HookNetworkMessage("OnTriggerTransform", OnMessageTriggerTransform)
Server.HookNetworkMessage("OnTriggerReviveFaded", OnMessageTriggerReviveFaded)
Server.HookNetworkMessage("OnTriggerBurnCorpse", OnMessageTriggerBurnCorpse)

Server.HookNetworkMessage("OnFinishHunting", OnMessageTriggerHunting)

Server.HookNetworkMessage("DropRagdoll", OnMessageTriggerDropRagdoll)
Server.HookNetworkMessage("TakeRagdoll", OnMessageTriggerTakeRagdoll)
Server.HookNetworkMessage("ImpulseRagdoll", OnMessageTriggerImpulseRagdoll)

Server.HookNetworkMessage("TriggerShadowStep", OnMessageTriggerShadowStep)
