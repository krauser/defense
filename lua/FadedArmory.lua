-- ===================== Faded Mod =====================
--
-- lua\FadedArmsLab.lua
--
--    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
--
-- ===================== Faded Mod =====================

local locale = LibCache:GetLibrary("LibLocales-1.0")
local strformat = string.format

Script.Load("lua/Armory.lua")

local clientArmoryId = nil

function setTraderArmoryId(id)
   clientArmoryId = id
end
function getTraderArmoryId()
   return clientArmoryId
end

local function canUseArmory(self, player)
   return (getTraderArmoryId() == self:GetId() and player and player:GetTeamNumber() == 1)
end

function Armory:GetItemList(forPlayer)
   local itemList = {
      kTechId.Welder,
      kTechId.LayMines,
      kTechId.LayFlameMines,
      -- kTechId.Rifle,
      kTechId.HeavyMachineGun,
      kTechId.GrenadeLauncher,
      kTechId.Flamethrower,
      kTechId.HealGrenade,
      kTechId.ClusterGrenade,
      kTechId.NapalmGrenade
      -- kTechId.Pistol,
      -- kTechId.Shotgun,
   }

   return itemList
   -- if (Shared.GetMapName() == "ns2_def_troopers") then
   --    return itemList
   -- end
   -- if (canUseArmory(self, forPlayer)) then
   --    return itemList
   -- end
   -- return ({kTechId.Pistol})
end

if (Server) then
   local armoryResupplyPlayer = Armory.ResupplyPlayer
   function Armory:ResupplyPlayer(player)
      armoryResupplyPlayer(self, player)
      -- if (Shared.GetMapName() == "ns2_def_troopers") then
      --    armoryResupplyPlayer(self, player)
      -- else
      --    if (canUseArmory(self, player)) then
      --       armoryResupplyPlayer(self, player)
      --    end
      -- end
   end
end

-- function Armory:GetCanBeUsed(player, useSuccessTable)
--    useSuccessTable.useSuccess = true
-- end

------------------------

-- This make the building bug (can't buy anything no matter what)
-- function Armory:GetRequiresPower()
--    return false
-- end

-- function InfantryPortal:GetRequiresPower()
--    return false
-- end
-- function Observatory:GetRequiresPower()
--    return false
-- end

-- function ArmsLab:GetRequiresPower()
--    return false
-- end
