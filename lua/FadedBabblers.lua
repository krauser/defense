-- ===================== Faded Mod =====================
--
-- lua\FadedBabblers.lua
--
--    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

-- local networkVars =
--    {
--       forPlayer = "integer",
--    }


if (Server) then
   function Babbler:TimeUp()

      -- if (not self:GetOwner())
      -- then
      self:TriggerEffects("death", {effecthostcoords = Coords.GetTranslation(self:GetOrigin()) })
      DestroyEntity(self)
      -- end

   end
end

local onCreate = Babbler.OnCreate
function Babbler:OnCreate()
   onCreate(self)
   -- self.forPlayer = nil
   -- if (kFadedHallucinationMarineId) then
   --    self.forPlayer = kFadedHallucinationMarineId
   --    self:SetPropagate(Entity.Propagate_Callback)
   -- end
   if (Server) then
      -- local marines_building = GetEntitiesWithMixinWithinRange(
      --    "PowerConsumer", self:GetOrigin(), 15)
      -- if (#marines_building > 0) then
      -- self:AddTimedCallback(Babbler.TimeUp, kBabblersLifetime)
      -- else -- Babblers only stay 20s in the main
      self:AddTimedCallback(Babbler.TimeUp, kBabblersLifetime)
      -- end
   end
   -- InitMixin(self, PlayerHallucinationMixin)
end





-- if (Server) then
--    local babblerMoveRandom = Babbler.MoveRandom
--    function Babbler:MoveRandom()
--       if (self.defenseTargetId) then
--          local marine = Shared.GetEntity(self.defenseTargetId)
--          if (marine) then
--             -- local points = PointArray()
--             -- Pathing.GetPathPoints(self:GetOrigin(), marine:GetOrigin(), points)
--             if (self:GetOrigin():GetDistanceTo(marine:GetOrigin()) > 10) then
--                self:SetMoveType(kBabblerMoveType.Move, marine, marine:GetOrigin())
--             else
--                if (math.random(1, 100) < 5) then
--                   self:JumpRandom()
--                end
--                self:SetMoveType(kBabblerMoveType.Attack, marine, marine:GetOrigin())
--             end
--             return self:GetIsAlive()
--          else
--             self:Kill()
--             return false
--          end
--       end
--       return true -- babblerMoveRandom(self)
--    end
-- end

-- if (Client) then
--    function HiveVisionMixin:OnUpdate(deltaTime)
--       local player = Client.GetLocalPlayer()
--       local model = self:GetRenderModel()

--       if model ~= nil then
--          if (((player:isa("Alien") and player:GetDarkVisionEnabled())
--                  or player:isa("Marine") and player:GetTeamNumber() == 2)
--              and self:isa("Marine")) then -- Only parasite marine, not mines
--             HiveVision_AddModel(model)
--          else
--             HiveVision_RemoveModel(model)
--          end
--       end
--    end
-- end





Script.Load("lua/MapBlipMixin.lua")

-- function defenseBabblerUpdate(self, deltaTime)

--    if (Server) then
--       self:MarkBlipDirty()
--       if (self.optiLastCheck == nil or self.optiLastCheck + 1 < Shared.GetTime()) then
--          -- AlienUpdateOrderCallback(self)
--          self.optiLastCheck = Shared.GetTime()
--          local marines = GetEntitiesForTeamWithinRange("Player", 1, self:GetOrigin(), kFadedOptiToLifeformDist)
--          -- local t = sortByProximity(self, marines)
--          if (#marines > 0) then
--             local m = nil

--             Shared.SortEntitiesByDistance(self:GetOrigin(), marines)
--             m = marines[1]
--          end
--       end
--    end
-- end

-- local babblerOnUpdate = Babbler.OnUpdate
-- function Babbler:OnUpdate(deltaTime)
--    babblerOnUpdate(self, deltaTime)

--    if (babblerOnUpdate) then
--       babblerOnUpdate(self, deltaTime)
--    end

--    local target = nil
--    if (self.defenseTargetId) then
--       defenseBabblerUpdate(self, deltaTime)
--    end

--    -- local isSelected = (self.GetIsSelected and (self:GetIsSelected( kAlienTeamType )))
--    -- if (isSelected or (target and target:GetOrigin():GetDistanceTo(self:GetOrigin()) < 4)) then
--    --    changeForm(self, false)
--    -- else
--    --    defenseBabblerUpdate(self, deltaTime)
--    -- end
--    -- -- if (not Server) then
--    -- -- end
-- end

if (Server) then

   local function UpdateCling(self, deltaTime, target)

      local success = false

      if target and not target:isa("AlienSpectator") and target.GetFreeBabblerAttachPointOrigin then

         local attachPointOrigin = target:GetFreeBabblerAttachPointOrigin()
         if attachPointOrigin then

            -- travelDistance = distance

            success = target:AttachBabbler(self)
            if (success == true) then
               self.clinged = true

               -- disable physic simulation
               self:SetGroundMoveType(true)
               self:SetOwner(target)
            end
         end

      end

      if not success then
         self:Detach()
      end
      return success
   end

   local function _attachBabbler(babbler, entity)

      -- if babbler:GetOwner() == self:GetOwner() then
      -- Adjust babblers move type.
      local moveType = kBabblerMoveType.Move
      local position = entity:GetOrigin()
      local giveOrder = true

      moveType = kBabblerMoveType.Cling
      position = HasMixin(entity, "Target") and entity:GetEngagementPoint() or entity:GetOrigin()
      if giveOrder then
         if babbler:GetIsClinged() then
            babbler:Detach()
         end
         babbler:SetMoveType(moveType, entity, position, true)
      end
      -- end
   end

   local babblerOnUpdate = Babbler.OnUpdate
   function Babbler:OnUpdate(deltaTime)
      babblerOnUpdate(self, deltaTime)
      if (Server and self.def_babbler_schield and self:GetCreationTime() + 6 < Shared.GetTime()) then
         if (not self:GetIsClinged()) then
            if (self.def_check_delay == nil or self.def_check_delay + 0.5 < Shared.GetTime()) then
               self.def_check_delay = Shared.GetTime()

               local target = nil
               local aliens = GetEntitiesForTeamWithinRange("Alien", 2, self:GetOrigin(), 3)
               for _, a in ipairs(aliens) do
                  if (a and a:GetIsAlive()) then
                     target = a
                     _attachBabbler(self, target)
                     -- if (UpdateCling(self, deltaTime, target) == true) then
                     -- self.targetId = target
                     -- break
                     -- end
                  end
               end

            end
         else
            if (not self:GetTarget() or not self:GetTarget():isa("Alien")
                or not self:GetTarget():GetIsAlive()) then
               self:Detach()
            end
         end

      end
   end

end -- ! Server

function Babbler:ModifyDamageTaken(damageTable, attacker, doer, damageType, hitPoint)
   damageTable.damage = math.min(15, damageTable.damage)
end

-- function Babbler:GetTarget()
--    local target = nil

--    -- Attach to nearby aliens
--    if (not target) then
--       target = self.defenseTargetId ~= nil and Shared.GetEntity(self.defenseTargetId)
--    end
--    if (not target) then
--       target = self.targetId ~= nil and Shared.GetEntity(self.targetId)
--    end
--    return target
-- end

local onInit = Babbler.OnInitialized
function Babbler:OnInitialized()
   onInit(self)

   -- -- InitMixin(self, OrdersMixin, { kMoveOrderCompleteDistance = 1 })
   -- if Server and not HasMixin(self, "MapBlip") then
   --    InitMixin(self, MapBlipMixin)
   -- end
end

-- Shared.LinkClassToMap("Babbler", Babbler.kMapName, networkVars)
