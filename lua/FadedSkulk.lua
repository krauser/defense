Script.Load("lua/SoftTargetMixin.lua")


function AlienGetSpeedBonusIfFar(self, speed)
   local nearest_marine = nil
   local marines = GetEntitiesForTeam("Player", 1)

   if (#marines > 0) then
      Shared.SortEntitiesByDistance(self:GetOrigin(), marines)
      nearest_marine = marines[1]
   end

   if (DefIsVirtual(self)) then
      if (nearest_marine
          and self:GetOrigin():GetDistanceTo(nearest_marine:GetOrigin()) > 45) then
         speed = speed * 1.6
      end
   end
   return speed
end

local skulkGetMaxSpeed = Skulk.GetMaxSpeed
function Skulk:GetMaxSpeed(possible)
   local speed = skulkGetMaxSpeed(self, possible) * getAlienSpeedScalar()

   speed = AlienGetSpeedBonusIfFar(self, speed)
   if (self.electrified) then
      speed = speed / kFadedPulseSlowDownFactor
   end
   -- if (self.boneWallSkulk == true) then
   --    return (speed / 1.8)
   -- else
      return (speed)
   --end
end


function Skulk:OnAdjustModelCoords(modelCoords)
   local scale = self.modelsize
   local coords = modelCoords
   -- local rand = ((math.random(1, 10) - 5) / 10)
   rand = 1
   coords.xAxis = coords.xAxis * rand -- Largeur
   coords.yAxis = coords.yAxis * rand -- Hauteur
   coords.zAxis = coords.zAxis * rand -- Profondeur
   return coords
end

local skulkOnInitialized = Skulk.OnInitialized
function Skulk:OnInitialized()
   skulkOnInitialized(self)

   -- if (Server) then
   --    local nb_snake_on_map = 0

   --    self:SetDesiredCamera(0.0, { move = true}, self:GetOrigin(), nil, 2.5)
   --    for _, b in ipairs(GetEntities("Skulk")) do
   --       if (b.boneWallSkulk == true) then
   --          nb_snake_on_map = nb_snake_on_map + 1
   --          break
   --       end
   --    end
   --    if (nb_snake_on_map == 0) then
   --       if (Server) then
   --          self:SetRelevancyDistance(0)
   --       end
   --       self.boneWallSkulk = true
   --    end
   -- end
   --   InitMixin(self, SoftTargetMixin) -- Make Bullet weapons more powerfull
end

-- function ViewModel:OnGetIsRelevant(player)
--    return self.boneWallSkulk ~= true
-- end

local skulkOnKill = Skulk.OnKill
function Skulk:OnKill(killer, doer, point, direction)
   skulkOnKill(self, killer, doer, point, direction)
end

-- function Skulk:OnGetIsRelevant(player)
--    return self.boneWallSkulk ~= true
-- end

function Skulk:GetBaseArmor()
   return kSkulkArmor * (getAlienHealthScalar() * 1.05)
end

function Skulk:GetBaseHealth()
   return kSkulkHealth * (getAlienHealthScalar() * 1.05)
end

-- ----------------------
-- function SmoothPathPoints(points, maxDistance, maxPoints)

--    PROFILE("PathingUtility:SmoothPathPoints")

--    local numPoints   = #points
--    local maxPoints   = maxPoints
--    numPoints = math.min(maxPoints, numPoints)
--    local i = 1
--    while i < numPoints do

--       points[i] = points[i] + Vector(-1 * (math.random(1, 3)), 0, -1 * (math.random(1, 3)))
--       points[i + 1] = points[i + 1] + Vector(-1 * (math.random(1, 3)), 0, -1 * (math.random(1, 3)))
--       local point1 = points[i]
--       local point2 = points[i + 1]

--       -- If the distance between two points is large, add intermediate points

--       local delta = point2 - point1
--       local distance = delta:GetLength()
--       local numNewPoints = math.floor(distance / maxDistance)
--       local p = 0

--       for j = 1, numNewPoints do

--          local f = j / numNewPoints
--          local newPoint = point1 + delta * f
--          if table.find(points, newPoint) == nil then

--             i = i + 1
--             table.insert( points, i, newPoint )
--             p = p + 1

--          end
--       end
--       i = i + 1
--       numPoints = numPoints + p
--    end

-- end

local skulkOnUpdate = Skulk.OnUpdate
function Skulk:OnUpdate(deltaTime)
   skulkOnUpdate(self, deltaTime)
   if (Server and DefIsVirtual(self)) then
      -- if (self.boneWallSkulk == true) then
      --    if (self.bonewallLastCheck == nil or self.bonewallLastCheck + 0.2 < Shared.GetTime()) then
      --       self.bonewallLastCheck = Shared.GetTime()
      --       local origin = self:GetOrigin() - self:GetViewAngles():GetCoords().zAxis * 2
      --       local trace = Shared.TraceRay(origin, origin - Vector(0, 10, 0), CollisionRep.Default, PhysicsMask.AllButPCsAndRagdolls, EntityFilterTwo(self, self))
      --       local displayOrigin = trace.endPoint


      --       if (4 > #GetEntitiesForTeamWithinRange(BoneWall.kMapName, 2, self:GetOrigin(), 4)) then
      --          CreateEntity(BoneWall.kMapName, displayOrigin, 2)
      --       end

      --    end
      -- end

      local target = nil
      if (self.defenseTargetId) then
         target = Shared.GetEntity(self.defenseTargetId)
         if (target and not target:isa("Player")) then -- No babbler change if building
            return
         end
      end

      --    -- Babbler p
      --    local isSelected = (self.GetIsSelected and (self:GetIsSelected( kAlienTeamType )))
      --    if (false and not isSelected and (self.defenseLastSelectedTime == nil or self.defenseLastSelectedTime > Shared.GetTime() + kFadedAlienComBotStandByDelay))
      --    then
      --       if (self:GetCreationTime() + 1 < Shared.GetTime()) then
      --          if (self.optiLastCheck == nil or self.optiLastCheck + 1 < Shared.GetTime()) then
      --             self.optiLastCheck = Shared.GetTime()

      --             local sk = getUnspawnedLifeform(Skulk.kMapName)
      --             if (sk > 0 and #GetEntitiesForTeamWithinRange("Player", 1, self:GetOrigin(), kFadedOptiToBabblerDist) == 0)
      --             then
      --                local old_hp = self:GetHealth()
      --                local old_ap = self:GetArmor()
      --                local old_coord = self:GetCoords()
      --                local old_orig = self:GetOrigin()
      --                -- self:SetOrigin(Vector(math.random(1, 100), 100 + math.random(1, 100), math.random(1, 100)))
      --                local new_ent = spawnAlienCreature(Babbler.kMapName, old_orig)
      --                if (new_ent) then
      --                   -- new_ent:SetCoords(old_coord)
      --                   new_ent.defenseTargetId = self.defenseTargetId
      --                   new_ent:SetHealth(old_hp)
      --                   new_ent:SetArmor(old_ap)
      --                   self:ClearOrders()
      --                   DestroyEntity(self)
      --                   -- else
      --                   --    self:SetOrigin(old_orig)
      --                end
      --             end
      --          end
      --       end
      --    end
   end
end
