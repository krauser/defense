-- ===================== Faded Mod =====================
--
-- lua\FadedMarine.lua
--
--    Created by: Rio (rio@myrio.de)
--    Updated by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

-- Script.Load("lua/DigestMixin.lua")
Script.Load("lua/Marine.lua")
Script.Load("lua/InfestationMixin.lua")
--Script.Load("lua/DetectableMixin.lua")
-- Script.Load("lua/FireMixin.lua")

-- if (Client) then
--    Script.Load("lua/ColoredSkinsMixin.lua")
-- end
Script.Load("lua/FadedDistortionMixin.lua")

local locale = nil
if (Client) then
   Script.Load("lua/FadedMarine_Client.lua")
elseif (Server) then
   locale = LibCache:GetLibrary("LibLocales-1.0")
end

sacrified_marine = {}

-- -- If true, the next marine created will be a veteran
kFadedIsNextMarineVeteran = false
kFadedHunterList = {}
kFadedMedicList = {}

-- Marine.kWalkMaxSpeed = 5.1
-- Marine.kRunMaxSpeed = 6.1
-- Marine.kRunInfestationMaxSpeed = 6.1--5.2

function    FadedGetMedicList()
   return (kFadedMedicList)
end

local networkVars =
   {
      GrenadeLauncherRegular = "boolean",
      -- En lien avec FadedFade.lua
      kFadedTransformation = "boolean",
      kFadedIsVeteran = "boolean",

      kFadedCampDuration = "integer",
      kFadedLastCampPosition = "vector",
      kVeteranSkinUpdated = "boolean",
      -- Fake marine name when using a corpse
      kFakeMarineName = "string",

      kFadedConvertToHeavyShotgun = "boolean",

      -- Variable used to track attacker id (to assign a kill)
      kFadedRevealByFire = "boolean",
      kFadedRevealAttackerName = "string",

      -- Objective_mod (only allow the armory to ressuply once)
      kFadedArmoryUsageLeft = "integer",

      -- If the body has been used, eaten or burned
      kFadedBodyUsed = "boolean",
      kFadedReserved = "boolean",
      kFadedTimeout = "time",
      kFadedStabTrigger = "boolean",
      kFadedIsObsUp = "boolean",

      -- Hunting ability
      kFadedHuntingStartTime = "time",
      kFadedHunter = "boolean",
      kFadedHuntTriggerPos = "vector",

      -- Medic ability
      kFadedMedic = "boolean",

      kFadedLastChatWarning = "time",
   }

--AddMixinNetworkVars(DetectableMixin, networkVars)
-- AddMixinNetworkVars(FireMixin, networkVars)


------------- Stab icon and effect with the Fake Marine
-- function Marine:GetUsablePoints()
--    if (Server) then
--       Print("Server")
--    else
--       Print("Client")
--    end
--    return { self:GetOrigin() + Vector(0, 0.5, 0), -- Origin
--         self:GetOrigin() + Vector(0, 0.5, -0.5), -- Behind
--         self:GetOrigin() + Vector(0, 0.5, 0.5), -- Front
--         self:GetOrigin() + Vector(-0.5, 0.5, 0), -- Left
--         self:GetOrigin() + Vector(0.5, 0.5, 0), -- Right
--    }
-- end

-- function Marine:GetCanBeUsed(player, useSuccessTable)
--    -- if (player and player:isa("Player") and player:GetName() ~= self:GetName()) then
--    --    Print("Marine " .. self:GetName() .. " used by " .. player:GetName())
--       useSuccessTable.useSuccess = true
--       -- useSuccessTable.useSuccess =
--       --      (self:GetTeamNumber() == 2
--       --          and player:GetTeamNumber() == 1)
--    -- end
--    -- useSuccessTable.useSuccess = false
-- end

-- if Server then
--    function Marine:OnUse(player, elapsedTime, useSuccessTable)
--       Print("Marine " .. self:GetName() .. " used by " .. player:GetName())
--       -- if self:GetIsValidRecipient(player) then
--       --      Print("Can used the body (and use it)")
--       -- end
--    end
-- end

-----------



function MarineBotUpdateOrderCallback(self)

   local targets = {}

   -- Clogs are not sightable (do not add in the enum)
   for _, name in ipairs({"Alien", "Babbler", "TunnelEntrance", "BoneWall", "Cyst", "Whip", "Hydra"})
   do
      for _, ent in ipairs(GetEntitiesForTeam(name, 2)) do
         table.insert(targets, ent)
      end
   end

   Shared.SortEntitiesByDistance(self:GetOrigin(), targets)

   for _, entity in ipairs(targets) do
      local order = kTechId.Attack

      -- local dist = self:GetOrigin():GetDistanceTo(entity:GetOrigin())
      -- if (dist < 15 and not GetWallBetween(self:GetOrigin(), entity:GetOrigin())) then
      --    order = kTechId.Attack
      -- end
      self:GiveOrder(order, entity:GetId(), entity:GetOrigin(), nil, true, false)
      break
   end

   return self:GetIsAlive()
end

-- local function _MarineBotInitCallbacks(self)
--    if (Server and GetGamerules():GetGameStarted() and not self.client) then
--       Entity.AddTimedCallback(self, MarineBotUpdateOrderCallback, 1)
--    end
-- end

-- function MarineBotInitCallbacks(self)
--    if (Server and GetGamerules():GetGameStarted() and not self.client) then
--       -- one sec delay for the self.client to works
--       Entity.AddTimedCallback(self, _MarineBotInitCallbacks, 1)
--    end
-- end

local buyMenuOpeningLeft = 0
local marineOnInitialized = Marine.OnInitialized
function Marine:OnInitialized()

   -- MarineBotInitCallbacks(self)
   self.creationTime = Shared.GetTime()
   if (Client and Client.GetLocalPlayer() == self)
   then
      -- if (self:GetIsAlive() and self:GetTeamNumber() == 1) then
      --    Client:SendSelectEquipmentMessage()
      -- end
      self.GUIAboveHeadIcon = GetGUIManager():CreateGUIScript("FadedGUIAboveHeadIcon")
      self.GUIAlienCounter = GetGUIManager():CreateGUIScript("FadedGUIAlienCounter")
      -- self.GUISentryBattery = GetGUIManager():CreateGUIScript("FadedGUISentryBattery")
      -- -- self.GUIMinimapClick = GetGUIManager():CreateGUIScript("GUI")
   end
   if (Client) then
      if (kFadedHunterList[self:GetName()] == true) then
         self:PromoteToHunter() -- Promote the marine to a hunter
      end
      if (kFadedMedicList[self:GetName()] == true) then
         self:PromoteToMedic()
      end
      -- -- We are a fake marine here
      -- if (Client.GetLocalPlayer() == self and self:GetTeamNumber() == 2) then
      --    local Drop = Client.GetOptionString("input/Drop", "Drop")
      --    local Use = Client.GetOptionString("input/Use", "Use")
      --    Client.SendNetworkMessage("OnChatCallBack",
      --                              { msg = "FADED_DROP_CORPSE_HINT", local_key = Drop },
      --                              true)
      --    Client.SendNetworkMessage("OnChatCallBack",
      --                              { msg = "FADED_STAB_HINT", local_key = Use },
      --                              true)
      -- end

      -- InitMixin(self, InfestationMixin)
      -- InitMixin(self, DigestMixin)
      -- InitMixin(self, UsableMixin)
      -- InitMixin(self, PickupableMixin, { kRecipientType = "Fade" })
   end
   marineOnInitialized(self)
   if (Client and Client.GetLocalPlayer() == self
       and buyMenuOpeningLeft > 0) then
      self:Buy()
      buyMenuOpeningLeft = buyMenuOpeningLeft - 1
   end
   if (self:GetTeamNumber() == 2) then
      self:SetMaxHealth(LiveMixin.kMaxHealth)
      self:SetMaxArmor(LiveMixin.kMaxArmor)
      self:SetHealth(self:GetMaxHealth())
      self:SetArmor(self:GetMaxArmor())
      self:SetRelevancyDistance(0)
      if (Server) then
         self:SetModel(nil) -- No collision
      end
   end

   -- if (Client) then
   --    if (math.random(0, 2) == 1) then
   --      Client:SetSelectedEquipment(Shotgun.kMapName, Scan.kMapName)
   --    end
   -- end
   -- if (Client) then
   --    self:InitializeSkin()
   -- end

   if (self.ActivateNanoShield and #GetEntitiesForTeamWithinRange("Player", 2, self:GetOrigin(), 6) > 0)
   then -- Spawn protection
      self:ActivateNanoShield()
   end

end

if (Server) then
   function Marine:InitWeapons()

      Player.InitWeapons(self)

      self:GiveItem(Shotgun.kMapName)
      self:GiveItem(Axe.kMapName)
      self:GiveItem(Builder.kMapName)

      if (self:GetDeaths() == 0) then
         player:GiveItem(LaySentryBattery.kMapName)
      end

      if (self:IsMedic()) then
         self:GiveItem(GrenadeLauncher.kMapName)
         self:SetActiveWeapon(GrenadeLauncher.kMapName)
      else
         self:GiveItem(Rifle.kMapName)
         self:SetActiveWeapon(Rifle.kMapName)
      end

      -- if (getWaveNb() > 1) then
      --    self:SetQuickSwitchTarget(Pistol.kMapName)
      -- else
      --    self:SetQuickSwitchTarget(Shotgun.kMapName)
      -- end

   end
end

local marineOnCreate = Marine.OnCreate
function Marine:OnCreate()
   marineOnCreate(self)
   --InitMixin(self, DetectableMixin)
   -- InitMixin(self, FireMixin)

   -- if (Client) then
   -- Note: Add notes please about the mixin
   -- We don't have any network variables, so this hack should be fine.
   --    InitMixin(self, DistortionMixin)
   -- end
   -- Safe level
   -- self.parasited = true

   self.kFadedMedic = self:IsMedic()
   self.kFadedHunter = false
   self.kFadedHuntTriggerPos = Vector(0,0,0)
   self.kFadedHuntingStartTime = nil
   self.kFadedLastChatWarning = Shared.GetTime()
   self.kFadedConvertToHeavyShotgun = false

   self.kFadedIsObsUp = false
   self.kFadedArmoryUsageLeft = kFadedArmoryHealthAmmoSupply

   self.GrenadeLauncherRegular = true
   self.kFadedTransformation = false

   self.kFadedIsVeteran = kFadedIsNextMarineVeteran
   kIsNextMarineVeteran = false

   self.kFadedCampDuration = 0
   self.kFadedLastCampPosition = self:GetOrigin()
   self.kVeteranSkinUpdated = false
   self.kFakeMarineName = "Kill me I am a Spy"
   self.kFadedRevealByFire = false
   self.kFadedRevealAttackerName = ""
   self.kFadedBodyUsed = false
   self.kFadedReserved = false
   self.kFadedTimeout = Shared.GetTime()
   self.kFadedStabTrigger = false

   -- All marine start with the flashlight ON
   self:SetDefaultFlashlight()
   self:SetFlashlightOn(true)

   if Client then -- light when holding the battery
      self.light = Client.CreateRenderLight()

      self.light:SetType(RenderLight.Type_Spot)
      self.light:SetColor(Color(.8, .8, 1))
      self.light:SetInnerCone(math.rad(30))
      self.light:SetOuterCone(math.rad(60))
      self.light:SetIntensity(0.12)
      self.light:SetRadius(26)
      self.light:SetAtmosphericDensity(0)
      self.light:SetSpecular(true)


      -- TODO:
      -- Les portes (ouvrir fermé, weldable,vie affiché)
      -- Le lerk crash flame avec la lumière
      -- les vagues, les respawns
      -- Les skulks invisible
      -- La music d'ambiance

      self.lightTimer = nil
   end
end

local marineOnDestroy = Marine.OnDestroy
function Marine:OnDestroy()
   marineOnDestroy(self)
   if (Client) then
      GetGUIManager():DestroyGUIScript(self.GUIAboveHeadIcon)
      GetGUIManager():DestroyGUIScript(self.GUIAlienCounter)
      -- GetGUIManager():DestroyGUIScript(self.GUISentryBattery)
      -- GetGUIManager():DestroyGUIScript(self.GUIInsight_PlayerHealthbars)
   end
end

if (Client) then
   local function playerUpdateGhostModel(self)
      local weapon = self:GetActiveWeapon()
      if (weapon) then
         if weapon:isa("LaySentryBattery") then
            self.currentTechId = kTechId.Sentry
         elseif weapon:isa("LaySkulk") then
            self.currentTechId = kTechId.Skulk
         elseif weapon:isa("LayGorge") then
            self.currentTechId = kTechId.Gorge
         elseif weapon:isa("LayLerk") then
            self.currentTechId = kTechId.Lerk
         elseif weapon:isa("LayFade") then
            self.currentTechId = kTechId.Fade
         elseif weapon:isa("LayOnos") then
            self.currentTechId = kTechId.Onos
         else
            return
         end
         self.ghostStructureCoords = weapon:GetGhostModelCoords()
         self.ghostStructureValid = weapon:GetIsPlacementValid()
         self.showGhostModel = weapon:GetShowGhostModel()
      end
   end

   -- Display the ghost structure when the marine want to place a building
   local marineUpdateGhostModel = Marine.UpdateGhostModel
   function Marine:UpdateGhostModel()
      marineUpdateGhostModel(self)
      playerUpdateGhostModel(self)
   end
   -- Display the ghost structure when the JetpackMarine want to place a building
   local jetpackMarineUpdateGhostModel = JetpackMarine.UpdateGhostModel
   function JetpackMarine:UpdateGhostModel()
      jetpackMarineUpdateGhostModel(self)
      playerUpdateGhostModel(self)
   end
   function Marine:AddGhostGuide(origin, radius)
   end
   function JetpackMarine:AddGhostGuide(origin, radius)
   end

end

-- Denie fake marines to weld them self
function Marine:GetCanBeWeldedOverride()
   return (self:GetArmor() < self:GetMaxArmor() and self:GetTeamNumber() == 1), false
end

-- -- Not needed (only if we want more fine management, ex: Custom scoreboard weap name)-- -- Returns the name of the primary weapon
-- local marineGetPlayerStatusDesc = Marine.GetPlayerStatusDesc
-- function Marine:GetPlayerStatusDesc()
--    local st = marineGetPlayerStatusDesc(self)
--    if (st == kPlayerStatus.Void) then
--       local weapon = self:GetWeaponInHUDSlot(1)
--       if (weapon and weapon:isa("HeavyShotgun")) then
--      st = kPlayerStatus.HeavyShotgun
--       end
--    end
--    return (st)
-- end

if (Server) then
   -- Forward faded marine specifics upgrade to the JetpackMarine
   -- - hunter ability
   -- - Minimap (if obs is up)
   function Marine:GiveJetpack()

      -- Faded code
      local isHunter = self:hasHunterAbility()
      local isObsUp = self.kFadedIsObsUp
      local ressuplyLeft = self.kFadedArmoryUsageLeft
      local isVeteran = self.kFadedIsVeteran
      local armor = self:GetArmor()
      local maxArmor = self:GetMaxArmor()

      -- This is legacy ns2 code
      local activeWeapon = self:GetActiveWeapon()
      local activeWeaponMapName = nil
      local health = self:GetHealth()

      if activeWeapon ~= nil then
         activeWeaponMapName = activeWeapon:GetMapName()
      end

      local jetpackMarine = self:Replace(JetpackMarine.kMapName, self:GetTeamNumber(), true, Vector(self:GetOrigin()))

      jetpackMarine:SetActiveWeapon(activeWeaponMapName)
      jetpackMarine:SetHealth(health)
      ------------------------------------------
      -- Faded code
      if (isHunter) then
         jetpackMarine:PromoteToHunter()
      end
      jetpackMarine.kFadedIsObsUp = isObsUp -- Server side
      Server.SendNetworkMessage( -- Client side
         jetpackMarine, "SetObsState",
         {
            kFadedIsObsUp = isObsUp,
         }, true)
      jetpackMarine.kFadedArmoryUsageLeft = ressuplyLeft
      jetpackMarine.kFadedIsVeteran = isVeteran
      jetpackMarine:SetArmor(armor)
      jetpackMarine:SetMaxArmor(maxArmor)

   end
end

function Marine:GetArmoryUseLeft()
   return (self.kFadedArmoryUsageLeft)
end

function Marine:DecArmoryUseLeft()
   if (self.kFadedArmoryUsageLeft > 0) then
      self.kFadedArmoryUsageLeft = self.kFadedArmoryUsageLeft - 1
   end
end

-- Getter/setter to wrap local variable



local function killAllLifeform()
   local total_killed = 0
   for _, ent_name in ipairs({"Skulk", "Babbler", "Gorge", "Lerk", "Fade", "Onos", "BoneWall", "Hydra", "Cyst", "Clog", "Whip", "TunnelEntrance"}) do
      for _, entity in ipairs(GetEntitiesForTeam(ent_name, 2)) do
         if (entity and entity:GetIsAlive()) --and not entity.client)
         then
            entity:Kill()
            total_killed = total_killed + 1
         end
      end
   end
   return total_killed
end

-- local liveMixinKill = LiveMixin.Kill
-- function LiveMixin:Kill(attacker, doer, point, direction)
--    if (Server and self:isa("Marine") and GetGamerules():GetGameStarted()) then
--       local player = self
--       if (player and player:GetTeamNumber() == 1 and player:GetIsAlive())-- and player:GetIsRookie())
--       then
--          local isSuicide = not doer and not attacker
--          local fullHealth = true--player:GetHealth() / player:GetMaxHealth() > 0.7
--          if (isSuicide and fullHealth) then
--             self:doStabDamage()
--          end
--       end
--    end

--    liveMixinKill(self, attacker, doer, point, direction)
-- end

local _messages =
   {
      "And there was war in heaven",
      "And God said: lord, have mercy on my son",
      "I am the LORD your God.",
      "Idolatry and witchcraft; hatred, discord, healousy, fits of rage and selfish ambition",
      "For if God did not spare angels when they sinned, but sent them to hell",
      "You were the model of perfection, full of wisdom and perfect in beauty",
      "Then they will go away to eternal punishement",
      "The wicked go down the the realm of the dead",
      "The one who sins is the one who will die",
      "And these shall go away into everlasting punishement",
      "Death and hell were cast into the lake of fire",
      "God has mercy, I don't.",
      "Mwouhahahahahaha",
      "*;..;*"
   }

local _tmp = {}
local function randomHellMessage(player)
   if (table.getn(_tmp) == 0) then
      for k, v in ipairs(_messages) do
         _tmp[k] = v
      end
   end
   -- hint_nb_displayed = hint_nb_displayed + 1
   -- if (hint_messages[hint_nb_displayed] == nil) then
   --    hint_nb_displayed = 1
   -- end
   local msg_nb = math.random(1, table.getn(_tmp))
   local msg = _tmp[msg_nb]
   table.remove(_tmp, msg_nb)
   return msg
end

function SatanPunishMarines()
   if (math.random() < 0.85/100) -- X % chances per second (so one punish each 50s approx)
   then
      local msg = randomHellMessage(victim)
      for _, victim in ipairs(GetEntitiesForTeam("Marine", 1)) do
         local id = victim:GetName()
         if (id and sacrified_marine[id] and sacrified_marine[id] == id) then
            if (victim and victim:GetIsAlive() and victim:GetTeamNumber() == 1 and victim:isa("Marine"))
            then
               local rand = math.random()
               victim:doStabDamage()
               victim:SatanMessage(msg)
            end
         end
      end
   end
   return true
end

-- local playerOnUpdate = Player.OnUpdate
-- function Player:OnUpdate(deltaTime)
--    playerOnUpdate(self, deltaTime)

-- end

local originalOnKill = Marine.OnKill
function Marine:OnKill(attacker, doer, point, direction)
   --self:ClearMedicAbility()

   originalOnKill(self, attacker, doer, point, direction)

   local sentryBattery = self:GetWeapon(LaySentryBattery.kMapName)
   if (sentryBattery) then -- Drop the sentry battery if the owner die
      self:ClearOrders()
      sentryBattery:Dropped(self)
   end

end

-- local marineTriggerSuicide = Marine.TriggerSuicide
-- function Marine:TriggerSuicide()

--    if (marineTriggerSuicide) then
--       Shared:FadedMessage("Suicide detected")
--       marineTriggerSuicide(self)
--    end
-- end



-- -- No damage indicator
-- function Marine:UpdateDamageIndicators()
-- end

-- Little patch when we are killed with the flamethrower and sound is still up
-- Adding "and self.loopingFireSound ~= nil"


-- TODO:
-- GUI number of lifeform remaining
-- MAX number on map
-- Marine invisible et invincible
-- Fix le redplug
-- Fix le skulk qui disparait
-- Proximity, empécher le drop trop d'un marine ou si capmax est activé
-- Resources personnel
-- Limiter à 1/5 com alien
-- Ghost qui ne s'affiche (blueprint)
-- Spawn les skulks 3 par 3

-- function Flamethrower:Dropped(prevOwner)

--    ClipWeapon.Dropped(self, prevOwner)

--    if Server and self.loopingFireSound ~= nil then
--       self.createParticleEffects = false
--       self.loopingFireSound:Stop()
--    end
-- end

-- local marineDrop = Marine.Drop
-- function Marine:Drop(weapon, ignoreDropTimeLimit, ignoreReplacementWeapon)
--    Print("Drop called")
--    if (weapon and not weapon:isa("Welder")) then
--       marineDrop(self, weapon, ignoreDropTimeLimit, ignoreReplacementWeapon)
--    end
-- end

-- Release the Fake Marine body to become a Fade again
-- The fake marine can pick weapon if there are nearby
function Marine:releaseMarineDisguise(input)
   -- local nb_weapon_around = #GetEntitiesWithinRange("ClipWeapon",
   --                             self:GetOrigin(),
   --                             2)
   local key = (bit.band(input.commands, Move.Drop) ~= 0)
   -- if (key and nb_weapon_around == 0) then
   if (key) then
      self.kFadedTransformation = true
   end
end

function Marine:GetFakeMarineName()
   -- return (self:GetName())
   return (self.kFakeMarineName)
end
function Marine:SetFakeMarineName(fakeName)
   self.kFakeMarineName = fakeName
   if (Server) then
      self:FadedMessage("Try to act like an enemy while disguised as '" .. fakeName .. "'")
   end
end

-- local getCanBeWeldedOverride = Marine.GetCanBeWeldedOverride
-- function Marine:GetCanBeWeldedOverride(doer)
--    -- if (self:GetTeamNumber() == 1) then
--    --    return getCanBeWeldedOverride(self)
--    -- else
--    Print("Caleed GetCanBeWeldedOverride")
--       return true, true
--    -- end
-- end

-- local change = false
function Marine:SetAsNextFade(input)
   if (self:GetActiveWeapon()
          and self:GetActiveWeapon():isa("Axe")
       and self:GetFlashlightOn()) then
      if (self.kFadedCode1 == nil and bit.band(input.commands, Move.Reload) ~= 0) then
         self.kFadedCode1 = true
      elseif (self.kFadedCode1 and self.kFadedCode3 == nil
              and bit.band(input.commands, Move.Drop) ~= 0) then
         self.kFadedCode2 = true
      elseif (self.kFadedCode2 and bit.band(input.commands, Move.Reload) ~= 0) then
         self.kFadedCode3 = true
      elseif (self.kFadedCode3 and bit.band(input.commands, Move.Drop) ~= 0)
      then
         -- if (Server) then
         -- kFlamethrowerRange = 60
         -- kFlameRadius = 5
         self.kFadedReserved = not(self.kFadedReserved)
         self:SetFlashlightOn(false)
         -- if (Server) then
         --    fadedNextPlayersName[self:GetName()] = true
         -- end
         -- Marine:DropAllWeapons(self)
         -- Marine:GiveItem(self, Axe.kMapName)
         -- Marine:GiveItem(self, Pistol.kMapName)
         -- Marine:GiveItem(self, Flamethrower.kMapName)
         -- end
         -- self.kFadedTransformation = true
      end
   end
end

function Player:isAimingEntity(entityName, distance, teamNb)
   local viewCoords = self:GetViewAngles():GetCoords()
   local startPoint = self:GetEyePos()
   local kRange = distance

   -- double trace; first as a ray to allow us to hit through narrow openings, then as a fat box if the first one misses
   local trace = Shared.TraceRay(startPoint, startPoint + viewCoords.zAxis * kRange,
                                 CollisionRep.Move,
                                 PhysicsMask.All,
                                 EntityFilterOneAndIsa(self, "Babbler"))
   if not trace.entity then
      local extents = GetDirectedExtentsForDiameter(viewCoords.zAxis, 0.3)
      trace = Shared.TraceBox(extents, startPoint,
                              startPoint + viewCoords.zAxis * kRange,
                              CollisionRep.Move,
                              PhysicsMask.All,
                              EntityFilterOneAndIsa(self, "Babbler"))
   end

   -- Print("Trace ray")
   if trace.fraction < 1 then

      local hitObject = trace.entity
      local direction = GetNormalizedVector(trace.endPoint - startPoint)
      local impactPoint = trace.endPoint - direction * kHitEffectOffset

      --self:isAliveMarineAroundInView(kFadedStabViewDistance)
      -- if (not hitObject and impactPoint) then
      --    Print("We hit a wall")
      -- end
      if (entityName) then
         if (hitObject and hitObject:isa(entityName)) then
            if (not teamNb or hitObject:GetTeamNumber() == teamNb) then
               return true, trace.entity, direction, impactPoint
            end
         end
      else -- Want to check for a wall
         if (not hitObject and impactPoint) then
            return true, nil, direction, impactPoint
         end
      end
   end

   return false, nil, nil, nil
end

-- Perform a vector dot product to check if the alien is behind
-- (compare the angle between the two view vector)
local function _checkIfBehindTheMarine(fakeMarine, target)
   local targetOrigin = fakeMarine:GetOrigin()
   local eyePos = target:GetOrigin()

   local toEntity = Vector(0, 0, 0)

   -- Reuse vector
   toEntity.x = targetOrigin.x - eyePos.x
   toEntity.y = targetOrigin.y - eyePos.y
   toEntity.z = targetOrigin.z - eyePos.z

   -- Normalize vector
   local toEntityLength = math.sqrt(toEntity.x * toEntity.x + toEntity.y * toEntity.y + toEntity.z * toEntity.z)
   if toEntityLength > kEpsilon then
      toEntity.x = toEntity.x / toEntityLength
      toEntity.y = toEntity.y / toEntityLength
      toEntity.z = toEntity.z / toEntityLength
   end

   local seeingEntityAngles = GetEntityViewAngles(target)
   local normViewVec = seeingEntityAngles:GetCoords().zAxis
   local dotProduct = Math.DotProduct(toEntity, normViewVec)

   local s = math.acos(dotProduct)
   -- Because it use angle from view, 45° is the side
   local isVisible = (s > math.rad(200/2))
   return (isVisible)
end

function Marine:checkIfAimBattery()
   local status, entity, direction, impactPoint =
      self:isAimingEntity("Sentry", 2, 1)
   if (status and entity and entity:isAttachedToBuilding() == false) then
      return true, entity, direction, impactPoint
   else
      return false, nil, nil, nil
   end
   return false, nil, nil, nil
end

-- Trace a Ray and check if the Fake marine can Stab
function Marine:checkIfFakeMarineCanStab()
   -- local status, entity, direction, impactPoint =
   --    self:isAimingEntity("Marine", kFadedStabRange, 1)
   -- if (status) then
   --    status = _checkIfBehindTheMarine(self, entity)
   --    if (status) then
   --       -- Can stab and behind a marine
   --       if (#GetEntitiesForTeamWithinRangeInLOS(self, -1, "Player", 1, self:GetOrigin(), kFadedInstantStabMinUnseenDist) <= 1) then -- instant stab
   --          return 3, entity, direction, impactPoint
   --       end
   --       return 2, entity, direction, impactPoint
   --    end
   --    -- Can't stab but the Faded aim at a marine
   --    return 1, nil, nil, nil
   -- end
   -- Not behind and not aiming
   return false, nil, nil, nil
end

-- Trigger the stab on the victim
-- As we are a marine and because of several issue (transformation coord, animation, etc)
-- I simulate the stab by
-- * Doing damage by hand
-- * Play sound of stab
-- * Print hit damage on faded screen
-- local kStabSound_hit = PrecacheAsset("sound/NS2.fev/alien/fade/swipe_both")
-- local kStabSound_stab = PrecacheAsset("sound/NS2.fev/alien/fade/stab_marine")
local kStabSound_poison = PrecacheAsset("sound/NS2.fev/alien/lerk/spores_hit")
local _fadedName = nil
local _entity = nil
local _impact = nil
local _direction = nil
local _lethal_stab = false
function Marine:doStabDamage()
   local stab_damages = kFadedStabDamage * 18
   if (self and self:GetIsAlive() and Server)-- and _fadedName)
   then
      -- local faded = Shared:GetPlayerByName(_fadedName)
      local newItem = CreateEntity(StabBlink.kMapName, nil, 2)
      if (newItem) then
         -- If noone is seeing us then instant stab
         -- if (_lethal_stab == true)
         -- then
         --    _lethal_stab = false
         --    stab_damages = 666
         -- end


         -- local msg = BuildDamageMessage(self,
         --                                stab_damages, self:GetOrigin())

         -- Server.SendNetworkMessage(faded, "Damage", msg, false)

         -- Double swipe sound
         -- StartSoundEffectOnEntity(kStabSound_hit, entity)
         -- StartSoundEffectOnEntity(kStabSound_stab, entity)
         StartSoundEffectOnEntity(kStabSound_poison, self)
         -- newItem:SetParent(self)
         self:SetPoisoned(self)
         self:SetHealth(math.max(1, self:GetHealth() - stab_damages))
         newItem:DoDamage(2, self, self:GetOrigin(), self:GetViewAngles():GetCoords().zAxis, "flesh", false, true)
         DestroyEntity(newItem)
         _fadedName, _entity, _impact, _direction = nil
      end
   end
end

if (Server) then
   -- Trigger the stab server side (server fct)
   function Marine:FakeMarineTriggerStab(input)
      if (self:GetTeamNumber() == 2) then
         if (bit.band(input.commands, Move.Use) ~= 0) then
            local status, entity, direction, impact = self:checkIfFakeMarineCanStab()
            -- 2 mean aim the marine AND is behind
            if ((status == 2 or status == 3)
                and self.kFadedTransformation == false) then
               -- self:doStabDamage(entity, direction, impact)
               _entity = entity
               _impact = impact
               _direction = direction
               _fadedName = self:GetName()
               _lethal_stab = (status == 3)
               self.kFadedStabTrigger = true
               self.kFadedTransformation = true
               self:FadedMessage(string.format(
                                    locale:ResolveString("FADED_STAB_SUCCESS"),
                                    entity:GetName()))
               -- entity:AddTimedCallback(Marine.doStabDamage, 0)
               entity:doStabDamage()
            end
         end
      end
   end
end


local playerHandleButtons = Marine.HandleButtons
function Marine:HandleButtons(input)
   playerHandleButtons(self, input)
   -- self:UpdateLightBattery(input)
   -- Debug code for developpement only
   if ((self.kFadedTimeout + 1.5) > Shared.GetTime())
   then
      self:SetAsNextFade(input)
   else
      self.kFadedCode1 = nil
      self.kFadedCode2 = nil
      self.kFadedCode3 = nil
      self.kFadedTimeout = Shared.GetTime()
   end

   if (Server) then
      self:FakeMarineTriggerStab(input)
   end
   if (self:GetTeamNumber() == 2) then
      self:releaseMarineDisguise(input)
   end
   local key = (bit.band(input.commands, Move.Use) ~= 0)
   if (Client and key) then
      local viewCoords = self:GetViewAngles():GetCoords()
      local startPoint = self:GetEyePos()
      local kRange = kFadedHuntingRayRange
      if (GetWallBetween(startPoint, startPoint + viewCoords.zAxis * kRange))
      then

         if (key and self:hasHunterAbility()) then
            if (not self:GetHuntingFraction()) then
               self:startHuntingTime()
            end
         end
      else
         self:clearHuntingTime()
      end
   end
end

function Marine:IsMedic()
   if ((Server or Client) and kFadedMedicList[self:GetName()] == true) then
      return (true)
   end
   return self.kFadedMedic
end

function send_medic_info(st, name)
   if (Server) then
      for _, marine in pairs(GetGamerules():GetTeam1():GetPlayers()) do
         Server.SendNetworkMessage(marine, "SetAsMedic", {st = st, name = name}, true)
      end
      for _, alien in pairs(GetGamerules():GetTeam2():GetPlayers()) do
         Server.SendNetworkMessage(alien, "SetAsMedic", {st = st, name = name}, true)
      end
      for _, str in ipairs({"Spectator"}) do
         for _, spectator in ientitylist(Shared.GetEntitiesWithClassname(str)) do
            if (spectator) then
               Server.SendNetworkMessage(spectator, "SetAsMedic", {st = st, name = name}, true)
            end
         end
      end
   end
end

function Marine:PromoteToMedic()
   self.kFadedMedic = true
   if ((Client or Server)) then
      kFadedMedicList[self:GetName()] = true
   end
   if (Server) then
      send_medic_info(true, self:GetName())
   end
   -- if (Server) then
   --    kFadedMedicList[self:GetId()] = true
   -- end
   -- Server.SendNetworkMessage(self, "SetAsMedic", {st = true}, true)
end

function Marine:ClearMedicAbility()
   self.kFadedMedic = false

   if ((Client or Server)) then
      kFadedMedicList[self:GetName()] = false
   end
   if (Server) then
      send_medic_info(false, self:GetName())
   end
end

function Marine:PromoteToHunter()
   self.kFadedHunter = true
   if Client and kFadedNoCustomFlashlight == false then
      self.flashlight:SetColor( kFadedLightNoDetectColor )
      self.flashlight:SetIntensity( 10 )
      self.flashlight:SetRadius( 18 )
      self.flashlight:SetInnerCone( math.rad(7) )
      self.flashlight:SetOuterCone( math.rad(14) )
      self.flashlight:SetAtmosphericDensity(0)

   end
   if (Server) then
      for _, marine in pairs(GetGamerules():GetTeam1():GetPlayers()) do
         Server.SendNetworkMessage(marine, "SetAsHunter", {st = true, name = self:GetName()}, true)
      end
      for _, alien in pairs(GetGamerules():GetTeam2():GetPlayers()) do
         Server.SendNetworkMessage(alien, "SetAsHunter", {st = true, name = self:GetName()}, true)
      end
   end
end

function Marine:SetDefaultFlashlight()
   if Client then -- NS2 legacy flashlight
      self.flashlight:SetColor( Color(.8, .8, 1) )
      self.flashlight:SetInnerCone( math.rad(28) )
      self.flashlight:SetOuterCone( math.rad(33) )
      self.flashlight:SetIntensity( 10 )
      self.flashlight:SetRadius( 18 )
      self.flashlight:SetAtmosphericDensity(0)
   end
end

function Marine:ClearHunterAbility()
   self.kFadedHunter = false
   self:SetDefaultFlashlight()
   if (Server) then
      for _, marine in pairs(GetGamerules():GetTeam1():GetPlayers()) do
         Server.SendNetworkMessage(marine, "SetAsHunter", {st = false, name = self:GetName()}, true)
      end
      for _, alien in pairs(GetGamerules():GetTeam2():GetPlayers()) do
         Server.SendNetworkMessage(alien, "SetAsHunter", {st = false, name = self:GetName()}, true)
      end
   end
end

function Marine:hasHunterAbility()
   return (self.kFadedHunter)
end

-- Returns the name of the primary weapon
local marineGetPlayerStatusDesc = Marine.GetPlayerStatusDesc
function Marine:GetPlayerStatusDesc()

   local status = marineGetPlayerStatusDesc(self)
   if (status and status == kPlayerStatus.Shotgun and self.kFadedConvertToHeavyShotgun == true) then
      status = kPlayerStatus.HShotgun
   end
   return status
end


if (Client) then


   -- hunting progress of the marine (0.00 --> 1.00)
   function Marine:GetHuntingFraction()
      if (self.kFadedHuntingStartTime) then
         local faded_dist = GetFadeDistancePercent(self:GetOrigin(), 30)
         local current = Shared.GetTime() - self.kFadedHuntingStartTime
         local end_time = kFadedHuntTime * faded_dist + 0.1
         local progress = (((current / end_time) * 100) / 100)
         if (progress > 1) then
            progress = 1
         end
         return (progress)
      end
      return (nil)
   end

   function Marine:clearHuntingTime()
      self:setLastHuntingTime(nil)
   end

   function Marine:startHuntingTime()
      self:setLastHuntingTime(Shared.GetTime())
      self.kFadedHuntTriggerPos = self:GetOrigin()
   end

   function Marine:setLastHuntingTime(time)
      self.kFadedHuntingStartTime = time
   end

   local function SmoothPathPoints(points, maxPoints, maxDistance)

      local i = 1
      local a = 0.15 -- alpha for color

      local numPoints = #points
      while (i + 3 < numPoints and i < maxPoints) do

         local point1 = points[i]
         local point2 = points[i + 3]

         if (point1 and point2) then
            -- If the distance between two points is large, add intermediate points

            local delta = point2 - point1
            local distance = delta:GetLength()
            local numNewPoints = math.floor(distance / maxDistance)

            for j = 1, numNewPoints do

               local f = j / numNewPoints
               local newPoint = point1 + delta * f
               -- if table.find(points, newPoint) == nil then

               DebugWireSphere(newPoint, 0.06,
                               kFadedHuntPathDuration,
                               0, 0.8, 0, a) -- r, g, b, a

               -- end
            end
            -- DebugWireSphere(points[i], 0.06,
            --                 kFadedHuntPathDuration,
            --                 0, 0.8, 0, a) -- r, g, b, a
         end
         i = i + 3
      end
   end

   function HunterDrowPathFinder(self, nearest_faded_orig)
      local points = PointArray()
      local dist = 0.7

      Pathing.GetPathPoints(self:GetModelOrigin(),
                            nearest_faded_orig, points)
      if points and #points > 1 then
         -- for i = 2, (math.min(#points, kFadedHuntMaxPointOnPath) / 2)
         -- do
         SmoothPathPoints(points, kFadedHuntMaxPointOnPath, dist)

         if (#points <= kFadedHuntMaxPointOnPath) then
            DebugBox(nearest_faded_orig, nearest_faded_orig, GetExtents(kTechId.Fade), kFadedHuntPathDuration, 0, 0.8, 0, 0.15)
         end
      end
      -- --end
   end

   local function hunterAbility(self, actionFinder, input)
      if (self:GetIsAlive() and self:GetTeamNumber() == 1
             and self:isa("Marine") and self:hasHunterAbility())
      then
         local viewCoords = self:GetViewAngles():GetCoords()
         local startPoint = self:GetEyePos()
         local kRange = kFadedHuntingRayRange
         local hasMoved = self:GetOrigin():GetDistanceTo(self.kFadedHuntTriggerPos)

         if (GetWallBetween(startPoint, startPoint + viewCoords.zAxis * kRange))
         then
            -- if (hasMoved <= 0.5) then
            local huntingFraction = self:GetHuntingFraction()
            actionFinder.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("Use"), nil,
                                                "FADED_MARINE_HUNT_HELP",
                                                huntingFraction)
            if (huntingFraction and huntingFraction >= 1) then
               self:clearHuntingTime()
               Client.SendNetworkMessage("OnFinishHunting",
                                         { location = Vector(0, 0, 0) }, true)

            end -- eating fraction
            if (hasMoved >= 0.5) then
               self:clearHuntingTime()
            end
            -- end
         else
            self:clearHuntingTime()
         end -- is aiming
      end
   end

   local marineOnProcessMove = MarineActionFinderMixin.OnProcessMove
   function MarineActionFinderMixin:OnProcessMove(input)
      local hint = ""
      marineOnProcessMove(self, input)
      if (self:GetIsAlive() and self:GetTeamNumber() == 2) then
         -- local status, _ = self:checkIfFakeMarineCanStab()
         -- if (status) then
         --    if (status == 1)
         --    then
         --       hint = "FADED_CANT_USE_CORPSE_RAW"
         --       self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("Use"), nil, hint, nil)
         --    elseif (status == 2) then
         --       hint = "FADED_STAB_HELP"
         --       self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("Use"), nil, hint, nil)
         --    elseif (status == 3) then
         --       hint = "FADED_INSTANT_STAB_HELP"
         --       self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("Use"), nil, hint, nil)
         --    end
         --    -- else
         --    --    self.actionIconGUI:Hide()
         --    -- end
         --    -- Only enable this code for a Fake Marine
         -- end
      elseif (self:isa("Marine") and self:GetIsAlive() and self:GetTeamNumber() == 1)
      then
         if (self:isAimingEntity("Sentry", 2, 1) == true
                and self:GetWeaponInHUDSlot(LaySentryBattery.GetHUDSlot()) == nil)
         then
            hint = "FADED_TAKE_BATTERY"
            self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("Use"), nil, hint)
         elseif (self:GetActiveWeapon() and self:GetActiveWeapon():isa("GrenadeThrower")
                 and self:isAimingEntity(nil, 2, 1) == true) then
            hint = "FADED_PLANT_GRENADE"
            self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("SecondaryAttack"), nil, hint)
            -- elseif (self:isAimingEntity("FlameMines", 2, 1) == true) then
            --    hint = ""
            --    self.actionIconGUI:ShowIcon(BindingsUI_GetInputValue("Drop"), nil, hint)
         else
            hunterAbility(Client.GetLocalPlayer(), self, input)
         end
      end
   end
end

-- ----------------- Used for dead marine pickup

-- function Marine:GetIsValidRecipient(recipient)
--    if (recipient and recipient:isa("Fade") and recipient:GetTeamNumber() == 2
--        and self:GetIsAlive() == false) then
--       return (true)
--    end
--    return (false)
-- end
-- function Marine:OnTouch(recipient)
--    Print("Dead body picked by " .. recipient:GetName())
-- end
-- function Marine:GetIsPermanent()
--    return (true)
-- end


-- Class_Reload( "Marine", networkVars )
-- Shared.LinkClassToMap("Marine", Spectator.kMapName, networkVars)

-- function Marine:UpdateLightBattery(input)
--    local flashlightPressed = bit.band(input.commands, Move.ToggleFlashlight) ~= 0

--    if (flashlightPressed) then
--       self.kFadedLightBatteryLeft = self.kFadedLightBatteryLeft * -1
--    end
--    -- if (not self:GetFlashlightOn()) then
--    --    return (false)
--    -- end

--    return (true)
-- end

if Client then

   local bipRefresh = 0
   local maxDistance = 40
   local marineUpdateClientEffects = Marine.UpdateClientEffects

   local bipDelay_range = {0.1, 3}
   local volume_range = {1, 2}
   local pitch_range = {0.0, 0.17}

   kFadedIsBeeperEnable = false
   function MarineToggleBeeper(self)
      if (self) then
         kFadedIsBeeperEnable = not kFadedIsBeeperEnable
      end
   end

   function Marine:UpdateClientEffects(deltaTime, isLocal)
      local distance = maxDistance
      local proximity = nil
      if (marineUpdateClientEffects) then
         marineUpdateClientEffects(self, deltaTime, isLocal)
         if (kFadedIsBeeperEnable and bipRefresh < Shared.GetTime() and isLocal) then
            local marineHUD = ClientUI.GetScript("Hud/Marine/GUIMarineHUD")
            if marineHUD then
               marineHUD:SetIsVisible(false)
            end

            bipRefresh = Shared.GetTime()
            faded = GetEntitiesForTeam("Fade", 2)
            local faded_id = FindNearestEntityId("Player", self:GetOrigin())
            local faded = Shared.GetEntity(faded_id)
            local volume = volume_range[1]
            local pitch = pitch_range[1]

            if (faded) then
               local self_orig = self:GetOrigin()
               distance = faded:GetOrigin():GetDistanceTo(self_orig)
               distance = Clamp(distance, 1, maxDistance)

            else
               distance = maxDistance
            end
            -- 100% ==> really close
            -- 0%   ==> really far
            -- -- the closer he is, the higher proximity is to 100%
            proximity = 100 - ((100 / maxDistance) * distance)
            -- proximity = (proximity * proximity * 2) / 100 -- exp curve
            -- proximity = Clamp(proximity, 0, 100)

            bipRefresh = bipRefresh + bipDelay_range[2]
               - ((bipDelay_range[2] - bipDelay_range[1]) / 100) * proximity
            volume = volume
               + ((volume_range[2] - volume_range[1]) / 100) * proximity
            pitch = pitch
               + ((pitch_range[2] - pitch_range[1]) / 100) * proximity
            if (proximity >= 2) then
               PlaySound(self, Marine.radarPing, volume, pitch)
            end
            PlaySound(self, Marine.radarPong, volume, pitch)
            ping_pong = not ping_pong
         end
      end
   end

   local marineOnUpdateRender = Marine.OnUpdateRender
   function Marine:OnUpdateRender()
      if (marineOnUpdateRender) then
         marineOnUpdateRender(self)
      end

      if self == Client.GetLocalPlayer() then
         local start_effect_at = 85
         local disorient_force = 1.15 / 2
         local disorient2_force = 1 / 2
      end

      if (kFadedCanUsePrimaryWithBattery == false) then
         local foundWeapon = nil
         local foundBattery = nil
         for i = 0, self:GetNumChildren() - 1 do

            local child = self:GetChildAtIndex(i)
            if (child:isa("Weapon")) then
               if (child:GetMapName() == weaponMapName) then
                  foundWeapon = child
               elseif (child:GetMapName() == LaySentryBattery.kMapName) then
                  foundBattery = true
               end
            end

         end

         local coords = self:GetCoords()
         if self:GetFlashlightOn() and self.light ~= nil then
            self.light:SetIsVisible(true)
            coords.origin = coords.origin + coords.yAxis * 0.2

            self.light:SetCoords(coords)
            self.lightTimer = Shared.GetTime()
         else
            self.light:SetIsVisible(false)
         end
      end
   end

   local marineOnDestroy = Marine.OnDestroy
   function Marine:OnDestroy()
      if (marineOnDestroy) then
         marineOnDestroy(self)
      end
      if self.light ~= nil then
         Client.DestroyRenderLight(self.light)
         self.light = nil
      end
   end
end

local marineShouldAutopickupWeapons = Marine.ShouldAutopickupWeapons
function Marine:ShouldAutopickupWeapons()
   if (self:GetTeamNumber() == 2) then
      return false
   end
   return marineShouldAutopickupWeapons(self)
end

local marineShouldAutopickupBetterWeapons = Marine.ShouldAutopickupBetterWeapons
function Marine:ShouldAutopickupBetterWeapons()
   if (self:GetTeamNumber() == 2) then
      return false
   end
   return marineShouldAutopickupBetterWeapons(self)
end

local marineHandleButtons = Marine.HandleButtons
function Marine:HandleButtons(input)

   if (self:GetTeamNumber() == 2) then
      if self:GetCanControl() then
         local dropPressed = bit.band(input.commands, Move.Drop) ~= 0

         if (not dropPressed) then
            return marineHandleButtons(self, input)
         else
            return
         end
      end
   end
   return marineHandleButtons(self, input)
end
