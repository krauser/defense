
function Gorge:GetBaseArmor()
    return kGorgeArmor * getAlienHealthScalar()
end

function Gorge:GetBaseHealth()
   return kGorgeHealth * getAlienHealthScalar()
end

local gorgeGetMaxSpeed = Gorge.GetMaxSpeed
function Gorge:GetMaxSpeed(possible)
   local speed = gorgeGetMaxSpeed(self, possible) * getAlienSpeedScalar()

   speed = AlienGetSpeedBonusIfFar(self, speed)
   if (self.electrified) then
      speed = speed / kFadedPulseSlowDownFactor
   end
   return (speed)
end

local gorgeOnUpdate = Gorge.OnUpdate
function Gorge:OnUpdate(deltaTime)
   gorgeOnUpdate(self, deltaTime)

   if (Server) then
      -- Chances to be a gorge capable of spawning a tunnel
      local gorgesTunnelDropperChances = wavesGetTunnelDropperChances()
      -- per second
      local tunnelDropChances = wavesGetTunnelDropChances()
      if (self.defenseTunnelDropper == nil) then
         if (math.random() < gorgesTunnelDropperChances) then
            self.defenseTunnelDropper = true
         else
            self.defenseTunnelDropper = false
         end
      end

      if (self.defenseTunnelDropper) then
         if (self.defenseGorgeTunnelLastDropTry == nil) then
            self.defenseGorgeTunnelLastDropTry = Shared.GetTime()
         end

         if (self.defenseGorgeTunnelLastDropTry + 1 < Shared.GetTime()) then
            self.defenseGorgeTunnelLastDropTry = Shared.GetTime()

            if (#GetEntitiesForTeamWithinRange("Player", 1, self:GetOrigin(), 20) > 0) then
               local orig = GetLocationAroundFor(self:GetOrigin(), kTechId.Armory, 4)
               if (orig) then
                  orig = orig + Vector(0, -0.6, 0)
                  local t = CreateEntity(TunnelEntrance.kMapName, orig, 2)
                  if (t) then
                     self.defenseTunnelDropper = false
                  end
               end
            end
         end
      end
   end
end
