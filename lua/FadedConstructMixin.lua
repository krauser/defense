local constructMixinGetCanConstruct = ConstructMixin.GetCanConstruct
function ConstructMixin:GetCanConstruct(constructor)
   if (HasMixin(self, "GhostStructure") and self:GetIsGhostStructure())
   then
      if (self.def_buy_done ~= true) then
         if (constructor and constructor:isa("Marine") and constructor:GetTeamNumber() == 1) then
            local cost = LookupTechData(self:GetTechId(), kTechDataCostKey) * 2

            -- if (constructor.def_last_build_err_msg == nil) then
            --    constructor.def_last_build_err_msg = Shared.GetTime()
            -- end

            -- if (constructor.def_last_build_err_msg + 1 < Shared.GetTime()) then
            if (constructor:GetResources() < cost) then
               -- constructor.def_last_build_err_msg = Shared.GetTime()
               -- constructor:FadedMessage("Not enough PRes (need " .. tostring(cost) .. " or more)")
            else
               return constructMixinGetCanConstruct(self, constructor)
               -- constructor:AddResources(-cost)
               -- self.def_buy_done = true
            end
            -- end
         end
      end
      if (self.def_buy_done == true) then
         return constructMixinGetCanConstruct(self, constructor)
      end
   else
      return constructMixinGetCanConstruct(self, constructor)
   end
   return false
end

if Server then

   local ConstructMixinOnUse = ConstructMixin.OnUse
   function ConstructMixin:OnUse(player, elapsedTime, useSuccessTable)
      useSuccessTable.useSuccess = false
      if (player and player:isa("Marine") and player:GetTeamNumber() == 1
             and HasMixin(self, "GhostStructure") and self:GetIsGhostStructure())
      then
         local cost = LookupTechData(self:GetTechId(), kTechDataCostKey)

         if (player.def_last_build_err_msg == nil) then
            player.def_last_build_err_msg = Shared.GetTime()
         end

         if (player:GetResources() < cost) then
            if (player.def_last_build_err_msg + 0.6 < Shared.GetTime()) then
               player.def_last_build_err_msg = Shared.GetTime()
               player:FadedMessage("You need at least " .. tostring(cost) .. " res")
            end
            return false
         else
            if (ConstructMixinOnUse) then
               return ConstructMixinOnUse(self, player, elapsedTime, useSuccessTable)
            end
            -- player:AddResources(-cost)
            -- self.def_buy_done = true
         end
      end
      if (ConstructMixinOnUse) then
         return ConstructMixinOnUse(self, player, elapsedTime, useSuccessTable)
      end
      return false
   end


   local constructMixinKill = ConstructMixin.Kill
   function ConstructMixin:Kill(attacker, doer, point, direction)
      -- local allowed_ent = {Armory.kMapName, PhaseGate.kMapName, Observatory.kMapName, PrototypeLab.kMapName, InfantryPortal.kMapName, ArmsLab.kMapName}
      local allowed_ent = {PhaseGate.kMapName, InfantryPortal.kMapName, Observatory.kMapName, PrototypeLab.kMapName}

      if (constructMixinKill) then
         constructMixinKill(self, attacker, doer, point, direction)
      end

      if (HasMixin(self, "GhostStructure") and not self:GetIsGhostStructure()) then
         for _, entname in ipairs(allowed_ent) do
            if (attacker and (attacker:isa("Alien")
                                 or attacker:isa("Babbler")
                                 or (attacker.GetTeamNumber and attacker:GetTeamNumber() == 2))
                and entname == self:GetMapName()) then
               local new = CreateEntity(self:GetMapName(), self:GetOrigin(), self:GetTeamNumber())
               if (new) then
                  new:SetAngles(self:GetAngles())
               end
            end
         end
      end
   end
end
