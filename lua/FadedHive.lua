if (Server) then

   local hiveOnTakeDamage = Hive.OnTakeDamage
   function Hive:OnTakeDamage(damage, attacker, doer, point, direction, damageType, preventAlert)
      hiveOnTakeDamage(self, damage, attacker, doer, point, direction, damageType, preventAlert)
      if (attacker and attacker:isa("Player") and attacker:GetTeamNumber() == 1) then

         if (self.def_hive_last_signal == nil) then
            self.def_hive_last_signal = Shared.GetTime()
         end
         if (self.def_hive_last_signal + 0.4 < Shared.GetTime()) then
            skipTraderWave()
            self.def_hive_last_signal = Shared.GetTime()

            for _, lf in ipairs({"Alien", "Babbler"}) do
               for _, a in ipairs(GetEntitiesForTeam(lf, 2)) do

                  -- Only redirect alien that are directly busy
                  if (DefIsVirtual(a)) then
                     if (#GetEntitiesForTeamWithinRange("Player", 1, a:GetOrigin(), 15) == 0) then

                        a.defenseTargetId = attacker:GetId()
                        a.def_hive_defense = Shared.GetTime()
                        -- a:GiveOrder(o, attacker:GetId(), attacker:GetOrigin(), nil, true, false)
                     end
                  end
               end
            end


            -- local id = FindNearestEntityId("Egg", self:GetOrigin())
            -- local nearest_egg = Shared.GetEntity(id)
            if (isServerLoadMin()) then
               -- if (self.def_last_hatch == nil) then
               --    self.def_last_hatch = Shared.GetTime()
               -- end

               -- if (self.def_last_hatch + 0.5 < Shared.GetTime()) then -- Hatch an egg each N s at most
               --    self.def_last_hatch = Shared.GetTime()

               for _, egg in ipairs(GetEntitiesForTeamWithinRange("Egg", 2, self:GetOrigin(), 25)) do

                  if (egg and egg:GetCreationTime() + 6 < Shared.GetTime()) then
                     local egg_orig = egg:GetOrigin()

                     local rand = math.random()
                     local alien = nil
                     local lifeform_name = nil

                     if (rand > 0.97 and getUnspawnedLifeform(Onos.kMapName) > 0) then
                        lifeform_name = Onos.kMapName
                     elseif (rand > 0.93 and getUnspawnedLifeform(Fade.kMapName) > 0) then
                        lifeform_name = Fade.kMapName
                     elseif (rand > 0.86 and getUnspawnedLifeform(Lerk.kMapName) > 0) then
                        lifeform_name = Lerk.kMapName
                     elseif (rand > 0.70 and getUnspawnedLifeform(Gorge.kMapName) > 0) then
                        lifeform_name = Gorge.kMapName
                     elseif (getUnspawnedLifeform(Skulk.kMapName) > 0) then
                        lifeform_name = Skulk.kMapName
                     end

                     if (lifeform_name) then
                        alien = spawnAlienCreature(lifeform_name, egg_orig)

                        if (alien) then
                           lifeformSpawned(lifeform_name)

                           egg:TriggerEffects("egg_death")
                           DestroyEntity(egg)
                           alien:SetOrigin(egg_orig)
                        end
                        break
                     end
                  end
               end
               -- end
            end
         end

      end

   end
end


local hiveOnUpdate = Hive.OnUpdate
function Hive:OnUpdate(deltaTime)
   hiveOnUpdate(self, deltaTime)
   defenseRandomAlienBuildSpawn(self)
end

if (Server) then
   local hiveOnKill = Hive.OnKill
   function Hive:OnKill(deltaTime)
      ConstructMiniMarineBase(self:GetOrigin())
      FadedSpawnBuilding(kTechId.InfantryPortal, InfantryPortal.kMapName, self:GetOrigin(), Vector(0, -0.6, 0))
      hiveOnKill(self, deltaTime)
      forceNextWave() -- Kill all the aliens, and end the current wave
   end
end

-- Nobody can com
function Hive:GetIsPlayerValidForCommander(player)
   return false --player ~= nil and player:isa("Alien") and CommandStructure.GetIsPlayerValidForCommander(self, player)
end
