
function ViewModel:OnGetIsRelevant(player)
   return self:GetTeamNumber() == 2 and self.client
end

function Fade:OnGetIsRelevant(player)
   return self:GetTeamNumber() == 2 and self.client
end

local function _setRelevancyCustom(self, relevancy)
   if (Shared.GetMapName() == "ns2_kf_farm") then
      kMaxRelevancyDistance = 60
      if (relevancy) then
         self:SetRelevancyDistance(relevancy)
      else
         self:SetRelevancyDistance(kMaxRelevancyDistance)
      end
   elseif (Shared.GetMapName() == "ns2_def_troopers") then
      kMaxRelevancyDistance = 50
      if (relevancy) then
         self:SetRelevancyDistance(relevancy)
      else
         self:SetRelevancyDistance(kMaxRelevancyDistance)
      end
   end
end

local playerOnCreate = Player.OnCreate
function Player:OnCreate(deltatime)
   playerOnCreate(self, deltatime)
   _setRelevancyCustom(self)
end

local alienOnCreate = Alien.OnCreate
function Alien:OnCreate(deltatime)
   alienOnCreate(self, deltatime)
   _setRelevancyCustom(self)
end

local projectileOnCreate = Projectile.OnCreate
function Projectile:OnCreate(deltatime)
   projectileOnCreate(self, deltatime)
   _setRelevancyCustom(self)
end

local predictedProjectileOnCreate = PredictedProjectile.OnCreate
function PredictedProjectile:OnCreate(deltatime)
   predictedProjectileOnCreate(self, deltatime)
   _setRelevancyCustom(self)
end

local grenadeOnCreate = Grenade.OnCreate
function Grenade:OnCreate(deltatime)
   grenadeOnCreate(self, deltatime)
   _setRelevancyCustom(self)
end

local resourcePointOnCreate = ResourcePoint.OnInitialized
function ResourcePoint:OnInitialized(deltatime)
   resourcePointOnCreate(self, deltatime)
   _setRelevancyCustom(self, 0)
end

TechPoint.kModelName = PrecacheAsset("models/misc/tech_point/tech_point.model")
local kGraphName = PrecacheAsset("models/misc/tech_point/tech_point.animation_graph")

-- Same as vanilla but with the AttachEffect removed (model is set to nil in Gamerules()
function TechPoint:OnInitialized()

   ScriptActor.OnInitialized(self)

   self:SetModel(TechPoint.kModelName, kGraphName)

   self:SetTechId(kTechId.TechPoint)

   self.extendAmount = math.min(1, math.max(0, self.extendAmount))

   if Server then

      -- 0 indicates all teams allowed for random selection process.
      self.allowedTeamNumber = self.teamNumber or 0
      self.smashed = false
      self.smashScouted = false
      self.showObjective = false
      self.occupiedTeam = 0

      -- This Mixin must be inited inside this OnInitialized() function.
      if not HasMixin(self, "MapBlip") then
         InitMixin(self, MapBlipMixin)
      end

      -- self:SetRelevancyDistance(Math.infinity)
      self:SetExcludeRelevancyMask(bit.bor(kRelevantToTeam1, kRelevantToTeam2, kRelevantToReadyRoom))

   elseif Client then

      InitMixin(self, UnitStatusMixin)

      -- local coords = self:GetCoords()
      -- self:AttachEffect(TechPoint.kTechPointEffect, coords)
      -- self:AttachEffect(TechPoint.kTechPointLightEffect, coords, Cinematic.Repeat_Loop)

   end
   if (Shared.GetMapName() == "ns2_kf_farm") then
      _setRelevancyCustom(self, 0)
   end
end
