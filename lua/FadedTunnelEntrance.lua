
-- function TunnelEntrance:GetCanDigest(player)
--    return false
-- end

if (Server) then
   local tunnelEntranceOnUpdate = TunnelEntrance.OnUpdate
   function TunnelEntrance:OnUpdate(deltaTime)
      tunnelEntranceOnUpdate(self, deltaTime)
      self.connected = true
      defenseRandomAlienBuildSpawn(self)
   end

   -- Added function
   function TunnelEntrance:DefenseSpawnEntity(mapName)
      return
   end


   function TunnelEntrance:UpdateConnectedTunnel()

      local hasValidTunnel = self.tunnelId ~= nil and Shared.GetEntity(self.tunnelId) ~= nil

      if hasValidTunnel or not self:GetIsBuilt() then
         return
      end

      foundTunnel = CreateEntity(Tunnel.kMapName,
                                 Vector(-100 - math.random()*100,
                                           -100 - math.random()*100,
                                           -100 - math.random()*100),
                                 self:GetTeamNumber())

      foundTunnel:AddExit(self)
      self.tunnelId = foundTunnel:GetId()
   end

end


-- Tunnel are only cosmetics to allow the mod to spawn aliens closer
function TunnelEntrance:GetCanBeUsed(player, useSuccessTable)
   useSuccessTable.useSuccess = false--not DefIsVirtual(player)
end

function TunnelEntrance:GetCanBeUsedConstructed()
    return false
end

function TunnelUserMixin:GetIsEnteringTunnel()
    return false--self.tunnelNearby and self.enterTunnelDesired
end
