-- ======= Copyright (c) 2003-2013, Unknown Worlds Entertainment, Inc. All rights reserved. =======
--
-- lua\Weapons\Marine\GasGrenade.lua
--
--    Created by:   Andreas Urwalek (andi@unknownworlds.com)
--
-- ========= For more information, visit us at http://www.unknownworlds.com =====================

Script.Load("lua/Weapons/PredictedProjectile.lua")
Script.Load("lua/OwnerMixin.lua")

class 'HealGrenade' (PredictedProjectile)
local kHealthSound = PrecacheAsset("sound/NS2.fev/marine/common/health")

HealGrenade.kMapName = "healgrenadeprojectile"
HealGrenade.kModelName = PrecacheAsset("models/marine/grenades/gr_nerve_world.model")
HealGrenade.kUseServerPosition = false

HealGrenade.kRadius = 0.085
HealGrenade.kClearOnImpact = true
HealGrenade.kClearOnEnemyImpact = true

local networkVars =
   {
      -- releaseGas = "boolean",
      -- nextRelease = "time"
   }

-- local kGasReleaseDelay = 0.34

AddMixinNetworkVars(BaseModelMixin, networkVars)
AddMixinNetworkVars(ModelMixin, networkVars)
AddMixinNetworkVars(TeamMixin, networkVars)


local function TimeUp(self)
   DestroyEntity(self)
end

function HealGrenade:OnCreate()

   PredictedProjectile.OnCreate(self)

   InitMixin(self, BaseModelMixin)
   InitMixin(self, ModelMixin)
   InitMixin(self, TeamMixin)
   InitMixin(self, DamageMixin)

   if Server then

      self:AddTimedCallback(HealGrenade.Detonate, 1.5)


      -- self:AddTimedCallback(HealGrenade.ReleaseGas, kGasReleaseDelay)
      -- self:AddTimedCallback(HealGrenade.UpdateNerveGas, 1)
   end

   -- self:SetUpdates(true)
   -- self.releaseGas = false
   -- self.nextRelease = Shared.GetTime() + kGasReleaseDelay
   self.clientGasReleased = false
end

-- local healGrenadeOnInitialized = HealGrenade.OnInitialized
-- function HealGrenade:OnInitialized()
--    healGrenadeOnInitialized(self)
--    self:
-- end

if (Server) then
   function HealGrenade:Detonate()

      local orig = self:GetOrigin()
      local team = self:GetTeamNumber()

      local hc = CreateEntity(HealCloud.kMapName, orig, team)
      hc:SetOwner(self:GetOwner())
      DestroyEntity(self)
   end
end

function HealGrenade:ProcessHit(targetHit, surface)
   local orig = self:GetOrigin()
   StartSoundEffectAtOrigin(kHealthSound, orig)
   if (Server) then
      self:Detonate()
   end
   return true
end

if Client then

   function HealGrenade:OnUpdateRender()

      PredictedProjectile.OnUpdateRender(self)

   end

elseif Server then

   function HealGrenade:OnUpdate(deltatime)
      PredictedProjectile.OnUpdate(self, deltatime)
   end

   -- function HealGrenade:ReleaseGas()
   --    self.releaseGas = true
   -- end

end

Shared.LinkClassToMap("HealGrenade", HealGrenade.kMapName, networkVars)

class 'HealCloud' (Entity)

HealCloud.kMapName = "healcloud"
HealCloud.kEffectName = PrecacheAsset("cinematics/marine/healcloud.cinematic")


local kSpreadDelay = 0.64

local kCloudMoveSpeed = 1.8

local networkVars =
   {
   }

AddMixinNetworkVars(TeamMixin, networkVars)

function HealCloud:OnCreate()

   Entity.OnCreate(self)

   InitMixin(self, TeamMixin)
   InitMixin(self, DamageMixin)

   self.creationTime = Shared.GetTime()

   if Server then


      self:AddTimedCallback(TimeUp, kFadedHealCloudLifetime)
      self:AddTimedCallback(HealCloud.DoOnDetonationHealing, 0)
      self:AddTimedCallback(HealCloud.DoHealing, kFadedHealCloudRefreshRate)
      InitMixin(self, OwnerMixin)

   end

   if Client then
      self.light = Client.CreateRenderLight()
      self.light:SetType( RenderLight.Type_Point )
      self.light:SetColor( Color(0xff, 0xff, 0xff) )
      self.light:SetIntensity( .01 )
      self.light:SetRadius( 1.4 )

      self.light:SetAtmosphericDensity(1)
      self.light:SetCastsShadows(false)
      self.light:SetSpecular(false)

      self.light:SetIsVisible(true)

      self.lightTimer = nil
   end

   self:SetUpdates(true)
   -- self:SetRelevancyDistance(kMaxRelevancyDistance)

end

if Client then

   function HealCloud:OnInitialized()

      local cinematic = Client.CreateCinematic(RenderScene.Zone_Default)
      cinematic:SetCinematic(HealCloud.kEffectName)
      cinematic:SetParent(self)
      cinematic:SetCoords(Coords.GetIdentity())

   end

end


function HealCloud:SetEndPos(endPos)
   self.endPos = Vector(endPos)
end


local function GetRecentlyDamaged(entityId, time)

   for index, pair in ipairs(gNerveGasDamageTakers) do
      if (pair[1] == entityId and pair[2] > time) then
         return true
      end
   end

   return false

end

local function SetRecentlyDamaged(entityId)
   for index, pair in ipairs(gNerveGasDamageTakers) do
      if pair[1] == entityId then
         table.remove(gNerveGasDamageTakers, index)
      end
   end
   table.insert(gNerveGasDamageTakers, {entityId, Shared.GetTime()})
end

local function GetIsInCloud(self, entity, radius)

   local targetPos = entity.GetEyePos and entity:GetEyePos() or entity:GetOrigin()
   return ((self:GetOrigin() - targetPos):GetLength() <= radius)

end

-- if (HasMixin(entity, "Fire")) then
--    entity:SetOnFire(self:GetOwner(), self)
-- end



function HealCloud:GetRadius()
   return (kFadedHealCloudRadius)
end


function HealCloud:DoOnDetonationHealing()
   self:DoHealing()
   return false -- stop callback
end

function HealCloud:DoHealing()
   -- hurt both marines and the Faded
   local player = self:GetOwner()
   if (player) then
      local is_medic = (player and player:IsMedic())
      local is_GL_healcloud = is_medic and player and player:GetActiveWeapon()
         and player:GetActiveWeapon():isa("GrenadeLauncher")
      local heal_per_wave = kFadedHealCloudHpPerRefresh

      if (is_medic and is_GL_healcloud) then
         heal_per_wave = kFadedGLHealCloudHpPerRefresh
      end
      for _, entity in ipairs(GetEntitiesWithMixinWithinRange("Live", self:GetOrigin(), 2*kFadedHealCloudRadius)) do

         if (entity:isa("Player") and entity:GetTeamNumber() == self:GetTeamNumber()
             and GetIsInCloud(self, entity, self:GetRadius())) then
            if (entity:GetMaxHealth() > entity:GetHealth()) then
               entity:AddHealth(heal_per_wave, false, true)
               StartSoundEffectAtOrigin(kHealthSound, entity:GetOrigin())
               if (Server) then
                  if (is_medic) then
                     player:ClearOrders()
                  end
                  player:AddScore(1) -- Point(s) per single heal
               end
            end
         end
      end
   end
   return true
end

function HealCloud:GetDeathIconIndex()
   return kDeathMessageIcon.GasGrenade
end

if (Client) then

   function HealCloud:OnUpdateRender()
      if (Entity.OnUpdateRender) then
         Entity.OnUpdateRender(self)
      end
      if self.light ~= nil then
         if ((self.creationTime + kFadedHealCloudLifetime)-1 <= Shared.GetTime())
         then
            local intensity = 0
            intensity = ((self.creationTime + kFadedHealCloudLifetime) - 0.05)
            intensity = (intensity - Shared.GetTime()) / 100
            self.light:SetIntensity(intensity)
         else
            self.light:SetRadius(self:GetRadius() * 2)
         end
         -- self.light:SetIntensity(self.light:GetIntensity()
         --                            + (math.random(1, 10) - 3) / 200)
         self.light:SetCoords(
            Coords.GetLookIn(
               self:GetOrigin() + Vector(0, 1.1, 0),
               self:GetCoords().zAxis))
         self.lightTimer = Shared.GetTime()
      end
   end

   function HealCloud:OnDestroy()
      if (Entity.OnDestroy) then
         Entity.OnDestroy(self)
      end
      if self.light ~= nil then
         Client.DestroyRenderLight(self.light)
         self.light = nil
      end
   end
end

if Server then

   function HealCloud:OnUpdate(deltaTime)
      if self.endPos then
         local newPos = SlerpVector(self:GetOrigin(), self.endPos, deltaTime * kCloudMoveSpeed)
         self:SetOrigin(newPos)
      end

   end

end

function HealCloud:GetDamageType()
   return kDamageType.Flame
end

GetTexCoordsForTechId(kTechId.GasGrenade)
gTechIdPosition[kTechId.HealGrenade] = kDeathMessageIcon.GasGrenade

Shared.LinkClassToMap("HealCloud", HealCloud.kMapName, networkVars)
