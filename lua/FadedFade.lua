
function Fade:GetBaseArmor()
    return kFadeArmor * getAlienHealthScalar()
end

function Fade:GetBaseHealth()
   return kFadeHealth * getAlienHealthScalar()
end

local fadeGetMaxSpeed = Fade.GetMaxSpeed
function Fade:GetMaxSpeed(possible)
   local speed = fadeGetMaxSpeed(self, possible) * getAlienSpeedScalar()

   speed = AlienGetSpeedBonusIfFar(self, speed)
   if (self.electrified) then
      speed = speed / kFadedPulseSlowDownFactor
   end
   return (speed)
end
