
local minimap_state = false

function defenseToggleMinimap(state)
   if (state) then
      minimap_state = state
   else
      minimap_state = not minimap_state
   end
end

local force_minimap = false
local last_check = Shared.GetTime()
local function isDetectedOnMinimap(self)
   -- if (DefIsVirtual(self)) then
   if (minimap_state or (self.GetIsDetected and self:GetIsDetected())) then
      return true
   end
   if (last_check + 0.5 < Shared.GetTime()) then
      local nb = #GetEntitiesForTeam("Alien", 2)
      force_minimap = (0 < nb and nb <= 5)
      last_check = Shared.GetTime()
   end
   if (force_minimap == true) then
      return true
   end
   -- end
   return false
end

function DropPack:OnGetMapBlipInfo()
   local blipType = nil
   local blipTeam = nil
   blipType = kMinimapBlipType.Undefined
   blipTeam = 1
   return true, blipType, blipTeam, false, false
end

function Babbler:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Undefined
   local blipTeam = 2

   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function Gorge:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Gorge
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end
function Skulk:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Skulk
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end
function Fade:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Fade
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function Lerk:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Lerk
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function Onos:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Onos
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function BoneWall:OnGetMapBlipInfo()
   local blipType = -1
   local blipTeam = 2
   if (minimap_state) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function TunnelEntrance:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.TunnelEntrance
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function Hydra:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Hydra
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function Whip:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Whip
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function Hive:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Hive
   local blipTeam = 2
   -- if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   -- end
   return true, blipType, blipTeam, false, false
end

function Drifter:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Drifter
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function Veil:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Veil
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function Spur:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Spur
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end
function Shell:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Shell
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function Crag:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Crag
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function Shift:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Shift
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function Veil:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Veil
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end

function Egg:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Egg
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end


function Clog:OnGetMapBlipInfo()
   local blipType = kMinimapBlipType.Clog
   local blipTeam = 2
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return true, blipType, blipTeam, false, false
end


function ResourcePoint:OnGetMapBlipInfo()
   local blipType = -1
   local blipTeam = -1
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return false, blipType, blipTeam, false, false
end


function TechPoint:OnGetMapBlipInfo()
   local blipType = -1
   local blipTeam = -1
   if (isDetectedOnMinimap(self)) then
      blipTeam = -1
   end
   return false, blipType, blipTeam, false, false
end

