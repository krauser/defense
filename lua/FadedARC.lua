kARCDamageType = kDamageType.Normal
ARC.kSplashRadius = 10 -- 7
ARC.kBarrelMoveRate = 100 -- 100
ARC.kMinFireRange = 6

local ARCOnCreate = ARC.OnCreate
function ARC:OnCreate()
   ARCOnCreate(self)
   local entityList = Shared.GetEntitiesWithClassname("GameInfo")
   if (entityList:GetSize() > 0) then
      local gameInfo = entityList:GetEntityAtIndex(0)
      local state = gameInfo:GetState()

      if (state ~= kGameState.Started) then
         self.deployMode = ARC.kDeployMode.Deployed
      end
   end
end

function ARC:GetCanFireAtTarget(target, targetPoint)

    if target == nil then
        return false
    end

    if not HasMixin(target, "Live") or not target:GetIsAlive() then
        return false
    end

    if not GetAreEnemies(self, target) then
        return false
    end

    -- if not target.GetReceivesStructuralDamage or not target:GetReceivesStructuralDamage() then
    --     return false
    -- end

    -- don't target eggs (they take only splash damage)
    if target:isa("Egg") then
        return false
    end

    return self:GetCanFireAtTargetActual(target, targetPoint)

end

function ARC:GetCanFireAtTargetActual(target, targetPoint)

    -- if not target.GetReceivesStructuralDamage or not target:GetReceivesStructuralDamage() then
    --     return false
    -- end

    -- don't target eggs (they take only splash damage)
    if target:isa("Egg") then
        return false
    end

    -- if not target:GetIsSighted() and not GetIsTargetDetected(target) then
    --     return false
    -- end

    local distToTarget = (target:GetOrigin() - self:GetOrigin()):GetLengthXZ()
    if (distToTarget > ARC.kFireRange) or (distToTarget < ARC.kMinFireRange) then
        return false
    end

    if (target and not DefIsVirtual(target)) then -- Do not target players
       return false
    end

    return true

end


if (Server) then

   local function PerformAttack(self)

      if self.targetPosition then

         self:TriggerEffects("arc_firing")
         -- Play big hit sound at origin

         -- don't pass triggering entity so the sound / cinematic will always be relevant for everyone
         GetEffectManager():TriggerEffects("arc_hit_primary", {effecthostcoords = Coords.GetTranslation(self.targetPosition)})

         -- Faded: Added here
         local hitEntities = {}
         local _hitEntities = GetEntitiesWithinRange("Alien", self.targetPosition, ARC.kSplashRadius)
         -- Shared:FadedMessage("Entities hit: " .. tostring(#hitEntities))

         -- Do damage to every target in range
         local max_hit_per_shot = 5
         for i = 1, (math.min(max_hit_per_shot, #_hitEntities)) do
            if (DefIsVirtual(_hitEntities[i])) then -- Do not kill players
               table.insert(hitEntities, _hitEntities[i])
            end
         end

         -- Kill all babblers too in range (can't survive that anyway)
         local babblers_hit = GetEntitiesWithinRange("Babbler", self.targetPosition, ARC.kSplashRadius)
         for _, b in ipairs(babblers_hit) do
            b:Kill()
         end
         local cyst_hit = GetEntitiesWithinRange("Cyst", self.targetPosition, ARC.kSplashRadius)
         for _, b in ipairs(cyst_hit) do
            b:Kill()
         end

         RadiusDamage(hitEntities, self.targetPosition, ARC.kSplashRadius, ARC.kAttackDamage, self, true)

         -- Play hit effect on each
         for index, target in ipairs(hitEntities) do

            if HasMixin(target, "Effects") then
               target:TriggerEffects("arc_hit_secondary")
            end
            -- local targetOrigin = GetTargetOrigin(target)

            -- -- Trace line to each target to make sure it's not blocked by a wall
            -- local wallBetween = false
            -- local distanceFromTarget = (targetOrigin - self.targetPosition):GetLength()
            -- local damageDirection = targetOrigin - self.targetPosition
            -- damageDirection:Normalize()

            -- self:DoDamage(ARC.kAttackDamage, target, target:GetOrigin(), damageDirection, "none")
            -- Shared:FadedMessage("Damages done (" .. tostring(ARC.kAttackDamage))
         end

      end

      -- reset target position and acquire new target
      self.targetPosition = nil
      self.targetedEntity = Entity.invalidId

   end


   -- Same as NS2
   function ARC:OnTag(tagName)

      PROFILE("ARC:OnTag")

      if tagName == "fire_start" then
         PerformAttack(self)
      elseif tagName == "target_start" then
         self:TriggerEffects("arc_charge")
      elseif tagName == "attack_end" then
         self:SetMode(ARC.kMode.Targeting)
      elseif tagName == "deploy_start" then
         self:TriggerEffects("arc_deploying")
      elseif tagName == "undeploy_start" then
         self:TriggerEffects("arc_stop_charge")
      elseif tagName == "deploy_end" then

         -- Clear orders when deployed so new ARC attack order will be used
         self.deployMode = ARC.kDeployMode.Deployed
         self:ClearOrders()
         -- notify the target selector that we have moved.
         self.targetSelector:AttackerMoved()

         self:AdjustMaxHealth(kARCDeployedHealth)

         local currentArmor = self:GetArmor()
         if currentArmor ~= 0 then
            self.undeployedArmor = currentArmor
         end

         self:SetMaxArmor(kARCDeployedArmor)
         self:SetArmor(self.deployedArmor)

      elseif tagName == "undeploy_end" then

         self.deployMode = ARC.kDeployMode.Undeployed

         self:AdjustMaxHealth(kARCHealth)
         self.deployedArmor = self:GetArmor()
         self:SetMaxArmor(kARCArmor)
         self:SetArmor(self.undeployedArmor)

      end

   end

end -- Server
