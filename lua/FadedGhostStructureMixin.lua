
local kGhoststructureMaterial = PrecacheAsset("cinematics/vfx_materials/ghoststructure.material")

if (Server) then
    function GhostStructureMixin:OnTouchInfestation()

        -- if self:GetIsGhostStructure() and LookupTechData(self:GetTechId(), kTechDataNotOnInfestation, false) then
        --     ClearGhostStructure(self)
        -- end

    end
end

local function SharedUpdate(self, deltaTime)
    PROFILE("GhostStructureMixin:OnUpdate")
    if Server and self:GetIsGhostStructure() then

        -- -- check for enemies in range and destroy the structure, return resources to team
        -- local enemies = GetEntitiesForTeamWithinRange("Player", GetEnemyTeamNumber(self:GetTeamNumber()), self:GetOrigin() + Vector(0, 0.3, 0), GhostStructureMixin.kGhostStructureCancelRange)
        -- table.copy(GetEntitiesForTeamWithinRange("Drifter", GetEnemyTeamNumber(self:GetTeamNumber()), self:GetOrigin() + Vector(0, 0.3, 0), GhostStructureMixin.kGhostStructureCancelRange), enemies, true)

        -- for _, enemy in ipairs (enemies) do

        --     if enemy:GetIsAlive() then

        --         ClearGhostStructure(self)
        --         break

        --     end

        -- end

    elseif Client then

        local model = nil
        if HasMixin(self, "Model") then
            model = self:GetRenderModel()
        end

        if model then

            if self:GetIsGhostStructure() then

                self:SetOpacity(0, "ghostStructure")

                if not self.ghostStructureMaterial then
                    self.ghostStructureMaterial = AddMaterial(model, kGhoststructureMaterial)
                end

            else

                self:SetOpacity(1, "ghostStructure")

                if RemoveMaterial(model, self.ghostStructureMaterial) then
                    self.ghostStructureMaterial = nil
                end

            end

        end

    end

end

function GhostStructureMixin:OnUpdate(deltaTime)
   SharedUpdate(self, deltaTime)
end

function GhostStructureMixin:OnProcessMove(input)
   SharedUpdate(self, input.time)
end

if (Server) then

   -- If we start constructing, make us no longer a ghost
   local GhostStructureMixinOnConstruct = GhostStructureMixin.OnConstruct
   function GhostStructureMixin:OnConstruct(builder, buildPercentage)
      if (self:GetIsGhostStructure() and self.def_buy_done ~= true) then
         local cost = LookupTechData(self:GetTechId(), kTechDataCostKey) * 2
         builder:AddResources(-cost)
         self.def_buy_done = true
      end
      if (GhostStructureMixinOnConstruct) then
         GhostStructureMixinOnConstruct(self, builder, buildPercentage)
      end
   end

   function GhostStructureMixin:OnTakeDamage()
      -- if self:GetIsGhostStructure() and self:GetHealthFraction() < 0.25 then
      --     ClearGhostStructure(self)
      -- end
   end
end
