------------ Taken from CommanderBrain.lua
local function GetRandomBuildPosition(techId, aroundPos, maxDist)

    local extents = GetExtents(techId)
    local validationFunc = LookupTechData(techId, kTechDataRequiresInfestation, nil) and GetIsPointOnInfestation or nil
    local randPos = GetRandomSpawnForCapsule(extents.y, extents.x, aroundPos, 0.01, maxDist, EntityFilterAll(), validationFunc)
    return randPos
end
--------------

local function _SmoothPathPoints(points, dst, maxPoints, maxDistance)

   local i = 1
   local a = 0.15 -- alpha for color
   local new_points = {}

   local numPoints = #points
   local point1 = points[i]
   local point2 = points[i]
   while (i + 3 < numPoints and i < maxPoints) do

      point1 = points[i]
      point2 = points[i + 3]

      if (point1 and point2) then
         -- If the distance between two points is large, add intermediate points

         local delta = point2 - point1
         local distance = delta:GetLength()
         local numNewPoints = math.floor(distance / maxDistance)

         for j = 1, numNewPoints do

            local f = j / numNewPoints
            local newPoint = point1 + delta * f
            -- if table.find(points, newPoint) == nil then
            table.insert(new_points, newPoint)
            -- end
         end
         -- DebugWireSphere(points[i], 0.06,
         --                 kFadedHuntPathDuration,
         --                 0, 0.8, 0, a) -- r, g, b, a
      end
      i = i + 3
   end

   local delta = point2 - dst
   local distance = delta:GetLength()
   local numNewPoints = math.floor(distance / maxDistance)

   for j = 1, numNewPoints do

      local f = j / numNewPoints
      local newPoint = point1 + delta * f
      -- if table.find(points, newPoint) == nil then
      table.insert(new_points, newPoint)
      -- end
   end

   return new_points
end

function getPath(a, b, smooth)
   local points = PointArray()

   local reacheable = Pathing.GetPathPoints(a, b, points)
   if (smooth and reacheable and #points > 0) then
      points = _SmoothPathPoints(points, b, 1000, 0.2)
   end
   return points
end

function getPathDist(a, b)
   return #getPath(a, b)
end
function getPathDistSort(a, b)
   return #getPath(a:GetOrigin(), b:GetOrigin())
end
function getPathDistReverseSort(a, b)
   return #getPath(b:GetOrigin(), a:GetOrigin())
end

-- Faded
function sortByPathProximity(alien, targets)
   local i = 1
   local sorted_targets = {}
   -- local points = PointArray()
   -- local dist = 0.7

   if (alien) then
      for _, target in ipairs(targets)
      do
         i = 0
         local added = false
         local t1 = getPathDist(alien:GetOrigin(), target:GetOrigin())--alien:GetOrigin():GetDistanceTo(target:GetOrigin())
         for it, target2 in ipairs(sorted_targets) do
            local t2 = getPathDist(alien:GetOrigin(), target2:GetOrigin())--alien:GetOrigin():GetDistanceTo(target2:GetOrigin())
            if (t1 < t2) then
               table.insert(sorted_targets, it, target)
               added = true
               break
            end
         end
         if (added == false) then
            table.insert(sorted_targets, #sorted_targets + 1, target)
         end
      end
      -- for it, rt in ipairs(sorted_rt_point) do
      --    Shared:FadedMessage("Nearest marine from rt n'" .. tostring(it)
      --                           .. " is at " .. _getDistFromNeareastMarine(rt) .. "m")
      -- end
      return (sorted_targets)
   end
end

--------------------------------------

function isValidAlienSpawnPointOrig(orig, relax)
   local marines = nil
   local IP = nil
   local PG = nil
   local armory = nil
   local unpowered_dist = 20
   if (not relax) then
      marines = GetEntitiesForTeamWithinRange("Player", 1, orig, 40)
      IP = GetEntitiesForTeamWithinRange("InfantryPortal", 1, orig, 30)
      PG = GetEntitiesForTeamWithinRange("PhaseGate", 1, orig, 30)
      armory = GetEntitiesForTeamWithinRange("Armory", 1, orig, 30)
   else
      marines = GetEntitiesForTeamWithinRange("Player", 1, orig, 35)
      IP = GetEntitiesForTeamWithinRange("InfantryPortal", 1, orig, 35)
      PG = GetEntitiesForTeamWithinRange("PhaseGate", 1, orig, 20)
      armory = GetEntitiesForTeamWithinRange("Armory", 1, orig, 20)
   end

   if (#marines ~= 0)
   then
      return false, "Too close from a marine"
   end

   for _, _a in ipairs(armory) do
      if (_a and (_a:GetIsPowered() or _a:GetOrigin():GetDistanceTo(orig) < unpowered_dist))
      then
         return false, "Too close from an armory"
      end
   end

   for _, _ip in ipairs(IP) do
      if (_ip and (_ip:GetIsPowered() or _ip:GetOrigin():GetDistanceTo(orig) < unpowered_dist))
      then
         return false, "Too close from an IP"
      end
   end

   for _, pg in ipairs(PG) do
      if (_pg and (_pg:GetIsPowered() or _pg:GetOrigin():GetDistanceTo(orig) < unpowered_dist))
      then
         return false, "Too close from a PG"
      end
   end
   return true, ""
end

function getRtPoints(classname)
   local rt = {}
   -- local cl = classname
   -- if (cl == nil) then
   --    cl = "ResourcePoint"
   -- end
   local rts = Shared.GetEntitiesWithClassname("ResourcePoint")
   for index, entity in ientitylist(rts) do
      if (isValidAlienSpawnPointOrig(entity:GetOrigin())) then
         table.insert(rt, entity)
      end
   end

   -- rts = Shared.GetEntitiesWithClassname("TechPoint")
   -- for index, entity in ientitylist(rts) do

   --    local marines = GetEntitiesForTeamWithinRange("Player", 1, entity:GetOrigin(), 35)
   --    local IP = GetEntitiesForTeamWithinRange("InfantryPortal", 1, entity:GetOrigin(), 35)
   --    local PG = GetEntitiesForTeamWithinRange("PhaseGate", 1, entity:GetOrigin(), 35)
   --    if (#marines == 0 and #IP == 0 and #PG == 0)
   --    then
   --       table.insert(rt, entity)
   --    end

   -- end

   if (#rt == 0) then -- Force at least 1 RT to be added
      for index, entity in ientitylist(rts) do
         table.insert(rt, entity)
         break
      end
   end
   return rt
end

local function _rotateRandom()
   local nb = 0
   -- random is more random (and don't give twice the same value), don't ask me why
   for i = 1, 10 do
      nb = nb + math.random()
   end
   return nb
end

function getRandomRT()
   local rt = getRtPoints()
   if (#rt == 0) then
      rt = getRtPoints("TechPoint")
   end

   _rotateRandom()
   rt_nb = math.random(1, #rt)
   return rt[rt_nb]
end

function getRandomRTAroundMarine()
   if (Shared.GetMapName() == "ns2_def_storm") then
      return getRandomRT() -- No need for proximity optimization on this map
   else
      local RTs = getRtPoints()
      local marines = GetEntitiesForTeam("Player", 1)
      if (#marines > 0) then
         _rotateRandom()
         local m = marines[math.random(1, #marines)]
         RTs = sortByPathProximity(m, RTs)
         -- Shared.SortEntitiesByDistance(self:GetOrigin(), targets)
         local rt_nb = math.random(1, math.min(#RTs, 4))
         return RTs[rt_nb]
      end
      return RTs[1]
   end
end

function getRandomRTAroundUnbuildHiveOrMarine()
   local rt = nil
   local uhive = {}
   local cur_hive = nil

   -- _rotateRandom()
   for _, hive in ipairs(GetEntitiesForTeam("Hive", 2)) do
      if (hive and hive:GetIsAlive() and hive.GetIsBuilt and not hive:GetIsBuilt()) then
         table.insert(uhive, hive)
      end
   end

   if (#uhive == 0 or math.random() < 0.8) then -- Only a fraction of aliens spawn near unbuild hives
      return getRandomRTAroundMarine()
   else
      local RTs = getRtPoints()

      cur_hive = uhive[math.random(1, #uhive)]
      RTs = sortByPathProximity(cur_hive, RTs)

      local rt_nb = math.random(1, math.min(#RTs, 4))
      return RTs[rt_nb]
   end
end

--------------------------------------

if (Server) then
   local nb_marines_alive = 0
   local nb_marine_alive_delay = Shared.GetTime()
   function getNbPlayerAliveForTeam1()
      if (nb_marine_alive_delay + 1 < Shared.GetTime()) then
         nb_marine_alive_delay = Shared.GetTime()

         local nb = 0
         for _, marine in ipairs(GetGamerules():GetTeam1():GetPlayers()) do
            if (marine ~= nil and (marine:isa("Player"))
                and marine:GetIsAlive()) then
               nb = nb + 1
            end
         end
         nb_marines_alive = nb
      end
      return nb_marines_alive
   end
end

function getScalar(base, nb, min_nb, max_nb, min, max)
   -- if (not min) then min = 0.4 end
   -- if (not max) then max = 1.5 end
   return Clamp(min + ((max - min) / 100) * ((nb - min_nb) / (max_nb - min_nb)) * 100, min, max)
   -- return min + ((max - min) / 100) * ((nb - min_nb) / (max_nb - min_nb)) * 100
end

function getScalarPlayers(base, marine_max_nb, min, max)
   if (not min) then min = 0.4 end
   if (not max) then max = 1.5 end
   -- if (Server) then
   --    local nb_marines_alive = getNbPlayerAliveForTeam1()
   --    -- local scalar_bonus = nb_marines_alive / marine_max_nb
   --    -- return Clamp(base * scalar_bonus, min, max)
   --    return Clamp(base * (min + ((max - min) / marine_max_nb) * nb_marines_alive), 0, max)
   -- end
   -- return 1
   return getScalar(base, getNbPlayerAliveForTeam1(), 1, marine_max_nb, min, max)
end

------------------------------------------

function spawnAlienCreature(mapName, ref_orig)
   -- local rt = getRandomRT()
   local spawnPoint = nil

   if (ref_orig == nil) then
      local rt = getRandomRTAroundUnbuildHiveOrMarine()
      ref_orig = rt:GetOrigin()
   end
   if (mapName ~= Contamination.kMapName) then
      -- spawnPoint = spawnPoint + Vector(0, 1, 0)
      spawnPoint = GetRandomBuildPosition(kTechId.Skulk, ref_orig, 5)
      -- spawnPoint = GetLocationAroundFor(ref_orig, kTechId.Onos, 5)
      local i = 0
      while (not spawnPoint and i < 10) do
         spawnPoint = GetRandomBuildPosition(kTechId.Skulk, ref_orig + Vector(math.random()-0.5, math.random(), math.random()-0.5), 5)
         if (spawnPoint and spawnPoint:GetDistanceTo(ref_orig) > 6) then
            spawnPoint = nil
         end
         i = i + 1
      end
      if (not spawnPoint) then
         spawnPoint = ref_orig + Vector(0, math.random()*3, 0)
      end
   end

   if (spawnPoint == nil) then
      spawnPoint = ref_orig
   end

   local ent = CreateEntity(mapName, spawnPoint, 2)
   if (ent) then
      -- local marines = GetEntitiesForTeam("Player", 1, orig)
      -- if (#marines > 0) then
      --    marines = sortByPathProximity(ent, marines)
      --    local max_points = 160
      --    local points = getPath(marines[1]:GetOrigin(), orig)
      --    if (#points > max_points) then -- Spawn alien closer
      --       for i = 0, 10 do -- Try 10 times, or abort
      --          orig = GetLocationAroundFor(points[max_points - i], kTechId.Onos, 5)
      --          if (orig) then
      --             ent:SetOrigin(orig)
      --             break
      --          end
      --       end
      --    end
      -- end

      local scalar = getAlienHealthScalar()
      ent:SetMaxHealth(Clamp(ent:GetMaxHealth() * scalar, 1, LiveMixin.kMaxHealth))
      ent:SetMaxArmor(Clamp(ent:GetMaxArmor() * scalar, 1, LiveMixin.kMaxArmor))
      ent:SetHealth(ent:GetMaxHealth())
      ent:SetArmor(ent:GetMaxArmor())

      if (ent:isa("Lerk")) then
         ent.defenseAggressiveAI = true
         ent.defenseAggressiveAIDistance = 50
         if (math.random() < 0.75) then
            ent.def_has_range_attack = true
         end

      elseif (ent:isa("Onos")) then
         ent.defenseAggressiveAI = true
         ent.defenseAggressiveAIDistance = 20
      elseif (ent:isa("Fade")) then
         ent.defenseAggressiveAI = true
         ent.defenseAggressiveAIDistance = 20
      elseif (ent:isa("Gorge")) then
         ent.defenseAggressiveAI = true
         ent.defenseAggressiveAIDistance = 20
         if (math.random() < 0.67) then
            ent.def_healing_gorgies = true
         else
            ent.def_healing_gorgies = false
         end

      elseif (ent:isa("Skulk")) then-- and math.random(1, 100) < 8) then
         ent.defenseAggressiveAI = true
         ent.defenseAggressiveAIDistance = 20
         if (Shared.GetMapName() == "ns2_def_troopers") then
            if (math.random() < 0.15) then
               ent.def_xeno_skulk = true
            end
         else
            if (math.random() < 0.25) then
               ent.def_xeno_skulk = true
            end
         end

      elseif (ent:isa("Babbler")) then
         --ent:SetOrigin(ent:GetOrigin())
         ent.brain = SkulkBrain()
         if (math.random() > 0.1) then
            ent.def_babbler_schield = true
         end
      end

      if (math.random() < 0.4) then
         ent.defenseAttackSentry = true
      end

      if (math.random() < 0.7) then
         ent.def_atk_most_wounded = true
      end

      -- ent.isHallucination = true
      -- InitMixin(ent, PlayerHallucinationMixin)
      -- InitMixin(ent, SoftTargetMixin)
      -- InitMixin(ent, OrdersMixin, { kMoveOrderCompleteDistance = kPlayerMoveOrderCompleteDistance })

   end
   -- SpawnSinglePlayer(ent, rt:GetOrigin())
   return ent
end

function randomBonusDrop()
   local nb_drop = #GetEntities("AmmoPack") + #GetEntities("MedPack") + #GetEntities("CatPack")
   if (nb_drop < 23) then
      local ent = nil
      local rt = nil

      rt = getRandomRT()

      for i = 0, 12 do -- Choose a RT with no drop if possible
         local hasRtDropAlready = false

         local _rt = getRandomRT()
         for _, dropName in ipairs({"AmmoPack", "MedPack", "CatPack"}) do
            if (#GetEntitiesForTeamWithinRange(dropName, 2, _rt:GetOrigin(), 3) > 0) then
               hasRtDropAlready = true
               break
            end
         end
         if (hasRtDropAlready == false) then
            rt = _rt
            break
         end
      end

      local rand = math.random()
      if (rand > 0.4) then
         ent = CreateEntity(AmmoPack.kMapName, rt:GetOrigin() + Vector(0,0.2,0), 1)
      elseif (rand > 0.2) then
         ent = CreateEntity(CatPack.kMapName, rt:GetOrigin() + Vector(0,0.2,0), 1)
      else
         ent = CreateEntity(MedPack.kMapName, rt:GetOrigin() + Vector(0,0.2,0), 1)
      end
   end
end
