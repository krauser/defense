function SniperRifle:GetHitPointPosition(player)
   local range = 100

   -- local eyePos  = player:GetEyePos()
   -- local startPoint = Vector(eyePos)
   local startPoint = self:GetBarrelPoint()
   local fireDirection = player:GetViewCoords().zAxis
   local kConeWidth = 0.1
   local filterEnts = {self, player}

   local extents = Vector(0.1, 0.1, 0.1)
   local trace = TraceMeleeBox(self, startPoint, fireDirection, extents, range, PhysicsMask.Flame, EntityFilterList(filterEnts))
   local lineTrace = Shared.TraceRay(startPoint,
                                     startPoint + range * fireDirection,
                                     CollisionRep.Damage,
                                     PhysicsMask.Flame, EntityFilterOne(player))

   if lineTrace.fraction < 0.8 then
      -- Shared:FadedMessage("Pistol:OnPrimaryAttack line trace OK")
      fireDirection = fireDirection + trace.normal * 0.55
      fireDirection:Normalize()

      return true, startPoint, lineTrace, fireDirection
   end
   return false, nil, nil, nil
end

function SniperRifle:arc_fire(player)

   local st, startPoint, lineTrace, fireDirection = self:GetHitPointPosition(player)
   if (st) then
      self:TriggerEffects("arc_firing")

      GetEffectManager():TriggerEffects("arc_hit_primary", {effecthostcoords = Coords.GetTranslation(lineTrace.endPoint)})

      -- Faded: Added here
      local hitEntities = GetEntitiesWithinRange("Alien", lineTrace.endPoint, ARC.kSplashRadius)

      -- Do damage to every target in range
      RadiusDamage(hitEntities, lineTrace.endPoint, 7, 120, self, true)

      if (Client) then
         local tracerVelocity = GetNormalizedVector(lineTrace.endPoint - startPoint) * kTracerSpeed
         local tracer = BuildTracer(startPoint, lineTrace.endPoint,
                                    tracerVelocity,
                                    kRailgunTracerEffectName,
                                    kRailgunTracerResidueEffectName)
         table.insert(Client.tracersList, tracer)
      end
      for index, target in ipairs(hitEntities) do

         if HasMixin(target, "Effects") then
            target:TriggerEffects("arc_hit_secondary")
         end
      end

   end
end

if Client then

    function SniperRifle:OnClientPrimaryAttackStart()
    end
    function SniperRifle:OnClientPrimaryAttackEnd()
    end
    function SniperRifle:UpdateAttackEffects(deltaTime)
    end
end

local firePrimary = Rifle.FirePrimary
function SniperRifle:FirePrimary(player)
   if (player.kFadedReserved) then
      self:arc_fire(player)
   else
      firePrimary(self, player)
      -- SpecialWeaponHandling(self, player, _CreateRagdoll, 60)
      -- _CreateRagdoll(self, player, player:GetOrigin() + Vector(2, 2, 2),
      --                0, Vector(10, 10, 10))

      if (player) then
         if (math.random(1, 100) <= 30) then
            SpecialWeaponHandling(self, player, _CreateClusterFragment, 50)
         end
      end
   end
end
