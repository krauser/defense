-- ===================== Faded Mod =====================
--
-- lua\FadedPlayer_Client.lua
--
--    Create by: JB (jeanbaptiste.laurent.pro@gmail.com)
-- As a babbler player, we do not want blur when stun
--
-- =====================================================

Script.Load("lua/Player_Client.lua")

local kGrenades =
   {
      kTechId.ClusterGrenade,
      kTechId.GasGrenade,
      kTechId.NapalmGrenade, -- Faded added
      kTechId.HealGrenade, -- Faded added
      kTechId.PulseGrenade,
   }


-- Same as ns2, but use a different "kGrenades" table instead
function PlayerUI_GetHasItem(techId)
    local hasItem = false
    local isaGrenade = table.contains(kGrenades, techId)

    if techId and techId ~= kTechId.None then

       local player = Client.GetLocalPlayer()
        if player then

       local items = GetChildEntities(player, "ScriptActor")

       for index, item in ipairs(items) do

          if item:GetTechId() == techId then

                    hasItem = true
                    break

          elseif isaGrenade and table.contains(kGrenades, item:GetTechId()) then

             hasItem = true
             break

          end

       end

        end

    end

    return hasItem
end

-- local playerUpdateCrossHairText = Player.UpdateCrossHairText
-- function Player:UpdateCrossHairText(entity)
--    if (entity and entity:isa("SentryBattery") and entity:GetTeamNumber() == 2) then
--       self.crossHairText = "Dead meat" -- Hide trix to attach corpses
--    elseif (entity and entity:isa("Ragdoll") and self:GetTeamNumber() == 1 and self:GetIsAlive()) then
--       self.crossHairText = "Burn me"
--    else
--       playerUpdateCrossHairText(self, entity)
--    end
-- end

-- function Player:OnUpdateRender()

--    PROFILE("Player:OnUpdateRender")

--    if self:GetIsLocalPlayer() then

--       local stunned = HasMixin(self, "Stun") and self:GetIsStunned()
--       -- Faded: Here line changed
--       local blurEnabled = (self.buyMenu ~= nil or stunned) and not self:isa("Skulk")
--         self:SetBlurEnabled(blurEnabled)

--         self.lastOnUpdateRenderTime = self.lastOnUpdateRenderTime or Shared.GetTime()
--         local now = Shared.GetTime()
--         self:UpdateScreenEffects(now - self.lastOnUpdateRenderTime)
--         self.lastOnUpdateRenderTime = now

--     end

-- end

-- function Player:GetShowUnitStatusForOverride(forEntity)
--    -- return not GetAreEnemies(self, forEntity) or (forEntity:GetOrigin() - self:GetOrigin()):GetLength() < 8
--    Print("Called GetShowUnitStatusForOverride and return true")
--    return (true)
-- end
