-- ===================== Faded Mod =====================
--
-- lua\FadedGUIDeathMessages.lua
--
--    Created by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

-- Only print death notification if the victim is around
local GUIDeathMessagesAddMessage = GUIDeathMessages.AddMessage
function GUIDeathMessages:AddMessage(killerColor, killerName, targetColor, targetName, iconIndex, targetIsPlayer)
   local _printDeathMessage = false
   local p = Client.GetLocalPlayer()
   if (p and targetName and #targetName > 0 and targetIsPlayer)
   then
      _printDeathMessage = true
   end
   if (_printDeathMessage) then
      GUIDeathMessagesAddMessage(self, killerColor, killerName, targetColor, targetName, iconIndex, targetIsPlayer)
   end
end
