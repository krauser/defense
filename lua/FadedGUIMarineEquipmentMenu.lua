-- ===================== Faded Mod =====================
--
-- lua\FadedGUIMarineEquipmentMenu.lua
--
--    Created by: Rio (rio@myrio.de)
--    Updated by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

Script.Load("lua/GUIAnimatedScript.lua")

local locale = LibCache:GetLibrary("LibLocales-1.0")

class 'FadedGUIMarineEquipmentMenu' (GUIAnimatedScript)

FadedGUIMarineEquipmentMenu.kBuyMenuTexture = "ui/marine_buy_textures.dds"
FadedGUIMarineEquipmentMenu.kBuyHUDTexture = "ui/marine_buy_icons.dds"
FadedGUIMarineEquipmentMenu.kRepeatingBackground = "ui/menu/grid.dds"
FadedGUIMarineEquipmentMenu.kContentBgTexture = "ui/menu/repeating_bg.dds"
FadedGUIMarineEquipmentMenu.kContentBgBackTexture = "ui/menu/repeating_bg_black.dds"
FadedGUIMarineEquipmentMenu.kResourceIconTexture = "ui/pres_icon_big.dds"
FadedGUIMarineEquipmentMenu.kBigIconTexture = "ui/marine_buy_bigicons.dds"
FadedGUIMarineEquipmentMenu.kButtonTexture = "ui/marine_buymenu_button.dds"
FadedGUIMarineEquipmentMenu.kMenuSelectionTexture = "ui/marine_buymenu_selector.dds"
FadedGUIMarineEquipmentMenu.kScanLineTexture = "ui/menu/scanLine_big.dds"
FadedGUIMarineEquipmentMenu.kArrowTexture = "ui/menu/arrow_horiz.dds"

FadedGUIMarineEquipmentMenu.kFont = "fonts/AgencyFB_small.fnt"
FadedGUIMarineEquipmentMenu.kFont2 = "fonts/AgencyFB_small.fnt"

FadedGUIMarineEquipmentMenu.kDescriptionFontName = "fonts/AgencyFB_small.fnt"
FadedGUIMarineEquipmentMenu.kDescriptionFontSize = GUIScale(20)

FadedGUIMarineEquipmentMenu.kScanLineHeight = GUIScale(256)
FadedGUIMarineEquipmentMenu.kScanLineAnimDuration = 5

FadedGUIMarineEquipmentMenu.kArrowWidth = GUIScale(32)
FadedGUIMarineEquipmentMenu.kArrowHeight = GUIScale(32)
FadedGUIMarineEquipmentMenu.kArrowTexCoords = { 1, 1, 0, 0 }

FadedGUIMarineEquipmentMenu.kSmallIconSize = GUIScale( Vector(100, 50, 0) )
FadedGUIMarineEquipmentMenu.kMenuIconSize = GUIScale( Vector(190, 80, 0) )
FadedGUIMarineEquipmentMenu.kSelectorSize = GUIScale( Vector(215, 110, 0) )
FadedGUIMarineEquipmentMenu.kIconTopOffset = 10
FadedGUIMarineEquipmentMenu.kItemIconYOffset = {}

FadedGUIMarineEquipmentMenu.kEquippedIconTopOffset = 58

FadedGUIMarineEquipmentMenu.kBackgroundWidth = GUIScale(600)
FadedGUIMarineEquipmentMenu.kBackgroundHeight = GUIScale(520)


-- Same as vanilla NS2 (at one or two lines added)
local gWeaponDescription = nil
function MarineBuy_GetWeaponDescription(techId)

    if not gWeaponDescription then

       gWeaponDescription = { }
       gWeaponDescription[kTechId.Axe] = "WEAPON_DESC_AXE"
       gWeaponDescription[kTechId.Pistol] = "WEAPON_DESC_PISTOL"
       gWeaponDescription[kTechId.Rifle] = "WEAPON_DESC_RIFLE"
       gWeaponDescription[kTechId.Shotgun] = "WEAPON_DESC_SHOTGUN"
       gWeaponDescription[kTechId.HeavyShotgun] = "WEAPON_DESC_HEAVYSHOTGUN" -- FADED
       gWeaponDescription[kTechId.Flamethrower] = "WEAPON_DESC_FLAMETHROWER"
       gWeaponDescription[kTechId.GrenadeLauncher] = "WEAPON_DESC_GRENADELAUNCHER"
       gWeaponDescription[kTechId.HeavyMachineGun] = "WEAPON_DESC_HMG"
       gWeaponDescription[kTechId.Welder] = "WEAPON_DESC_WELDER"
       gWeaponDescription[kTechId.LayMines] = "WEAPON_DESC_MINE"
       gWeaponDescription[kTechId.LayFlameMines] = "WEAPON_DESC_FLAME_MINE" -- FADED
       gWeaponDescription[kTechId.LaySentryBattery] = "WEAPON_DESC_SENTRY" -- FADED
       gWeaponDescription[kTechId.Scan] = "WEAPON_DESC_SCAN" -- FADED
       gWeaponDescription[kTechId.MedPack] = "WEAPON_DESC_MEDPACK" -- FADED
       gWeaponDescription[kTechId.ClusterGrenade] = "WEAPON_DESC_CLUSTER_GRENADE"
       gWeaponDescription[kTechId.GasGrenade] = "WEAPON_DESC_GAS_GRENADE"
       gWeaponDescription[kTechId.NapalmGrenade] = "WEAPON_DESC_NAPALM_GRENADE" -- TODO
       gWeaponDescription[kTechId.HealGrenade] = "WEAPON_DESC_HEAL_GRENADE" -- TODO
       gWeaponDescription[kTechId.PulseGrenade] = "WEAPON_DESC_PULSE_GRENADE"
       gWeaponDescription[kTechId.Jetpack] = "WEAPON_DESC_JETPACK"
       gWeaponDescription[kTechId.Exosuit] = "WEAPON_DESC_EXO"
       gWeaponDescription[kTechId.DualMinigunExosuit] = "WEAPON_DESC_DUALMINIGUN_EXO"
       gWeaponDescription[kTechId.UpgradeToDualMinigun] = "WEAPON_DESC_DUALMINIGUN_EXO"
       gWeaponDescription[kTechId.ClawRailgunExosuit] = "WEAPON_DESC_CLAWRAILGUN_EXO"
       gWeaponDescription[kTechId.DualRailgunExosuit] = "WEAPON_DESC_DUALRAILGUN_EXO"
       gWeaponDescription[kTechId.UpgradeToDualRailgun] = "WEAPON_DESC_DUALRAILGUN_EXO"

    end

    local description = gWeaponDescription[techId]
    if not description then
        description = ""
    end

    return Locale.ResolveString(description)

end


local smallIconHeight = 64
local smallIconWidth = 128
local gSmallIconIndex = nil
local function GetSmallIconPixelCoordinates(itemTechId)

    if not gSmallIconIndex then

        gSmallIconIndex = {}
        gSmallIconIndex[kTechId.Axe] = 4
        gSmallIconIndex[kTechId.Pistol] = 3
        gSmallIconIndex[kTechId.Rifle] = 1
        gSmallIconIndex[kTechId.Shotgun] = 5
        gSmallIconIndex[kTechId.HeavyShotgun] = 5 -- FADED
        gSmallIconIndex[kTechId.GrenadeLauncher] = 8
        gSmallIconIndex[kTechId.HeavyMachineGun] = 50
        gSmallIconIndex[kTechId.Flamethrower] = 6
        gSmallIconIndex[kTechId.Jetpack] = 24
        gSmallIconIndex[kTechId.Exosuit] = 25
        gSmallIconIndex[kTechId.Welder] = 10
        gSmallIconIndex[kTechId.LayMines] = 21
        gSmallIconIndex[kTechId.LayFlameMines] = 21 -- FADED
        gSmallIconIndex[kTechId.LaySentryBattery] = 10 -- FADED
        gSmallIconIndex[kTechId.Scan] = 4 -- FADED
        gSmallIconIndex[kTechId.MedPack] = 4 -- FADED
        gSmallIconIndex[kTechId.DualMinigunExosuit] = 26

        gSmallIconIndex[kTechId.ClusterGrenade] = 42
        gSmallIconIndex[kTechId.GasGrenade] = 43
        gSmallIconIndex[kTechId.NapalmGrenade] = 43 -- Faded
        gSmallIconIndex[kTechId.HealGrenade] = 43 -- Faded
        gSmallIconIndex[kTechId.PulseGrenade] = 44
    end

    local index = gSmallIconIndex[itemTechId]
    if not index then
        index = 0
    end

    local y1 = index * smallIconHeight
    local y2 = (index + 1) * smallIconHeight

    return 0, y1, smallIconWidth, y2

end

FadedGUIMarineEquipmentMenu.kBigIconSize = GUIScale( Vector(320, 256, 0) )
FadedGUIMarineEquipmentMenu.kBigIconOffset = GUIScale(20)

local gBigIconIndex = nil
local bigIconWidth = 400
local bigIconHeight = 300
local function GetBigIconPixelCoords(techId, researched)

    if not gBigIconIndex then

        gBigIconIndex = {}
        gBigIconIndex[kTechId.Axe] = 0
        gBigIconIndex[kTechId.Pistol] = 1
        gBigIconIndex[kTechId.Rifle] = 2
        gBigIconIndex[kTechId.Shotgun] = 3
        gBigIconIndex[kTechId.HeavyShotgun] = 3 -- FADED
        gBigIconIndex[kTechId.GrenadeLauncher] = 4
        gBigIconIndex[kTechId.Flamethrower] = 5
        gBigIconIndex[kTechId.Jetpack] = 6
        gBigIconIndex[kTechId.Exosuit] = 7
        gBigIconIndex[kTechId.Welder] = 8
        gBigIconIndex[kTechId.LayMines] = 9
        gBigIconIndex[kTechId.LayFlameMines] = 9 -- FADED
        gBigIconIndex[kTechId.LaySentryBattery] = 8 -- FADED
        gBigIconIndex[kTechId.Scan] = 0 -- FADED
        gBigIconIndex[kTechId.MedPack] = 0 -- FADED
        gBigIconIndex[kTechId.DualMinigunExosuit] = 10

    gBigIconIndex[kTechId.ClusterGrenade] = 12
        gBigIconIndex[kTechId.GasGrenade] = 13
        gBigIconIndex[kTechId.NapalmGrenade] = 13 -- Faded
        gBigIconIndex[kTechId.HealGrenade] = 13 -- Faded
        gBigIconIndex[kTechId.PulseGrenade] = 14
    end

    local index = gBigIconIndex[techId]
    if not index then
        index = 0
    end

    local x1 = 0
    local x2 = bigIconWidth

    if not researched then

        x1 = bigIconWidth
        x2 = bigIconWidth * 2

    end

    local y1 = index * bigIconHeight
    local y2 = (index + 1) * bigIconHeight

    return x1, y1, x2, y2

end

FadedGUIMarineEquipmentMenu.kTextColor = Color(kMarineFontColor)

FadedGUIMarineEquipmentMenu.kMenuWidth = GUIScale(190)
FadedGUIMarineEquipmentMenu.kMenuHeight = GUIScale(64)

FadedGUIMarineEquipmentMenu.kPadding = GUIScale(8)
FadedGUIMarineEquipmentMenu.kBackgroundWidth = GUIScale(600)
FadedGUIMarineEquipmentMenu.kBackgroundHeight = GUIScale(520)
-- We want the background graphic to look centered around the circle even though there is the part coming off to the right.
FadedGUIMarineEquipmentMenu.kBackgroundXOffset = GUIScale(0)

FadedGUIMarineEquipmentMenu.kEnabledColor = Color(1, 1, 1, 1)
FadedGUIMarineEquipmentMenu.kCloseButtonColor = Color(1, 1, 0, 1)

FadedGUIMarineEquipmentMenu.kButtonWidth = GUIScale(160)
FadedGUIMarineEquipmentMenu.kButtonHeight = GUIScale(64)

FadedGUIMarineEquipmentMenu.kItemNameOffsetX = GUIScale(28)
FadedGUIMarineEquipmentMenu.kItemNameOffsetY = GUIScale(256)

FadedGUIMarineEquipmentMenu.kItemDescriptionOffsetY = GUIScale(300)
FadedGUIMarineEquipmentMenu.kItemDescriptionSize = GUIScale( Vector(450, 180, 0) )

FadedGUIMarineEquipmentMenu.weaponList = {
            kTechId.Rifle,
            -- kTechId.Shotgun,
            kTechId.HeavyMachineGun,
            -- kTechId.HeavyShotgun, -- FADED
            kTechId.GrenadeLauncher,
            kTechId.Flamethrower,
        }

FadedGUIMarineEquipmentMenu.equipmentList = {
            kTechId.Welder,
            kTechId.LayMines,
            kTechId.LayFlameMines, -- FADED
            -- kTechId.PulseGrenade,
            kTechId.ClusterGrenade,
            -- kTechId.LaySentryBattery,
            -- kTechId.Jetpack,
        }

FadedGUIMarineEquipmentMenu.equipmentList2 = {
            -- kTechId.GasGrenade,
            kTechId.NapalmGrenade,
            kTechId.HealGrenade,
            -- kTechId.Scan,
            kTechId.MedPack,
}

local fadedWeaponXPos = -FadedGUIMarineEquipmentMenu.kMenuWidth * 3 - FadedGUIMarineEquipmentMenu.kArrowWidth * 3 - FadedGUIMarineEquipmentMenu.kPadding * 3
local fadedEquipment1XPos = -FadedGUIMarineEquipmentMenu.kMenuWidth * 2 - FadedGUIMarineEquipmentMenu.kArrowWidth * 1 - FadedGUIMarineEquipmentMenu.kPadding * 3
local fadedEquipment2XPos = -FadedGUIMarineEquipmentMenu.kMenuWidth - FadedGUIMarineEquipmentMenu.kPadding * 2


function FadedGUIMarineEquipmentMenu:Initialize()

   GUIAnimatedScript.Initialize(self)

   self.mouseOverStates = { }

   local player = Client.GetLocalPlayer()

   self.selectedWeapon = Client:GetSelectedWeapon() or kTechId.Rifle
   self.selectedEquipment = Client:GetSelectedEquipment() or kTechId.Welder

   self:_InitializeBackground()
   self:_InitializeMenuHeader()
   self:_InitializeWeapons()
   self:_InitializeWeaponsHeader()
   self:_InitializeEquipment()
   self:_InitializeEquipment2()
   self:_InitializeEquipmentHeader()
   self:_InitializeSaveButton()
   self:_InitializeCloseButton()
   self:_InitializeContent()
end

function FadedGUIMarineEquipmentMenu:Update(deltaTime)
   GUIAnimatedScript.Update(self, deltaTime)

   self:_UpdateItems(deltaTime)
   self:_UpdateSaveButton(deltaTime)
   self:_UpdateCloseButton(deltaTime)
   self:_UpdateContent(deltaTime)
end

function FadedGUIMarineEquipmentMenu:Uninitialize()
   GUIAnimatedScript.Uninitialize(self)

   self:_UninitializeBackground()
   self:_UninitializeItems()
   self:_UninitializeSaveButton()
   self:_UninitializeCloseButton()
   self:_UninitializeContent()
end

function FadedGUIMarineEquipmentMenu:_InitializeBackground()
   -- This invisible background is used for centering only.
   self.background = GUIManager:CreateGraphicItem()
   self.background:SetSize(Vector(Client.GetScreenWidth(), Client.GetScreenHeight(), 0))
   self.background:SetAnchor(GUIItem.Left, GUIItem.Top)
   self.background:SetColor(Color(0.05, 0.05, 0.1, 0.5)) -- alpha was 0.7
   self.background:SetLayer(kGUILayerPlayerHUDForeground4)

   //self.repeatingBGTexture = GUIManager:CreateGraphicItem()
   //self.repeatingBGTexture:SetSize(Vector(Client.GetScreenWidth(), Client.GetScreenHeight(), 0))
   //self.repeatingBGTexture:SetTexture(FadedGUIMarineEquipmentMenu.kRepeatingBackground)
   //self.repeatingBGTexture:SetTexturePixelCoordinates(0, 0, Client.GetScreenWidth(), Client.GetScreenHeight())
   //self.background:AddChild(self.repeatingBGTexture)

   self.content = GUIManager:CreateGraphicItem()
   -- FadedGUIMarineEquipmentMenu.kMenuWidth * 2 + FadedGUIMarineEquipmentMenu.kArrowWidth * 2 + FadedGUIMarineEquipmentMenu.kPadding * 2
   local size = Vector(FadedGUIMarineEquipmentMenu.kBackgroundWidth, FadedGUIMarineEquipmentMenu.kBackgroundHeight, 0)
   self.content:SetSize(size)
   self.content:SetAnchor(GUIItem.Middle, GUIItem.Center)
   self.content:SetPosition(Vector(0, -size.y / 2, 0))
   self.content:SetTexture(FadedGUIMarineEquipmentMenu.kContentBgTexture)
   self.content:SetTexturePixelCoordinates(0, 0, FadedGUIMarineEquipmentMenu.kBackgroundWidth, FadedGUIMarineEquipmentMenu.kBackgroundHeight)
   self.content:SetColor( Color(1,1,1,0.8) )
   self.background:AddChild(self.content)

   self.scanLine = self:CreateAnimatedGraphicItem()
   self.scanLine:SetSize( Vector( Client.GetScreenWidth(), FadedGUIMarineEquipmentMenu.kScanLineHeight, 0) )
   self.scanLine:SetTexture(FadedGUIMarineEquipmentMenu.kScanLineTexture)
   self.scanLine:SetLayer(kGUILayerPlayerHUDForeground4)
   self.scanLine:SetIsScaling(false)

   self.scanLine:SetPosition( Vector(0, -FadedGUIMarineEquipmentMenu.kScanLineHeight, 0) )
   self.scanLine:SetPosition( Vector(0, Client.GetScreenHeight() + FadedGUIMarineEquipmentMenu.kScanLineHeight, 0), FadedGUIMarineEquipmentMenu.kScanLineAnimDuration, "MARINEBUY_SCANLINE", AnimateLinear, MoveDownAnim)
end

function FadedGUIMarineEquipmentMenu:_UninitializeBackground()
   GUI.DestroyItem(self.background)
   self.background = nil

   self.content = nil
end

function FadedGUIMarineEquipmentMenu:_InitializeMenuHeader()
   self.menuHeader, self.menuHeaderTitle = self:_InitializeContentHeader(self.content, 0, "FADED_MARINE_SELECT_DESCRIPTION", Client.GetOptionString("input/Buy", "Buy"))
end

function FadedGUIMarineEquipmentMenu:_InitializeWeaponsHeader()
   self.weaponHeader, self.weaponHeaderTitle = self:_InitializeContentHeader(self.menu, fadedWeaponXPos, "FADED_MARINE_SELECT_WEAPON")
end

function FadedGUIMarineEquipmentMenu:_InitializeWeapons()
   self.menu = GetGUIManager():CreateGraphicItem()
   self.menu:SetPosition(Vector(fadedWeaponXPos, 0, 0))
   self.menu:SetTexture(FadedGUIMarineEquipmentMenu.kContentBgTexture)
   self.menu:SetSize(Vector(FadedGUIMarineEquipmentMenu.kMenuWidth, FadedGUIMarineEquipmentMenu.kBackgroundHeight, 0))
   self.menu:SetTexturePixelCoordinates(0, 0, FadedGUIMarineEquipmentMenu.kMenuWidth, FadedGUIMarineEquipmentMenu.kBackgroundHeight)
   self.content:AddChild(self.menu)

   -- self.menuWeaponTitle = GetGUIManager():CreateTextItem()
   -- self.menuWeaponTitle:SetFontName(FadedGUIMarineEquipmentMenu.kFont)
   -- self.menuWeaponTitle:SetFontIsBold(true)
   -- self.menuWeaponTitle:SetAnchor(GUIItem.Middle, GUIItem.Top)
   -- self.menuWeaponTitle:SetTextAlignmentX(GUIItem.Align_Center)
   -- self.menuWeaponTitle:SetTextAlignmentY(GUIItem.Align_Max)
   -- self.menuWeaponTitle:SetColor(FadedGUIMarineEquipmentMenu.kTextColor)
   -- self.menuWeaponTitle:SetText(Locale.ResolveString("Weapons"))
   -- self.menu:AddChild(self.menuWeaponTitle)

   self.itemButtons = { }

   local selectorPosX = -FadedGUIMarineEquipmentMenu.kSelectorSize.x + FadedGUIMarineEquipmentMenu.kPadding
   local fontScaleVector = Vector(0.8, 0.8, 0)

   for k, itemTechId in ipairs(self.weaponList) do
      local graphicItem = GUIManager:CreateGraphicItem()
      graphicItem:SetSize(FadedGUIMarineEquipmentMenu.kMenuIconSize)
      graphicItem:SetAnchor(GUIItem.Middle, GUIItem.Top)
      graphicItem:SetPosition(Vector(-FadedGUIMarineEquipmentMenu.kMenuIconSize.x/ 2, FadedGUIMarineEquipmentMenu.kIconTopOffset + (FadedGUIMarineEquipmentMenu.kMenuIconSize.y) * k - FadedGUIMarineEquipmentMenu.kMenuIconSize.y, 0))
      graphicItem:SetTexture(kInventoryIconsTexture)
      graphicItem:SetTexturePixelCoordinates(GetSmallIconPixelCoordinates(itemTechId))

      local graphicItemActive = GUIManager:CreateGraphicItem()
      graphicItemActive:SetSize(FadedGUIMarineEquipmentMenu.kSelectorSize)

      graphicItemActive:SetPosition(Vector(selectorPosX, -FadedGUIMarineEquipmentMenu.kSelectorSize.y / 2, 0))
      graphicItemActive:SetAnchor(GUIItem.Right, GUIItem.Center)
      graphicItemActive:SetTexture(FadedGUIMarineEquipmentMenu.kMenuSelectionTexture)
      graphicItemActive:SetIsVisible(false)

      graphicItem:AddChild(graphicItemActive)

      local selectedArrow = GUIManager:CreateGraphicItem()
      selectedArrow:SetSize(Vector(FadedGUIMarineEquipmentMenu.kArrowWidth, FadedGUIMarineEquipmentMenu.kArrowHeight, 0))
      selectedArrow:SetAnchor(GUIItem.Left, GUIItem.Center)
      selectedArrow:SetPosition(Vector(-FadedGUIMarineEquipmentMenu.kArrowWidth - FadedGUIMarineEquipmentMenu.kPadding, -FadedGUIMarineEquipmentMenu.kArrowHeight * 0.5, 0))
      selectedArrow:SetTexture(FadedGUIMarineEquipmentMenu.kArrowTexture)
      selectedArrow:SetColor(FadedGUIMarineEquipmentMenu.kTextColor)
      selectedArrow:SetTextureCoordinates(unpack(FadedGUIMarineEquipmentMenu.kArrowTexCoords))
      selectedArrow:SetIsVisible(false)

      graphicItem:AddChild(selectedArrow)

      self.menu:AddChild(graphicItem)
      table.insert(self.itemButtons, { Button = graphicItem, Highlight = graphicItemActive, TechId = itemTechId, Arrow = selectedArrow, IsWeapon = true } )

   end

   -- to prevent wrong display before the first update
   self:_UpdateItems(0)
end

function FadedGUIMarineEquipmentMenu:_UpdateItems(deltaTime)
   for i, item in ipairs(self.itemButtons) do
      if self:_GetIsMouseOver(item.Button) then
         item.Highlight:SetIsVisible(true)
         self.hoverItem = item.TechId
      else
         item.Highlight:SetIsVisible(false)
      end

      local useColor = Color(1,1,1,1)

      item.Button:SetColor(useColor)
      item.Highlight:SetColor(useColor)
      item.Arrow:SetIsVisible(self.selectedWeapon == item.TechId or self.selectedEquipment == item.TechId)
   end
end

function FadedGUIMarineEquipmentMenu:_UninitializeItems()
   for i, item in ipairs(self.itemButtons) do
      GUI.DestroyItem(item.Button)
   end
   self.itemButtons = nil
end

function FadedGUIMarineEquipmentMenu:_InitializeContentHeader(parent, xpos, text, extra_param)
   header = GetGUIManager():CreateGraphicItem()

   local size = parent:GetSize()
   header:SetSize(Vector(size.x, FadedGUIMarineEquipmentMenu.kMenuHeight, 0))
   header:SetPosition(Vector(xpos, -FadedGUIMarineEquipmentMenu.kMenuHeight, 0))
   header:SetTexture(FadedGUIMarineEquipmentMenu.kContentBgBackTexture)
   header:SetTexturePixelCoordinates(0, 0, FadedGUIMarineEquipmentMenu.kMenuWidth, FadedGUIMarineEquipmentMenu.kMenuHeight)
   self.content:AddChild(header)

   headerTitle = GetGUIManager():CreateTextItem()
   headerTitle:SetFontName(FadedGUIMarineEquipmentMenu.kFont)
   headerTitle:SetFontIsBold(true)
   headerTitle:SetAnchor(GUIItem.Middle, GUIItem.Center)
   headerTitle:SetTextAlignmentX(GUIItem.Align_Center)
   headerTitle:SetTextAlignmentY(GUIItem.Align_Center)
   headerTitle:SetColor(FadedGUIMarineEquipmentMenu.kTextColor)
   local rs = Locale.ResolveString(text)
   if (extra_param) then
      headerTitle:SetText(string.format(rs, tostring(extra_param)))
   else
      headerTitle:SetText(rs)
   end
   header:AddChild(headerTitle)
   return header, headerTitle
end

function FadedGUIMarineEquipmentMenu:_InitializeEquipmentHeader()
   self.equipmentHeader, self.equipmentHeaderTitle = self:_InitializeContentHeader(self.equipmentMenu, fadedEquipment1XPos, "FADED_MARINE_SELECT_EQUIPMENT")
end

function FadedGUIMarineEquipmentMenu:_InitializeEquipment()
   self.equipmentMenu = GetGUIManager():CreateGraphicItem()
   self.equipmentMenu:SetPosition(Vector(fadedEquipment1XPos, 0, 0))
   self.equipmentMenu:SetTexture(FadedGUIMarineEquipmentMenu.kContentBgTexture)
   self.equipmentMenu:SetSize(Vector(FadedGUIMarineEquipmentMenu.kMenuWidth, FadedGUIMarineEquipmentMenu.kBackgroundHeight, 0))
   self.equipmentMenu:SetTexturePixelCoordinates(0, 0, FadedGUIMarineEquipmentMenu.kMenuWidth, FadedGUIMarineEquipmentMenu.kBackgroundHeight)
   self.content:AddChild(self.equipmentMenu)

   local selectorPosX = -FadedGUIMarineEquipmentMenu.kSelectorSize.x + FadedGUIMarineEquipmentMenu.kPadding
   local fontScaleVector = Vector(0.8, 0.8, 0)

   for k, itemTechId in ipairs(self.equipmentList) do
      local graphicItem = GUIManager:CreateGraphicItem()
      graphicItem:SetSize(FadedGUIMarineEquipmentMenu.kMenuIconSize)
      graphicItem:SetAnchor(GUIItem.Middle, GUIItem.Top)
      graphicItem:SetPosition(Vector(-FadedGUIMarineEquipmentMenu.kMenuIconSize.x/ 2, FadedGUIMarineEquipmentMenu.kIconTopOffset + (FadedGUIMarineEquipmentMenu.kMenuIconSize.y) * k - FadedGUIMarineEquipmentMenu.kMenuIconSize.y, 0))
      graphicItem:SetTexture(kInventoryIconsTexture)
      graphicItem:SetTexturePixelCoordinates(GetSmallIconPixelCoordinates(itemTechId))

      local graphicItemActive = GUIManager:CreateGraphicItem()
      graphicItemActive:SetSize(FadedGUIMarineEquipmentMenu.kSelectorSize)

      graphicItemActive:SetPosition(Vector(selectorPosX, -FadedGUIMarineEquipmentMenu.kSelectorSize.y / 2, 0))
      graphicItemActive:SetAnchor(GUIItem.Right, GUIItem.Center)
      graphicItemActive:SetTexture(FadedGUIMarineEquipmentMenu.kMenuSelectionTexture)
      graphicItemActive:SetIsVisible(false)

      graphicItem:AddChild(graphicItemActive)

      local selectedArrow = GUIManager:CreateGraphicItem()
      selectedArrow:SetSize(Vector(FadedGUIMarineEquipmentMenu.kArrowWidth, FadedGUIMarineEquipmentMenu.kArrowHeight, 0))
      selectedArrow:SetAnchor(GUIItem.Left, GUIItem.Center)
      selectedArrow:SetPosition(Vector(-FadedGUIMarineEquipmentMenu.kArrowWidth - FadedGUIMarineEquipmentMenu.kPadding, -FadedGUIMarineEquipmentMenu.kArrowHeight * 0.5, 0))
      selectedArrow:SetTexture(FadedGUIMarineEquipmentMenu.kArrowTexture)
      selectedArrow:SetColor(FadedGUIMarineEquipmentMenu.kTextColor)
      selectedArrow:SetTextureCoordinates(unpack(FadedGUIMarineEquipmentMenu.kArrowTexCoords))
      selectedArrow:SetIsVisible(false)

      graphicItem:AddChild(selectedArrow)

      self.equipmentMenu:AddChild(graphicItem)
      table.insert(self.itemButtons, { Button = graphicItem, Highlight = graphicItemActive, TechId = itemTechId, Arrow = selectedArrow, IsWeapon = false } )

   end

   -- to prevent wrong display before the first update
   self:_UpdateItems(0)
end

function FadedGUIMarineEquipmentMenu:_InitializeEquipment2()
   self.equipmentMenu2 = GetGUIManager():CreateGraphicItem()
   self.equipmentMenu2:SetPosition(Vector(fadedEquipment2XPos, 0, 0))
   self.equipmentMenu2:SetTexture(FadedGUIMarineEquipmentMenu.kContentBgTexture)
   self.equipmentMenu2:SetSize(Vector(FadedGUIMarineEquipmentMenu.kMenuWidth, FadedGUIMarineEquipmentMenu.kBackgroundHeight, 0))
   self.equipmentMenu2:SetTexturePixelCoordinates(0, 0, FadedGUIMarineEquipmentMenu.kMenuWidth, FadedGUIMarineEquipmentMenu.kBackgroundHeight)
   self.content:AddChild(self.equipmentMenu2)

   local selectorPosX = -FadedGUIMarineEquipmentMenu.kSelectorSize.x + FadedGUIMarineEquipmentMenu.kPadding
   local fontScaleVector = Vector(0.8, 0.8, 0)

   for k, itemTechId in ipairs(self.equipmentList2) do
      local graphicItem = GUIManager:CreateGraphicItem()
      graphicItem:SetSize(FadedGUIMarineEquipmentMenu.kMenuIconSize)
      graphicItem:SetAnchor(GUIItem.Middle, GUIItem.Top)
      graphicItem:SetPosition(Vector(-FadedGUIMarineEquipmentMenu.kMenuIconSize.x/ 2, FadedGUIMarineEquipmentMenu.kIconTopOffset + (FadedGUIMarineEquipmentMenu.kMenuIconSize.y) * k - FadedGUIMarineEquipmentMenu.kMenuIconSize.y, 0))
      graphicItem:SetTexture(kInventoryIconsTexture)
      graphicItem:SetTexturePixelCoordinates(GetSmallIconPixelCoordinates(itemTechId))

      local graphicItemActive = GUIManager:CreateGraphicItem()
      graphicItemActive:SetSize(FadedGUIMarineEquipmentMenu.kSelectorSize)

      graphicItemActive:SetPosition(Vector(selectorPosX, -FadedGUIMarineEquipmentMenu.kSelectorSize.y / 2, 0))
      graphicItemActive:SetAnchor(GUIItem.Right, GUIItem.Center)
      graphicItemActive:SetTexture(FadedGUIMarineEquipmentMenu.kMenuSelectionTexture)
      graphicItemActive:SetIsVisible(false)

      graphicItem:AddChild(graphicItemActive)

      local selectedArrow = GUIManager:CreateGraphicItem()
      selectedArrow:SetSize(Vector(FadedGUIMarineEquipmentMenu.kArrowWidth, FadedGUIMarineEquipmentMenu.kArrowHeight, 0))
      selectedArrow:SetAnchor(GUIItem.Left, GUIItem.Center)
      selectedArrow:SetPosition(Vector(-FadedGUIMarineEquipmentMenu.kArrowWidth - FadedGUIMarineEquipmentMenu.kPadding, -FadedGUIMarineEquipmentMenu.kArrowHeight * 0.5, 0))
      selectedArrow:SetTexture(FadedGUIMarineEquipmentMenu.kArrowTexture)
      selectedArrow:SetColor(FadedGUIMarineEquipmentMenu.kTextColor)
      selectedArrow:SetTextureCoordinates(unpack(FadedGUIMarineEquipmentMenu.kArrowTexCoords))
      selectedArrow:SetIsVisible(false)

      graphicItem:AddChild(selectedArrow)

      self.equipmentMenu2:AddChild(graphicItem)
      table.insert(self.itemButtons, { Button = graphicItem, Highlight = graphicItemActive, TechId = itemTechId, Arrow = selectedArrow, IsWeapon = false } )

   end

   -- to prevent wrong display before the first update
   self:_UpdateItems(0)
end

function FadedGUIMarineEquipmentMenu:_InitializeCloseButton()
   self.closeButton = GUIManager:CreateGraphicItem()
   self.closeButton:SetAnchor(GUIItem.Right, GUIItem.Bottom)
   self.closeButton:SetSize(Vector(FadedGUIMarineEquipmentMenu.kButtonWidth, FadedGUIMarineEquipmentMenu.kButtonHeight, 0))
   self.closeButton:SetPosition(Vector(-FadedGUIMarineEquipmentMenu.kButtonWidth, FadedGUIMarineEquipmentMenu.kPadding, 0))
   self.closeButton:SetTexture(FadedGUIMarineEquipmentMenu.kButtonTexture)
   self.closeButton:SetLayer(kGUILayerPlayerHUDForeground4)
   self.content:AddChild(self.closeButton)

   self.closeButtonText = GUIManager:CreateTextItem()
   self.closeButtonText:SetAnchor(GUIItem.Middle, GUIItem.Center)
   self.closeButtonText:SetFontName(FadedGUIMarineEquipmentMenu.kFont)
   self.closeButtonText:SetTextAlignmentX(GUIItem.Align_Center)
   self.closeButtonText:SetTextAlignmentY(GUIItem.Align_Center)
   self.closeButtonText:SetText(Locale.ResolveString("EXIT"))
   self.closeButtonText:SetFontIsBold(true)
   self.closeButtonText:SetColor(FadedGUIMarineEquipmentMenu.kCloseButtonColor)
   self.closeButton:AddChild(self.closeButtonText)
end

function FadedGUIMarineEquipmentMenu:_UpdateCloseButton(deltaTime)
   if self:_GetIsMouseOver(self.closeButton) then
      self.closeButton:SetColor(Color(1, 1, 1, 1))
   else
      self.closeButton:SetColor(Color(0.5, 0.5, 0.5, 1))
   end
end

function FadedGUIMarineEquipmentMenu:_UninitializeCloseButton()
   GUI.DestroyItem(self.closeButton)
   self.closeButton = nil
end

function FadedGUIMarineEquipmentMenu:_InitializeSaveButton()
   self.saveButton = GUIManager:CreateGraphicItem()
   self.saveButton:SetAnchor(GUIItem.Right, GUIItem.Bottom)
   self.saveButton:SetSize(Vector(FadedGUIMarineEquipmentMenu.kButtonWidth, FadedGUIMarineEquipmentMenu.kButtonHeight, 0))
   self.saveButton:SetPosition(Vector(-(FadedGUIMarineEquipmentMenu.kButtonWidth * 2) - FadedGUIMarineEquipmentMenu.kPadding, FadedGUIMarineEquipmentMenu.kPadding, 0))
   self.saveButton:SetTexture(FadedGUIMarineEquipmentMenu.kButtonTexture)
   self.saveButton:SetLayer(kGUILayerPlayerHUDForeground4)
   self.content:AddChild(self.saveButton)

   self.saveButtonText = GUIManager:CreateTextItem()
   self.saveButtonText:SetAnchor(GUIItem.Middle, GUIItem.Center)
   self.saveButtonText:SetFontName(FadedGUIMarineEquipmentMenu.kFont)
   self.saveButtonText:SetTextAlignmentX(GUIItem.Align_Center)
   self.saveButtonText:SetTextAlignmentY(GUIItem.Align_Center)
   self.saveButtonText:SetText(locale:ResolveString("FADED_MARINE_SELECT_EQUIPMENT_SAVE"))
   self.saveButtonText:SetFontIsBold(true)
   self.saveButtonText:SetColor(FadedGUIMarineEquipmentMenu.kCloseButtonColor)
   self.saveButton:AddChild(self.saveButtonText)
end

function FadedGUIMarineEquipmentMenu:_UpdateSaveButton(deltaTime)
   if self:_GetIsMouseOver(self.saveButton) then
      self.saveButton:SetColor(Color(1, 1, 1, 1))
   else
      self.saveButton:SetColor(Color(0.5, 0.5, 0.5, 1))
   end
end

function FadedGUIMarineEquipmentMenu:_UninitializeSaveButton()
   GUI.DestroyItem(self.saveButton)
   self.saveButton = nil
end

function FadedGUIMarineEquipmentMenu:_InitializeContent()
   self.itemName = GUIManager:CreateTextItem()
   self.itemName:SetFontName(FadedGUIMarineEquipmentMenu.kFont)
   self.itemName:SetFontIsBold(true)
   self.itemName:SetAnchor(GUIItem.Left, GUIItem.Top)
   self.itemName:SetPosition(Vector(FadedGUIMarineEquipmentMenu.kItemNameOffsetX , FadedGUIMarineEquipmentMenu.kItemNameOffsetY , 0))
   self.itemName:SetTextAlignmentX(GUIItem.Align_Min)
   self.itemName:SetTextAlignmentY(GUIItem.Align_Min)
   self.itemName:SetColor(FadedGUIMarineEquipmentMenu.kTextColor)
   self.itemName:SetText("no selection")

   self.content:AddChild(self.itemName)

   self.portrait = GetGUIManager():CreateGraphicItem()
   self.portrait:SetAnchor(GUIItem.Middle, GUIItem.Top)
   self.portrait:SetPosition(Vector(-FadedGUIMarineEquipmentMenu.kBigIconSize.x/2, FadedGUIMarineEquipmentMenu.kBigIconOffset, 0))
   self.portrait:SetSize(FadedGUIMarineEquipmentMenu.kBigIconSize)
   self.portrait:SetTexture(FadedGUIMarineEquipmentMenu.kBigIconTexture)
   self.portrait:SetTexturePixelCoordinates(GetBigIconPixelCoords(kTechId.Axe))
   self.portrait:SetIsVisible(false)
   self.content:AddChild(self.portrait)

   self.itemDescription = GetGUIManager():CreateTextItem()
   //self.itemDescription:SetFontIsBold(true)
   self.itemDescription:SetFontSize(FadedGUIMarineEquipmentMenu.kDescriptionFontSize)
   self.itemDescription:SetAnchor(GUIItem.Middle, GUIItem.Top)
   self.itemDescription:SetPosition(Vector(-FadedGUIMarineEquipmentMenu.kItemDescriptionSize.x / 2, FadedGUIMarineEquipmentMenu.kItemDescriptionOffsetY, 0))
   self.itemDescription:SetTextAlignmentX(GUIItem.Align_Min)
   self.itemDescription:SetTextAlignmentY(GUIItem.Align_Min)
   self.itemDescription:SetColor(FadedGUIMarineEquipmentMenu.kTextColor)
   self.itemDescription:SetTextClipped(true, FadedGUIMarineEquipmentMenu.kItemDescriptionSize.x - 2* FadedGUIMarineEquipmentMenu.kPadding, FadedGUIMarineEquipmentMenu.kItemDescriptionSize.y - FadedGUIMarineEquipmentMenu.kPadding)

   self.content:AddChild(self.itemDescription)
end

function FadedGUIMarineEquipmentMenu:_UpdateContent(deltaTime)
   local techId = self.hoverItem
   if not self.hoverItem then
      techId = self.selectedWeapon
   end

   if techId then
      local color = Color(1, 1, 1, 1)

      self.itemName:SetColor(color)
      self.portrait:SetColor(color)
      self.itemDescription:SetColor(color)

      self.itemName:SetText(Locale.ResolveString(LookupTechData(techId, kTechDataDisplayName, "")))
      self.portrait:SetTexturePixelCoordinates(GetBigIconPixelCoords(techId, researched))
      self.itemDescription:SetText(MarineBuy_GetWeaponDescription(techId))
      self.itemDescription:SetTextClipped(true, FadedGUIMarineEquipmentMenu.kItemDescriptionSize.x - 2* FadedGUIMarineEquipmentMenu.kPadding, FadedGUIMarineEquipmentMenu.kItemDescriptionSize.y - FadedGUIMarineEquipmentMenu.kPadding)
   end

   local contentVisible = techId ~= nil and techId ~= kTechId.None

   self.portrait:SetIsVisible(contentVisible)
   self.itemName:SetIsVisible(contentVisible)
   self.itemDescription:SetIsVisible(contentVisible)
end

function FadedGUIMarineEquipmentMenu:_UninitializeContent()
   GUI.DestroyItem(self.itemName)
end

function FadedGUIMarineEquipmentMenu:_GetIsMouseOver(overItem)
   local mouseOver = GUIItemContainsPoint(overItem, Client.GetCursorPosScreen())
   if mouseOver and not self.mouseOverStates[overItem] then
      MarineBuy_OnMouseOver()
   end
   self.mouseOverStates[overItem] = mouseOver
   return mouseOver
end

function FadedGUIMarineEquipmentMenu:SendKeyEvent(key, down)
   local closeMenu = false
   local inputHandled = false

   if key == InputKey.MouseButton0 and self.mousePressed ~= down then
      self.mousePressed = down

      local mouseX, mouseY = Client.GetCursorPosScreen()
      if down then
         inputHandled, closeMenu = self:_HandleItemClicked(mouseX, mouseY) or inputHandled

         if not inputHandled then
            -- Check if the close button was pressed.
            if self:_GetIsMouseOver(self.closeButton) then
               closeMenu = true
               inputHandled = true
            elseif self:_GetIsMouseOver(self.saveButton) then
               closeMenu = true
               inputHandled = true

               local player = Client.GetLocalPlayer()
               Client:SetSelectedEquipment(self.selectedWeapon, self.selectedEquipment)
            end
         end
      end
   end

   if (closeMenu) then
      local player = Client.GetLocalPlayer()
      if player then
         player:CloseMenu()
      end
   end

   return inputHandled
end

function FadedGUIMarineEquipmentMenu:_HandleItemClicked(mouseX, mouseY)
   for i, item in ipairs(self.itemButtons) do
      if self:_GetIsMouseOver(item.Button) then
         if (item.IsWeapon) then
            self.selectedWeapon = item.TechId
         else
            self.selectedEquipment = item.TechId
         end

         return true, true
      end
   end

   return false, false
end
