local init = false
local force_next = false

local wave_nb = 0
local squad_nb = 1
local last_squad_created = 0

local trader_wave = false
local trader_delay = 10
local trader_armory = nil
local next_wave_time = 0

local remaining_lifeform = {}
local unspawn_lifeform = {}

wave_pop = 0
wave_max_pop = 0

function getRemainingLifeform(mapName)
   if (remaining_lifeform[mapName]) then
      return (remaining_lifeform[mapName])
   end
   return 0
end

function getUnspawnedLifeform(mapName)
   if (unspawn_lifeform[mapName]) then
      return (unspawn_lifeform[mapName])
   end
   return 0
end


function lifeformCounterSet(mapName, nb)
   remaining_lifeform[mapName] = nb
end

function unspawnLifeformCounterSet(mapName, nb)
   unspawn_lifeform[mapName] = nb
end

local function genSquad(minplayer, babblers, skulk, gorge, lerk, fade, onos, bonewall)
   local squad = {minplayer = minplayer[1]}

   if (babblers and babblers > 0)then table.insert(squad, {mapName = Babbler.kMapName, nb = babblers}) end
   if (skulk and skulk > 0) then table.insert(squad, {mapName =    Skulk.kMapName, nb = skulk}) end
   if (gorge and gorge > 0) then table.insert(squad, {mapName =    Gorge.kMapName, nb = gorge}) end
   if (lerk  and lerk  > 0) then table.insert(squad, {mapName =     Lerk.kMapName, nb = lerk})  end
   if (fade  and fade  > 0) then table.insert(squad, {mapName =     Fade.kMapName, nb = fade})  end
   if (onos  and onos  > 0) then table.insert(squad, {mapName =     Onos.kMapName, nb = onos})  end
   if (bonewall and bonewall > 0) then table.insert(squad, {mapName = BoneWall.kMapName, nb = bonewall}) end

   return (squad)
end

-- return wave.tunnel_dropper
-- return wave.tunnel_drop_rate

local waveScaleFix = 60/100
local dmgScaleFix = 100/100
local hpScaleFix = 100/100

defense_waves_config =
   {
      -- {
      --    hp_scalar = 0.2 * hpScaleFix, dmg_scalar = 0.2 * dmgScaleFix,
      --    tunnel_dropper = 0.6 * waveScaleFix, tunnel_drop_rate = 0.1,
      --    alien_scale = 0.04, max_delay_between_squad = 35, squads = {
      --       genSquad({ 0}, 2, 4, 0, 0, 0, 0, 0),
      --       genSquad({ 0}, 2, 4, 1, 0, 0, 0, 0),
      --       genSquad({ 0}, 2, 4, 0, 1, 0, 0, 0),
      --       genSquad({ 0}, 2, 5, 0, 0, 0, 0, 0),
      --       genSquad({ 0}, 2, 2, 2, 1, 0, 0, 0)
      --    }
      -- },
      {
         hp_scalar = 0.4 * hpScaleFix, dmg_scalar = 0.4 * dmgScaleFix, upgrades = {},
         tunnel_dropper = 0.05, tunnel_drop_rate = 0.08,
         alien_scale = 0.05 * waveScaleFix, max_delay_between_squad = 30, squads = {
            genSquad({ 0}, 5, 3, 0, 0, 0, 0, 0),
            genSquad({ 0}, 2, 3, 1, 0, 0, 0, 0),
            genSquad({ 0}, 2, 3, 0, 1, 0, 0, 0),
            genSquad({ 3}, 2, 5, 0, 0, 0, 0, 0),
            genSquad({ 5}, 2, 3, 2, 0, 0, 0, 0),
            genSquad({ 7}, 2, 6, 0, 0, 0, 0, 0),
            -- genSquad({ 7}, 2, 6, 0, 0, 0, 0, 0),
            genSquad({10}, 2, 1, 1, 1, 0, 0, 0),

            -- genSquad({ 0}, 2, 1, 3, 0, 0, 0, 0),
            -- genSquad({ 0}, 2, 1, 0, 2, 0, 0, 0),
            -- genSquad({ 0}, 2, 3, 0, 0, 0, 0, 1),
            -- genSquad({ 3}, 2, 5, 0, 0, 0, 0, 0),
            -- genSquad({ 5}, 2, 1, 3, 2, 0, 0, 0),
            -- genSquad({ 7}, 2, 6, 0, 0, 0, 0, 0),
            -- genSquad({10}, 2, 1, 1, 1, 0, 0, 0)
         }
      },

      {
         hp_scalar = 0.6 * hpScaleFix, dmg_scalar = 0.6 * dmgScaleFix, upgrades = {kTechId.Armor1},
         tunnel_dropper = 0.1, tunnel_drop_rate = 0.5,
         alien_scale = 0.06 * waveScaleFix, max_delay_between_squad = 20, squads = {
            -- genSquad({ 0}, 2, 4, 1, 0, 0, 0, 0),
            -- genSquad({ 0}, 2, 4, 1, 0, 0, 0, 0),
            genSquad({ 0}, 2, 3, 0, 0, 1, 0, 1),
            genSquad({ 0}, 2, 4, 0, 2, 0, 0, 0),
            genSquad({ 0}, 2, 4, 2, 2, 1, 0, 0),
            -- genSquad({ 3}, 2, 5, 0, 0, 0, 0, 1),
            genSquad({ 5}, 10, 1, 3, 2, 0, 0, 0),
            genSquad({ 5}, 2, 1, 1, 3, 0, 0, 0),
            genSquad({ 7}, 2, 5, 1, 0, 0, 0, 0),
            -- genSquad({ 7}, 2, 6, 0, 0, 0, 0, 0),
            genSquad({10}, 2, 1, 1, 1, 0, 0, 0)
         }
      },

      {
         hp_scalar = 0.7 * hpScaleFix, dmg_scalar = 0.6 * dmgScaleFix, upgrades = {kTechId.Weapons1},
         tunnel_dropper = 0.1, tunnel_drop_rate = 0.5,
         alien_scale = 0.06 * waveScaleFix, max_delay_between_squad = 20, squads = {
            genSquad({ 0}, 50, 4, 3, 0, 0, 2, 0),
            genSquad({ 0}, 50, 4, 3, 0, 0, 0, 0),
            genSquad({ 3}, 50, 4, 3, 0, 0, 0, 0),
            genSquad({ 7}, 50, 4, 3, 0, 0, 0, 0),
            -- genSquad({ 0}, 50, 4, 3, 0, 0, 2, 0),
            -- genSquad({ 0}, 50, 4, 3, 0, 0, 0, 0),
            -- genSquad({ 3}, 50, 4, 3, 0, 0, 0, 0),
            -- genSquad({ 7}, 50, 4, 3, 0, 0, 0, 0),
         }
      },

      {
         hp_scalar = 0.7 * hpScaleFix, dmg_scalar = 0.6 * dmgScaleFix, upgrades = {kTechId.Armor2},
         tunnel_dropper = 0.0, tunnel_drop_rate = 0.08,
         alien_scale = 0.06 * waveScaleFix, max_delay_between_squad = 20, squads = {

            -- genSquad({ 0}, 2, 2, 3, 0, 0, 0, 1),
            -- genSquad({ 0}, 2, 5, 0, 0, 1, 0, 0),
            -- genSquad({ 0}, 2, 4, 1, 0, 1, 0, 0),
            -- genSquad({ 3}, 2, 5, 0, 0, 0, 0, 0),
            -- genSquad({ 5}, 2, 0, 3, 2, 0, 0, 0),
            -- genSquad({ 7}, 2, 1, 1, 2, 0, 0, 0),

            genSquad({ 0}, 2, 0, 1, 6, 0, 0, 0),
            genSquad({ 0}, 2, 0, 1, 6, 0, 0, 0),
            genSquad({ 0}, 2, 0, 1, 6, 0, 0, 0),
            genSquad({ 7}, 2, 0, 1, 6, 0, 0, 0),
            genSquad({ 3}, 10, 3, 0, 0, 0, 1, 1),
            genSquad({ 5}, 2, 0, 1, 6, 0, 0, 0),
            genSquad({ 7}, 2, 0, 1, 2, 0, 0, 1)
         }
      },

      {
         hp_scalar = 0.7 * hpScaleFix, dmg_scalar = 0.6 * dmgScaleFix, upgrades = {kTechId.Weapons2},
         tunnel_dropper = 0.2, tunnel_drop_rate = 0.08,
         alien_scale = 0.1 * waveScaleFix, max_delay_between_squad = 18, squads = {
            genSquad({ 0}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 1, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 1, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 1, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 0}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 3}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 3}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 3}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 3}, 0, 2, 0, 1, 0, 0, 0),
            genSquad({ 3}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 5}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 5}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 5}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 5}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 5}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 7}, 0, 2, 0, 1, 0, 0, 0),
            genSquad({ 7}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 7}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 7}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 7}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 7}, 0, 2, 0, 0, 0, 0, 0),
            genSquad({ 7}, 0, 2, 0, 1, 0, 0, 0),
         }
      },

      {
         hp_scalar = 0.8 * hpScaleFix, dmg_scalar = 0.6 * dmgScaleFix, upgrades = {kTechId.Weapons3},
         tunnel_dropper = 0.1, tunnel_drop_rate = 0.1,
         alien_scale = 0.075 * waveScaleFix, max_delay_between_squad = 18, squads = {

            -- genSquad({ 0}, 2, 2, 1, 3, 0, 0, 0),
            -- genSquad({ 0}, 2, 5, 1, 0, 1, 0, 0),
            -- genSquad({ 0}, 2, 6, 0, 0, 0, 0, 0),
            -- genSquad({ 3}, 2, 5, 0, 1, 0, 0, 1),
            -- -- genSquad({ 3}, 2, 5, 0, 1, 0, 0, 0),
            -- genSquad({ 5}, 5, 5, 0, 1, 0, 0, 0),
            -- -- genSquad({ 5}, 5, 3, 3, 0, 0, 0, 0),
            -- genSquad({ 7}, 5, 1, 1, 3, 0, 0, 0),
            -- genSquad({10}, 5, 0, 1, 0, 1, 1, 0),

            genSquad({ 0}, 5, 5, 0, 0, 1, 0, 0),
            genSquad({ 0}, 5, 3, 0, 0, 0, 2, 1),
            genSquad({ 0}, 5, 1, 3, 0, 0, 0, 0),
            genSquad({ 3}, 5, 5, 1, 0, 0, 0, 0),
            genSquad({ 5}, 5, 1, 1, 3, 0, 0, 0),

            genSquad({ 0}, 5, 5, 0, 0, 1, 0, 0),
            genSquad({ 0}, 2, 5, 2, 0, 0, 0, 0),
         }
      },

      {
         hp_scalar = 1.1 * hpScaleFix, dmg_scalar = 0.85 * dmgScaleFix, upgrades = {kTechId.Armor3},
         tunnel_dropper = 0.06, tunnel_drop_rate = 0.1,
         alien_scale = 0.09 * waveScaleFix, max_delay_between_squad = 18, squads = {
            -- genSquad({ 0}, 2, 2, 1, 0, 0, 1, 0), -- Regular stuff
            -- genSquad({ 0}, 2, 6, 0, 0, 0, 0, 0),
            -- genSquad({ 0}, 2, 6, 0, 0, 0, 0, 1),
            -- -- genSquad({ 3}, 2, 5, 0, 0, 1, 0, 0),
            -- genSquad({ 5}, 2, 2, 0, 0, 0, 0, 0),
            -- genSquad({ 7}, 2, 0, 6, 0, 2, 0, 0),

            -- genSquad({ 0}, 2, 1, 1, 3, 0, 0, 0), -- Keep them busy with the onos while the lerks go high
            -- genSquad({ 5}, 2, 2, 0, 1, 0, 1, 1),
            -- genSquad({ 0}, 2, 1, 1, 4, 0, 0, 0),
            -- genSquad({ 3}, 2, 6, 0, 0, 0, 0, 0),
            genSquad({ 5}, 2, 6, 0, 0, 0, 0, 0),
            genSquad({ 3}, 2, 3, 2, 0, 0, 1, 0),
            genSquad({ 0}, 2, 1, 1, 3, 0, 0, 0),
            genSquad({ 5}, 2, 1, 1, 4, 0, 0, 0),

            genSquad({ 0}, 2, 6, 1, 0, 1, 0, 0),
            -- genSquad({ 0}, 2, 6, 1, 0, 0, 0, 0),
            genSquad({ 0}, 2, 0, 1, 6, 0, 0, 0),
            genSquad({ 3}, 2, 0, 1, 6, 0, 0, 0),
            genSquad({ 5}, 2, 2, 1, 3, 1, 0, 0),
            genSquad({10}, 2, 1, 0, 0, 0, 1, 0),
            genSquad({ 3}, 2, 0, 1, 6, 0, 0, 0),
            -- genSquad({ 5}, 2, 0, 1, 6, 0, 0, 0),
         }
      },

      {
         hp_scalar = 1.3, dmg_scalar = 1.15 * dmgScaleFix, upgrades = {},
         tunnel_dropper = 0.3, tunnel_drop_rate = 0.1,
         alien_scale = 0.14 * waveScaleFix, max_delay_between_squad = 18, squads = {
            -- genSquad({ 0}, 5, 2, 4, 0, 0, 1, 1), -- Regular stuff
            -- genSquad({ 0}, 5, 3, 4, 0, 0, 1, 0),
            -- genSquad({ 0}, 5, 6, 0, 0, 1, 0, 0),
            -- genSquad({ 5}, 5, 6, 0, 0, 0, 0, 0),
            -- genSquad({ 7}, 5, 0, 6, 0, 2, 0, 0),

            -- genSquad({ 0}, 5, 1, 1, 3, 0, 0, 0), -- Keep them busy with the onos while the lerks go high
            -- genSquad({ 5}, 5, 2, 1, 1, 0, 1, 1),
            -- genSquad({ 0}, 5, 1, 1, 4, 0, 0, 0),
            -- genSquad({ 5}, 5, 6, 0, 0, 0, 0, 0),
            -- genSquad({ 3}, 5, 3, 2, 0, 0, 1, 1),
            genSquad({ 0}, 5, 1, 1, 3, 0, 0, 0),
            genSquad({ 5}, 5, 1, 1, 4, 0, 0, 0),

            genSquad({ 7}, 5, 5, 0, 0, 1, 0, 0),
            genSquad({ 0}, 5, 0, 4, 0, 0, 1, 0),
            -- genSquad({ 0}, 5, 0, 4, 0, 0, 0, 0),
            genSquad({ 0}, 5, 0, 4, 1, 0, 0, 1),
            genSquad({ 3}, 5, 5, 0, 0, 0, 0, 0),
            -- genSquad({ 5}, 5, 0, 4, 0, 0, 0, 0),
            genSquad({ 0}, 5, 2, 1, 0, 0, 1, 0), -- Regular stuff
            genSquad({ 0}, 5, 5, 0, 0, 1, 0, 0),
            genSquad({ 5}, 5, 4, 0, 1, 0, 0, 0),
            genSquad({ 7}, 5, 0, 0, 2, 2, 0, 0),

            genSquad({ 0}, 5, 1, 1, 3, 0, 0, 0), -- Keep them busy with the onos while the lerks go high
            genSquad({ 5}, 5, 0, 2, 1, 0, 1, 1),
            genSquad({ 0}, 5, 3, 1, 4, 0, 0, 0),
            genSquad({ 5}, 5, 6, 0, 0, 0, 0, 0),
            genSquad({ 3}, 5, 3, 2, 0, 0, 1, 0),
            genSquad({ 0}, 5, 1, 1, 3, 0, 0, 0),
            genSquad({ 5}, 5, 1, 1, 4, 0, 0, 0),

            genSquad({ 7}, 5, 5, 0, 0, 1, 0, 1),
            genSquad({ 0}, 5, 0, 4, 2, 0, 0, 0),
            genSquad({ 0}, 5, 0, 4, 2, 0, 1, 0),
            genSquad({ 0}, 5, 5, 0, 5, 0, 1, 0),
            genSquad({ 5}, 5, 5, 0, 5, 0, 1, 0),
         }
      }
   }

function getWaveNb()
   return wave_nb
end

function isTraderWave()
   return trader_wave
end

function getBotsNumberAlive()
   local nb_bot = 0
   -- , "BoneWall" -- Do not count bonewalls, they are buggy
   for _, ent_name in ipairs({"Skulk", "Gorge", "Lerk", "Fade", "Onos"}) do
      for _, entity in ipairs(GetEntitiesForTeam(ent_name, 2)) do
         if (entity and entity:GetIsAlive() and DefIsVirtual(entity)) then
            nb_bot = nb_bot + 1
         end
      end
   end
   return nb_bot
end

function forceNextSquad(player, nb_squad)
   force_next = nb_squad and nb_squad or 1
end

function forceNextWave()
   if (isTraderWave()) then
      next_wave_time = Shared.GetTime()
   else
      squad_nb = 9999
      for _, ent_name in ipairs({"Skulk", "Babbler", "Gorge", "Lerk", "Fade", "Onos", "BoneWall"}) do
         for _, entity in ipairs(GetEntitiesForTeam(ent_name, 2)) do
            if (entity and entity:GetIsAlive() and DefIsVirtual(entity)) then
               entity:Kill()
            end
         end
      end
   end
   remaining_lifeform = {}
   unspawn_lifeform = {}
end

predef_lifeforms = {
   nil,
   nil,
   {
      {mapName = Babbler.kMapName , nb = 900},
      {mapName = Skulk.kMapName   , nb = 200},
      {mapName = Gorge.kMapName   , nb = 3},
      {mapName = Lerk.kMapName    , nb = 10},
      {mapName = Fade.kMapName    , nb = 2},
      {mapName = Onos.kMapName    , nb = 2},
      {mapName = BoneWall.kMapName, nb = 3},
      hook = true,
      alien_scale_bonus = 4,
   },
   {
      {mapName = Babbler.kMapName , nb = 50},
      {mapName = Skulk.kMapName   , nb = 5},
      {mapName = Gorge.kMapName   , nb = 2},
      {mapName = Lerk.kMapName    , nb = 75},
      {mapName = Fade.kMapName    , nb = 1},
      {mapName = Onos.kMapName    , nb = 3},
      {mapName = BoneWall.kMapName, nb = 2},
      hook = true,
   },
}

function generateWaves()
   local max_wave_nb = 5 + math.floor(getScalarPlayers(1, 8, 0, 1))
   local upgrades = {false,
                     kTechId.Armor1, kTechId.Weapons1,
                     kTechId.Armor2, kTechId.Weapons2,
                     kTechId.Armor3, kTechId.Weapons3}
   defense_waves_config = {}

   for i = 1, max_wave_nb do
      table.insert(defense_waves_config, {})
      if i <= #upgrades and upgrades[i] then
         defense_waves_config[i].upgrades = {upgrades[i]}
      else
         defense_waves_config[i].upgrades = {}
      end

      defense_waves_config[i].hp_scalar = getScalar(1, i, 1, max_wave_nb, 0.4, 1.2)
      defense_waves_config[i].dmg_scalar = getScalar(1, i, 1, max_wave_nb, 0.4, 1.1)
      defense_waves_config[i].speed_scalar = getScalar(1, i, 1, max_wave_nb, 0.4, 1.2)
      defense_waves_config[i].tunnel_dropper = math.random() / 4
      defense_waves_config[i].tunnel_drop_rate = 0.08
      defense_waves_config[i].alien_scale = getScalar(1, i, 1, max_wave_nb, 0.03, 0.08)
      if (predef_lifeforms[i] and predef_lifeforms[i].hook == true and predef_lifeforms[i].alien_scale_bonus) then
         defense_waves_config[i].alien_scale = defense_waves_config[i].alien_scale * predef_lifeforms[i].alien_scale_bonus
      end

      -- if (predef_lifeforms[i]) then
      --    lifeforms = predef_lifeforms[i]
      -- else
      local lifeforms =
         {
            {mapName = Babbler.kMapName , nb = 5  + getScalar(1, i, 1, max_wave_nb, 0, 160/2.1)},
            {mapName = Skulk.kMapName   , nb = 10 + getScalar(1, i, 1, max_wave_nb, 0, 90/2.1)},
            {mapName = Gorge.kMapName   , nb = 3  + getScalar(1, i, 1, max_wave_nb, 0, 55/2.1)},
            {mapName = Lerk.kMapName    , nb = 0  + getScalar(1, i, 1, max_wave_nb, 0, 60/2.1)},
            {mapName = Fade.kMapName    , nb = 0  + getScalar(1, i, 1, max_wave_nb, 0, 25/2.1)},
            {mapName = Onos.kMapName    , nb = 0  + getScalar(1, i, 1, max_wave_nb, 0, 12/2.1)},
            {mapName = BoneWall.kMapName, nb = 0  + getScalar(1, i, 1, max_wave_nb, 0, 16/2.1)}
         }
      -- end

      defense_waves_config[i].squads = {}
      local nb_squad  = 0--5  + getScalar(1, i, max_wave_nb, 1, max_wave_nb)

      -- One squad per lifeform, waves are a lot smoother in pressure
      for j = 1, #lifeforms do
         if (predef_lifeforms[i] and predef_lifeforms[i].hook == true) then
            lifeforms[j].nb = getScalar(1, i, 1, max_wave_nb, 0, predef_lifeforms[i][j].nb)
         end
         nb_squad = nb_squad + lifeforms[j].nb * getScalarPlayers(1, 17, 0.5, 1)
         --math.floor(getScalarPlayers(1, 10, lifeforms[j].nb, 0, 100))
      end

      local squad = {}
      for e = 1, nb_squad do
         local do_create_squad = false

         for id = 1, #lifeforms do
            if (squad[id] == nil) then
               squad[id] = 0
            end
            squad[id] = squad[id] + (lifeforms[id].nb / nb_squad)
         end

         for id = 1, #lifeforms do
            if (squad[id] > 0) then
               do_create_squad = true
               break
            end
         end

         if (do_create_squad) then
            table.insert(defense_waves_config[i].squads,
                         genSquad({0},
                                  math.floor(squad[1]),
                                  math.floor(squad[2]),
                                  math.floor(squad[3]),
                                  math.floor(squad[4]),
                                  math.floor(squad[5]),
                                  math.floor(squad[6]),
                                  math.floor(squad[7])
                         ))
            for id = 1, #lifeforms do
               squad[id] = squad[id] - math.floor(squad[id])
            end
         end
      end

   end
end

function resetWaves()
   init = true
   force_next = false
   wave_nb = 0
   squad_nb = 1
   trader_wave = 0
   trader_armory = nil
   next_wave_time = 0
   last_squad_created = 0
   setTraderArmoryId(-1)
   setTraderProtoId(-1)

   sacrified_marine = {} -- Do not keep sacrified marine in between rounds
   generateWaves()
end

local hive_build_count = 0
local hive_check_delay = Shared.GetTime()
local function _getBuildHiveNb()
   if (hive_check_delay + 1 < Shared.GetTime()) then
      hive_check_delay = Shared.GetTime()

      local nb_hive = 0
      for _, hive in ipairs(GetEntitiesForTeam("Hive", 2)) do
         if (hive.GetIsBuilt and hive:GetIsBuilt()) then
            nb_hive = nb_hive + 1
         end
      end
      hive_build_count = nb_hive
   end
   return hive_build_count
end

function getAlienSpeedScalar()
   if (wave_nb == 0) then
      return 1
   end

   local scalar = 1
   -- local wave_scalar = (1 / #defense_waves_config) * wave_nb
   scalar = getScalarPlayers(1, 16, 0.90, 1.15)

   local nb_hive = _getBuildHiveNb()

   -- if (GetGamerules():GetTeam2():GetNumPlayers() > 0) then
   --    scalar = scalar * 1.15 -- Less possible to put pressure by hand, make it easier
   -- end

   return scalar + scalar * (nb_hive * 0.05)
end

function getAlienHealthScalar()
   if (wave_nb == 0) then
      return 1
   end

   local scalar = 1
   local nb_hive = _getBuildHiveNb()

   -- defense_waves_config[wave_nb].hp_scalar
   local wave_scalar = (1 / #defense_waves_config) * wave_nb
   if (Shared.GetMapName() == "ns2_def_troopers") then
      scalar = getScalarPlayers(wave_scalar, 24, 0.6, 2)
   else
      scalar = getScalarPlayers(wave_scalar, 24, 0.6, 1.6)
   end

   return scalar + scalar * (nb_hive * 0.10)
end

function getAlienDamageScalar()
   local scalar = 1
   if (wave_nb == 0) then
      return scalar
   end

   local nb_hive = _getBuildHiveNb()

   -- defense_waves_config[wave_nb].dmg_scalar
   local wave_scalar = (1 / #defense_waves_config) * wave_nb
   scalar = getScalarPlayers(wave_scalar, 24, 0.10, 1.2)
   return scalar + scalar * (nb_hive * 0.08)
end

function areAllWavesFinished()
   if (wave_nb > #defense_waves_config and getBotsNumberAlive() == 0) then
      return true
   end
   return false
end

function getWaveAlienNb()
   local total_alien = 0
   local nb_players = getNbPlayerAliveForTeam1()
   local wave = defense_waves_config[wave_nb]
   for _, squad in ipairs(wave.squads) do
      if (squad.minplayer <= nb_players) then
         for _, alien in ipairs(squad) do
            total_alien = total_alien + alien.nb
         end
      end
   end
   return total_alien
end

function canAlienFieldComSpawnAlien(self, player, spawn_orig)
   local isPositionValid = true

   isPositionValid = getUnspawnedLifeform(self:GetDropMapName()) > 0
   if (not isPositionValid) then

      for _, bot in ipairs(GetEntitiesForTeam(self:GetDropMapName(), 2)) do
         if (bot and DefIsVirtual(bot) and #GetEntitiesForTeamWithinRange("Player", 1, bot:GetOrigin(), 25) == 0)
         then
            DestroyEntity(bot)
            isPositionValid = true
            break
         end
      end

      if (Server) then
         player:FadedMessage("No more " .. self:GetDropMapName() .. " to spawn")
      end
      return false
   end
   isPositionValid, errmsg = isValidAlienSpawnPointOrig(spawn_orig, true)
   if (not isPositionValid) then
      if (Server) then
         player:FadedMessage(errmsg)
      end
      return false
   end
   -- isPositionValid = wave_pop < wave_max_pop
   -- if (not isPositionValid) then
   --    if (Server) then
   --       player:FadedMessage("Max pop reached (" .. wave_max_pop .. " * " .. (wave_max_pop) .. ")")
   --    end
   --    return false
   -- end
   -- isPositionValid = isServerLoadGood()
   -- if (not isPositionValid) then
   --    if (Server) then
   --       player:FadedMessage("Server too busy (can't spawn more bots)")
   --    end
   --    return false
   -- end
   return true
end

function getMinAlienOnMap(automatic_mod)
   if (wave_nb == 0) then
      return 0
   end

   local function _popMalusScalePerAlienPlayer()
      local pop_malus = 0

      -- Make an alien player count as N bots to balance (quick to implement and good enough atm)
      for _, p in ipairs(GetGamerules():GetTeam2():GetPlayers()) do
         if (p and p:isa("Alien") and p:GetIsAlive() and not DefIsVirtual(p)) then
            if (p:isa("Skulk")) then
               pop_malus = 1
            elseif (p:isa("Gorge")) then
               pop_malus = 3
            elseif (p:isa("Lerk")) then
               pop_malus = 1.5 -- Lerk bots are probabily more evil/aggressive than players
            elseif (p:isa("Fade")) then
               pop_malus = 4
            elseif (p:isa("Onos")) then
               pop_malus = 5
            end
         end
      end

      return pop_malus
   end

   -- if (automatic_mod) then
   --    if (GetGamerules():GetTeam2():GetNumPlayers() > 0) then
   --       return 0 -- Only spawn if 0 alien on map
   --    end
   -- end

   local wave = defense_waves_config[wave_nb]
   local max_aliens = 50
   local nb_players = GetGamerules():GetTeam1():GetNumPlayers()--getNbPlayerAliveForTeam1()
   local min_aliens = 0

   if (wave) then
      min_aliens = math.floor(max_aliens - max_aliens * math.exp(-wave.alien_scale * nb_players))
   end

   min_aliens = Clamp(min_aliens - _popMalusScalePerAlienPlayer(), 5, max_aliens)
   if (Shared.GetMapName() == "ns2_def_troopers") then
      min_aliens = min_aliens * 1.6
   elseif (Shared.GetMapName() == "ns2_kf_farm") then
      min_aliens = min_aliens * 1.1
   end

   return min_aliens
end

function refreshPopCounterData()
   -- +5 to give the alien field com a bit of freedom (less restrictive)
   wave_pop = getBotsNumberAlive()
   wave_max_pop = getMinAlienOnMap(false) + 5
   for _, entname in ipairs({"Player", "Spectator"}) do
      for _, marine in ipairs(GetEntities(entname)) do
         if (not DefIsVirtual(marine)) then
            Server.SendNetworkMessage(marine, "LifeformCounterSetPop", {
                                         pop = wave_pop,
                                         max_pop = wave_max_pop}, true)
         end
      end
   end
end

function refreshAllCounterData(team_specifique)
   refreshPopCounterData()
   for _, entname in ipairs({"Player", "Spectator"}) do
      local ents = nil
      if (team_specifique) then
         ents = GetEntitiesForTeam(entname, team_specifique)
      else
         ents = GetEntities(entname)
      end

      for _, marine in ipairs(ents) do
         if (not DefIsVirtual(marine)) then
            Server.SendNetworkMessage(marine, "LifeformCounterSet", {
                                         lifeformMapName = Skulk.kMapName,
                                         nb = remaining_lifeform[Skulk.kMapName],
                                         unspawn_nb = unspawn_lifeform[Skulk.kMapName]}, true)
            Server.SendNetworkMessage(marine, "LifeformCounterSet", {
                                         lifeformMapName = Lerk.kMapName,
                                         nb = remaining_lifeform[Lerk.kMapName],
                                         unspawn_nb = unspawn_lifeform[Lerk.kMapName]}, true)
            Server.SendNetworkMessage(marine, "LifeformCounterSet", {
                                         lifeformMapName = Gorge.kMapName,
                                         nb = remaining_lifeform[Gorge.kMapName],
                                         unspawn_nb = unspawn_lifeform[Gorge.kMapName]}, true)
            Server.SendNetworkMessage(marine, "LifeformCounterSet", {
                                         lifeformMapName = Fade.kMapName,
                                         nb = remaining_lifeform[Fade.kMapName],
                                         unspawn_nb = unspawn_lifeform[Fade.kMapName]}, true)
            Server.SendNetworkMessage(marine, "LifeformCounterSet", {
                                         lifeformMapName = Onos.kMapName,
                                         nb = remaining_lifeform[Onos.kMapName],
                                         unspawn_nb = unspawn_lifeform[Onos.kMapName]}, true)
         end
      end
   end


   -- Shared:FadedMessage("Skulks: " .. tostring(unspawn_lifeform[Skulk.kMapName]))
   -- Shared:FadedMessage("Gorges: " .. tostring(unspawn_lifeform[Gorge.kMapName]))
   -- Shared:FadedMessage("Lerks : " .. tostring(unspawn_lifeform[Lerk.kMapName]))
   -- Shared:FadedMessage("Fades : " .. tostring(unspawn_lifeform[Fade.kMapName]))
   -- Shared:FadedMessage("Onoses: " .. tostring(unspawn_lifeform[Onos.kMapName]))

end

if (Server) then

   function lifeformSpawned(mapName)
      if (unspawn_lifeform[mapName] and unspawn_lifeform[mapName] > 0) then
         unspawn_lifeform[mapName] = unspawn_lifeform[mapName] - 1
         refreshAllCounterData(2) -- Marines do not need that info updaded
      end
   end

   function lifeformKilled(mapName)
      if (remaining_lifeform[mapName] and remaining_lifeform[mapName] > 0) then
         remaining_lifeform[mapName] = remaining_lifeform[mapName] - 1
         refreshAllCounterData()
      end
   end

   local skulkOnKill = Skulk.OnKill
   function Skulk:OnKill()
      if (skulkOnKill) then skulkOnKill(self) end
      lifeformKilled(Skulk.kMapName)
   end
   local gorgeOnKill = Gorge.OnKill
   function Gorge:OnKill()
      if (gorgeOnKill) then gorgeOnKill(self) end
      lifeformKilled(Gorge.kMapName)
   end
   local lerkOnKill = Lerk.OnKill
   function Lerk:OnKill()
      if (lerkOnKill) then lerkOnKill(self) end
      lifeformKilled(Lerk.kMapName)
   end
   local fadeOnKill = Fade.OnKill
   function Fade:OnKill()
      if (fadeOnKill) then fadeOnKill(self) end
      lifeformKilled(Fade.kMapName)
   end
   local onosOnKill = Onos.OnKill
   function Onos:OnKill()
      if (onosOnKill) then onosOnKill(self) end
      lifeformKilled(Onos.kMapName)
   end

   -- local babblerOnKill = Babbler.OnKill
   -- function Babbler:OnKill()
   --    if (babblerOnKill) then
   --       babblerOnKill(self)
   --    end
   --    lifeformKilled(Skulk.kMapName)
   -- end
end

function _isServerLoad(minTickRate, minIdlePer)
   -- local server, no stress checks test
   if (Server and Server.IsDedicated()) then
      local perfData = Shared.GetServerPerformanceData()
      if (perfData) then
         local dMs      = math.floor(perfData:GetDurationMs())
         local tickrate = math.floor(1000 / perfData:GetUpdateIntervalMs())
         local idlePer  = math.floor(100  * perfData:GetTimeSpentIdling() / dMs)

         if (tickrate <= minTickRate or idlePer <= minIdlePer) then
            Print("Warning: Server perf are dying, delaying aliens spawn (tickrate: " .. tostring(tickrate) .. " idle: " .. tostring(idlePer) .. "%%)")
            Print("Warning: Min tickrate/idle percent allowed: " .. tostring(minTickRate) .. " tick / " .. tostring(minIdlePer) .. "%% idle")
            return false
         end
      end
   end
   return true
end

function isServerLoadMin()
   return _isServerLoad(12, 10)
end

function isServerLoadGood()
   return _isServerLoad(18, 25)
end

local function triggerNextSquad()
   if (wave_nb == 0) then
      return -1
   end
   local wave = defense_waves_config[wave_nb]
   local bot_alive = getBotsNumberAlive()
   local min_alien = getMinAlienOnMap(true)

   -- for i = 1, 25 do
   --    local min_alien2 = math.floor(max_aliens - max_aliens * math.exp(-wave.alien_scale * i))
   --    Shared:FadedMessage("Min_alien for " .. tostring(i) .. " : " .. tostring(min_alien2))
   -- end
   -- Shared:FadedMessage("Min_alien for " .. tostring(nb_players) .. " : " .. tostring(min_alien))

   -- See lua/ServerPerformanceData.lua for these (info displayed in console with net_stats)
   if (not isServerLoadGood()) then
      return -1
   end

   if (bot_alive == 0 or bot_alive < min_alien) then
      return 0
   end
   -- if ((Shared.GetTime() - last_squad_created) > wave.max_delay_between_squad) then
   --    return 0
   -- end
   return -1
end

local function updateGUICounter()
   local wave = defense_waves_config[wave_nb]
   local total_wave = 0
   local progress = 0
   local total_squads = 0
   local current_squad_nb = 0

   local i = 1
   local nb_players = GetGamerules():GetTeam1():GetNumPlayers()
   for _, squad in ipairs(wave.squads) do
      if (squad_nb == i) then
         current_squad_nb = total_squads
      end
      if (nb_players >= squad.minplayer) then
         total_squads = total_squads + 1
      end
      i = i + 1
   end

   progress = (current_squad_nb / total_squads) * 100
   if (trader_wave) then
      progress = 100
   end
   -- for _, marine in ipairs(GetEntities("Player")) do
   --    Server.SendNetworkMessage(marine, "GUIAlienCounter", {
   --                                 progress = progress,
   --                                 wave_nb = wave_nb,
   --                                 wave_total = #defense_waves_config
   --                                                         }, true)
   -- end
end

local function getRandomTunnel()
   local _tunnels = GetEntitiesForTeam("TunnelEntrance", 2)
   local tunnels = {}
   local marines = {}

   for _, t in ipairs(_tunnels) do
      -- Do not use tunnel far away from marines
      marines = GetEntitiesForTeamWithinRange("Player", 1, t:GetOrigin(), 40)
      if (t:GetTunnelEntity() and #marines > 0 and t:GetIsBuilt()
             and t:GetCreationTime() + 5 < Shared.GetTime())
      then
         table.insert(tunnels, t)
      end
   end
   if (#tunnels > 0) then
      return tunnels[math.random(1, #tunnels)]
   end
   return nil
end

function wavesGetTunnelDropperChances() -- Chances to be a gorge capable of dropping tunnel
   local wave = defense_waves_config[wave_nb]

   if (wave) then
      return wave.tunnel_dropper
   end
   return 0
end

function wavesGetTunnelDropChances() -- Chances to spawn a tunnel each sec
   local wave = defense_waves_config[wave_nb]

   if (wave) then
      return wave.tunnel_drop_rate
   end
   return 0
end

local function spawnEntUsingTunnel(tunnel, mapname)
   local internal_tunnel = tunnel:GetTunnelEntity()

   l = spawnAlienCreature(mapname, getRandomRTAroundMarine():GetOrigin())

   if (l) then
      if (internal_tunnel:GetExitA()) then
         internal_tunnel:UseExit(l, internal_tunnel:GetExitA(), kTunnelExitSide.A)
      elseif (internal_tunnel:GetExitB()) then
         internal_tunnel:UseExit(l, internal_tunnel:GetExitB(), kTunnelExitSide.B)
      else
         return
      end
      -- function Tunnel:UseExit(entity, exit, exitSide)
   end
   return
end

local function spawnNextSquad()
   if (wave_nb == 0) then
      return 0
   end

   for _, marine in ipairs(GetEntities("Player")) do
      Server.SendNetworkMessage(marine, "GUIAlienCounter", {
                                   progress = nil,
                                   wave_nb = wave_nb,
                                   wave_total = #defense_waves_config
                                                           }, true)
   end

   local wave = defense_waves_config[wave_nb]

   local next_squad = nil
   local nb_players = GetGamerules():GetTeam1():GetNumPlayers()
   while (squad_nb <= #wave.squads) do
      next_squad = wave.squads[squad_nb]
      if (next_squad.minplayer <= nb_players) then
         local squad_spawn_point = getRandomRTAroundMarine()

         -- Shared:FadedMessage("Squad " .. tostring(squad_nb) .. "/" .. tostring(#wave.squads) .. " spawned")
         -- updateGUICounter()

         for _, alien in ipairs(next_squad) do
            for i = 1, alien.nb do
               if (getUnspawnedLifeform(alien.mapName) > 0) then
                  tunnel = getRandomTunnel()
                  if (tunnel and math.random() < 0.35) then
                     spawnEntUsingTunnel(tunnel, alien.mapName)
                  else
                     spawnAlienCreature(alien.mapName, squad_spawn_point:GetOrigin(), true)
                  end
                  lifeformSpawned(alien.mapName)
               end
            end
         end

         squad_nb = squad_nb + 1
         last_squad_created = Shared.GetTime()
         return 1
      end
      squad_nb = squad_nb + 1 -- Skip squad (only available with more players)
   end
   return 0
end

local last_message = 0
local function triggerNextWave()
   if (wave_nb == 0) then
      return 0
   end

   if (trader_wave == false or (trader_wave == true and next_wave_time < Shared.GetTime())) then
      if (wave_nb <= #defense_waves_config) then
         if (squad_nb > #defense_waves_config[wave_nb].squads
             and getBotsNumberAlive() == 0) then
            return 0
         end
      end
   end
   return -1
end

function TechTree:GetHasTech(techId)

    if techId == kTechId.None then
        return true
    else

        local hasTech = false

        local node = self:GetTechNode(techId)

        if node then
           hasTech = node:GetHasTech()
        end

        return hasTech

    end

    return false

end

local function startNextWave()
   squad_nb = 1
   trader_wave = false
   wave_nb = wave_nb + 1
   if (wave_nb <= #defense_waves_config) then
      local musics =
         {
            "sound/NS2.fev/ambient/ns1_pad_1",
            "sound/NS2.fev/ambient/ns1_pad_2",
            "sound/NS2.fev/ambient/ns1_pad_3"
         }


      local wave = defense_waves_config[wave_nb]
      local nb_players = getNbPlayerAliveForTeam1()

      remaining_lifeform = {}
      unspawn_lifeform = {}
      for _, squad in ipairs(wave.squads) do
         if (squad.minplayer <= nb_players) then
            for _, lifeform in ipairs(squad) do
               if (remaining_lifeform[lifeform.mapName] == nil) then
                  remaining_lifeform[lifeform.mapName] = 0
               end
               if (unspawn_lifeform[lifeform.mapName] == nil) then
                  unspawn_lifeform[lifeform.mapName] = 0
               end
               remaining_lifeform[lifeform.mapName] = remaining_lifeform[lifeform.mapName] + lifeform.nb
               unspawn_lifeform[lifeform.mapName] = unspawn_lifeform[lifeform.mapName] + lifeform.nb
            end
         end
      end

      -- for _, marine in ipairs(GetEntitiesForTeam("Player", 1)) do
      --    Server.SendNetworkMessage(marine, "PlayClientPrivateMusic", { music = "sound/NS2.fev/ambient/descent/docking_background_music", volume = 2 }, true)
      -- end

      if (wave) then
         if (wave.upgrades) then
            local marinetechtree = GetTechTree(kTeam1Index)
            for _, up in ipairs(wave.upgrades) do
               if (up and not marinetechtree:GetTechNode(up):GetResearched())
               then -- Unlock if not already on
                  local armslab = GetEntitiesForTeam("ArmsLab", 1)
                  if (#armslab > 0) then
                     marinetechtree:GetTechNode(up):SetResearched(true)
                     marinetechtree:QueueOnResearchComplete(up, armslab[1])
                  end
               end
            end
         end

         for _, marine in ipairs(GetEntitiesForTeam("Player", 1)) do
            local music = musics[math.random(1, #musics)]
            Server.SendNetworkMessage(marine, "PlayClientPrivateMusic", { music = music, volume = 2 }, true)
            if (marine.ClearOrders) then
               marine:ClearOrders()
            end
            Server.SendNetworkMessage(marine, "setTraderBuildingIds", { armory_id = -1, proto_id = -1 }, true)
         end
         setTraderArmoryId(-1)
         setTraderProtoId(-1)

      end
   end

   -- Shared:FadedMessage("Wave n'" .. tostring(wave_nb) .. "/" .. #defense_waves_config
   --                        .. " --> aliens: " .. tostring(getWaveAlienNb()))
end

local function triggerTraderWave()
   if (wave_nb > 0 and triggerNextWave() == 0 and trader_wave == false) then
      return 0
   end
   return -1
end

local function respawnDeadMarines()
   local marine_spawn_points = {}
   for _, alive_p in ipairs(GetEntitiesForTeam("InfantryPortal", 1))
   do
      table.insert(marine_spawn_points, alive_p)
   end
   -- if (#marine_spawn_points == 0) then
   --    for _, alive_p in ipairs(GetEntitiesForTeam("Armory", 1))
   --    do
   --       table.insert(marine_spawn_points, alive_p)
   --    end
   -- end
   if (#marine_spawn_points == 0) then
      for _, alive_p in ipairs(GetEntitiesForTeam("Player", 1))
      do
         table.insert(marine_spawn_points, alive_p)
      end
   end

   if (#marine_spawn_points > 0) then
      local i = math.random(1, #marine_spawn_points)
      for _, player in ipairs(GetEntitiesForTeam("Player", 1))
      do
         local sp = marine_spawn_points[i]:GetOrigin()
         local orig = GetLocationAroundFor(sp, kTechId.Marine, 12)
         if (player and player:GetIsAlive() ~= true) then
            local np = player:Replace(Marine.kMapName, 1, false, orig)
            if (np) then
               np:SetCameraDistance(0)
            end
         end
      end
   end
end

function skipTraderWave()
   next_wave_time = Shared.GetTime()
end

local function startTraderWave()
   remaining_lifeform = {}
   unspawn_lifeform = {}

   trader_wave = true
   if (Shared.GetMapName() == "ns2_def_troopers") then
      next_wave_time = Shared.GetTime() + 10
   else
      next_wave_time = Shared.GetTime() + trader_delay
   end

   if (wave_nb > #defense_waves_config) then
      next_wave_time = Shared.GetTime() + 0 -- Skip trader after end of last wave
   end

   for _, marine in ipairs(GetEntitiesForTeam("Player", 1)) do
      Server.SendNetworkMessage(marine, "PlayClientPrivateMusic", { music = "sound/NS2.fev/ambient/Decaypad", volume = 2 }, true)
   end
   respawnDeadMarines()
   -- updateGUICounter()
   local armories = GetEntitiesForTeam("Armory", 1)
   if (#armories > 0) then
      for i = 1, 20 do
         local it = math.random(1, #armories)
         new_armory = armories[it]
         if (new_armory ~= trader_armory) then
            break
         end
      end
      if (new_armory) then
         trader_armory = new_armory
      end
      ------
      local protolabs = GetEntitiesForTeamWithinRange("PrototypeLab", 1, trader_armory:GetOrigin(), 15)
      local proto_id = -1
      if (#protolabs > 0) then
         proto_id = protolabs[1]:GetId()
      end
      -- ------
      -- for _, marine in ipairs(GetEntitiesForTeam("Player", 1)) do
      --    if (marine.GiveOrder) then
      --       marine:GiveOrder(kTechId.Move, trader_armory:GetId(), nil)
      --    end
      --    Server.SendNetworkMessage(marine, "setTraderBuildingIds", { armory_id = trader_armory:GetId(), proto_id = proto_id }, true)
      -- end


      setTraderArmoryId(trader_armory:GetId())
      setTraderProtoId(proto_id)
   end
   -- Shared:FadedMessage("Next wave in " .. tostring(trader_delay) .. "s")
end

local wave_check_delay = 0
function handleWaves()
   if (not GetGamerules():GetGameStarted()) then
      return true
   end

   -- for _, skulk_player in ipairs(GetEntitiesForTeam("Alien", 2)) do
   --    if (skulk_player and skulk_player.client) then
   --       changeToAlienFieldCom(skulk_player)
   --    end
   -- end

   if (init == false) then
      resetWaves()
   end

   if (Shared.GetMapName() ~= "ns2_def_troopers") then
      randomBonusDrop()
   end

   -- wave_check_delay = wave_check_delay + timePassed
   -- if (wave_check_delay >= 1) then
   --    wave_check_delay = 0

   if (force_next) then
      for i = 1, math.min(30, force_next) do
         spawnNextSquad()
      end
      force_next = false
   else
      if (triggerTraderWave() == 0) then
         startTraderWave()
      elseif (triggerNextWave() == 0) then
         startNextWave()
      else
         local i = 0
         while (i < 4 and triggerNextSquad() == 0) do
            if (spawnNextSquad() ~= 1) then
               break
            end
            i = i + 1
         end
      end
   end
   -- end
   refreshAllCounterData()
   return true
end
