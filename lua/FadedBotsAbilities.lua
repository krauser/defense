

function defHasAbility(techId)

   ----------------------------
   -- Aliens
   if (Shared.GetMapName() == "ns2_def_troopers") then
      if (techId == kTechId.Xenocide) then
         return getWaveNb() >= 3
      else
         return true -- No hive on this map
      end
   end

   if (techId == kTechId.Leap) then
      return #GetEntitiesForTeam("Hive", 2) >= 1
   elseif (techId == kTechId.Xenocide) then
      return #GetEntitiesForTeam("Hive", 2) >= 2
   elseif (techId == kTechId.BileBomb) then
      return #GetEntitiesForTeam("Hive", 2) >= 1
   elseif (techId == kTechId.Spores or kTechId.Umbra) then
      return #GetEntitiesForTeam("Hive", 2) >= 1
   elseif (techId == kTechId.Stomp) then
      return #GetEntitiesForTeam("Hive", 2) >= 2
   elseif (techId == kTechId.BoneShield) then
      return true
   end

   ----------------------------
   -- Environnement on infest

   if (techId == kTechId.Whip) then
      return true
   elseif (techId == kTechId.Hydra) then
      return true
   elseif (techId == kTechId.BabblerEgg) then
      return true
   elseif (techId == kTechId.Web) then
      return true
   end

   return false
end
