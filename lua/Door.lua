-- ======= Copyright (c) 2003-2014, Unknown Worlds Entertainment, Inc. All rights reserved. =======
--
-- lua\Door.lua
--
--    Created by:   Charlie Cleveland (charlie@unknownworlds.com) and
--                  Max McGuire (max@unknownworlds.com)
--
-- ========= For more information, visit us at http://www.unknownworlds.com =====================

Script.Load("lua/ScriptActor.lua")
Script.Load("lua/Mixins/ModelMixin.lua")
Script.Load("lua/PathingMixin.lua")
Script.Load("lua/MapBlipMixin.lua")

-- Faded
Script.Load("lua/LiveMixin.lua")
Script.Load("lua/TeamMixin.lua")
Script.Load("lua/WeldableMixin.lua")

class 'Door' (ScriptActor)

Door.kMapName = "door"

Door.kInoperableSound = PrecacheAsset("sound/NS2.fev/common/door_inoperable")
Door.kOpenSound = PrecacheAsset("sound/NS2.fev/common/door_open")
Door.kCloseSound = PrecacheAsset("sound/NS2.fev/common/door_close")
Door.kLockSound = PrecacheAsset("sound/NS2.fev/common/door_lock")
Door.kUnlockSound = PrecacheAsset("sound/NS2.fev/common/door_unlock")

Door.kState = enum( {'Open', 'Close', 'Locked', 'DestroyedFront', 'DestroyedBack', 'Welded'} )
Door.kStateSound = { [Door.kState.Open] = Door.kOpenSound,
                     [Door.kState.Close] = Door.kCloseSound,
                     [Door.kState.Locked] = Door.kLockSound,
                     [Door.kState.DestroyedFront] = "",
                     [Door.kState.DestroyedBack] = "",
                     [Door.kState.Welded] = Door.kLockSound,  }

local kUpdateAutoUnlockRate = 1
local kUpdateAutoOpenRate = 0.3
local kWeldPercentagePerSecond = 1 / kDoorWeldTime
local kHealthPercentagePerSecond = 0.9 / kDoorWeldTime

local kModelNameDefault = PrecacheAsset("models/misc/door/door.model")
local kModelNameClean = PrecacheAsset("models/misc/door/door_clean.model")
local kModelNameDestroyed = PrecacheAsset("models/misc/door/door_destroyed.model")

local kDoorAnimationGraph = PrecacheAsset("models/misc/door/door.animation_graph")

local networkVars =
{
    weldedPercentage = "float",

    -- Stores current state (kState )
    state = "enum Door.kState",
    damageFrontPose = "float (0 to 100 by 0.1)",
    damageBackPose = "float (0 to 100 by 0.1)"
}

AddMixinNetworkVars(BaseModelMixin, networkVars)
AddMixinNetworkVars(ModelMixin, networkVars)
AddMixinNetworkVars(LiveMixin, networkVars) -- Faded
AddMixinNetworkVars(TeamMixin, networkVars) -- faded

local kDoorLockTimeout = 6
local kDoorLockDuration = 4

local function UpdateAutoUnlock(self, timePassed)

    -- auto open the door after kDoorLockDuration time has passed
    local state = self:GetState()

    if state == Door.kState.Locked and self.timeLastLockTrigger + kDoorLockDuration < Shared.GetTime() then

        self:SetState(Door.kState.Open)
        self.lockTimeOut = Shared.GetTime() + kDoorLockTimeout

    end

    return true

end

local function UpdateAutoOpen(self, timePassed)

    -- If any players are around, have door open if possible, otherwise close it
    local state = self:GetState()

    if (self:GetIsAlive()) then
       if state == Door.kState.Open or state == Door.kState.Close then

          local desiredOpenState = false

          local entities = Shared.GetEntitiesWithTagInRange("Door", self:GetOrigin(), DoorMixin.kMaxOpenDistance)
          for index = 1, #entities do

             local entity = entities[index]
             local opensForEntity, openDistance = entity:GetCanDoorInteract(self)

             if opensForEntity then

                local distSquared = self:GetDistanceSquared(entity)
                if (not HasMixin(entity, "Live") or entity:GetIsAlive()) and entity:GetIsVisible() and distSquared < (openDistance * openDistance) then

                   -- Faded: only marines can open doors
                   -- if (entity:GetTeamNumber() == 1) then
                      desiredOpenState = true
                   -- end
                   break

                end

             end

          end

          if desiredOpenState and self:GetState() == Door.kState.Close then
             self:SetState(Door.kState.Open)
          elseif not desiredOpenState and self:GetState() == Door.kState.Open then
             self:SetState(Door.kState.Close)
          end

       end
    else
       self:SetState(Door.kState.Open)
    end

    return true

end

local function InitModel(self)

    local modelName = kModelNameDefault
    if self.clean then
        modelName = kModelNameClean
    end

    self:SetModel(modelName, kDoorAnimationGraph)

end

function Door:OnCreate()

    ScriptActor.OnCreate(self)

    InitMixin(self, BaseModelMixin)
    InitMixin(self, ModelMixin)
    InitMixin(self, PathingMixin)
    InitMixin(self, LiveMixin) -- Faded
    InitMixin(self, TeamMixin)

    if Server then

        -- self:AddTimedCallback(UpdateAutoUnlock, kUpdateAutoUnlockRate)
        -- self:AddTimedCallback(UpdateAutoOpen, kUpdateAutoOpenRate)

    end

    self.state = Door.kState.Open


end


------------- Faded

function Door:GetHealthbarOffset()
    return 2
end

-- function Door:OnTakeDamage()
--    Shared:FadedMessage("It huuurts (hp/ap: "
--                           .. self:GetHealth() .. ":"
--                           .. self:GetArmor() .. ")")
-- end

local kDamagedCinematic = PrecacheAsset("cinematics/marine/exo/hurt_severe.cinematic")

-- local doorOnKillClient = Door.OnKillClient
-- function Door:OnKillClient()
--    if (doorOnKillClient) then
--       doorOnKillClient(self)
--    end
--    if (self.damagedCinematic) then
--       -- Client.DestroyCinematic(damagedCinematic)
--       -- damagedCinematic = nil
--       self.damagedCinematic:SetIsVisible(true)
--    end
-- end

function Door:PreOnKill()
   Shared:FadedMessage(self:GetLocationName() .. " door has been destroyed\n")
   self:SetState(Door.kState.Open)

   local params = {}
   params[kEffectHostCoords] = Coords.GetLookIn( self:GetOrigin(), self:GetCoords().zAxis )
   params[kEffectSurface] = "metal"

   self:TriggerEffects("grenade_explode", params)
end
-------------------



function Door:OnInitialized()

    ScriptActor.OnInitialized(self)

    InitMixin(self, WeldableMixin)

    if Server then

        InitModel(self)

        self:SetPhysicsType(PhysicsType.Kinematic)

        self:SetPhysicsGroup(PhysicsGroup.CommanderUnitGroup)

        -- This Mixin must be inited inside this OnInitialized() function.
        if not HasMixin(self, "MapBlip") then
            InitMixin(self, MapBlipMixin)
        end

    end

    if (Client) then
       local coords = self:GetCoords()

       self.damagedCinematic = Client.CreateCinematic(RenderScene.Zone_Default)
       self.damagedCinematic:SetCinematic(kDamagedCinematic)
       self.damagedCinematic:SetRepeatStyle(Cinematic.Repeat_Endless)
       self.damagedCinematic:SetCoords(coords)
       self.damagedCinematic:SetIsVisible(false)
    end

    self:SetTeamNumber(1)
end

function Door:Reset()
   if (self.damagedCinematic) then
      self.damagedCinematic:SetIsVisible(false)
   end

   self:SetPhysicsType(PhysicsType.Kinematic)
   self:SetPhysicsGroup(0)

   self:SetState(Door.kState.Close)

   InitModel(self)

end

-- Faded
function Door:GetShowHealthFor(player)
    return true  -- false
end

-- Faded
function Door:GetReceivesStructuralDamage()
    return true -- false
end

function Door:GetIsWeldedShut()
    return self:GetState() == Door.kState.Welded
end

function Door:GetDescription()

    local doorName = GetDisplayNameForTechId(self:GetTechId())
    local doorDescription = doorName

    local state = self:GetState()

    if state == Door.kState.Welded then
        doorDescription = string.format("Welded %s", doorName)
    end

    return doorDescription

end

function Door:SetState(state, commander)

   if self.state ~= state then

      self.state = state

      if Server then

         local sound = Door.kStateSound[self.state]
         if sound ~= "" then

            self:PlaySound(sound)

            if commander ~= nil then
               Server.PlayPrivateSound(commander, sound, nil, 1.0, commander:GetOrigin())
            end

         end

      end

   end

end

function clearAllDeadDoorEffects()
   for _, door in ipairs(GetEntities("Door")) do
      if (door.damagedCinematic) then
         door.damagedCinematic:SetIsVisible(false)
      end
   end
end

function Door:GetState()
    return self.state
end

function Door:GetCanBeUsed(player, useSuccessTable)
   -- if (player and player:GetTeamNumber() == 1) then
   if (player and player:GetTeamNumber() ~= 1) then
      useSuccessTable.useSuccess = false
   end
   if (self.defenseLastUsed == nil) then
      self.defenseLastUsed = 0
   end
   if (self.defenseLastUsed and self.defenseLastUsed + 2.2 < Shared.GetTime()) then
      useSuccessTable.useSuccess = true
   else
      useSuccessTable.useSuccess = false
   end
   if (not self:GetIsAlive()) then
      if (self.defenseLastDestroyMsg == nil or self.defenseLastDestroyMsg + 1 < Shared.GetTime()) then
         if (Server) then
            Server.PlayPrivateSound(player, "sound/NS2.fev/common/door_inoperable", nil, 1.0, player:GetOrigin())
         end
         -- Server.SendNetworkMessage(player, "PlayClientPrivateMusic", { music = , volume = 2 }, true)
         -- player:FadedMessage("This door has been destroyed")
         self.defenseLastDestroyMsg = Shared.GetTime()
      end
   end
   -- else
   --    useSuccessTable.useSuccess = false
   -- end
end

function Door:OnUse(player, elapsedTime, useSuccessTable)
   self:GetCanBeUsed(player, useSuccessTable)
   if (self:GetIsAlive()) then
      if (useSuccessTable.useSuccess) then
         self.defenseLastUsed = Shared.GetTime()
         if (self:GetState() == Door.kState.Locked) then
            -- player:FadedMessage("Opening door")
            self:SetState(Door.kState.Open)
         else
            -- player:FadedMessage("Locking door")
            self:SetState(Door.kState.Locked)
         end
      end
   end
end

function Door:OnUpdateRender()
   if (Client and not self:GetIsAlive()) then
      self.damagedCinematic:SetIsVisible(true)
   end
end

function Door:OnUpdateAnimationInput(modelMixin)

   PROFILE("Door:OnUpdateAnimationInput")

   local open = self.state == Door.kState.Open
   local lock = self.state == Door.kState.Locked or self.state == Door.kState.Welded

   modelMixin:SetAnimationInput("open", open)
   modelMixin:SetAnimationInput("lock", lock)
end

Shared.LinkClassToMap("Door", Door.kMapName, networkVars)
