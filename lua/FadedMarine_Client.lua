-- ===================== Faded Mod =====================
--
-- lua\FadedMarine_Client.lua
--
--    Created by: Rio (rio@myrio.de)
--
-- =====================================================

local random = math.random


Player.screenEffects.distortion = Client.CreateScreenEffect("shaders/Disorient.screenfx")
Player.screenEffects.distortion:SetActive(false)


Player.screenEffects.distortion2 = Client.CreateScreenEffect("shaders/FadeBlink.screenfx")
Player.screenEffects.distortion2:SetActive(false)

Marine.radarPong = PrecacheAsset("sound/NS2.fev/common/ping")
Marine.radarPing = PrecacheAsset("sound/NS2.fev/marine/structures/observatory_scan")

function PlaySound(entity, cachedSound, volume, pitch)

   if not entity.forsakenSoundInstances then
      entity.forsakenSoundInstances = {}
   end

   if not entity.forsakenSoundInstances[cachedSound] then
      local soundIndex = Shared.GetSoundIndex(cachedSound)
      entity.forsakenSoundInstances[cachedSound] = Client.CreateSoundEffect(soundIndex)
   else
      entity.forsakenSoundInstances[cachedSound]:Stop()
   end

   local soundInstance = entity.forsakenSoundInstances[cachedSound]
   soundInstance:SetParent(Client.GetLocalPlayer():GetId())
   soundInstance:Start()

   soundInstance:SetVolume(volume)
   soundInstance:SetPitch(pitch)
end

function StopSound(entity, cachedSound)
   if not entity.forsakenSoundInstances then
      return
   end

   if entity.forsakenSoundInstances[cachedSound] then
      entity.forsakenSoundInstances[cachedSound]:Stop()
   end
end


-- TODO: Surcharger les fonction "askMedpack/ammo" pour eviter les erreurs

-- Player.screenEffects.distortion = Client.CreateScreenEffect("shaders/Distortion.screenfx")
-- Player.screenEffects.distortion:SetActive(true)

-- kPlayerEffectData["distortion_loop"] =
-- {
--     distortionFX =
--     {
--         {looping_sound = "sound/NS2.fev/alien/structures/shade/disorientate", active = true, done = true},
--         {stop_sound = "sound/NS2.fev/alien/structures/shade/disorientate", active = false, done = true},
--     }
-- }

-- local marineUpdateClientEffects = Marine.UpdateClientEffects
-- function Marine:UpdateClientEffects(deltaTime, isLocal)
--     marineUpdateClientEffects(self, deltaTime, isLocal)

--     Player.screenEffects.distortion:SetActive(true)
--     if Player.screenEffects.distortion then
--        -- Print("Marine:UpdateClientEffects: set param")
--        Player.screenEffects.distortion:SetParameter("startTime", Shared.GetTime())
--         Player.screenEffects.distortion:SetParameter("time", Shared.GetTime())
--         Player.screenEffects.distortion:SetParameter("amount", random(0, 9))
--     end
-- end

-- local playerOnDestroy = Marine.OnDestroy
-- function Marine:OnDestroy()
--     playerOnDestroy(self)
--     self:UpdateDistortionSoundLoop(false)
-- end

-- local playerUpdateScreenEffects = Marine.UpdateScreenEffects
-- function Marine:UpdateScreenEffects(deltaTime)
--     playerUpdateScreenEffects(self, deltaTime)
--     self:UpdateDistortionFX()
-- end

-- function Marine:UpdateDistortionFX()
--     if Player.screenEffects.distortion then
--         local amount = 0
--         if HasMixin(self, "Distortion") then
--             amount = self:GetDistortionAmount()
--         end

--         local state = (amount > 0)
--         if not self:GetIsThirdPerson() or not state then
--             Player.screenEffects.distortion:SetActive(state)
--         end

--         Player.screenEffects.distortion:SetParameter("amount", amount)

--     end

--     self:UpdateDistortionSoundLoop(state)
-- end

-- function Marine:UpdateDistortionSoundLoop(state)
--     if state ~= self.playerDisorientSoundLoopPlaying then
--         self:TriggerEffects("distortion_loop", {active = state})
--         self.playerDisorientSoundLoopPlaying = state
--     end
-- end

local marineOnInitialize = Marine.OnInitialized
function Marine:OnInitialized()
    marineOnInitialize(self)

    if (Client) then
        self:AddHelpWidget("FadedGUIMarineBuyHelp", kFadedModGUIHelpLimit)
    end
end

local marineHandleButtons = Marine.HandleButtons
function Marine:HandleButtons(input)
    marineHandleButtons(self, input)

    //if Client and not Shared.GetIsRunningPrediction() then

        self.buyLastFrame = self.buyLastFrame or false
        -- Player is bringing up the buy menu (don't toggle it too quickly)
        local buyButtonPressed = bit.band(input.commands, Move.Buy) ~= 0
        if not self.buyLastFrame and buyButtonPressed and Shared.GetTime() > (self.timeLastMenu + 0.3) then
            self:Buy()
            self.timeLastMenu = Shared.GetTime()
        end

        self.buyLastFrame = buyButtonPressed

    //end
end

function Marine:FadedEquipmentMenuOpen()
    return self.fadedEquipmentMenu ~= nil
end

--local distortion = 0
function Marine:Buy()
   -- Don't allow display in the ready room
   if (self:GetTeamNumber() == 1 and Client.GetLocalPlayer() == self) then
      -- Player.screenEffects.distortion:SetActive(true)
      -- Player.screenEffects.distortion:SetParameter("amount", distortion)
      --       distortion = distortion + 0.1
      if (not self.fadedEquipmentMenu) then
         self.fadedEquipmentMenu = GetGUIManager():CreateGUIScript("FadedGUIMarineEquipmentMenu")
         MouseTracker_SetIsVisible(true, "ui/Cursor_MenuDefault.dds", true)
      else
         self:CloseMenu()
      end
   end
end

local marineCloseMenu = Marine.CloseMenu
function Marine:CloseMenu()
   if (self.fadedEquipmentMenu) then
      GetGUIManager():DestroyGUIScript(self.fadedEquipmentMenu)
      self.fadedEquipmentMenu = nil
      MouseTracker_SetIsVisible(false)
      return true
   else
      return (marineCloseMenu(self))
   end

   return false
end

function JetpackMarine:Buy()
   Marine.Buy(self)
end

function JetpackMarine:CloseMenu()
    Marine.CloseMenu(self)
end

-- local oldMarineBuy_GetEquipment = MarineBuy_GetEquipment
-- function MarineBuy_GetEquipment()
--    local player = Client.GetLocalPlayer()
--    local items = GetChildEntities( player, "ScriptActor" )
--    local inventory = oldMarineBuy_GetEquipment()

--    for index, item in ipairs(items) do
--       local techId = item:GetTechId()

--       if techId == kTechId.FlameMine or techId == kTechId.Mine or techId == kTechId.LaySentryBattery
--       then
--          inventory[kTechId.FlameMine] = true
--          inventory[kTechId.Mine] = true
--          inventory[kTechId.LaySentryBattery] = true
--       end
--    end

--    return inventory

-- end

function Client:GetSelectedWeapon()
    self.selectedWeapon = self.selectedWeapon or kTechId.Rifle
    return self.selectedWeapon
end

function Client:GetSelectedEquipment()
    self.selectedEquipment = self.selectedEquipment or kTechId.Welder
    return self.selectedEquipment
end

function Client:SetSelectedEquipment(weapon, equipment)
    self.selectedWeapon = weapon or kTechId.Rifle
    self.selectedEquipment = equipment or kTechId.Welder
    Client:SendSelectEquipmentMessage()
end

local marineOnCountDown = Marine.OnCountDown
function Marine:OnCountDown()
   marineOnCountDown(self)
   Client:SendSelectEquipmentMessage()
end

function Client:SendSelectEquipmentMessage()
   local local_player = Client.GetLocalPlayer()
   local weap = self:GetSelectedWeapon()
   local equip = self:GetSelectedEquipment()

   -- TODO: for testing, remove
   -- if (math.random(0, 2) == 1) then
   --    Client.SendNetworkMessage("SelectEquipment", { Weapon = weap,
   --                              Equipment = kTechId.Scan}, true)
   -- else
      Client.SendNetworkMessage("SelectEquipment", { Weapon = weap,
                             Equipment = equip}, true)
   -- end

   local_player.kFadedConvertToHeavyShotgun = false
   if (local_player and local_player:isa("Marine") and local_player:GetIsAlive())
   then
      if (LookupTechData(weap, kTechDataMapName) == HeavyShotgun.kMapName) then
     local_player.kFadedConvertToHeavyShotgun = true
      end
      -- local gameTime = GetGamerules():GetGameStartTime()
      -- if gameTime ~= 0 then
      --      gameTime = floor(Shared.GetTime()) - gameTime
      -- end

      -- if (gameTime < kFadedModTimeInSecondsSelectingEquipmentIsAllowed) then
      --      if (LookupTechData(equip, kTechDataMapName) == Scan.kMapName) then
      --         -- local_player.kFadedHunter = true
      --         local_player:PromoteToHunter()
      --      else
      --         local_player:ClearHunterAbility()
      --      end
      -- end
   end
end

---------------------
