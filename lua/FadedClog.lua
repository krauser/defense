

-- if (Server) then
   local clogs_to_refresh = {}

   function clearAllClogsWhip()
      clogs_to_refresh = {}
   end

   local function getChildPos(clog)
      local extents = GetExtents(kTechId.Clog)
      local direction = Vector(clog:GetAngles():GetCoords().yAxis)

      return (clog:GetOrigin() + direction * 1.1)
   end

   function _clog_opti(self)
      if (self.clogRoot) then
         if (self.clogRootDeleteChildOutLOSOpti == nil) then
            self.clogRootDeleteChildOutLOSOpti = Shared.GetTime()
         end
         if (self.clogRootDeleteChildOutLOSOpti < Shared.GetTime()) then
            self.clogRootDeleteChildOutLOSOpti = Shared.GetTime() + 1
            local clog_create = false
            local clog_destroy = false
            local max_dist = 40
            local id = FindNearestEntityId("Player", self:GetOrigin())
            if (id ~= -1) then
               local player = Shared.GetEntity(id)
               local is_seen = false
               if (player and player:GetIsAlive()
                      and not GetWallBetween(self:GetOrigin(),
                                             player:GetModelOrigin())) then
                  is_seen = true
               end
               local dist = self:GetOrigin():GetDistanceTo(player:GetOrigin())
               if (self.clogChild == nil and not is_seen and dist < 10) then
                  clog_create = true
               elseif (is_seen == false and dist > 15) then
                  clog_destroy = true
               end
            end
            Shared:FadedMessage("Clog destory: " .. tostring(clog_destroy) .. " / " .. tostring(clog_create))
            if (clog_destroy) then
               local child = Shared.GetEntity(self.clogChild)
               self.clogChild = nil
               child:Kill()
            elseif (clog_create) then
               _createClogWhip(self, self.clogRootChildNb)
            end
         end
      end
   end

   function refreshAllClogWhipPos()
      local i = 1
      while i <= #clogs_to_refresh do
         if (clogs_to_refresh[i]["triggerTime"] < Shared.GetTime()) then


            local clog = Shared.GetEntity(clogs_to_refresh[i]["clogId"])

            if (clog and clog.SyncAttachPos) then
               clog:SyncAttachPos()
               _clog_opti(clog)
            end

            -- if (clog and clog.clogParent) then
            --    local parent = Shared.GetEntity(clog.clogParent)
            --    if (parent) then
            --       clog:SetOrigin(getChildPos(parent))
            --       clog:SetAngles(SlerpAngles(clog:GetAngles(), parent:GetAngles(), 0.5))
            --    end
            --    -- setAllChildPos(child)
            -- end
            table.remove(clogs_to_refresh, i)
         else
            i = i + 1
         end
      end
   end

   local function registerAllChildPos(clog, triggerTime)
      if (clog and clog.clogChild) then
         local new_triggerTime = triggerTime + 2
         -- Entity.AddTimedCallback(Shared.GetEntity(clog.clogChild),
         --                         setAllChildPos, 1)
         -- setAllChildPos()
         table.insert(clogs_to_refresh,
                      {
                         ["triggerTime"] = new_triggerTime,
                         ["clogId"] = clog.clogChild
                      } )
         registerAllChildPos(Shared.GetEntity(clog.clogChild), new_triggerTime)
      end
      -- if (clog and clog.clogParent) then
      --    local parent = Shared.GetEntity(clog.clogParent)
      --    clog:SetAngles(parent:GetAngles())
      -- end
   end

   local function _createClogWhip(parent, nb)
      local new_clog = CreateEntity(Clog.kMapName, parent:GetOrigin(),
                                    parent:GetTeamNumber())
      if (new_clog) then
         -- new_clog.modelSize = Clamp(parent.modelSize / 1.1, 0.2, 2)
         parent.clogChild = new_clog:GetId()
         new_clog.clogParent = parent:GetId()

         -- new_clog:SetParent(parent)
         -- new_clog:SetAttachPoint("babbler_attach1")
         -- new_clog:SetAttachPoint(parent:GetOrigin() + Vector(0, extents.y, 0))

         -- Shared:FadedMessage("Clog created (left: " .. tostring(nb))
         if (nb > 0) then
            new_clog.moveSpeed = new_clog.moveSpeed * 1.1
            new_clog.xSeed = new_clog.xSeed / 1.1
            new_clog.ySeed = new_clog.ySeed / 1.1
            if (math.random() <= 0.1) then
               new_clog.clogOwnAngle = true

            end
            _createClogWhip(new_clog, nb - 1)
         end
      else
         Shared:FadedMessage("Failed to create Clog")
      end
   end
   function Clog:createClogWhip(nb)
      self.clogRoot = true
      _createClogWhip(self, nb)
   end

   local function getCosSeed(self)
      return (Shared.GetTime() - self.angleSeed)
   end

   function Clog:UpdatePos()
      if (self.clogInitDone) then
         -- Divide to slow down the movement
         local seed = getCosSeed(self) * (self.moveSpeed)
         -- Divide to reduce range interval
         local range1 = math.cos(seed) * self.xSeed
         local range3 = math.sin(seed) * self.ySeed
         -- local vector_diff = Vector(0, 0, range)
         -- self:SetOrigin(self.clogCreationOrig + vector_diff)

         -- SlerpAngles(self:GetAngles(), Angles(0, 0, range), 1)
         local parent = nil
         if (self.clogParent) then
            parent = Shared.GetEntity(self.clogParent)
         end
         if (not parent or self.clogOwnAngle) then
            self:SetAngles(Angles(range1, 0, range3))
         else
            self:SetAngles(parent:GetAngles())
         end
         registerAllChildPos(self, Shared.GetTime())
      end
      -- if (self.clogChild) then
      --    local child = Shared.GetEntity(self.clogChild)
      --    child.clogCreationOrig = self:GetOrigin() + vector_diff
      -- end
   end

   -- function Clog:OnAdjustModelCoords(modelCoords)
   --    local coords = modelCoords
   --    local scale = self.modelsize
   --    if (scale == nil) then
   --       if (self.clogParent) then
   --       end
   --       scale = 1
   --    end
   --    -- local rand = ((math.random(1, 10) - 5) / 10)
   --    coords.xAxis = coords.xAxis * scale -- Largeur
   --    coords.yAxis = coords.yAxis * scale -- Hauteur
   --    coords.zAxis = coords.zAxis * scale -- Profondeur
   --    return coords
   -- end

   function ClogKillAfterTimeout(self)
      if (self and self:GetIsAlive() and (self.clogRoot or self.clogChild or self.clogParent))
      then
         self:Kill()
      end
   end

   local clogOnCreate = Clog.OnCreate
   function Clog:OnCreate()
      clogOnCreate(self)
      local precision = 100
      -- self.modelSize = 1
      self.clogRoot = false
      local angle = 1
      self.xSeed = angle - math.random()
      self.ySeed = angle - self.xSeed
      self.angleSeed = Shared.GetTime()-- + math.random(1, 100)
      self.moveSpeed = math.random(0.5 * precision, 1 * precision) / precision
      -- self.clogPos = math.cos(getCosSeed(self))
      self.clogChild = nil
      self.clogParent = nil
      self.clogInitDone = true
   end

   local clogOnInitialized = Clog.OnInitialized
   function Clog:OnInitialized()

      clogOnInitialized(self)
      if (Server) then
         Entity.AddTimedCallback(self, ClogKillAfterTimeout, 40 + math.random() * 10)
      end
   end

   -- function Clog:OnUpdate2(deltaTime)
   --    if (self.clogDelta == nil) then
   --       self.clogDelta = FindNearestEntityId("Clog", self:GetOrigin())
   --    end
   --    if (self.clogDelta) then
   --       local clog = Shared.GetEntity(self.clogDelta)
   --       if (clog) then
   --          local direction = Vector(clog:GetAngles():GetCoords().xAxis)

   --          self:SetOrigin(clog:GetOrigin() - direction * 1.2)
   --          local angle = clog:GetAngles()
   --          angle.roll = angle.roll + math.rad(90)
   --          -- angle.pitch = angle.pitch + math.rad(90)
   --          -- angle.yaw = angle.yaw + math.rad(90)
   --          self:SetAngles(angle)
   --       else
   --          self.clogDelta = nil
   --       end
   --    end
   -- end

   function Clog:SyncAttachPos()
      if (self.clogParent) then
         local parent = Shared.GetEntity(self.clogParent)
         local extents = GetExtents(kTechId.Clog)

         if (parent)
         then
            local seed = getCosSeed(self) * (self.moveSpeed)
            -- Divide to reduce range interval
            local range1 = math.cos(seed) * self.xSeed
            local range3 = math.sin(seed) * self.ySeed
            -- local vector_diff = Vector(0, 0, range)
            -- self:SetOrigin(self.clogCreationOrig + vector_diff)

            -- SlerpAngles(self:GetAngles(), Angles(0, 0, range), 1)

            -- if (self:GetOrigin():GetDistanceTo(getChildPos(parent)) > 1.2)
            -- then
            self:SetOrigin(getChildPos(parent))
            self:SetAngles(parent:GetAngles())
            local pa = self:GetAngles()
            pa.pitch = pa.pitch + range1
            pa.yaw = pa.yaw + range1
            pa.roll = pa.roll + range1
            self:SetAngles(pa)
            -- self:SetAngles(SlerpAngles(parent:GetAngles(), Angles(-1,0,-1), 0.5))
            -- end
         end
      end
   end

   local clogOnUpdate = Clog.OnUpdate
   function Clog:OnUpdate(timePassed)
      if (clogOnUpdate) then
         clogOnUpdate(self, timePassed)
      end

      -- self:OnUpdate2(timePassed)
      -- if (nextRefresh < Shared.GetTime()) then
      --    nextRefresh = Shared.GetTime() + 1

      --    if (self.clogRoot and not self.clogRootInit) then
      --       createClogWhip(self, 8)
      --       self.clogRoot = true
      --       self.clogRootInit = true
      --    end
      -- end
      if (Server and self.clogRoot) then
         self:UpdatePos()
      else
         self:SyncAttachPos()
      end
      -- if (self:GetOwnerEntityId() == nil) then
      --    Shared:FadedMessage("Creating 5 clogs")
      -- end
   end

   local clogOnKill = Clog.OnKill
   function Clog:OnKill()

      if (self.clogParent) then
         local parent = Shared.GetEntity(self.clogParent)
         if (parent) then
            parent.clogChild = nil
         end
      end
      if (self.clogChild) then
         local child = Shared.GetEntity(self.clogChild)
         if (child) then
            child:Kill()
         end
      end

      clogOnKill(self)

   end
-- elseif Client then

   function Clog:GetShowHealthFor()
      return true
   end
-- end
