-- Script.Load("lua/Weapons/Marine/Grenade.lua")
-- -- Script.Load("lua/GameEffectsMixin.lua")
-- -- Script.Load("lua/FireMixin.lua")

-- -- local networkVars =
-- -- {
-- -- }

-- -- AddMixinNetworkVars(GameEffectsMixin, networkVars)
-- -- AddMixinNetworkVars(FireMixin, networkVars)


-- if (Server) then

--    local grenadeOnCreate = Grenade.OnCreate
--    function Grenade:OnCreate()
--       grenadeOnCreate(self)

--       -- InitMixin(self, GameEffectsMixin)
--       -- InitMixin(self, FireMixin)
--       -- self:SetOnFire(self:GetOwner(), self)
--    end

--    -- function Grenade:GetIsAlive()
--    --    return (true)
--    -- end

--    -- function Grenade:DeductHealth()
--    -- end

--    function Grenade:Detonate(targetHit)
--       local hitEntities = GetEntitiesWithMixinWithinRange("Live",
--                                                           self:GetOrigin(),
--                                                           5.5)
--       -- self:TriggerEffects("flamethrower_attack_start")

--       local params = { surface = GetSurfaceFromEntity(targetHit) }
--       params[kEffectHostCoords] = Coords.GetLookIn( self:GetOrigin(), self:GetCoords().zAxis)

--       -- self:TriggerEffects("grenade_explode", params)
--       -- self:TriggerEffects("flamethrower_attack")
--       for _, ent in ipairs(hitEntities) do
--          if (HasMixin(ent, "Fire")) then
--             ent:SetOnFire(self:GetOwner(), self)
--          end
--       end
--    end
-- end

Grenade.kClearOnImpact = true
Grenade.kClearOnEnemyImpact = true

if Server then

   function Grenade:ProcessHit(targetHit, surface, normal, endPoint )

      self:Detonate(targetHit, hitPoint )

   end
end

-- function Grenade:ProcessHit(targetHit, surface)
--    local orig = self:GetOrigin()
--    if (Server) then
--       self:Detonate()
--    end
--    return true
-- end

-- Shared.LinkClassToMap("Grenade", Grenade.kMapName, networkVars, true)

-- if (Server) then
-- local kGrenadeCameraShakeDistance = 15
-- local kGrenadeMinShakeIntensity = 0.02
-- local kGrenadeMaxShakeIntensity = 0.13

--    function Grenade:Detonate(targetHit)

--       -- Do damage to nearby targets.
--       local _hitEntities = GetEntitiesWithMixinWithinRange("Live", self:GetOrigin(), kGrenadeLauncherGrenadeDamageRadius)

--       -- Faded code added: prevent GL to kill a marines behind a shield
--       local hitEntities = {}
--       for _, hitEntity in ipairs(_hitEntities) do
--          if (hitEntity) then
--             local trace = Shared.TraceRay(self:GetOrigin(), hitEntity:GetOrigin(), CollisionRep.Move, PhysicsMask.Bullets, EntityFilterOnly("SentryBattery"))

--             if trace.fraction == 1 then
--                table.insert(hitEntities, hitEntity)
--             elseif not trace.entity then
--                table.insert(hitEntities, hitEntity)
--             elseif trace.entity == targetEntity then
--             end
--          end
--       end

--       -- Remove grenade and add firing player.
--       table.removevalue(hitEntities, self)

--       -- full damage on direct impact
--       if targetHit then
--          table.removevalue(hitEntities, targetHit)
--          self:DoDamage(kGrenadeLauncherGrenadeDamage, targetHit, targetHit:GetOrigin(), GetNormalizedVector(targetHit:GetOrigin() - self:GetOrigin()), "none")
--       end

--       RadiusDamage(hitEntities, self:GetOrigin(), kGrenadeLauncherGrenadeDamageRadius, kGrenadeLauncherGrenadeDamage, self)

--       -- TODO: use what is defined in the material file
--       local surface = GetSurfaceFromEntity(targetHit)

--       if GetIsVortexed(self) then
--          surface = "ethereal"
--       end

--       local params = { surface = surface }
--       params[kEffectHostCoords] = Coords.GetLookIn( self:GetOrigin(), self:GetCoords().zAxis)

--       self:TriggerEffects("grenade_explode", params)

--       CreateExplosionDecals(self)
--       TriggerCameraShake(self, kGrenadeMinShakeIntensity, kGrenadeMaxShakeIntensity, kGrenadeCameraShakeDistance)

--       DestroyEntity(self)

--    end
-- end


ClusterFragment.kModelName = PrecacheAsset("models/effects/frag_metal.model")
function ClusterFragment:GetProjectileModel()
    return ClusterFragment.kModelName
end
