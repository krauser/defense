-- ===================== Faded Mod =====================
--
-- lua\FadedGlobals.lua
--
--    Created by: Rio (rio@myrio.de)
--    Update by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

kFadedModVersion = "Defense"

-----------------------------------------------
------------------ Fade global ----------------
-----------------------------------------------

kFadedModPregameLength = 1
kFadedModTimeTillMapChange = 10

kLerkFlapEnergyCost = 1

-- Time the server waits to start the first round (so people can join)
kFadedNextMapWaitingTime = 30
-- Time to refresh the "first game in" message
kFadedNextMapMessageRefresh = 10
-- Delay between two round
kFadedModTimeTillNewRound = 8

kFadedHintEnable = true

-------- Fade global for the cloak configuration

kOnosCost = 10
kFadeCost = 7
kLerkCost = 5
kGorgeCost = 2

kDissolveSpeed = 2.25
kDissolveDelay = 10

-- kFadedModMaxCloakedFraction = 0.98
--Values related to the faded Cloack ability
-- kFadedModBaseOpacity = -0.20
-- Opacity of the faded camo (skin hole in the camo)
-- 1 is the default, 2 means twice more hole in the skin, 0.5 is half
kFadedModOpacity = 2
kFadedMinSpeedForOpacity = 2.2
-- kFadedModSpeedOpacitydModificator = 0.05
--kFadedModAttackingOpacityModificator = 0.3
-- kFadedModLowestOpacity = 0.1

-- The detection distance of the marine light in meter
kFadedLightDetectDist = 30
-- Time in second the light can be used
kFadedLightTime = 50
kFadedLightRegePerSec = 0.6

kFadedLightNoDetectColor = Color(.1, .1, 1)
kFadedLightDetectColor = Color(1, 0, 0)

-- Amont of sanity (2 means all rege & damage are divided by 2)
kFadedSanityAmountScale = 1

-- Infinite parasite duration
kParasiteDuration = -1
-- Sanity for the cloak malus
-- Minimum percent of distance view when the Marine is low in sanity
-- Ex: for value 70
--     If the marine, full sanity see at 100 meters, at 0 if will see 70
-- kFadedMinViewQuality = 70
-- Minimum amount of sanity lost to start the progressive view distance
-- reduction.
-- Ex: If 40, the sanity will reduce from 60 to 0
kFadedMinMissingSanityForMalus = 40

-- kFadedBaseCloakFraction = 0
-- kFadedCloakFraction = 0
-- kFadedMinimalCloak = 0.04
-- kFadedMarineMaximalDistanceOfDetection = 23
-- kFadedMarineMaximalDetectionDistance = 2
kFadedClossestMarineFound = true


----------------
-- Hunt ability

-- Maximum points on the path
kFadedHuntMaxPointOnPath = 100
-- Max distance to show the "Use" bar
kFadedHuntingRayRange = 1.5
-- Time to reveal the path in second
kFadedHuntTime = 1.8
-- Time before we clear the path
kFadedHuntPathDuration = 8



------------------
-- Spectator babblers
-- kBabblerDamage = kBiteDamage

-- kBiteDamage = 20
-- kSpitDamage = 30
-- kLerkBiteDamage = 7
-- kSwipeDamage = 15
-- SwipeBlink.kDamage = kSwipeDamage
-- kSwipeDamageType = kDamageType.Puncture
kGoreDamage = 100--90

-- kGoreDamage = 60
-- kGoreDamageType = kDamageType.Structural

kXenocideDamage = 110--200
-- kXenocideDamageType = kDamageType.Normal
-- kXenocideRange = 14
kXenocideEnergyCost = 1


kMarineInitialIndivRes = 15

kJetpackCost = 20

kFadedSpectatorBabblerHealth = 10
kFadedSpectatorBabblerArmor = 0
kBiteEnergyCost = 4
kFadedBabblerJumpHeigh = 2
kFadedBabblerPlayerJumpCost = 20

-- Time in second before the next babbler spawn wave
-- kFadedBabblerPlayerSpawnInterval = 40

-- Time added to the babbler spawn wave per babbler
-- so, with a few they will respawn a lot by themselves, and lesser
-- in endgame
kFadedInstantBabblerSpawnWithProto = true
kFadedDelayOnSpawnWavePerBabbler = 4
kFadedMaxBabblerPlayerSpawnDelay = 60
-- Minimum distance to have when respawning as a babbler
kFadedMinDistFromMarineToRespw = 20

-- Delay in sec between 2 chat msg "you will respawn in"
kFadedBabblerPlayerRespawnMsg = 15
kFadedBabblerPlayerEnergyRegen = 40

kFadedBabblerPlayerWalkSpeed = 4.9
kFadedBabblerPlayerJumpSpeed = 12

kFadedBabblerOnKillScoreReward = 2

-- Percent of chances to drop a ammo pack when killing a babbler
kFadedBabblerAmmoPackSpawnChances = 0

-----------------------------------------------
-----------------------------------------------
-- Objective mod

kFadedDisableObjectivMod = true

kFadedCanUsePrimaryWithBattery = false
-- Set to true if you want a battery to be dropped each time a building is powered up
kFadedSpawnBatteryOnBuildingPowerUp = false

kFadedOptiToBabblerDist = 45
kFadedOptiToLifeformDist = 38

local marineBuildingHPScale = 0.8

kARCHealth = 300
kARCArmor = 100

kFadedSentryCost = 30
kSentryHealth = 350
kSentryArmor = 150

kCommandStationHealth = 1200 * marineBuildingHPScale
kCommandStationArmor = 300 * marineBuildingHPScale
kExtractorHealth = 800 * marineBuildingHPScale
kExtractorArmor = 50 * marineBuildingHPScale
kArmsLabHealth = 1000 * marineBuildingHPScale
kArmsLabArmor = 200 * marineBuildingHPScale

kArmoryHealth = 1000 * marineBuildingHPScale
kArmoryArmor = 180 * marineBuildingHPScale


kAdvancedArmoryHealth = 1500 * marineBuildingHPScale
kAdvancedArmoryArmor = 400 * marineBuildingHPScale
kRoboticsFactoryHealth = 1000 * marineBuildingHPScale
kRoboticsFactoryArmor = 200 * marineBuildingHPScale

Observatory.kDetectionRange = 28 --22

-- Weight of the sentryBattery (for reference:
-- shotgun = 0.14, rifle = 0.13, pistol = 0.00
kFadedSentryBatteryWeight = 0.20
kSentryBatteryHealth = 1000 -- Default 600
kSentryBatteryArmor = 300 -- Default 200

kSentryBatteryHealth = 200
kSentryBatteryArmor = 100

kInfantryPortalHealth = 1600 * marineBuildingHPScale
kInfantryPortalArmor = 1100 * marineBuildingHPScale

kPhaseGateHealth = 1000  * marineBuildingHPScale-- 2000
kPhaseGateArmor = 180  * marineBuildingHPScale-- 550

kDoorHealth = 1400 * marineBuildingHPScale
kDoorArmor = 600 * marineBuildingHPScale

kFadedObsEnablePassiveDetection = true

kPowerPointHealth = 1200 * marineBuildingHPScale
kPowerPointArmor = 700 * marineBuildingHPScale

kObservatoryHealth = 1200 * marineBuildingHPScale
kObservatoryArmor = 300 * marineBuildingHPScale

-- TODO: add bonus for alien for each build hive
-- TODO: limit alien population for alien players (ligne 452 FadedWavesKf)
-- TODO: comme pour les armory, repartir les obs sur la map (5 ?)



-- kArmsLabHealth = LiveMixin.kMaxHealth - 1
-- kArmsLabArmor = LiveMixin.kMaxArmor - 1

-- Max range to display the icon of the sentyr battery
kFadedSentryBatteryVisibleRange = 50

--kMaxRelevancyDistance = 80

-- Spawn a battery on battery each N second
kFadedBatterySpawnDelay = 55
kFadedBatterySpawnAtRoundStart = 1

-- Give 2 item per marines (30% weap / 70% supply (ammo/medpack))
-- If 0, nothing is given
kFadedArmorySupplyAmount = 2
kFadedArmoryAllowWeapon = true  -- Allows the armory to drop weapon
kFadedArmoryAllowSupply = false -- Allows the armory to drop supply (medpack/ammo)
-- Number of time a marine can heal/get ammo on the armory automaticaly
kFadedArmoryHealthAmmoSupply = 2

kFadedCanMarinesBuyWeapons = true
kShotgunCost = 5
kClusterGrenadeCost = 8
kGasGrenadeCost = 10
kNapalmGrenadeCost = 5
kPulseGrenadeCost = 10
kMineCost = 5
kFlamethrowerCost = 10
kGrenadeLauncherCost = 10
kWelderCost = 5

kPlayerWeldRate = 43
kStructureWeldRate = 145
kDoorWeldTime = 19

kWelderDamagePerSecond = 70
kWelderDamageType = kDamageType.Flame
kWelderFireDelay = 0.2
kSelfWeldAmount = 10

kItemStayTime = 60
kWeaponStayTime = 30

kAxeDamage = 120
kAxeDamageType = kDamageType.Normal
kRifleMeleeDamage = 90

kFadedGrenadeSecondary = true -- TODO: add to the server.json

kFadedObsGiveMinimap = true
kFadedObsGiveDamageDisplay = true

kFadedRoboticsFactoryGiveMac = true

kFadedArmslabGiveWeapons1 = true
kFadedArmslabGiveArmor1 = true

-----------------------------------------------
-----------------------------------------------

-- Faded capability
kFadedRespawnedEnable = true
kFadedRespawnedFadeHealth = 30
kFadedRespawnedFadeEnergy = 1
kFadedRespawnedFadeHealthCost = 50

kFadedReviveScore = 15

-----------------------------------------------


kFadedModTwoPlayerToStartMessage = 20


local fadedReductionFactor = 0.80
-- Base amount of healt/armor the fade has
kFadeBaseHealth = tonumber(string.format("%.2f", 55*fadedReductionFactor)) -- old 100
kFadeBaseArmor = tonumber(string.format("%.2f", 10*fadedReductionFactor))  -- old 50

-- Number of marine is the team,
-- this is updated at the start of the round (in FadedAlien.lua)
kMarineNb = 0
-- Number of fade
kFadeNb = 0
-- Number of marine per fade
-- Ex: If 10: the second fade will come with 11 marines
kFadedAddFadeMarineScale = 10
-- Extra division perform when we have multiple fade (to balance DPS)
-- Ex: if value is "1.2", fade hp/armor will be divided by 1.2 (20%)
-- If true, the maluce is enable
kMultiFadeMaluce = false
kFadedHPReductionPerFade = 1.2

kFadedAmbiantSound = true
kFadedSoundIfFadeNearby = true

-- Amount of healt/armor given to the fade for each marine
kFadeHealthPerMarine = tonumber(string.format("%.2f", 37*fadedReductionFactor)) -- 35
kFadeArmorPerMarine = tonumber(string.format("%.2f", 18*fadedReductionFactor)) -- 10

--kFadedModMaxMarinePlayersUntilFadeScale = 6

-- Total amount of Point to dispatch when a Faded die
-- If a marine deal 10% damage to the Faded, he will have 10 points
kFadedModScorePointsForFadeKill = 100
-- Point to give if a Faded kill the marine
kFadedModScorePointsForAllMarineKill = 100


kFadedModFadeCanBeSetOnFire = true

kEnergyUpdateRate = 0.7

-----------------------------------------------
------------------ Game Balance global --------
-----------------------------------------------

----------------------------

--Values related to the Devour Corps Ability

--kFadedEatingStopedTime = 3
kFadedEatingRegen = 35
kFadedEatingRegenArmor = 5
kFadedEatingTime = 4

kExtractorArmorResearchTime = 20 / 3
kGrenadeTechResearchTime = 45 / 3
kAdvancedArmoryResearchTime = 90 / 3
kShotgunTechResearchTime = 30 / 3
kHeavyMachineGunTechResearchTime = 60 / 3
kGrenadeLauncherTechResearchTime = 20 / 3
kAdvancedWeaponryResearchTime = 35 / 3
kARCArmorTechResearchTime = 30 / 3
kMineResearchTime  = 20 / 3
kJetpackTechResearchTime = 90 / 3
kJetpackFuelTechResearchTime = 60 / 3
kJetpackArmorTechResearchTime = 60 / 3
kExosuitTechResearchTime = 90 / 3
kExosuitLockdownTechResearchTime = 60 / 3
kExosuitUpgradeTechResearchTime = 60 / 3
kPhaseTechResearchTime = 45 / 3
kWeapons1ResearchTime = 60 / 3
kWeapons2ResearchTime = 90 / 3
kWeapons3ResearchTime = 120 / 3
kArmor1ResearchTime = 60 / 3
kArmor2ResearchTime = 90 / 3
kArmor3ResearchTime = 120 / 3
kNanoArmorResearchTime = 60 / 3

kSentriesPerBattery = 6

kNanoShieldDuration = 8--5
kNanoShieldDamageReductionDamage = 0.45 --0.68

-------------------------------

kResourceTowerResourceInterval = 1
kTeamResourcePerTick = 0.26

kAmmoPackCost = 3
kMedPackCost = 3

kSentryCost = 20
kARCCost = 150
kPlayingTeamInitialTeamRes = 100
kMaxTeamResources = 200
kHiveBiomass = 9
kUpgradedJetpackUseFuelRate = .20
kJetpackingAccel = 1.12
kJetpackUseFuelRate = 0.11  -- 0.21 -- ns2 default
kJetpackReplenishFuelRate = .08

kFadedExoSpeedBonusFactor = 1.12 -- add 10% speed

kExosuitCost = 50
kDualExosuitCost = 50
kDualExosuitCost = 50
kDualRailgunExosuitCost = 50

kExosuitHealth = 100
kExosuitArmor = 600
kExosuitArmorPerUpgradeLevel = 50

-------------------------------

-- Cost of the bile bomb
kBileBombEnergyCost = 20
-- Bile bomb fire rate in second
kBileBombFireRate = 4
-- Bile bomb speed
kFadedModBombVelocity = 23

-- Max distance in which the marine receive death notification
kFadedDeathMessageDistance = 25


--------- Mines
--
-- Restriction of the number of mine in an area
-- for ex:
-- * kFadedModMinesRestrictionCount = 1
-- * kFadedModMinesRestrictionRange = 20
-- Mean: Allow at max 1 mine each 20 meter
--

kFadedModMinesRestrictionCount = 5
kFadedModMinesRestrictionRange = 1
-- Number of mine per marine
kNumMines = 2
-- Trigger radius for the mine to explode
kMineTriggerRange = 2.8
-- Default: 125
kMineDamage = 180
kMineAlertTime = 4 -- Default 8

-- Flame

kFadedFlameMinesNum = 2
kFadedFlameMinesRadius = 8

-----------

kFadedCanDropWelder = false

kFadedBabblerAbilityEnable = true
kFadedBabblerAbilityHealthCost = 10
kFadedBabblerAbilityNumBabbler = 10
-- Maximum number of babbler alive on the map
kMaxBabblerOnMap = 100
-- Build time of babbler egs

kBabblerEggBuildTime = 10

-- Babblers lifetime
kBabblersLifetime = 90

kFadedAntiCampEnable = false
-- Radius de la zone pour le camp
kAntiCampRadius = 10
-- Number of babbler when the anti camp trigger
kFadedNumBabblersPerEgg = 4

kNumBabblersPerEgg = 6

-- Max camp time before babbler spawn
kCampMaxTime = 45

-- Dispatch marine around the map
-- Allow also the Faded to be spawn on Resource Point
KFadedRandomDispatchSpawn = true
-- Minimum distance between the Faded and marine when the round start
kFadedSafetySpawnRadius = 40
-- Minimum number of marine before dispatch
kFadedRandomDispatchMinPlayer = 6

---------------- Flame

kClawDamage = 100
kRailgunDamage = 100
kRailgunChargeDamage = 220

-- Energy drain (default = 3)
kFlameThrowerEnergyDamage = 3
-- Damage of the fire surface on ground
Flame.kDamage = 7
Flame.kLifeTime = 5.6
-- Burn damage over time
kBurnDamagePerSecond = 12.5
-- Burn duration
kFlamethrowerBurnDuration = 6
-- Direct burn damage
kFlamethrowerDamage = 14
-- Reduction de la taille chargeur du lance flame (original = 50)
kFlamethrowerClipSize = 50
-- Reduce the damage of flame on marines
-- If its equal to "2", flame damage on ground are divided by 2
kFadedFlameFriendlyFireDamageReductionFactor = 0

kFadedAlienComBotStandByDelay = 60*5

----------------

kSentryAttackDamageType = kDamageType.Light
kSentryAttackBaseROF = .15
kSentryAttackRandROF = 0.0
kSentryAttackBulletsPerSalvo = 1
kConfusedSentryBaseROF = 0.20

kSentryDamage = 6.5

kBileBombDamage = 32 --55 -- per second

kSkulkGestateTime = 1
kGorgeGestateTime = 3
kLerkGestateTime = 7
kFadeGestateTime = 10
kOnosGestateTime = 15

kChargeEnergyCost = 20 --30 -- per second
kStompEnergyCost = 40 --30

kOnosHealth = 900 * 1.10
kOnosArmor = 450 * 1.20

function StompMixin:GetHasSecondary(player)
   return player:GetHasThreeHives() and DefIsVirtual(player)
end

-- kFadeHealth = 300
-- kFadeArmor = 100

-- kLerkHealth = 154
-- kLerkArmor = 40

-- kGorgeHealth = 200--160
-- kGorgeArmor = 130--75

-- kSkulkHealth = 85--70
-- kSkulkArmor = 20--10

kBabblerHealth = 5
kBabblerArmor = 0

kTunnelEntranceHealth = 1000 / 1.25
kTunnelEntranceArmor = 100 / 1.25
kMatureTunnelEntranceHealth = 1250 / 1.25
kMatureTunnelEntranceArmor = 200 / 1.25

kHealsprayDamage = 9--8
kHealsprayEnergyCost = 4--12
kMinBuildTimePerHealSpray = 0.9/1.5--0.9
kMaxBuildTimePerHealSpray = 1.8/1.5--1.8

kExtractorBuildTime = 8

kEggGestateTime = 15

kContaminationCooldown = 10
kContaminationCost = 20

---------------- All grenades

-- Maximum numbers of grenades
kMaxHandGrenades = 3

----------------- Cluster Grenade

kClusterGrenadeDamageRadius = 11 -- Default 10
kClusterGrenadeDamage = 90 --55
kClusterFragmentDamageRadius = 7 -- 6
kClusterFragmentDamage = 30 -- 20

----------------- Flare Grenade and light

kFadedFlareEnable = true
kFlareLifeTime = 40
kFadedFlareAmmoCost = 2
kFadedFlareIntensity = 6
kFadedFlareRadius = 18

kFadedBatteryLightColor = Color(1, 1, 1)
kFadedBatteryLightIntensity = 7
kFadedBatteryLightRadius = 14

----------------- Heal Grenade
kFadedMaxHealGrenades = 3
kFadedHealCloudRadius = 7


-- Max HP per grenade
-- Heal once on detonation, and once each Xs
kFadedHealCloudLifetime = 2
kFadedHealCloudRefreshRate = 0.80
kFadedHealCloudHpPerRefresh = 45
-- Reduction for the GL of the medic
kFadedGLHealCloudHpPerRefresh = 20

----------------- Napalm Grenade

kFadedNapalmGrenadeCost = 15
kFadedMaxNapalmGrenades = 1
kFadedNapalmDamagePerSecond = 10

kFadedNapalmCloudLifetime = 6 -- (default nerve gaz => 6s)
kFadedNapalmCloudRadius = 7 -- default (same as nerve gaz cloud)
-- Radius within the Faded goes auto in flame
-- kFadedBurnRadius = 2
kFadedNapalmPutInFireDelay = 0.85

-- Marine do not burn, to compensate and make the cloud dangerous
-- (so you can't just walk in and out easy), make more damages
kFadedOnMarineDamageMultiplyer = 6

--- Babbler anticamp only attack the camper
--- heavyshptgun double shot, check if deployed
--- cheatcode for the mega babler for pheonix (scaled)

-------------------- Medic Pistol

kPistolCost = 0
kPistolClipSize = 15
kPistolDamage = 35 -- 25
kPistolDamageType = kDamageType.Light

kFadedMedicPistolDmg = 10
kFadedMedicPistolBlastRadius = 4
kFadedMedicPistolDetonationDelay = 1.7

kFadedPistolAltCheatEnable = false
-------------------- Rifle

kShotgunDamage = 12.5

kPistolWeight = 0.0
kRifleWeight = 0.12
kHeavyRifleWeight = 0.15
kHeavyMachineGunWeight = 0.15
kGrenadeLauncherWeight = 0.15
kFlamethrowerWeight = 0.14
kShotgunWeight = 0.04 --0.14



-- Increase damage to be more usefull in combat
-- SG can deal    170dmg/0.88s ==>        193dps
--> Usefull in close and fast engagement, can deal 170dmg instant
--> Can reload in combat and shoot if necessary
-- Rifle can deal 18bullets/1s ==> 18*13.5 =    243dps
--> Long distance, but hard to deal all the damage in close combat + long reload
--> Non stopable reload
-- Shotgun time to empty a full clip: 6*0.88 = 5.28 (0.88 == rate of fire)
-- Rifle   time to empty a full clip: 75/18  = 3.60 (18 == Bullets per seconde)
kRifleCost = 10
kRifleDamage = 13.5
kRifleClipSize = 65
kRifleDamageType = kDamageType.Normal

-- kHeavyRifleDamage = 45
-- kHeavyRifleDamageType = kDamageType.Puncture
-- kHeavyRifleClipSize = 200
-- kHeavyMachineGunCost = 25

kHeavyMachineGunCost = 15
kHeavyMachineGunDamage = 8
kHeavyMachineGunDamageType = kDamageType.Puncture
kHeavyMachineGunClipSize = 100
kHeavyMachineGunClipNum = 6


-- PulseGrenade damage (base = 110)
kFadedPulseSlowDownFactor = 2.12
kPulseGrenadeDamage = 55
kPulseGrenadeDamageRadius = 9 -- default 7
kPulseGrenadeEnergyDamage = 9

kGrenadeLauncherGrenadeDamage = 110


kSporesDamageType = kDamageType.Gas
kSporesDustDamagePerSecond = 8
kSporesDustFireDelay = 0.2
kSporesDustEnergyCost = 15
kSporesDustCloudRadius = 2.4
kSporesDustCloudLifetime = 7

kSpikeMaxDamage = 1.6*2
kSpikeMinDamage = 0.8*2

kHydrasPerHive = 6
kClogsPerHive = 50
kNumWebsPerGorge = 50
kNumBabblerEggsPerGorge = 10

kHiveHealth = 7000/1.15
kHiveArmor = 800/1.15

kHiveBuildTime = 65--180

kEggGenerationRate = 8
kAlienEggsPerHive = 5

------------------------------
-- Warning: Remember to update en gamestring/enUS.txt for weapon description
-- --> WEAPON_DESC_HEAVYSHOTGUN <--
kFadedHSGCheatEnable = false
kFadedHeavyShotgunDamage = 10

kMACCost = 20

kFadedHeavyShotgunKnockBack = 19
kFadedHeavyShotgunPelletPerShot = 68
-- Fire rate compared to the regular shotgun (2 means 2 type slower)
kFadedHeavyShotgunFireRate = 1.4
-- Number of pellet compared to the single shot
-- 2 means twice more pellets
kFadedHeavyShotgunDoubleShotPelletRatio = 1.6
------------------------------

-- kBlinkEnergyCost = 12
kFadedBlinkHpCost = 6
kStartBlinkEnergyCost = 3
kFadedEnzimeAfterBlink = true

kBoneWallHealth = 200

kBoneWallArmor = 0
kBoneWallHealthPerBioMass = 0

kWallGripEnergyCost = 5
kLeapEnergyCost = 24

-- Number of marine per veteran allowed
-- (5 means: 1 veteran every 5 marines)
kFadedVeteranScale = 5
-- If 1.1, increase damage by 10%
-- If 1.6, increase damage by 60%
-- (damage = damage * kFadedVeteranDamageBonusFactor)
kFadedVeteranDamageBonusFactor = 1.1
-- Armor amount/health of Veteran
kFadedVeteranArmor = 50
kFadedVeteranHealth = 110
-- Speed bonus of a veteran (Ex: if 0.1, give 10% speed bonus)
kFadedVeteranSpeedBonus = 0.09

-- Damage dealed by each swip of the fade on a Marine

-- 1 ==> Fake marine can takes PG, anything else it can't
kFadedAllowFakeMarineToTakePG = 1

-- Instant stab only if a marine is not visible or away from at least this range
kFadedInstantStabMinUnseenDist = 30

-- If the a Marine view a Fake marine less than N meters, he can't stab
-- Ex: 2 ==> The fade can stab if no one see him below 2 meters
kFadedStabViewDistance = 2
-- Direct damage from the stab
kFadedStabDamage = 10
-- Poison damage to the marine (3.4*25 == 85.0 health damages)
kFadedStabPoisonDamage = 3.4
kBitePoisonDamage = kFadedStabPoisonDamage -- per second
kPoisonBiteDuration = 7
-- Min amount of life of the marine with the poison
kPoisonDamageThreshhold = 2
-- Damage per second on the sanity of the marine
kFadedPoisonSanityDamagePerSec = 6 -- Per second

kStabDamage = 70
kStabDamageType = kDamageType.Normal

kFadedStabRange = 2
kFadedSwipeRange = 1.6
-- Time to denied swipe after stab to force the fade to flie away
kFadedDeniedSwipeAfterStab = 0
-- Do not edit this global

-- Damage amount on nearby marine when a marine die
-- Radius of an explosive grenade (original: 4.8)
-- I increase this one to balance the fact that the fade is fast
-- and to help for cover (otherwise, except for direct hit its not
-- very usefull)
kGrenadeLauncherGrenadeDamageRadius = 7

kFadedGLCheatEnable = false
-- Damage reduction on Friendly fire of the GL
-- but not on the owner of the GL
kFadedGLFriendlyFireDamageReductionFactor = 2
kFadedGLSecondary = true


kFadedModGUIHelpLimit = 3

kFadedModDistortionRadius = 16
kFadedModDistortionIntensity = 16
kFadedModDistortionAcceleration = 1.6

kFadedModLightColorScale = 0.10
kFadedModLightIntensity = 1 -- 0.30

kFadedModAllTalkEnabled = false

-- Unused
-- kFadedModFairFadedRotation = true

kTimeStartFade = 10
kTimeEndFade = 11

kMinigunDamage = 13.7
kMinigunDamageType = kDamageType.Normal

kGorgeCost = 999
kGorgeEggCost = 999
kLerkCost = 999
kLerkEggCost = 999
kFadeCost = 999
kFadeEggCost = 999
kOnosCost = 999
kOnosEggCost = 999

----------------------------------------------
----------------- Ragdol gobal
----------------------------------------------

-- Min distance to take/eat away from view of a marine
kFadedTakeBodyDistance = 25
-- Max distance for the fade to eat/use a body
-- or to burn a body
kFadedEatUseDistance = 2.5
-- Range of detection for veteran of any Fake Marine
kVeteranFakeMarineRangeDetection = 8
-- Every N second, the Veteran is alerted if a Fake Marine
-- is nearby of M meter.
kVeteranAlertRefreshTime = 5
-- % of Energy left when a Fake marine get Fade
-- Ex: 0.3 mean 30% of his max energy
kFadedTransformationEnergyPenality = 0.35
-- % of Energy left when the Fake marine stab
kFadedTransformationOnStabEnergyPenality = 0.20
-- Infestation radius generated by dead body
-- For ex: Hive infestation is 20
kFadedDeadBodyInfestationRadius = 4
-- Default of NS2: 0.25
kFadedDeadBodyInfestationRate = 0.1
kFadedDeadBodyLifeTime = 60*10
-- Radius to give order if the player has a flame weapon
kFadedBurnOrderRadius = 10

-- Gaz above dead marine
-- Pourcent of chance of a gaz spawn
-- A value of '1' mean 1/1000
kFadedDeadBodyToxicGazChance = 1
kSporesDustDamagePerSecond = 4
kSporesDustCloudRadius = 2.5
kSporesDustCloudLifetime = 8

---------------------------
-----------------------------------------------
------- Hallucination global (do not touch) ---
-----------------------------------------------

kFadedSpawnFadeHallucination = nil
-- Remove hallucination cloud effect
HallucinationCloud.kSplashEffect = nil
EnzymeCloud.kSplashEffect = nil
EnzymeCloud.kRepeatEffect = nil

-- Remove spawn sound of hallucination
-- Return the place in the table of each element
-- (See  GeneralEffects.lua)
local function removeSpawnSoundFor(entityName)
   local soundIt = 1
   local soundItMax = #kGeneralEffectData.spawn.spawnSoundEffects
   local cineIt = 1
   local cineItMax = #kGeneralEffectData.spawn.spawnEffects
   -- Disable sound
   while (soundIt <= soundItMax)
   do
      local tmp = kGeneralEffectData.spawn.spawnSoundEffects[soundIt]
      if (tmp and tmp.classname == entityName) then
         table.remove(kGeneralEffectData.spawn.spawnSoundEffects, soundIt)
         -- kGeneralEffectData.spawn.spawnSoundEffects[soundIt].sound = nil
         break
      end
      soundIt = soundIt + 1
   end
   -- Disable Effect
   while (cineIt <= cineItMax)
   do
      local tmp = kGeneralEffectData.spawn.spawnEffects[cineIt]
      if (tmp and tmp.classname == entityName) then
         -- kGeneralEffectData.spawn.spawnEffects[cineIt].cinematic = nil
         table.remove(kGeneralEffectData.spawn.spawnEffects, cineIt)
         break
      end
      cineIt = cineIt + 1
   end
   return soundIt, cineIt
end

local function removeSoundForSkulk()
   local t = kPlayerEffectData.land.landSoundEffects
   for _, sub_table in ipairs(t) do
      if (sub_table.player_sound == "sound/NS2.fev/alien/skulk/land")
      then
         sub_table.player_sound = ""
      end
   end

   t = kGeneralEffectData.death.deathSoundEffects
   for _, sub_table in ipairs(t) do
      if (sub_table.sound == "sound/NS2.fev/alien/skulk/death")
      then
         sub_table.sound = ""
      end
   end

   t = kDamageEffects.flinch.flinchEffects
   for _, sub_table in ipairs(t) do
      if (sub_table.sound == "sound/NS2.fev/alien/skulk/wound"
             or sub_table.sound == "sound/NS2.fev/alien/skulk/wound_serious")
      then
         sub_table.sound = ""
      end
   end

   t = kDamageEffects.damage_sound.damageSounds
   for _, sub_table in ipairs(t) do
      if (sub_table.sound and sub_table.doer == "BiteLeap") then
         sub_table.sound = ""
      end
   end
end

local function replaceSoundForBabblers()
end

-- -- Hallucination cloud sound/cloud effect disable
-- -- note: The letter case are correct
removeSpawnSoundFor("Hallucination")
-- removeSpawnSoundFor("Babbler")
-- removeSpawnSoundFor("Fade")
-- removeSpawnSoundFor("Skulk")
removeSpawnSoundFor("SentryBattery")
removeSoundForSkulk()
-- replaceSoundForBabblers()

----------------------------- Remove Fake marine footstep specific sound
local function removeFootStep()
   local it = 1
   local max = #kPlayerEffectData.footstep.footstepSoundEffects
   while (it <= max) do
      local data = kPlayerEffectData.footstep.footstepSoundEffects[it]
      if (data and string.find(data.sound, "sprint_")or string.find(data.sound, "footstep_")) then
         data.sound = string.gsub(data.sound, "_for_enemy", "")
      end
      it = it + 1
   end
end
removeFootStep()
-- sprint_
-- footstep_
-- kPlayerEffectData.footstep.footstepSoundEffects

-----------------------------------------------
------------------ Server config global -------
-----------------------------------------------

-- Base timer for the mod
kFadedModBaseRoundTimerInSecs = (60*3 + 45)
-- Max amount of time for a round
kFadedMaxRoundTimer = (60*12)
kFadedModBonusTimePerMarine = 25
kFadedModRoundTimerInSecs = 0

kFadedModSpawnProtectionEnabled = true
kFadedModSpawnProtectionTime = 8

kFadedModFriendlyFireEnabled = false
kFriendlyFireScalar = 0.1

kFadedModFadedSelectionChance = 80
kFadedModFadedNextSelectionChance = 10

-- Time during which the marine is allowed to purchase weapon
-- when the round begin
kFadedModTimeInSecondsSelectingEquipmentIsAllowed = 16

-- See FadedServerConfig.lua for round global
-- Script.Load("lua/FadedCustomGlobals.lua")
