-- ===================== Faded Mod =====================
--
-- lua\FadedTeamAttack.lua
--
--    Created by: Rio (rio@myrio.de)
--    Update by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

local locale = LibCache:GetLibrary("LibLocales-1.0")

--[[
   Brief: Apply damage on a marine

   Note: The bug where a player become invincible when decreasing his
   healtPoint down to 1 (with the eclipse laser for exemple) has been
   removed with the "if (attacker ~= nil)" condition. No error are raise
   and the code execute himself properly. This was because the laser
   was not an attacker entity.
--]]
local marineOnTakeDamage = Marine.OnTakeDamage
function Marine:OnTakeDamage(damage, attacker, doer, point)
   marineOnTakeDamage(self, damage, attacker, doer, point)
   -- If the Fade is on Fire, transform back into a Fade
end

function ViewModel:OnUpdateAnimationInput(modelMixin)

   PROFILE("ViewModel:OnUpdateAnimationInput")

   local parent = self:GetParent()
   if (parent) then
      parent:OnUpdateAnimationInput(modelMixin)
   end
end
