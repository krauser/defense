
local function _resetCounter(self)
   self.defLastCheck = Shared.GetTime()
   self.defCountDownResetVal = 30 + math.random() * 60*1
   self.defCountDownHive = self.defCountDownResetVal
end

local dist = 40
local techPointOnUpdate = TechPoint.OnUpdate
function TechPoint:OnUpdate(deltaTime)
   techPointOnUpdate(self, deltaTime)

   if (Server and GetGamerules and GetGamerules():GetGameStarted()) then
      if (getWaveNb() < 3 + #GetEntitiesForTeam("Hive", 2) * 2) then
         _resetCounter(self)
         return
      end

      if (self.defLastCheck == nil) then
         _resetCounter(self)
      end

      if (not self.defHiveDropped and self.defLastCheck + 1 < Shared.GetTime()) then
         self.defLastCheck = Shared.GetTime()

         if (#GetEntitiesForTeamWithinRange("Player", 1, self:GetOrigin(), dist) == 0
                and #GetEntitiesForTeamWithinRange("Sentry", 1, self:GetOrigin(), dist / 2) == 0
         )
         then
            if (self.defCountDownHive > 0) then
               self.defCountDownHive = self.defCountDownHive - 1
            else -- Drop HIVE (too much time before a marine was around, it's alien now)
               if (#GetEntitiesForTeamWithinRange("Hive", 2, self:GetOrigin(), 10) == 0) then
                  Shared:FadedMessage("Hive dropped in " .. self:GetLocationName())
                  self:SpawnCommandStructure(2)
                  self.defCountDownHive = self.defCountDownResetVal
                  -- TODO: drop en random un gorge ou un drifter (pour varier)
                  -- TODO: checker le raliement alien pour defendre
                  -- TODO: Faire spawn les aliens proche de la hive (trier par rapport a une hive en random qui se construit) (devrait suffir pour defendre)
                  -- Spawn un tunnel gorge a coté de la hive si il y a un gorge (70% chance)
                  -- napalm + flame mine, ajouter des flames au sol autour
                  -- Fix hive not spawning buildings
                  if (math.random() > 0.65 and getUnspawnedLifeform(Gorge.kMapName)) then
                     local gorge_orig = GetLocationAroundFor(self:GetOrigin(), kTechId.Gorge, 6)
                     local gorge = spawnAlienCreature(Gorge.kMapName, gorge_orig)
                  else
                     local drifter_orig = GetLocationAroundFor(self:GetOrigin(), kTechId.Drifter, 6)
                     local drifter = CreateEntity(Drifter.kMapName, drifter_orig, 2)
                  end

                  for i = 1, 3 do
                     local cyst_orig = GetLocationAroundFor(self:GetOrigin(), kTechId.Cyst, 8)
                     local cyst = CreateEntity(Cyst.kMapName, cyst_orig + Vector(0, -0.6, 0), 2)
                  end
               end
            end
         else
            self.defCountDownHive = self.defCountDownResetVal
         end
      end
   end
end
