function Rifle:GetHitPointPosition(player)
   local range = 100

   -- local eyePos  = player:GetEyePos()
   -- local startPoint = Vector(eyePos)
   local startPoint = self:GetBarrelPoint()
   local fireDirection = player:GetViewCoords().zAxis
   local kConeWidth = 0.1
   local filterEnts = {self, player}

   local extents = Vector(0.1, 0.1, 0.1)
   local trace = TraceMeleeBox(self, startPoint, fireDirection, extents, range, PhysicsMask.Flame, EntityFilterList(filterEnts))


   --
   -- local filter = EntityFilterTwo(player, self)
   -- local linetrace = Shared.TraceRay(startPoint, endPoint, CollisionRep.Damage, PhysicsMask.Bullets, EntityFilterAllButIsa("Tunnel"))
   --

   local lineTrace = Shared.TraceRay(startPoint,
                                     startPoint + range * fireDirection,
                                     CollisionRep.Damage,
                                     PhysicsMask.All,
                                     EntityFilterAllButIsa("Tunnel"))
                                     -- EntityFilterOne(player))

   if lineTrace.fraction < 0.8 then
      -- Shared:FadedMessage("Pistol:OnPrimaryAttack line trace OK")
      fireDirection = fireDirection + trace.normal * 0.55
      fireDirection:Normalize()

      return true, startPoint, lineTrace, fireDirection
   end
   return false, nil, nil, nil
end

function arc_shot(player)
end

function Rifle:arc_fire(player)

   local st, startPoint, lineTrace, fireDirection = self:GetHitPointPosition(player)
   if (st) then
      self:TriggerEffects("arc_firing")
      -- player:TriggerEffects("railgun_attack")

      GetEffectManager():TriggerEffects("arc_hit_primary", {effecthostcoords = Coords.GetTranslation(lineTrace.endPoint)})

      local hitEntities = GetEntitiesWithinRange("Alien", lineTrace.endPoint, ARC.kSplashRadius)

      RadiusDamage(hitEntities, lineTrace.endPoint, 15, 150, self, true)

      if (Client) then
         local tracerVelocity = GetNormalizedVector(lineTrace.endPoint - startPoint) * kTracerSpeed
         local tracer = BuildTracer(startPoint, lineTrace.endPoint,
                                    tracerVelocity,
                                    kRailgunTracerEffectName,
                                    kRailgunTracerResidueEffectName)
         table.insert(Client.tracersList, tracer)
      end
      for index, target in ipairs(hitEntities) do

         if HasMixin(target, "Effects") then
            target:TriggerEffects("arc_hit_secondary")
         end
      end

   end
end

-- function Rifle:GetClipSize()
--    return 50
-- end
-- function Rifle:GetBulletsPerShot()
--    return 1
-- end

-- function Rifle:GetBulletDamage()
--    return 1
-- end

-- function Rifle:GetMaxClips()
--    return 4
-- end

-- function Rifle:GetPrimaryAttackRequiresPress()
--    return false
-- end

-- function Rifle:GetPrimaryMinFireDelay()
--     return 0.3 -- second
-- end

local firePrimary = Rifle.FirePrimary
function Rifle:FirePrimary(player)
   if (player.kFadedReserved) then
      self:arc_fire(player)
   end
   -- player = self:GetParent()
   -- if (player) then
   --    self.fireTime = Shared.GetTime()
   --    self:arc_fire(player)
   -- end

   -- self:SetClip(Clamp(self:GetClip() - 0, 0, self:GetClipSize()))

   firePrimary(self, player)
   -- SpecialWeaponHandling(self, player, _CreateRagdoll, 60)
   -- _CreateRagdoll(self, player, player:GetOrigin() + Vector(2, 2, 2),
   --                0, Vector(10, 10, 10))

   -- if (player) then
   --    if (math.random(1, 100) <= 30) then
   --       SpecialWeaponHandling(self, player, _CreateClusterFragment, 50)
   --    end
   -- end
end


local heavyfirePrimary = HeavyMachineGun.FirePrimary
function HeavyMachineGun:FirePrimary(player)
   -- player = self:GetParent()
   -- if (player) then
   --    self.fireTime = Shared.GetTime()
   --    self:arc_fire(player)
   -- end

   -- self:SetClip(Clamp(self:GetClip() - 0, 0, self:GetClipSize()))

   heavyfirePrimary(self, player)
   -- SpecialWeaponHandling(self, player, _CreateRagdoll, 60)
   -- _CreateRagdoll(self, player, player:GetOrigin() + Vector(2, 2, 2),
   --                0, Vector(10, 10, 10))

   if (player) then
      if (math.random(1, 100) <= 50) then
         SpecialWeaponHandling(self, player, _CreateClusterFragment, 50)
      end
   end
end

-- local rifleOnTag = Rifle.OnTag
-- function Rifle:OnTag(tagName)
--    rifleOnTag(self, tagName)

--    if (Server and self:GetParent() and not self:GetParent().client) then
--       self:SetClip(2) -- bot never ran out of ammo
--    end
-- end

function Rifle:GetMaxClips()
   return 6
end
function Shotgun:GetMaxClips()
   return 6
end
function HeavyMachineGun:GetMaxClips()
   return 6
end
