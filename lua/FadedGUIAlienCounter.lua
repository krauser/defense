-- ===================== Faded Mod =====================
--
-- lua\FadedGUIRoundTimer.lua
--
--    Created by: Rio (rio@myrio.de)
--    Updated by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

local strformat = string.format

class 'FadedGUIAlienCounter' (GUIScript)

FadedGUIAlienCounter.kGameTimeBackgroundSize = Vector(140, GUIScale(24), 0)
FadedGUIAlienCounter.kFontName = "fonts/AgencyFB_small.fnt"
FadedGUIAlienCounter.kGameTimeTextSize = GUIScale(6)

function FadedGUIAlienCounter:Initialize()
   self.gameTimeBackground = GUIManager:CreateGraphicItem()
   self.gameTimeBackground:SetSize(FadedGUIAlienCounter.kGameTimeBackgroundSize)
   self.gameTimeBackground:SetAnchor(GUIItem.Right, GUIItem.Top)
   self.gameTimeBackground:SetPosition( Vector(- FadedGUIAlienCounter.kGameTimeBackgroundSize.x - 5, 5, 0) )
   self.gameTimeBackground:SetIsVisible(false)
   self.gameTimeBackground:SetColor(Color(0,0,0,0.5))
   self.gameTimeBackground:SetLayer(kGUILayerCountDown)

   self.gameTime = GUIManager:CreateTextItem()
   self.gameTime:SetFontName(FadedGUIAlienCounter.kFontName)
   self.gameTime:SetFontSize(FadedGUIAlienCounter.kGameTimeTextSize)
   self.gameTime:SetAnchor(GUIItem.Middle, GUIItem.Center)
   self.gameTime:SetTextAlignmentX(GUIItem.Align_Center)
   self.gameTime:SetTextAlignmentY(GUIItem.Align_Center)
   self.gameTime:SetColor(Color(1, 1, 1, 1))
   self.gameTime:SetText("")
   self.gameTimeBackground:AddChild(self.gameTime)
end

function FadedGUIAlienCounter:Uninitialize()
   GUI.DestroyItem(self.gameTime)
   self.gameTime = nil
   GUI.DestroyItem(self.gameTimeBackground)
   self.gameTimeBackground = nil
end

local waveTotal = 0
local waveNb = 0
local waveProgress = 0
function setFadedGUIAlienCounterInfo(progress, wave_nb, total_wave)
   waveProgress = progress
   waveNb = wave_nb
   waveTotal = total_wave
end

local refresh_rate = 0
function FadedGUIAlienCounter:Update(deltaTime)
   local isVisible = true --waveProgress and waveProgress < 100
   local localPlayer = Client.GetLocalPlayer()

   self.gameTimeBackground:SetIsVisible(isVisible)
   self.gameTime:SetIsVisible(isVisible)
   -- refresh_rate = refresh_rate + deltaTime
   refresh_rate = 10
   if (refresh_rate > 1) then
      refresh_rate = 0

      local pop = wave_pop
      local max_pop = wave_max_pop
      local skulk = getRemainingLifeform(Skulk.kMapName)
      local gorge = getRemainingLifeform(Gorge.kMapName)
      local lerk = getRemainingLifeform(Lerk.kMapName)
      local fade = getRemainingLifeform(Fade.kMapName)
      local onos = getRemainingLifeform(Onos.kMapName)

      local uskulk = getUnspawnedLifeform(Skulk.kMapName)
      local ugorge = getUnspawnedLifeform(Gorge.kMapName)
      local ulerk = getUnspawnedLifeform(Lerk.kMapName)
      local ufade = getUnspawnedLifeform(Fade.kMapName)
      local uonos = getUnspawnedLifeform(Onos.kMapName)
      local gameTimeText = ""

      if (pop > max_pop) then
         pop = max_pop
      end
      -- .. "Progress: %d / %d\n"
      if (localPlayer and localPlayer:GetTeamNumber() == 2) then
         gameTimeText = strformat("Waves: %d / %d\n"
                                     .. "Pop: %d / %d\n"
                                     .. "%d(%d) <- Skulk\n"
                                     .. "%d(%d) <- Gorge\n"
                                     .. "%d(%d) <- Lerk\n"
                                     .. "%d(%d) <- Fade\n"
                                     .. "%d(%d) <- Onos\n",
                                  waveNb, waveTotal,
                                  pop, max_pop,
                                  -- waveProgress, 100,
                                  skulk, uskulk, gorge, ugorge, lerk, ulerk, fade, ufade, onos, uonos)
      else
         gameTimeText = strformat("Waves: %d / %d\n"
                                     .. "%d <- Skulk\n"
                                     .. "%d <- Gorge\n"
                                     .. "%d <- Lerk\n"
                                     .. "%d <- Fade\n"
                                     .. "%d <- Onos\n",
                                  waveNb, waveTotal,
                                  -- waveProgress, 100,
                                  skulk, gorge, lerk, fade, onos)
      end
      self.gameTime:SetText(gameTimeText)
   end
end
