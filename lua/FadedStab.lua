-- ===================== Faded Mod =====================
--
-- lua\FadedStabBlink.lua
--
--    Created by: Rio (rio@myrio.de)
--
-- =====================================================

-- Do not slow the fade during a stab
-- So we can stab from the a player moving whithout blink
kStabSpeed = kMaxSpeed

local networkVars =
   {
      firingPrimary = "boolean"
   }

function StabBlink:OnCreate()
   Ability.OnCreate(self)
   self.firingPrimary = false
   -- self:OnTag("")
end

function StabBlink:GetHUDSlot()
   return 3
end

function StabBlink:GetSecondaryTechId()
   return nil
end

local stabOnPrimaryAttack = StabBlink.OnPrimaryAttack
function StabBlink:OnPrimaryAttack(player)
   stabOnPrimaryAttack(self, player)
   -- local lastBileBomb = self:GetTimeLastBomb()
   -- if (lastBileBomb ~= 0) then
   --     lastBileBomb = Shared.GetTime() - lastBileBomb
   -- end

   -- if player:GetEnergy() >= self:GetEnergyCost() and lastBileBomb >= kFadedModBombFireRateinSeconds then
   --     self.firingPrimary = true
   -- else
   --     self.firingPrimary = false
   -- end
end

-- No secondary attack
function StabBlink:OnSecondaryAttack(player)
end

-- We don't need shadow step when we can leap
function StabBlink:GetCanShadowStep()
   return false
end
-- We don't need shadow step when we can leap
function StabBlink:GetBlinkAllowed()
   return false
end


Shared.LinkClassToMap("StabBlink", StabBlink.kMapName, networkVars)
