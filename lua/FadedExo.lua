
local kWalkMaxSpeed = 3.7 * kFadedExoSpeedBonusFactor -- default
local kMaxSpeed = 5.75 * kFadedExoSpeedBonusFactor -- default

local kThrusterUpwardsAcceleration = 2 * kFadedExoSpeedBonusFactor
local kThrusterHorizontalAcceleration = 23 * kFadedExoSpeedBonusFactor
-- added to max speed when using thrusters
local kHorizontalThrusterAddSpeed = 2.5 * kFadedExoSpeedBonusFactor

local exoOnInitialized = Exo.OnInitialized
function Exo:OnInitialized()
   if (exoOnInitialized) then
      exoOnInitialized(self)
   end
   if (Client and Client.GetLocalPlayer() == self)
   then
      self.GUIAlienCounter = GetGUIManager():CreateGUIScript("FadedGUIAlienCounter")
   end
end


local exoOnDestroy = Exo.OnDestroy
function Exo:OnDestroy()
   if (exoOnDestroy) then
      exoOnDestroy(self)
   end
   if (Client) then
      GetGUIManager():DestroyGUIScript(self.GUIAlienCounter)
   end
end

-- Same as ns2
function Exo:GetMaxSpeed(possible)

    if possible then
        return kWalkMaxSpeed
    end

    local maxSpeed = kMaxSpeed * self:GetInventorySpeedScalar()

    if self.catpackboost then
        maxSpeed = maxSpeed + kCatPackMoveAddSpeed
    end

    return maxSpeed

end

-- function Exo:GetCanBeWeldedOverride()
--    return false, true
-- end
-- function Exosuit:GetCanBeWeldedOverride()
--    return false, true
-- end

-- same as ns2
local kUpVector = Vector(0, 1, 0)
function Exo:ModifyVelocity(input, velocity, deltaTime)

    if self.thrustersActive then

        if self.thrusterMode == kExoThrusterMode.Vertical then

       velocity:Add(kUpVector * kThrusterUpwardsAcceleration * deltaTime)
       velocity.y = math.min(1.5, velocity.y)

        elseif self.thrusterMode == kExoThrusterMode.Horizontal then

       input.move:Scale(0)

       local maxSpeed = self:GetMaxSpeed() + kHorizontalThrusterAddSpeed
       local wishDir = self:GetViewCoords().zAxis
       wishDir.y = 0
       wishDir:Normalize()

       local currentSpeed = wishDir:DotProduct(velocity)
       local addSpeed = math.max(0, maxSpeed - currentSpeed)

       if addSpeed > 0 then

          local accelSpeed = kThrusterHorizontalAcceleration * deltaTime
          accelSpeed = math.min(addSpeed, accelSpeed)
          velocity:Add(wishDir * accelSpeed)

       end

        end

    end

end

local damageMixinDoDamage = DamageMixin.DoDamage
function DamageMixin:DoDamage(damage, target, point, direction, surface, altMode, showtracer)

   local attacker = nil
   -- Get the attacker
   if self:GetParent() and self:GetParent():isa("Player") then
      attacker = self:GetParent()
   elseif HasMixin(self, "Owner") and self:GetOwner() and self:GetOwner():isa("Player") then
      attacker = self:GetOwner()
   end

   if (attacker and target and target:isa("Exo")) then
      damage = damage * 2.9 -- More damages on exo (but a lot of HP to "feel" strong)
   end
   damageMixinDoDamage(self, damage, target, point, direction, surface, altMode, showtracer)
end
