Script.Load("lua/MapBlipMixin.lua")

local dropPackOnInitialized = DropPack.OnInitialized
function DropPack:OnInitialized()
   dropPackOnInitialized(self)
   if (Server) then
      -- This Mixin must be inited inside this OnInitialized() function.
      if not HasMixin(self, "MapBlip") then
         InitMixin(self, MapBlipMixin)
      end
   end
end

