-- ===================== Faded Mod =====================
--
-- lua\FadedAlien.lua
--
--    Created by: Rio (rio@myrio.de)
--    Update by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

Script.Load("lua/PlayerHallucinationMixin.lua")

kCystRedeployRange = 0 -- Avoid destruction of cyst

if (Server) then
   function AlienKillAfterTimeout(self)
      if (self and self:GetIsAlive() and self:isa("Alien")) then
         local timeout = 60*4
         if (Shared.GetMapName() == "ns2_def_troopers") then
            timeout = 90
         end

         local bot_alive = getBotsNumberAlive()
         if (bot_alive <= 4 and not self:GetIsSighted()) then
            if (self:GetCreationTime() + (60*4) < Shared.GetTime()) then
               self:Kill()
            elseif (self:GetCreationTime() + 60*2 + math.random(1, 15) < Shared.GetTime()) then
               if (not self:isa("Alien")) then
                  if (#GetEntitiesForTeamWithinRange("Player", 1, self:GetOrigin(), 10) == 0) then
                     self:DeductHealth(math.max(10, self:GetMaxHealth() / 10))
                  end
                  Entity.AddTimedCallback(self, AlienKillAfterTimeout, 1)
               end
            end
         end
         Entity.AddTimedCallback(self, AlienKillAfterTimeout, 5)
      end
      return (false)
   end

   local kMinDistToSpawnCyst = 45
   local kMinDistBetweenCyst = 6
   function AlienSpawnInfest(self)
      local marines = nil
      -- marines = GetEntitiesForTeamWithinRange("Player", 1,
      --                                         self:GetOrigin(),
      --                                         kMinDistToSpawnCyst)
      -- if (#marines == 0 and (not self.client))
      -- then
      local cysts = GetEntitiesForTeamWithinRange("Cyst", 2,
                                                  self:GetOrigin(),
                                                  kMinDistBetweenCyst)
      if (#cysts == 0) then
         -- local orig = GetLocationAroundFor(self:GetOrigin(), kTechId.Cyst, 3)
         -- CreateEntity(Cyst.kMapName, orig + Vector(), 2)
         FadedSpawnBuilding(kTechId.Cyst, Cyst.kMapName, self:GetOrigin(), Vector(0, -0.6, 0), 4, false, self:GetTeamNumber())
      end
      -- end

      return (self:GetIsAlive())
   end
end

------

function AlienforceDestroy(self)
   if (self and DefIsVirtual(self)) then
      self:Kill()
   end
end

local function _AlienInitCallbacks(self)
   if (Server and GetGamerules():GetGameStarted() and DefIsVirtual(self)) then
      Entity.AddTimedCallback(self, AlienUpdateOrderCallback, 1)
      Entity.AddTimedCallback(self, AlienKillAfterTimeout, 45 + math.random(1, 15))
      Entity.AddTimedCallback(self, AlienforceDestroy, 80)
      if (not self:isa("Lerk")) then
         --         Entity.AddTimedCallback(self, Alien.spawnInfest, math.random(10, 30) / 20)
      end
   end
   return false
end

function AlienInitCallbacks(self)
   if (Server and GetGamerules():GetGameStarted() and DefIsVirtual(self)) then
      -- one sec delay for the self.client to works
      Entity.AddTimedCallback(self, _AlienInitCallbacks, 1)
   end
end

local alienOnCreate = Alien.OnCreate
function Alien:OnCreate()
   alienOnCreate(self)
   -- Enable orders
   if (Server) then
      InitMixin(self, PlayerHallucinationMixin)
   end
end

local alienOnInitialized = Alien.OnInitialized
function Alien:OnInitialized()
   alienOnInitialized(self)
   AlienInitCallbacks(self)
   if not self.isHallucination then
      InitMixin(self, OrdersMixin, { kMoveOrderCompleteDistance = kPlayerMoveOrderCompleteDistance })
      -- if (Server and not self.client) then
      --    AlienUpdateOrderCallback(self)
      -- end
   end

   if (Client and Client.GetLocalPlayer() == self)
   then
      self.GUIAlienCounter = GetGUIManager():CreateGUIScript("FadedGUIAlienCounter")
   end

   -- if (DefIsVirtual(self)) then
   if (Server) then
      self:SetResources(50)
   end
   -- end
end


local alienOnDestroy = Alien.OnDestroy
function Alien:OnDestroy()
   if (alienOnDestroy) then
      alienOnDestroy(self)
   end
   if (Client) then
      GetGUIManager():DestroyGUIScript(self.GUIAlienCounter)
   end
end

if (Server) then
   local locale = LibCache:GetLibrary("LibLocales-1.0")

   -- Disable almost all regeneration for the Faded
   -- kAlienMinInnateRegeneration = 1
   -- kAlienMaxInnateRegeneration = 1

   function sortByProximity(alien, targets)
      local i = 1
      local sorted_targets = {}

      for _, target in ipairs(targets)
      do
         i = 0
         local added = false
         local t1 = alien:GetOrigin():GetDistanceTo(target:GetOrigin())
         for it, target2 in ipairs(sorted_targets) do
            local t2 = alien:GetOrigin():GetDistanceTo(target2:GetOrigin())
            if (t1 < t2) then
               table.insert(sorted_targets, it, target)
               added = true
               break
            end
        end
         if (added == false) then
            table.insert(sorted_targets, #sorted_targets + 1, target)
         end
      end
      -- for it, rt in ipairs(sorted_rt_point) do
      --    Shared:FadedMessage("Nearest marine from rt n'" .. tostring(it)
      --                           .. " is at " .. _getDistFromNeareastMarine(rt) .. "m")
      -- end
      return (sorted_targets)
   end
   function sortByProximityReverse(alien, targets)
      local i = 1
      local sorted_targets = {}

      for _, target in ipairs(targets)
      do
         i = 0
         local added = false
         local t1 = getPathDist(alien:GetOrigin(), target:GetOrigin())--alien:GetOrigin():GetDistanceTo(target:GetOrigin())
         for it, target2 in ipairs(sorted_targets) do
            local t2 = getPathDist(alien:GetOrigin(), target2:GetOrigin())--alien:GetOrigin():GetDistanceTo(target2:GetOrigin())
            if (t1 > t2) then
               table.insert(sorted_targets, it, target)
               added = true
               break
            end
        end
         if (added == false) then
            table.insert(sorted_targets, #sorted_targets + 1, target)
         end
      end
      -- for it, rt in ipairs(sorted_rt_point) do
      --    Shared:FadedMessage("Nearest marine from rt n'" .. tostring(it)
      --                           .. " is at " .. _getDistFromNeareastMarine(rt) .. "m")
      -- end
      return (sorted_targets)
   end

   function sortByWound(alien, targets)
      local i = 1
      local sorted_targets = {}
      local added = false

      for _, target in ipairs(targets)
      do
         i = 0
         local t1 = target:GetHealth() + target:GetArmor()
         for it, target2 in ipairs(sorted_targets) do
            local t2 = target2:GetHealth() + target2:GetArmor()
            if (t1 < t2) then
               table.insert(sorted_targets, it, target)
               added = true
               break
            end
         end
         if (added == false) then
            table.insert(sorted_targets, #sorted_targets + 1, target)
         end
      end
      -- for it, rt in ipairs(sorted_rt_point) do
      --    Shared:FadedMessage("Nearest marine from rt n'" .. tostring(it)
      --                           .. " is at " .. _getDistFromNeareastMarine(rt) .. "m")
      -- end
      return (sorted_targets)
   end

   function sortByLonelyness(alien, targets)
      local i = 1
      local sorted_targets = {}
      local added = false

      for _, target in ipairs(targets)
      do
         i = 0
         local t1 = #GetEntitiesForTeamWithinRange("Player", 1, target:GetOrigin(), 20)
         for it, target2 in ipairs(sorted_targets) do
            local t2 = #GetEntitiesForTeamWithinRange("Player", 1, target2:GetOrigin(), 20)
            if (t1 < t2) then
               table.insert(sorted_targets, it, target)
               added = true
               break
            end
         end
         -- if (added == false) then
         --    table.insert(sorted_targets, #sorted_targets + 1, target)
         -- end
      end
      -- for it, rt in ipairs(sorted_rt_point) do
      --    Shared:FadedMessage("Nearest marine from rt n'" .. tostring(it)
      --                           .. " is at " .. _getDistFromNeareastMarine(rt) .. "m")
      -- end
      return (sorted_targets)
   end


   local function GetCommander(teamNum)
      local commanders = GetEntitiesForTeam("Commander", teamNum)
      return commanders[1]
   end

   local kExtents = Vector(0.4, 0.5, 0.4) -- 0.5 to account for pathing being too high/too low making it hard to palce tunnels
   local function IsPathable(position) -- From GorgeTunnel weapon

      local noBuild = Pathing.GetIsFlagSet(position, kExtents, Pathing.PolyFlag_NoBuild)
      local walk = Pathing.GetIsFlagSet(position, kExtents, Pathing.PolyFlag_Walk)
      return not noBuild and walk

   end

   local function _getTargets(self)
      local target = nil
      local targets = {}
      local _targets = {
         GetEntitiesForTeam("Player", 1),
         GetEntitiesForTeam("Exosuit", 1), -- Exo without player on it (open)
         -- GetEntitiesForTeam("Armory", 1),
         --GetEntitiesForTeam("Door", 1),
         -- GetEntitiesForTeam("Extractor", 1),
         -- GetEntitiesForTeam("Armory", 1),
         -- GetEntitiesForTeam("RoboticsFactory", 1),
         -- GetEntitiesForTeam("MAC", 1),
         -- GetEntities("Powerpoint"),
         -- GetEntitiesForTeam("CommandStation", 1),
      }

      if (not self:isa("Lerk")) then
         table.insert(_targets, GetEntitiesForTeam("Sentry", 1))
         table.insert(_targets, GetEntitiesForTeam("InfantryPortal", 1))
      end

      local PPs = {}
      -- Only attack powerpoints (with the dmg 400% buff) with obs alone
      for _, pp in ipairs(GetEntities("PowerPoint")) do
         if (#GetEntitiesForTeamWithinRange("InfantryPortal", 1, pp:GetOrigin(), 15) == 0
                and #GetEntitiesForTeamWithinRange("PrototypeLab", 1, pp:GetOrigin(), 15) == 0
                and #GetEntitiesForTeamWithinRange("ArmsLab", 1, pp:GetOrigin(), 15) == 0
                and #GetEntitiesForTeamWithinRange("PhaseGate", 1, pp:GetOrigin(), 15) == 0
                and #GetEntitiesForTeamWithinRange("Armory", 1, pp:GetOrigin(), 15) == 0
                and #GetEntitiesForTeamWithinRange("Observatory", 1, pp:GetOrigin(), 15) > 0
         )
         then
            table.insert(PPs, pp)
         end
      end

      table.insert(_targets, PPs) -- Where outbase obs are

      -- if (#GetEntitiesForTeamWithinRange("ARC", 1, self:GetOrigin(), 15) > 0) then -- Focus arcs
      --    _targets = {
      --       GetEntitiesForTeam("Player", 1),
      --       GetEntitiesForTeam("ARC", 1)}
      -- end

      -- Only kill buildings if gorge or safe to do it
      local nb_pl = GetGamerules():GetTeam1():GetNumPlayers()
      local m_around = #GetEntitiesForTeamWithinRange("Player", 1, self:GetOrigin(), 15)
      local ep = nb_pl > 3 and getNbPlayerAliveForTeam1() <= 3
      if (ep or self:isa("Gorge") or m_around == 0) then
         if (#GetEntitiesForTeamWithinRange("InfantryPortal", 1, self:GetOrigin(), 5) > 0) then -- Focus IP
            _targets = {
               GetEntitiesForTeam("InfantryPortal", 1)
            }
         end
      end

      local ep1 = nb_pl > 3 and getNbPlayerAliveForTeam1() <= (nb_pl / 2) and not self:isa("Lerk")
      if (ep1 or m_around == 0) -- If marine team is almost dead, start killing buildings
      then
         local buildings = {}
         for _, name in ipairs({"Armory", "PrototypeLab", "PhaseGate"}) do
            for _, a in ipairs(GetEntitiesForTeam(name, 1)) do
               local do_attack = false
               if (a) then
                  if (a:isa("PowerPoint")) then
                     do_attack = (a:GetHealth() > 0)
                  else
                     do_attack = a and a:GetIsPowered()
                  end
               end
               if (do_attack) then -- Do not attack unpowered stuff
                  table.insert(buildings, a)
               end
            end
         end
         table.insert(_targets, buildings)
      end

      -- if (Shared.GetMapName() == "ns2_def_troopers") then
      --    table.insert(_targets, GetEntitiesForTeam("Armory", 1))
      -- end

      -- if (not ((math.random() < 0.4 and self:isa("Skulk")) or self:isa("Lerk"))) then
      --    buildings = GetEntitiesForTeam("Sentry", 1)
      -- end


      for _, ent_list in ipairs(_targets) do
         for _, ent in ipairs(ent_list) do
            if (ent and ent:GetIsAlive()) then
               local do_insert = true
               if (ent:isa("Sentry")) then
                  if (self.defenseAttackSentry ~= true) then
                     do_insert = false
                  end
               end

               if (HasMixin(ent, "GhostStructure")) then
                  if (ent.GetIsGhostStructure and ent:GetIsGhostStructure()) then
                     do_insert = false
                  end
               end

               -- if (self:isa("Onos")) then -- Onos do not fly around JP
               --    local points = PointArray()
               --    local isReachable = Pathing.GetPathPoints(self:GetOrigin(), ent:GetOrigin(), points)
               if (do_insert) then
                      --       if (isReachable) then
                      table.insert(targets, ent)
                      --       end
                      --    end
               end
            end
         end
      end

      if (#targets == 0) then
         for _, ent_list in ipairs(_targets) do
            for _, ent in ipairs(ent_list) do
               if (ent and ent:GetIsAlive()) then
                  table.insert(targets, ent)
                  break
               end
            end
         end
      end

      Shared.SortEntitiesByDistance(self:GetOrigin(), targets)
      return targets
   end

   local function _chooseBestTarget(self, targets)
      local new_target = nil
      local default_target = nil
      -- TODO/ les bot font des allez-retour (changement de cible en court de route)
      -- eviter de switcher trop vite et garder la même target si possible
      local nearest_marine = nil
      local nearest_building = nil
      local isSelected = (self.GetIsSelected and (self:GetIsSelected( kAlienTeamType )))
      local door = getAliveDoorAround(self)

      if (not isSelected and #targets > 0) then
         for _, t in ipairs(targets) do
            -- Only lerks and gorgies attack unreachable targets (can fly or range attack)
            if (self:isa("Lerk") or self:isa("Gorge")) then
               default_target = t
            else
               if (IsPathable(t:GetOrigin()))
               then
                  default_target = t
               end
            end
            if (default_target) then
               break
            end
         end

         if (door and #door > 0) then
            default_target = door[1] -- Attack doors if one is nearby and closed
         end

         if (default_target == nil) then
            default_target = targets[1] -- Force at least one target if none found
         end
      else
         self.defenseLastSelectedTime = Shared.GetTime()
      end

      if (self.def_atk_most_wounded and default_target and default_target:isa("Player")) then
         -- Special cases, choose lowest marine in HP/AP around (more evil !)
         if (self:isa("Gorge") or self:isa("Lerk") or self:isa("Fade")) then
            local p_around = GetEntitiesForTeamWithinRange("Player", 1, default_target:GetOrigin(), 10)
            local new_targets = sortByWound(self, p_around)

            if (new_targets and #new_targets > 0) then
               new_target = new_targets[1] -- Most wounded target first
            end
         end
      end

      if (new_target == nil) then
         new_target = default_target
      end

      local current_target = nil
      local current_target_id = self.defenseTargetId
      if (new_target and current_target_id) then -- Only change target if it is closer (in a path way)
         current_target = Shared.GetEntity(current_target_id)
         if (current_target and current_target:GetIsAlive()
                and new_target:GetOrigin():GetDistanceTo(self:GetOrigin())
                < current_target:GetOrigin():GetDistanceTo(self:GetOrigin())
         )
         then
            local pointsOld = PointArray()
            local pointsNew = PointArray()
            local oldr = Pathing.GetPathPoints(self:GetOrigin(), current_target:GetOrigin(), pointsOld)
            local newr = Pathing.GetPathPoints(self:GetOrigin(), new_target:GetOrigin(), pointsNew)
            if (not newr or #pointsNew == 0 or #pointsOld < #pointsNew) then
               new_target = current_target
            end
         end
      end

      return new_target
   end

   local function _canLerkGoToTargetStraight(bot, target)
      local minFlyHeigh = 16
      local maxFlyHeigh = 100
      local minTimeInSky = 2
      local maxTimeInSky = 5
      local skyAttackDuration = 10

      local marinePos = target:GetOrigin()
      local currentflyHeigh = getFlyHeigh(bot)
      local _, ceilingHeigh = IsOutside(target, maxFlyHeigh)
      -- local bothOutside = IsOutside(bot) and IsOutside(target)

      ceilingHeigh = math.min(ceilingHeigh, 30)
      maxFlyHeigh = Clamp(ceilingHeigh * (80/100), 1, maxFlyHeigh)
      minFlyHeigh = maxFlyHeigh * (50/100)

      local y_fly_heigh =  marinePos.y + minFlyHeigh
      local highPosBot = GetNormalizedVectorXZ(bot:GetOrigin()) + Vector(0, y_fly_heigh, 0)
      local highPosTarget = GetNormalizedVectorXZ(marinePos) + Vector(0, y_fly_heigh, 0)
      local OutAndVisible = (not GetWallBetween(highPosTarget, highPosBot)
                                and not GetWallBetween(bot:GetOrigin(), highPosBot))

      return OutAndVisible
   end

   local function _giveOrder(self, target)
      if (target) then
         -- After getting selected, stand still for 15s or attack if marines is nearby
         if (self.defenseLastSelectedTime == nil
                or self.defenseLastSelectedTime + kFadedAlienComBotStandByDelay < Shared.GetTime()
             or self:GetOrigin():GetDistanceTo(target:GetOrigin()) < 15) then

            self.defenseTargetId = target:GetId()
            if (true or self:GetPlayerOrder() == nil) then
               local order = kTechId.Move
               local dist = self:GetOrigin():GetDistanceTo(target:GetOrigin())
               local max_dist = 9
               if (self:isa("Lerk")) then
                  max_dist = 60
               end

               if (not GetWallBetween(self:GetModelOrigin(), target:GetModelOrigin()) and dist < max_dist)
               then
                  order = kTechId.Attack -- Last target (on sight, attack)
               else
                  if (self:isa("Lerk") and _canLerkGoToTargetStraight(self, target)) then
                     -- Shared:FadedMessage("Lerk attack bypass")
                     order = kTechId.Attack
                  end
               end


               self:GiveOrder(order, target:GetId(), target:GetOrigin(), nil, true, false)
            else
               -- Clear order if almost reach destination
               if (self:GetOrigin():GetDistanceTo(target:GetOrigin()) < 3) then
                  self:ClearOrders()
               end
            end
         end

         -- Shared:FadedMessage("Attacking target order done")
      else
         self:ClearOrders()
      end
   end


   function AlienUpdateOrderCallback(self)

      local target = nil
      local targets = nil

      if (not DefIsVirtual(self)) then -- No order to players
         return
      end

      -- Defend order prioritize (during 26s)
      if (self.def_hive_defense and self.def_hive_defense + 15 > Shared.GetTime()) then
         target = Shared.GetEntity(self.defenseTargetId)
      end

      if (target == nil) then -- Old target dead
         targets = _getTargets(self)
         target = _chooseBestTarget(self, targets)
      end

      _giveOrder(self, target)

      return (self:GetIsAlive())
   end

   -- local alienOnKill = Alien.OnKill
   -- function Alien:OnKill(attacker, doer, point, direction)
   --    if (attacker and doer == nil) then
   --       doer = attacker
   --    end
   --    if (not self.client) then
   --    end
   --    alienOnKill(self, attacker, doer, point, direction)
   -- end

   local playerOnKill = Player.OnKill
   function Player:OnKill(attacker, doer, point, direction)
      -- Patch a nil exception for the stab poison
      -- (because doer is nil but not attacker)
      -- See "function Player:OnKill(killer, doer, point, direction)"
      if (attacker and doer == nil) then
         doer = attacker
      end


      if (playerOnKill) then
         playerOnKill(self, attacker, doer, point, direction)
      end
   end

   function Alien:GetDarkVisionEnabled()
      return self.darkVisionOn
   end


elseif (Client) then
   function Alien:Buy() end
   function Skulk:Buy()  end
   function Gorge:Buy()  end
   function Lerk:Buy()  end
   function Fade:Buy()  end
   function Onos:Buy()  end
   -- function Player:GetIsAllowedToBuy() return true end --self:GetTeamNumber() == 1 end
end

-- function Alien:UpdateSilenceLevel()

--    self.silenceLevel = 3
--    -- if GetHasSilenceUpgrade(self) then
--    --    self.silenceLevel = GetVeilLevel(self:GetTeamNumber())
--    --  else
--    --      self.silenceLevel = 0
--    --  end

-- end

-- -- Get the opacity of the faded when he is moving
-- -- (small area of the body become visible)
-- function Alien:GetOpacityLevel()

--    kFadedModOpacity = kFadedModBaseOpacity

--    --if self.primaryAttacking then
--    --  kFadedModCloakedFraction = kFadedModCloakedFraction + kFadedModAttackingCloakedModificator
--    --end

--    kFadedModOpacity = kFadedModOpacity + ((math.abs(self.velocityLength) * kFadedModSpeedOpacitydModificator)/3)
--    kFadedModOpacity = kFadedModOpacity + ((math.abs(self.velocityYaw) * kFadedModSpeedOpacitydModificator)/3)
--    kFadedModOpacity = kFadedModOpacity + ((math.abs(self.velocityPitch) * kFadedModSpeedOpacitydModificator)/3)

--    if kFadedModOpacity <= 0  then
--       return 0
--    elseif kFadedModOpacity <= kFadedModLowestOpacity then
--       return kFadedModOpacity
--    else
--       return kFadedModLowestOpacity
--    end
-- end

if (Client) then
   -- -- Return the closest marine coord to the 'alien_orig' position
   -- -- Return nil if there are no one around
   -- local function _getClosestMarine(alien_orig)
   --    -- +1 to be above the maximum visibility range
   --    local distance = kFadedMarineMaximalDistanceOfDetection + 1
   --    local tmp = 0
   --    local closestMarine = nil
   --    local marine_orig = nil
   --    local marine_around = GetEntitiesForTeamWithinRange("Marine",
   --                               1, alien_orig,
   --                  kFadedMarineMaximalDistanceOfDetection)
   --    for _, marine in ipairs(marine_around) do
   --      if (marine and marine:GetIsAlive()) then
   --         if (marine_orig == nil) then
   --            marine_orig = marine:GetOrigin()
   --            closestMarine = marine
   --         end
   --         tmp = alien_orig:GetDistanceTo(marine:GetOrigin())
   --         if (tmp < distance) then
   --            marine_orig = marine:GetOrigin()
   --            distance = tmp
   --            closestMarine = marine
   --         end
   --      end
   --    end
   --    return marine_orig, closestMarine
   -- end

   -- -- Return the cloak amount of the Faded (from 0 to 1)
   -- local function _getCloakFraction(marine, marine_orig, alien_orig)
   --    local kFadedCloakFraction = kFadedBaseCloakFraction
   --    local kFadedClossestMarine = marine_orig:GetDistanceTo(alien_orig)

   --    local sanity = Clamp(marine:GetMentalHealth() + kFadedMinMissingSanityForMalus, kFadedMinViewQuality, 100)
   --    local max_view_distance = kFadedMarineMaximalDistanceOfDetection * (sanity / 100)
   --    -- Get the % of cloak
   --    if kFadedClossestMarine <= max_view_distance then
   --      local actual_percent = max_view_distance - kFadedClossestMarine
   --      local max_percent = max_view_distance - kFadedMarineMaximalDetectionDistance
   --      kFadedCloakFraction = (actual_percent)/(max_percent) * kFadedMinimalCloak
   --    end
   --    return (kFadedCloakFraction)
   -- end

   -- -- Get the cloak Fraction making it transparent at long range
   -- -- Return a value between 0 (full cloak) and 1 (visible)
   -- function Alien:GetCloakFraction()

   --    local kFadedClossestMarine = nil
   --    -- Default: Invisible
   --    local kFadedCloakFraction = 0
   --    -- The local player, not the alien (to update camo client side)
   --    local player = Client.GetLocalPlayer()
   --    local alien_orig = self:GetOrigin()
   --    local marine_orig = player:GetOrigin()

   --    if (player:GetTeamNumber() == 1 and player:isa("Marine"))
   --    then
   --      -- Local camo as seen by the marine or the local alien
   --      kFadedCloakFraction = _getCloakFraction(player, marine_orig, alien_orig)
   --    elseif (player:GetId() == self:GetId())
   --    then
   --      -- Camo as seen by the local Faded player (the visibility GUI)
   --      marine_orig, marine = _getClosestMarine(alien_orig)
   --      if (marine_orig) then
   --         kFadedCloakFraction = _getCloakFraction(marine, marine_orig, alien_orig)
   --      end
   --    else
   --      -- Local camo as seen by the others alien
   --      kFadedCloakFraction = 0.9
   --    end
   --    return kFadedCloakFraction
   -- end -- !function Alien:GetCloakFration()
end -- !Client

local playerOnTakeDamage = Player.OnTakeDamage
function Player:OnTakeDamage(damage, attacker, doer, point)
   if (Client or Server) then
      if (self:GetCreationTime() + 1 < Shared.GetTime()) then
         if (attacker ~= nil and attacker:isa("Player")) then
            local damageDoneList = self.damageDoneList or {}
            local damageDoneAttacker = damageDoneList[attacker:GetName()] or 0

            damageDoneAttacker = damageDoneAttacker + damage

            damageDoneList[attacker:GetName()] = damageDoneAttacker
            self.damageDoneList = damageDoneList
            --
            if (Server and DefIsVirtual(self)) then
               local isSelected = (self.GetIsSelected and (self:GetIsSelected( kAlienTeamType )))
               if (not isSelected and self.defenseLastSelectedTime) then
                  self.defenseLastSelectedTime = nil -- Hack to force the attack order
                  AlienUpdateOrderCallback(self)
               end
            end
         end
      end
   end
   if (playerOnTakeDamage) then
      return playerOnTakeDamage(self, damage, attacker, doer, point)
   end
end
