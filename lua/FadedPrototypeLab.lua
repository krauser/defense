Script.Load("lua/PrototypeLab.lua")

local clientProtoId = nil

function setTraderProtoId(id)
   clientProtoId = id
end
function getTraderProtoId()
   return clientProtoId
end

function PrototypeLab:GetItemList(forPlayer)
   local itemList = {
      kTechId.Pistol,
      -- kTechId.Shotgun,
      kTechId.Jetpack,
      kTechId.LaySentryBattery,
      -- kTechId.ClawRailgunExosuit,
      -- kTechId.Exo

      kTechId.DualMinigunExosuit,
      kTechId.DualRailgunExosuit,

      kTechId.HealGrenade,
      kTechId.ClusterGrenade,
      kTechId.NapalmGrenade
      -- kTechId.MAC,
      --      kTechId.Scan,
   }
   --- TODO: limit by 1/3 the number of exos on the field
   return itemList
end

function PrototypeLab:GetCanBeUsed(player, useSuccessTable)
   useSuccessTable.useSuccess = true
   -- if (Shared.GetMapName() == "ns2_def_troopers") then
   --    useSuccessTable.useSuccess = true
   -- else
   --    if (getTraderProtoId() == self:GetId() and player and player:GetTeamNumber() == 1) then
   --       useSuccessTable.useSuccess = true
   --    else
   --       useSuccessTable.useSuccess = false
   --    end
   -- end
end

-- This make the building bug (can't buy anything no matter what)
-- function PrototypeLab:GetRequiresPower()
--    return false
-- end
