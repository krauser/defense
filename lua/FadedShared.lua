-- ===================== Faded Mod =====================
--
-- lua\FadedShared.lua
--
--    Created by: Rio (rio@myrio.de)
--    Update by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

-- Load libraries
Script.Load("lua/libs/LibCache/LibCache.lua")
Script.Load("lua/libs/LibLocales-1.0/LibLocales.lua")
Script.Load("lua/libs/LibMessages-1.0/LibMessages.lua")
Script.Load("lua/libs/LibTimer-1.0/LibTimer.lua")

-- if Server then
--     Script.Load("lua/MarineTeam.lua")
--     Script.Load("lua/AlienTeam.lua")
-- end

Script.Load("lua/FadedTechIdAdjustement.lua")

Script.Load("lua/FadedNS2Utility.lua")
Script.Load("lua/FadedRifle.lua")
Script.Load("lua/FadedARC.lua")
Script.Load("lua/FadedDoor.lua")
Script.Load("lua/FadedArmslabs.lua")
Script.Load("lua/FadedClog.lua")
Script.Load("lua/FadedBoneWall.lua")
Script.Load("lua/FadedRelevancySettings.lua")

Script.Load("lua/FadedSpectator.lua")
Script.Load("lua/FadedDamageMixin.lua")

Script.Load("lua/FadedBotsAbilities.lua")

Script.Load("lua/FadedLeapMixin.lua")
Script.Load("lua/FadedHallucination.lua")

Script.Load("lua/FadedWavesShared.lua")

--Script.Load("lua/FadedWaves.lua")
Script.Load("lua/FadedWavesKF.lua")


Script.Load("lua/FadedCyst.lua")

Script.Load("lua/FadedEgg.lua")
Script.Load("lua/FadedBabblerEgg.lua")

Script.Load("lua/FadedPhaseGate.lua")

Script.Load("lua/FadedBuildingShared.lua")

-- Script.Load("lua/FadedHeavyShotgun.lua")
-- Script.Load("lua/LaySentryBattery.lua")
-- Script.Load("lua/FadedFlameMine.lua")
-- Script.Load("lua/FadedLayFlameMines.lua")

-- Script.Load("lua/FadedNapalmGrenade.lua")
-- Script.Load("lua/Flare.lua")
-- Script.Load("lua/FadedNapalmGrenadeThrower.lua")

----------------------

-- kFadedPlayerStatus = {'HShotgun'}

-- for key, name in ipairs( kPlayerStatus ) do
--    table.insert(kFadedPlayerStatus, kPlayerStatus[key])
-- end

-- kPlayerStatus = enum(kFadedPlayerStatus)

---------------------


Script.Load("lua/FadedGlobals.lua")
Script.Load("lua/FadedUpdate.lua")
Script.Load("lua/FadedScoringMixin.lua")
Script.Load("lua/FadedWeaponOwnerMixin.lua")

Script.Load("lua/FadedEffectsMixin.lua")

Script.Load("lua/FadedFlameMine.lua")
Script.Load("lua/FadedLayFlameMines.lua")

Script.Load("lua/FadedMapBlipInfo.lua")
Script.Load("lua/FadedTechPoint.lua")
Script.Load("lua/FadedHive.lua")

Script.Load("lua/FadedPowerPoint.lua")

Script.Load("lua/FadedSkulk.lua")
Script.Load("lua/FadedLerk.lua")
Script.Load("lua/FadedGorge.lua")
Script.Load("lua/FadedFade.lua")
Script.Load("lua/FadedOnos.lua")

Script.Load("lua/FadedArmory.lua")
Script.Load("lua/FadedPrototypeLab.lua")
Script.Load("lua/FadedShotgun.lua")

Script.Load("lua/FadedGrenade.lua")
Script.Load("lua/FadedNapalmGrenade.lua")
Script.Load("lua/FadedNapalmGrenadeThrower.lua")

Script.Load("lua/FadedHealGrenade.lua")
Script.Load("lua/FadedHealGrenadeThrower.lua")

Script.Load("lua/FadedDrifter.lua")

Script.Load("lua/FadedExo.lua")
Script.Load("lua/FadedDropPack.lua")

Script.Load("lua/FadedTunnelEntrance.lua")
Script.Load("lua/FadedGhostStructureMixin.lua")
Script.Load("lua/FadedConstructMixin.lua")

Script.Load("lua/FadedSentryBattery.lua")
-- Script.Load("lua/FadedCloak.lua")
-- Script.Load("lua/FadedRoundTimer.lua")
-- Script.Load("lua/FadedParasite.lua")

Script.Load("lua/FadedBabblers.lua")
-- Script.Load("lua/FadedBabblerAbility.lua")

Script.Load("lua/FadedMarine.lua")
Script.Load("lua/FadedVeteranMarine.lua")
Script.Load("lua/FadedMines.lua")
Script.Load("lua/FadedAlien.lua")

-- Script.Load("lua/FadedWhip.lua")
Script.Load("lua/Weapons/Marine/FadedGrenade.lua")
Script.Load("lua/Weapons/Marine/FadedMinigun.lua")

Script.Load("lua/Flare.lua")

Script.Load("lua/FadedGrenadeLauncher.lua")
Script.Load("lua/FadedFlamethrower.lua")
-- Script.Load("lua/FadedStab.lua")
Script.Load("lua/FadedClipWeapon.lua")
-- Script.Load("lua/FadedWeldableMixin.lua")

Script.Load("lua/FadedMoonlight.lua")
Script.Load("lua/FadedLights.lua")

Script.Load("lua/Weapons/Alien/FadedSpikesMixin.lua")
-- Script.Load("lua/Weapons/Marine/FadedNapalmGazGrenade.lua")
-- Script.Load("lua/Weapons/Marine/FadedNapalmGazGrenadeThrower.lua")
-- Script.Load("lua/FadedTechData.lua")
Script.Load("lua/FadedPistol.lua")
-- Script.Load("lua/FadedRagdoll.lua")
-- Script.Load("lua/FadedHallucination.lua")


local kSelectEquipmentMessage =
{
    Weapon = "enum kTechId",
    Equipment = "enum kTechId"
}

if (Client) then
   -- Sound list
   local Sounds =
      {
         -- ["AmbientMusic"] = { -- Long Ambiant sound
         --    { sound = "sound/faded.fev/ambient/clank", length = 12382, volume = 0.4},
         --    { sound = "sound/faded.fev/ambient/alien_noise", length = 11415, volume = 0.4},
         --    { sound = "sound/faded.fev/ambient/alien_noise2", length = 26409, volume = 0.4},
         --    { sound = "sound/faded.fev/ambient/alien_noise2", length = 26409, volume = 0.4},
         --    { sound = "sound/faded.fev/ambient/horror_ambient", length = 38243, volume = 0.4},
         -- },
         ["AmbientEffects"] = { -- Effect short Ambiant sound
            { sound = "sound/NS2.fev/ambient/atmospheric clanks", length = 5000, volume = 1},
            { sound = "sound/NS2.fev/ambient/alien flavour", length = 5000, volume = 1},
         },
         -- ["FadeApproach"] = { -- Fade approach sound
         --    { sound = "sound/faded.fev/ambient/whisper_tail1", length = 4234, volume = 1},
         --    { sound = "sound/faded.fev/ambient/whisper_tail2", length = 3720, volume = 1},
         --    { sound = "sound/faded.fev/ambient/whisper_tail3", length = 5605, volume = 1},
         -- },
         ["AloneMarine"] = { -- When the marine is alone
            { sound = "sound/NS2.fev/alien/onos/idle", length = 5000, volume = 0.4},
         },
         ["LastMarine"] = { -- When the marine is alone
            { sound = "sound/NS2.fev/music/main_menu", length = 12000, volume = 1},
         },
         ["Others"] = { -- Uncommon NS2 sound, to be sure (used in other files)
            { sound = "sound/NS2.fev/alien/structures/death_large"},
            { sound = "sound/NS2.fev/alien/structures/healing_mound_heal"},
            { sound = "sound/NS2.fev/ambient/docking/docking_music"},
            { sound = "sound/NS2.fev/common/door_inoperate"},
            { sound = "sound/NS2.fev/common/ping"},
            { sound = "sound/NS2.fev/ambient/descent/docking_background_music"},
            { sound = "sound/NS2.fev/ambient/Decaypad"},
            { sound = "sound/NS2.fev/ambient/ns1_pad_1"},
            { sound = "sound/NS2.fev/ambient/ns1_pad_2"},
            { sound = "sound/NS2.fev/ambient/ns1_pad_3"},
         },
      }

   for _, list in pairs(Sounds) do
      for _, ambientSound in pairs(list) do
         Client.PrecacheLocalSound(ambientSound.sound)
      end
   end
end

Shared.RegisterNetworkMessage("SelectEquipment", kSelectEquipmentMessage)
Shared.RegisterNetworkMessage("RoundTime", { time = "integer" })
-- Shared.RegisterNetworkMessage("SetAsVeteran",
--                   {
--                  -- VeteranName = "string",
--                  kFadedIsVeteran = "boolean",
--                   })

Shared.RegisterNetworkMessage("PlayClientPrivateMusic",
                  {
                     music = "string (128)",
                     volume = "integer",
                     nostop = "boolean",
                  })

Shared.RegisterNetworkMessage("kMarineNb_kFadeNB",
                              {
                                 kMarineNb = "integer",
                                 kFadeNb = "integer",
                              })

Shared.RegisterNetworkMessage("GUIAlienCounter",
                              {
                                 progress = "integer",
                                 wave_nb = "integer",
                                 wave_total = "integer",
                              })
Shared.RegisterNetworkMessage("LifeformCounterSet",
                              {
                                 lifeformMapName = "string (32)",
                                 nb = "integer",
                                 unspawn_nb = "integer",
                              })
Shared.RegisterNetworkMessage("LifeformCounterSetPop",
                              {
                                 pop = "integer",
                                 max_pop = "integer",
                              })


Shared.RegisterNetworkMessage("TriggerShadowStep",
                              {
                                 direction = "vector",
                              })

Shared.RegisterNetworkMessage("SendSelectEquipmentMessage",
                              {
                              })

Shared.RegisterNetworkMessage("EtherealEndTime",
                              {
                                 faded_id = "integer",
                              })

Shared.RegisterNetworkMessage("setFakeMarineName",
                              {
                                 fakeMarineRealName = "string (128)",
                                 fakeMarineName = "string (128)",
                              })
Shared.RegisterNetworkMessage("OnTriggerTransform",
                              {
                                 corpse_name = "string (32)",
                              })
Shared.RegisterNetworkMessage("OnTriggerEating",
                              {
                                 corpse_name = "string (32)",
                              })
Shared.RegisterNetworkMessage("OnTriggerReviveFaded",
                              {
                                 corpse_name = "string (32)",
                              })
Shared.RegisterNetworkMessage("OnTriggerBurnCorpse",
                              {
                                 corpse_name = "string (32)",
                              })

Shared.RegisterNetworkMessage("IsFadeEating",
                              {
                                 time = "time",
                              })
Shared.RegisterNetworkMessage("OnChatCallBack",
                              {
                                 msg = "string (128)",
                                 local_key = "string (32)",
                              })

-- Shared.RegisterNetworkMessage("ChangeModelSize",
--                   {
--                      modelSize = "float (0 to 10 by .1)",
--                      id = "integer",
--                   })

Shared.RegisterNetworkMessage("PlayWhisperSound", {})
-- Shared.RegisterNetworkMessage("deniedSwipeAfterStab",
--                   {
--                  time = "time",
--                   })

Shared.RegisterNetworkMessage("SetObsState",
                              {
                                 kFadedIsObsUp = "boolean",
                              })
Shared.RegisterNetworkMessage("OnFinishHunting",
                              {
                                 location = "vector",
                              })
Shared.RegisterNetworkMessage("ConfigModification",
                              {
                                 kFadedNoCustomFlashlight = "boolean",
                                 kFadedRespawnedEnable = "boolean",
                                 kFadedGLCheatEnable = "boolean",
                                 kFadedHSGCheatEnable = "boolean",
                              })
Shared.RegisterNetworkMessage("ConfigModificationLog",
                              {
                                 str = "string (128)",
                              })
Shared.RegisterNetworkMessage("SetAsHunter",
                              {
                                 st = "boolean",
                                 name = "string (32)",
                              })
Shared.RegisterNetworkMessage("SetAsMedic",
                              {
                                 st = "boolean",
                                 name = "string (32)",
                              })

Shared.RegisterNetworkMessage("ToggleBeeper", {})

Shared.RegisterNetworkMessage("TakeRagdoll",
                              {
                                 ragdoll_id = "integer",
                                 battery_id = "integer",
                              })
Shared.RegisterNetworkMessage("DropRagdoll",
                              {
                                 marineRagdoll = "boolean",
                                 fadedRagdoll = "boolean",
                                 playerName = "string (32)",
                                 modelName = "string (128)",
                                 graphName = "string (128)",
                              })

Shared.RegisterNetworkMessage("ImpulseRagdoll",
                              {
                                 ragdoll_coord = "vector",
                                 impulse_start_coord = "vector",
                                 impulse = "vector",
                                 name = "string (32)"
                              })


Shared.RegisterNetworkMessage("setTraderBuildingIds",
                              {
                                 armory_id = "integer",
                                 proto_id = "integer"
                              })

Shared.RegisterNetworkMessage("DEBUGCAMO_cloack_dec",
                              {
                              })
Shared.RegisterNetworkMessage("DEBUGCAMO_cloack_up",
                              {
                              })
Shared.RegisterNetworkMessage("DEBUGCAMO_visu_dec",
                              {
                              })

-- -- scoreboardFadePlayersName
-- Shared.RegisterNetworkMessage("AddScoreboardFadeList",
--                   {
--                  name = "string (32)"
--                   })
-- Shared.RegisterNetworkMessage("ResetFadedScoreboardList",
--                   {
--                   })



-- angle: [-1, 1] (0 to 1 is the 180 view in front of, and -1 to 0 is behind)
function GetEntitiesForTeamWithinRangeInLOS(source, angle, ent, team, orig, range)
   local ret = {}
   local trackEntities = GetEntitiesForTeamWithinRange(ent, team, orig, range)

   for t = 1, #trackEntities do
      local trackEntity = trackEntities[t]
      local inFrontData = source:GetViewCoords().zAxis:DotProduct(GetNormalizedVector(trackEntity:GetModelOrigin() - source:GetEyePos()))
      -- if (Client) then
      --    Print("angle: " .. tostring(inFrontData))
      -- end
      local isOnFlashLight = (inFrontData >= angle)
      if isOnFlashLight and trackEntity:GetIsAlive() then
         if (GetWallBetween(source:GetEyePos(), trackEntity:GetOrigin() + Vector(0, 0.8, 0)) == false) then
            table.insert(ret, trackEntity)
         end
      end
   end -- endfor
   return (ret)
end

function DefIsVirtual(ent)
   if (ent and ent.GetClient and ent:GetClient() and not ent:GetClient():GetIsVirtual()) then
      return false
   else
      return true
   end
end

function Scoreboard_OnResetGame()
end

-- local botTeamControllerSetMaxBots = BotTeamController.SetMaxBots
-- function BotTeamController:SetMaxBots(newMaxBots, com)
--    botTeamControllerSetMaxBots(self, 0) -- No filler bots (build 305)
-- end

function Scoreboard_Clear()
end

local libLocale = LibCache:GetLibrary("LibLocales-1.0")
if (Client) then
   function Locale.ResolveString(text)
      return libLocale:ResolveString(text) or ""
   end
end

function Shared:FadedMessage(chatMessage)
   if (Server) then
      if (chatMessage == nil) then return end
      Server.SendNetworkMessage("Chat", BuildChatMessage(false, "Defense Mod", -1, kTeamReadyRoom, kNeutralTeamType, chatMessage), true)
      Shared.Message("Chat All - Defense Mod: " .. chatMessage)
      Server.AddChatToHistory(chatMessage, "Defense Mod", 0, kTeamReadyRoom, false)
   elseif (Client) then
      Print("Faded client log: " .. chatMessage)
   else
   end
end
function Shared:SatanMessage(chatMessage)
   if (Server) then
      if (chatMessage == nil) then return end
      Server.SendNetworkMessage("Chat", BuildChatMessage(false, "Satan", -1, kTeamReadyRoom, kNeutralTeamType, chatMessage), true)
      Shared.Message("Chat All - Satan: " .. chatMessage)
      Server.AddChatToHistory(chatMessage, "Satan", 0, kTeamReadyRoom, false)
   elseif (Client) then
      Print("Faded client log: " .. chatMessage)
   else
   end
end

function Player:FadedMessage(chatMessage)
   if (Server) then
      self:SendMessage("Defense ", chatMessage)
   elseif (Client) then
      Print("Faded client log: " .. chatMessage)
   end
end

function Player:SatanMessage(chatMessage)
   if (Server) then
      self:SendMessage("Satan ", chatMessage)
   elseif (Client) then
      Print("Satan client log: " .. chatMessage)
   end
end

if (Server) then
   -- Returns a random player out of the given player ids.
   function Shared:GetRandomPlayer(playerIds)
      if (playerIds == nil or #playerIds <= 0) then return nil end

      local randomPlayerId = playerIds[math.random(1, #playerIds)]
      local randomPlayer = Shared.GetEntity(randomPlayerId)

      return randomPlayer
   end

   function Shared:FadedHintMessage(chatMessage)
      if (chatMessage == nil) then return end
      Server.SendNetworkMessage("Chat", BuildChatMessage(false, "Hint", -1, kTeamReadyRoom, kNeutralTeamType, chatMessage), true)
      -- Shared.Message("Chat All - Hint: " .. chatMessage)
      -- Server.AddChatToHistory(chatMessage, "Hint", 0, kTeamReadyRoom, false)
   end

   function Player:FadedHintMessage(chatMessage)
      self:SendMessage("Hint", chatMessage)
      if (Server) then
         Server.AddChatToHistory(chatMessage, "Hint", 0, kTeamReadyRoom, false)
         Shared.Message("Chat All - Hint: " .. chatMessage)
      end
   end

   function MarineTeam:FadedMessage(chatMessage)
      self:SendMessage("Deathmatch", chatMessage)
   end

   function AlienTeam:FadedMessage(chatMessage)
      self:SendMessage("Deathmatch", chatMessage)
   end

   function Shared:GetPlayerByName(playerName)
      if (playerName) then
         for _, team in pairs(GetGamerules():GetTeams()) do
            for _, player in pairs(team:GetPlayers()) do
               if (player:GetName():lower() == playerName:lower()) then
                  return player
               end
            end
         end
      end
      return nil
   end
   function Shared:GetPlayerBySteamId(id)
      if (id) then
         for _, team in pairs(GetGamerules():GetTeams()) do
            for _, player in pairs(team:GetPlayers()) do
               if (player:GetSteamId() == id) then
                  return player
               end
            end
         end
      end
      return nil
   end

elseif (Client) then
   Client.HookNetworkMessage("RoundTime", function(data) kFadedModRoundTimerInSecs = data.time end)

   Client.HookNetworkMessage("GUIAlienCounter",
                             function(data)
                                if (data) then
                                   if (setFadedGUIAlienCounterInfo) then
                                      setFadedGUIAlienCounterInfo(data.progress, data.wave_nb, data.wave_total)
                                   end
                                end
                             end)

   Client.HookNetworkMessage("LifeformCounterSet",
                             function(data)
                                if (data) then
                                   lifeformCounterSet(data.lifeformMapName, data.nb)
                                   unspawnLifeformCounterSet(data.lifeformMapName, data.unspawn_nb)
                                end
                             end)
   Client.HookNetworkMessage("LifeformCounterSetPop",
                             function(data)
                                if (data) then
                                   wave_pop = data.pop
                                   wave_max_pop = data.max_pop
                                end
                             end)


   Client.HookNetworkMessage("setTraderBuildingIds",
                             function(data)
                                if (data) then
                                   -- local faded = Shared.GetEntity(data.faded_id)
                                   -- if (faded) then
                                   --    -- faded.etherealEndTime = Shared.GetTime()
                                   setTraderArmoryId(data.armory_id)
                                   setTraderProtoId(data.proto_id)
                                   --    Shared:FadedMessage("etherealendtime set to " .. tostring(faded.etherealEndTime))
                                   -- end
                                end
                             end)


   Client.HookNetworkMessage("kMarineNb_kFadeNB",
                             function(data)
                                kFadeNb = data.kFadeNb
                                kMarineNb = data.kMarineNb
                             end)

   Client.HookNetworkMessage("EtherealEndTime",
                             function(data)
                                if (data) then
                                   -- local faded = Shared.GetEntity(data.faded_id)
                                   -- if (faded) then
                                   --    -- faded.etherealEndTime = Shared.GetTime()
                                   CloakableMixinEntEndBlink[data.faded_id] = Shared.GetTime()
                                   --    Shared:FadedMessage("etherealendtime set to " .. tostring(faded.etherealEndTime))
                                   -- end
                                end
                             end)

   Client.HookNetworkMessage("SetObsState",
                             function(data)
                                local p = Client.GetLocalPlayer()

                                if (p and p:isa("Marine")) then
                                   p.kFadedIsObsUp = data.kFadedIsObsUp
                                end
                             end)


   -- local whispers =
   --    {
   --       { sound = "sound/faded.fev/ambient/whisper_tail1", length = 4234, volume = 1},
   --       { sound = "sound/faded.fev/ambient/whisper_tail2", length = 3720, volume = 1},
   --       { sound = "sound/faded.fev/ambient/whisper_tail3", length = 5605, volume = 1}
   --    }

   -- for _, whisper in pairs(whispers) do
   --    if (Client) then
   --       Client.PrecacheLocalSound(whisper.sound)
   --    elseif (Server) then
   --       PrecacheAsset(whisper.sound)
   --    end
   -- end

   -- Client.HookNetworkMessage("PlayWhisperSound",
   --                           function(data)
   --                              local p = Client.GetLocalPlayer()

   --                              if (p) then
   --                                 local s = whispers[math.random(1, #whispers)]
   --                                 Shared.PlaySound(p, s.sound)
   --                              end
   --                           end)

   Client.HookNetworkMessage("SetAsHunter",
                             function(data)
                                local p = Client.GetLocalPlayer()

                                if (p) then
                                   -- See FadedMarine.lua for this variable
                                   kFadedHunterList[data.name] = data.st
                                   for index, hunter in ipairs(GetEntities("Marine")) do
                                      if (hunter:GetName() == data.name) then
                                         if (data.st) then
                                            hunter:PromoteToHunter()
                                         else
                                            hunter:ClearHunterAbility()
                                         end
                                         break
                                      end
                                   end

                                end
                             end)

   Client.HookNetworkMessage("SetAsMedic",
                             function(data)
                                local p = Client.GetLocalPlayer()


                                if (p) then
                                   kFadedMedicList[data.name] = data.st
                                   -- See FadedMarine.lua for this variable
                                   for index, medic in ipairs(GetEntities("Marine")) do
                                      if (medic:GetName() == data.name) then
                                         if (data.st) then
                                            medic:PromoteToMedic()
                                         else
                                            medic:ClearMedicAbility()
                                         end
                                         break
                                      end
                                   end
                                   -- if (data.st == true) then
                                   --    p:PromoteToMedic()
                                   -- else
                                   --    p:ClearMedicAbility()
                                   -- end
                                end
                             end)

   Client.HookNetworkMessage("OnFinishHunting",
                             function(data)
                                local p = Client.GetLocalPlayer()

                                if (p and p:isa("Marine")) then
                                   HunterDrowPathFinder(p, data.location)
                                end
                             end)
   Client.HookNetworkMessage("ConfigModification",
                             function(data)
                                kFadedNoCustomFlashlight = data.kFadedNoCustomFlashlight
                                kFadedRespawnedEnable = data.kFadedRespawnedEnable
                                kFadedGLCheatEnable = data.kFadedGLCheatEnable
                                kFadedHSGCheatEnable = data.kFadedHSGCheatEnable
                             end)
   Client.HookNetworkMessage("ConfigModificationLog",
                             function(data)
                                Print(data.str)
                             end)

   Client.HookNetworkMessage("DEBUGCAMO_cloack_dec",
                             function(data)
                                DecCloack(nil)
                             end)
   Client.HookNetworkMessage("DEBUGCAMO_cloack_up",
                             function(data)
                                UpCloack(nil)
                             end)
   Client.HookNetworkMessage("DEBUGCAMO_visu_up",
                             function(data)
                                UpVisu(nil)
                             end)
   Client.HookNetworkMessage("DEBUGCAMO_visu_dec",
                             function(data)
                                DecVisu(nil)
                             end)

   Client.HookNetworkMessage("AddScoreboardFadeList",
                             function(data)
                                scoreboardFadePlayersName[data.name] = true
                             end)
   Client.HookNetworkMessage("ResetFadedScoreboardList",
                             function(data)
                                scoreboardFadePlayersName = {}
                             end)

   Client.HookNetworkMessage("TakeRagdoll",
                             function(data)
                                local p = Client.GetLocalPlayer()

                                if (p and data) then
                                end
                             end)
   Client.HookNetworkMessage("ToggleBeeper",
                             function(data)
                                local p = Client.GetLocalPlayer()

                                if (p) then
                                   MarineToggleBeeper(p)
                                end
                             end)

   Client.HookNetworkMessage("ImpulseRagdoll",
                             function(data)
                                local p = Client.GetLocalPlayer()

                                if (p) then
                                   local ragdoll = GetEntities("Ragdoll", data.ragdoll_coord)
                                   -- not done yet
                                   if (#ragdoll > 0) then
                                      ragdoll = ragdoll[1]
                                      -- Print("First ragdoll found from server coord " .. ragdoll:GetOrigin():GetDistanceTo(data.impulse_start_coord) .. "meter")
                                      -- Print("Ragdoll is at " .. p:GetOrigin():GetDistanceTo(data.impulse_start_coord) .. "meter of me")
                                      -- ragdoll:SetOrigin(data.impulse_start_coord)
                                      ragdoll:GetPhysicsModel():AddImpulse(data.impulse_start_coord, data.impulse)
                                      -- else
                                      --    Print("No ragdoll found")
                                   end
                                end
                             end)

   -- Shared.RegisterNetworkMessage("DEBUGCAMO_cloack_dec",
   -- Shared.RegisterNetworkMessage("DEBUGCAMO_cloack_up",
   -- Shared.RegisterNetworkMessage("DEBUGCAMO_distor_dec",
   -- Shared.RegisterNetworkMessage("DEBUGCAMO_distor_up",

   -- Client.HookNetworkMessage(
   --    "ChangeModelSize",
   --    function(data)
   --       local p = Client.GetLocalPlayer()

   --       for _, babbler in ipairs(GetEntities("Skulk")) do
   --          if (babbler:GetIsAlive() and babbler:GetId() == data.id) then
   --             if (data.modelSize and babbler.modelSize ~= data.modelSize) then
   --                babbler.modelSize = data.modelSize
   --                if (babbler.hitBox) then
   --                   Shared.DestroyCollisionObject(babbler.hitBox)
   --                   babbler.hitBox = nil
   --                   babbler:OnUpdatePhysics()
   --                end
   --             end
   --             break
   --          end
   --       end
   --    end)





   local last_player_sound = nil
   Client.HookNetworkMessage(
      "PlayClientPrivateMusic",
      function(data)
         local p = Client.GetLocalPlayer()
         if (p) then
            if (data.nostop ~= true) then
               if (last_player_sound) then
                  Shared.StopSound(p, last_player_sound)
               end
               last_player_sound = data.music
            end
            Shared.PlaySound(p, data.music, data.volume)
            -- p:FadedMessage("Music " .. data.music .. " played")
         end
      end)

   Client.HookNetworkMessage(
      "SendSelectEquipmentMessage",
      function(data)
         local p = Client.GetLocalPlayer()

         if (p) then
            Client:SendSelectEquipmentMessage()
         end
      end)

   Client.HookNetworkMessage("setFakeMarineName",
                 function(data)
                local p = Client.GetLocalPlayer()

                if (p) then
                   for index, fakeMarine in ipairs(GetEntitiesForTeam("Marine", 2)) do
                      if (fakeMarine:GetName() == data.fakeMarineRealName) then
                     -- local fakeMarine = Shared:GetPlayerByName(data.fakeMarineRealName)
                     fakeMarine:SetFakeMarineName(data.fakeMarineName)
                      end
                   end
                end
                 end)
end
