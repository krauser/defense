
---------------------------
---------------------------
---------------------------

fadedkTechId = {
   -- FADED extra
   'LaySentryBattery',
   'LaySkulk', 'LayGorge', 'LayLerk', 'LayFade', 'LayOnos',
   'FlameMine', 'LayFlameMines', 'HeavyShotgun',
   'NapalmGrenade', 'NapalmGrenadeProjectile',
   'HealGrenade', 'HealGrenadeProjectile', 'SwipeLeap',
}

for key, name in ipairs( kTechId ) do
   if (#fadedkTechId <= 511) then -- should not get above 511
      table.insert(fadedkTechId, kTechId[key])
      -- Shared.Message("Adding to fadedkTechId[" .. tostring(#fadedkTechId) .. "] = " .. tostring(name))
   end
end

-- Increase techNode network precision if more needed
kTechIdMax  = math.pow(2, math.ceil( math.log( #fadedkTechId )/ math.log(2) ) ) - 1 -- use all the bits

-- To be compliant with ns2+
for i = #fadedkTechId + 1, kTechIdMax do
   fadedkTechId[i] = 'unused'.. i
   -- Shared.Message("Adding to fadedkTechId[" .. tostring(i) .. "] = " .. tostring("unused" .. i))
end

kTechId = enum(fadedkTechId)


-- Shared.Message("FadedTechTreeConstants loaded with kTechIdMax = " .. tostring(kTechIdMax))
