local spectatorOnInitialized = Spectator.OnInitialized
function Spectator:OnInitialized()
   if (spectatorOnInitialized) then
      spectatorOnInitialized(self)
   end
   if (Client and Client.GetLocalPlayer() == self)
   then
      self.GUIAlienCounter = GetGUIManager():CreateGUIScript("FadedGUIAlienCounter")
   end
end


local spectatorOnDestroy = Spectator.OnDestroy
function Spectator:OnDestroy()
   if (spectatorOnDestroy) then
      spectatorOnDestroy(self)
   end
   if (Client) then
      GetGUIManager():DestroyGUIScript(self.GUIAlienCounter)
   end
end
