-- ===================== Faded Mod =====================
--
-- lua\FadedUpdateServer.lua
--
--    Create by: JB (jeanbaptiste.laurent.pro@gmail.com)
--
-- =====================================================

local time_counter = 0

local function OnUpdateServer(deltaTime)
   local update_time = 0.5
   time_counter = time_counter + deltaTime
   -- -- Testing the blink loop code
   -- for index, player in ipairs(GetEntities("Fade")) do
   --    FadedOnUpdate(player, deltaTime)
   -- end
   if (time_counter > update_time
       and GetGamerules() and GetGamerules().team1) then
      local allPlayers = GetGamerules().team1:GetPlayers()

      for _, marine in pairs(allPlayers) do
         if (marine.SetIsSighted) then
            marine:SetIsSighted(true) -- Faded can see marine on minimap
         end

      end
      time_counter = time_counter - update_time
   end
end

Event.Hook("UpdateServer", OnUpdateServer)
