-- ===================== Faded Mod =====================
--
-- lua\FadedConsoleCommands.lua
--
--    Created by: Rio (rio@myrio.de)
--
-- =====================================================

local function OnCommandFaded(player)
    Shared.Message(string.format("Faded mod: version %s", kFadedModVersion or "error"))
end

-- No Faded console command
local function NoFadedCommand(player)
   Shared.Message("Faded mod: To be removed from de Faded list, "
                     .. "type 'nofade' on the chat")
end

local function UnstuckCommand(player)
   Shared.Message("Faded mod: This command has to be typed in the main chat.")
end

local function SpectateFadedCommand(player)
   Shared.Message("Faded mod: This command has to be typed in the main chat.")
end

local function killAllLifeform()
   local total_killed = 0
   for _, ent_name in ipairs({"Skulk", "Babbler", "Gorge", "Lerk", "Fade", "Onos", "BoneWall", "Hydra", "Cyst", "Clog", "Whip", "TunnelEntrance"}) do
      for _, entity in ipairs(GetEntitiesForTeam(ent_name, 2)) do
         if (entity and entity:GetIsAlive()) --and not entity.client)
         then
            entity:Kill()
            total_killed = total_killed + 1
         end
      end
   end
   return total_killed
end

-- -- local RealOnCommandKill = OnCommandKill
-- function OnCommandKill(client)

--    local player = client:GetControllingPlayer()
--    if (player:isa("Marine") and GetGamerules():GetGameStarted()) then
--       local id = player:GetName()--player:GetSteamId()
--       if (player and player:GetTeamNumber() == 1 and id and not DefIsVirtual(player))
--       then
--          local isSuicide = true --not doer and not attacker
--          local fullHealth = true --player:GetHealth() / player:GetMaxHealth() > 0.7
--          if (isSuicide and fullHealth) then
--             if (sacrified_marine[id] == nil) then
--                Shared:SatanMessage(tostring(player:GetName()) .. " sacrifice accepted")
--                sacrified_marine[id] = id

--                local total_killed = killAllLifeform()
--                Shared:SatanMessage(player:GetName() .. " and " .. tostring(total_killed) .. " souls sent to Hell")
--             else
--                player:SatanMessage("You have already sold your soul, now burn !")
--             end
--          end
--       end
--    end

--    -- if (RealOnCommandKill) then
--    --    RealOnCommandKill(client)
--    -- end
-- end

Event.Hook("Console_unstuck", UnstuckCommand)

Event.Hook("Console_spectatefade", SpectateFadedCommand)
Event.Hook("Console_spectatefaded", SpectateFadedCommand)

-- Event.Hook("Console_kill", OnCommandKill)
Event.Hook("Console_faded", OnCommandFaded)
Event.Hook("Console_nofade", NoFadedCommand)
Event.Hook("Console_nofaded", NoFadedCommand)
