
-- local eggOnInitialized = Egg.OnInitialized
-- function Egg:OnInitialized()
--    eggOnInitialized(self)
--    hatch(self)
-- end

local contaminationOnUpdate =  Contamination.OnUpdate
function Contamination:OnUpdate(deltaTime)
   contaminationOnUpdate(self, deltaTime)
   if (Server) then
      if (self.maxSpawn == nil) then
         self.maxSpawn = 5
      end
      if (self.spawnDelay == nil or self.spawnDelay + 0.3 < Shared.GetTime()) then
         if (self.nbSpawn == nil or self.nbSpawn < self.maxSpawn) then
            if (self.nbSpawn == nil) then
               self.nbSpawn = 0
            end
            self.nbSpawn = self.nbSpawn + 1
            local alien = nil
            local rand = math.random()
            -- if (rand > 0.97 and com) then
            --    alien = spawnAlienCreature(Onos.kMapName, self:GetOrigin())
            -- elseif (rand > 0.94 and com) then
            --    alien = spawnAlienCreature(Fade.kMapName, self:GetOrigin())
            if (rand > 0.90) then
               alien = spawnAlienCreature(Lerk.kMapName, self:GetOrigin())
               -- elseif (rand > 0.85 and com) then
               --    alien = spawnAlienCreature(Gorge.kMapName, self:GetOrigin())
            else
               alien = spawnAlienCreature(Babbler.kMapName, self:GetOrigin())
            end
            -- if (alien) then
            --    alien:SetOrigin(self:GetOrigin() + Vector(0, 1, 0))
            -- end
            self.spawnDelay = Shared.GetTime()
         end
      end
   end
end
