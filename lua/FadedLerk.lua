
function Lerk:GetBaseArmor()
    return kLerkArmor * getAlienHealthScalar()
end

function Lerk:GetBaseHealth()
   return kLerkHealth * getAlienHealthScalar()
end

local lerkGetMaxSpeed = Lerk.GetMaxSpeed
function Lerk:GetMaxSpeed(possible)
   local speed = lerkGetMaxSpeed(self, possible) * getAlienSpeedScalar()

   speed = AlienGetSpeedBonusIfFar(self, speed)
   if (self.electrified) then
      speed = speed / kFadedPulseSlowDownFactor
   end
   return (speed)
end

------------------------------------------------------

local lerkOnInitialized = Lerk.OnInitialized
function Lerk:OnInitialized()
   lerkOnInitialized(self)
   if (Server) then
      self:SetIsThirdPerson(7)
   end
end

local lerkOnUpdate = Lerk.OnUpdate
function Lerk:OnUpdate(deltaTime)
   lerkOnUpdate(self, deltaTime)
   -- if (Server) then
   --    local dist = 6
   --    local origin = self:GetEyePos() + self:GetViewAngles():GetCoords().zAxis * dist
   --    self:SetDesiredCamera(0.0, { move = true }, origin, nil, 0)
   -- end
end

local lerkOnTakeDamage = Lerk.OnTakeDamage
function Lerk:OnTakeDamage(damage, attacker, doer, point, direction, damageType, preventAlert)
   if (lerkOnTakeDamage) then
      lerkOnTakeDamage(self, damage, attacker, doer, point, direction, damageType, preventAlert)
   end
end


-- function Lerk:OnAdjustModelCoords(modelCoords)
--    local scale = 1
--    local coords = modelCoords

--    -- if (self.defenseDragon) then
--       scale = 2
--    -- end
--    coords.xAxis = coords.xAxis * scale -- Largeur
--    coords.yAxis = coords.yAxis * scale -- Hauteur
--    coords.zAxis = coords.zAxis * scale * 1.3 -- Profondeur
--    return coords
-- end
