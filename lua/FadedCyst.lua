kCystHealth = 15
kCystArmor = 0

kMatureCystHealth = 30
kMatureCystArmor = 0

kMinMatureCystHealth = 30

if (Server) then
   function Cyst:GetIsActuallyConnected()
      return true
   end
end


/**
   * Note: On the server side, used GetIsActuallyConnected()!
*/
function Cyst:GetIsConnected()
   return true--self.connected
end

function Cyst:GetIsConnectedAndAlive()
   return self:GetIsAlive() --self.connected and self:GetIsAlive()
end

function Cyst:GetInfestationRadius()
    return 8
end

function Cyst:GetInfestationMaxRadius()
    return 12
end

local selectableMixinGetIsSelectable = SelectableMixin.GetIsSelectable
function SelectableMixin:GetIsSelectable(byTeamNumber)
   if (not self:isa("Cyst")) then
      return selectableMixinGetIsSelectable(self, byTeamNumber)
   end
   return false
end

local refresh_rate = 10
function defenseRandomAlienBuildSpawn(self)
   if (Server) then
      if (self.defenseBuildRefresh == nil) then
         self.defenseBuildRefresh = Shared.GetTime()
      end
      if (self:GetCreationTime() + 10 < Shared.GetTime()) then
         if (self.defenseBuildRefresh + refresh_rate < Shared.GetTime()) then
            self.defenseBuildRefresh = Shared.GetTime()

            if (not self:isa("Hive")) then
               if (#GetEntitiesForTeamWithinRange("Player", 1, self:GetOrigin(), 70) == 0) then
                  return -- Do not spawn entities too far away
               end
            end

            if (math.random() < 0.5 or self:isa("Hive")) then
               local orig = GetLocationAroundFor(self:GetOrigin(), kTechId.Armory, 6)
               if (orig) then

                  orig = orig - Vector(0, 0.6, 0)
                  local item = math.random()
                  if ((not self:isa("Hive") and item > 0.40) or item > 0.70) then
                     local spores = CreateEntity(SporeCloud.kMapName, orig, 2)
                     if (spores) then
                        spores:SetTravelDestination( orig )
                     end
                  elseif (item > 0.30) then
                     CreateEntity(Babbler.kMapName, orig, 2)
                  elseif (item > 0.22) then

                     if (self:isa("Hive")) then
                        local rand_struct = math.random()
                        if (rand_struct > 0.85) then
                           CreateEntity(Crag.kMapName, orig, 2)
                        elseif (rand_struct > 0.70) then
                           CreateEntity(Shift.kMapName, orig, 2)
                        elseif (rand_struct > 0.55) then
                           CreateEntity(Shade.kMapName, orig, 2)
                        elseif (rand_struct > 0.40) then
                           CreateEntity(Shell.kMapName, orig, 2)
                        elseif (rand_struct > 0.25) then
                           CreateEntity(Spur.kMapName, orig, 2)
                        elseif (rand_struct > 0.10) then
                           CreateEntity(Veil.kMapName, orig, 2)
                        else
                           if (#GetEntitiesForTeamWithinRange("Drifter", 2, self:GetOrigin(), 30) < 2)
                           then
                              CreateEntity(Drifter.kMapName, orig, 2)
                           end
                        end
                     else
                        CreateEntity(BabblerEgg.kMapName, orig, 2)
                     end

                  elseif (item > 0.15) then
                     CreateEntity(Clog.kMapName, orig, 2) -- Just a clog
                  elseif (item > 0.05 and #GetEntitiesForTeam("Hydra", 2) < 12) then
                     if (math.random() > 0.25) then
                        CreateEntity(Hydra.kMapName, orig, 2)
                     else
                        CreateEntity(Whip.kMapName, orig, 2)
                     end
                  else
                     if (#GetEntitiesForTeam("Clog", 2) < 17) then
                        local clog = CreateEntity(Clog.kMapName, orig, 2)
                        if (clog) then
                           clog:createClogWhip(2 + math.floor(math.random() * 5))
                        end
                     end
                  end
               end
            end
         end
      end
   end
end

local cystOnUpdate = Cyst.OnUpdate
function Cyst:OnUpdate(deltaTime)
   cystOnUpdate(self, deltaTime)
   defenseRandomAlienBuildSpawn(self)
end

if (Server) then
   local cystOnInitialized = Cyst.OnInitialized
   function Cyst:OnInitialized()
      cystOnInitialized(self)
      self:SetConstructionComplete()
   end

   function Cyst:OnUpdate(deltaTime)

      PROFILE("Cyst:OnUpdate")

      ScriptActor.OnUpdate(self, deltaTime)

      self.hasChild = true
      self.connected = true

      -- if self:GetIsAlive() then

      --    -- ServerUpdate(self, deltaTime)
      --    self.hasChild = #self.children > 0

      -- else

      --    local destructionAllowedTable = { allowed = true }
      --    if self.GetDestructionAllowed then
      --       self:GetDestructionAllowed(destructionAllowedTable)
      --    end

      --    if destructionAllowedTable.allowed then
      --       DestroyEntity(self)
      --    end

      -- end

   end

end
