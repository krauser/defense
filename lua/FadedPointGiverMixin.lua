-- ===================== Faded Mod =====================
--
-- lua\FadedPointGiverMixin.lua
--
--    Created by: Rio (rio@myrio.de)
--
-- =====================================================

local floor = math.floor

-- local pointGiverMixinGetPointValue = PointGiverMixin.GetPointValue
-- function PointGiverMixin:GetPointValue()
--    if (self:isa("Skulk")) then
--       return (kFadedBabblerOnKillScoreReward)
--    elseif (self:isa("Player")) then
--       return (0)
--    else
--       return (pointGiverMixinGetPointValue(self))
--    end
-- end

-- -- Update kill/death/assist values + remove vanilla scoring system
-- local pointGiverMixinPreOnKill = PointGiverMixin.PreOnKill
-- local function addAssistKillDeath(self, attacker, doer, point, direction)
--    local old_score = 0
--    if (attacker and attacker:isa("Player")) then
--       old_score = attacker:GetScore()
--    end
--    -- Do not change player point (only kill/assist/death)
--    pointGiverMixinPreOnKill(self, attacker, doer, point, direction)
--    if (attacker and attacker:isa("Player")) then
--       attacker.score = old_score
--    end
-- end

-- -- Give assist Point to marine
-- -- Add point to the Faded when he kill a marine
-- function PointGiverMixin:PreOnKill(attacker, doer, point, direction)
--    addAssistKillDeath(self, attacker, doer, point, direction)
--    -- Give point to the Faded on kill
--    if (self:isa("Skulk") and attacker and attacker.AddScore) then
--       attacker:AddScore(kFadedBabblerOnKillScoreReward)
--    end
--    if (self:isa("Marine") and attacker
--        and attacker.AddScore and (attacker:isa("Player"))) then
--       local score = 10
--       if (kMarineNb > 1) then
--          score = math.floor(kFadedModScorePointsForAllMarineKill / (kMarineNb - 1))
--       end
--       attacker:AddScore(score)
--    end
-- end

-- -- Save damage to add point for Fade kill assist on Marine side
-- function Fade:OnTakeDamage(damage, attacker, doer, point)
--    if (attacker ~= nil and attacker:isa("Marine")) then
--       local damageDoneList = self.damageDoneList or {}
--       local damageDoneAttacker = damageDoneList[attacker:GetName()] or 0

--       damageDoneAttacker = damageDoneAttacker + damage

--       damageDoneList[attacker:GetName()] = damageDoneAttacker
--       self.damageDoneList = damageDoneList
--       self:OnTakeDamageMentalHealthBonus(damage, attacker, doer, point)
--    end
-- end


-------------------------------------------------------

local pointGiverMixinPreOnKill = PointGiverMixin.PreOnKill
function PointGiverMixin:PreOnKill(attacker, doer, point, direction)

   -- local damageDoneList = self.damageDoneList or {}
   -- for playerName, damage in pairs(damageDoneList) do
   --    -- Get player entity
   --    for index, playerEntity in ientitylist(Shared.GetEntitiesWithClassname("Player")) do
   --       if (playerEntity:GetName() == playerName) then
   --          local score = damage / 100
   --          local res = damage / 80
   --          playerEntity:AddScore(score)
   --          playerEntity:AddResources(res)
   --          break
   --       end
   --    end
   -- end
   -- self.damageDoneList = {}
   if (attacker and attacker:isa("Player")) then
      local res = 0
      if (self:isa("Babbler")) then
         res = 0.5
      elseif (self:isa("Skulk") or self:isa("BoneWall")) then
         res = 1.4
      elseif (self:isa("Gorge")) then
         res = 2.2
      elseif (self:isa("Lerk")) then
         res = 3.1
      elseif (self:isa("Fade")) then
         res = 6.2
      elseif (self:isa("Onos")) then
         res = 12

         ---------------------

      elseif (self:isa("Clog")) then
         res = 0.2
      elseif (self:isa("Cyst")) then
         res = 0.2
      elseif (self:isa("Drifter")) then
         res = 2
      elseif (self:isa("Hydra")) then
         res = 2
      elseif (self:isa("Whip")) then
         res = 2
      elseif (self:isa("TunnelEntrance")) then
         res = 4
      elseif (self:isa("Crag")) then
         res = 3
      elseif (self:isa("Shift")) then
         res = 3
      elseif (self:isa("Crag")) then
         res = 5
      elseif (self:isa("Spur")) then
         res = 5
      elseif (self:isa("Veil")) then
         res = 5
      elseif (self:isa("Hive")) then
         res = 300

      end

      -- if (attacker:isa("Exo")) then -- Less res as exos
      --    attacker:AddResources(res * 0.6)
      -- else
      --    attacker:AddResources(res * 1.5)
      -- end
      for _, m in ipairs(GetEntitiesForTeam("Player", 1)) do
         m:AddResources(res / 7.5)
      end
      attacker:AddResources(res / 4) -- Bonus award for the killer
   end

   -- TODO: Give a % of res to everyone, and a bonus for the killer (teamwork)

   return pointGiverMixinPreOnKill(self, attacker, doer, point, direction)
end

-- -- Hook to increase Veteran damage
-- -- Self is the weapon
-- -- Attacker is the entity attacking
-- -- Target is the entity who receive damage
-- local damageMixinDoDamage = DamageMixin.DoDamage
-- function DamageMixin:DoDamage(damage, target, point, direction, surface, altMode, showtracer)

--    -- Increase veteran Damage
--    if (attacker and target and attacker:GetTeamNumber() ~= target:GetTeamNumber())
--    then
--       local damageDoneList = target.damageDoneList or {}
--       local damageDoneAttacker = damageDoneList[attacker:GetName()] or 0

--       damageDoneAttacker = damageDoneAttacker + damage

--       damageDoneList[attacker:GetName()] = damageDoneAttacker
--       target.damageDoneList = damageDoneList

--       --------------------
--       -- attacker:AddScore(damage / 1000)
--       -- attacker:AddResources(1)
--    end

--    damageMixinDoDamage(self, damage, target, point, direction, surface, altMode, showtracer)
-- end
