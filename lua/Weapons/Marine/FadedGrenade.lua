
local kGrenadeSpeed = 31

local grenadeDetonate = Grenade.Detonate
function Grenade:Detonate(targetHit)
   if (targetHit and targetHit:isa("Whip") and targetHit:GetIsBuilt()) then
      if (self.def_last_whack == nil or self.def_last_whack + 1 < Shared.GetTime()) then
         self.def_last_whack = Shared.GetTime()

         local whip = targetHit
         local marine = self.GetOwner and self:GetOwner() or nil

         -- whip:TriggerEffects("Owner ?")
         if (marine) then

            whip:FaceTarget(marine)
            whip:TriggerEffects("whip_attack")

            -- Shared:FadedMessage("Bouncing attack effect")

            -- Shared:FadedMessage("Owner found")
            local dir = (marine:GetModelOrigin() - self:GetOrigin()):GetUnit()
            local orig = self:GetOrigin() + dir * 0.5
            local grenade = marine:CreatePredictedProjectile("Grenade", self:GetOrigin(), dir * kGrenadeSpeed, 0.7, 0.45)
            if (grenade) then
               grenade:SetOwner(whip)
               -- Shared:FadedMessage("Grenade created")
               if (Server) then
                  DestroyEntity(self)
               end
            end
            whip.slapping = true
            return
         end
      end
   end

   if (grenadeDetonate) then
      grenadeDetonate(self, targetHit)
   end
end
