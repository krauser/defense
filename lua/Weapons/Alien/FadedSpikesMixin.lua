local kFireRate = 1/3
local kDragonBreathDamages = 0.04 --kFlamethrowerDamage / 2

local function CreateFlame(self, player, position, normal, direction)

    -- create flame entity, but prevent spamming:
    local nearbyFlames = GetEntitiesForTeamWithinRange("Flame", player:GetTeamNumber(), position, 1.5)

    if table.count(nearbyFlames) == 0 then

        local flame = CreateEntity(Flame.kMapName, position, player:GetTeamNumber())
        flame:SetOwner(player)

        local coords = Coords.GetTranslation(position)
        coords.yAxis = normal
        coords.zAxis = direction

        coords.xAxis = coords.yAxis:CrossProduct(coords.zAxis)
        coords.xAxis:Normalize()

        coords.zAxis = coords.xAxis:CrossProduct(coords.yAxis)
        coords.zAxis:Normalize()

        flame:SetCoords(coords)

    end

end

local function ApplyConeDamage(self, player)
   local eyePos  = player:GetEyePos()
   local ents = {}

   local fireDirection = player:GetViewCoords().zAxis
   local extents = Vector(0.17, 0.17, 0.17)
   local remainingRange = self:GetRange()

   local startPoint = Vector(eyePos)
   local filterEnts = {self, player}

   for i = 1, 20 do
      if remainingRange <= 0 then
         break
      end

      local trace = TraceMeleeBox(self, startPoint, fireDirection, extents, remainingRange, PhysicsMask.Flame, EntityFilterList(filterEnts))

      --DebugLine(startPoint, trace.endPoint, 0.3, 1, 0, 0, 1)

      -- -- Check for spores in the way.
      -- if Server and i == 1 then
      --     BurnSporesAndUmbra(self, startPoint, trace.endPoint)
      -- end

      if trace.fraction ~= 1 then
         if trace.entity then
            if HasMixin(trace.entity, "Live") then
               table.insertunique(ents, trace.entity)
            end
            table.insertunique(filterEnts, trace.entity)
         else

            -- Make another trace to see if the shot should get deflected.
            local lineTrace = Shared.TraceRay(startPoint, startPoint + remainingRange * fireDirection, CollisionRep.Damage, PhysicsMask.Flame, EntityFilterOne(player))

            if lineTrace.fraction < 0.8 then
               fireDirection = fireDirection + trace.normal * 0.55
               fireDirection:Normalize()
               if Server then
                  CreateFlame(self, player, lineTrace.endPoint, lineTrace.normal, fireDirection)
               end
            end

            remainingRange = remainingRange - (trace.endPoint - startPoint):GetLength()
            startPoint = trace.endPoint -- + fireDirection * kConeWidth * 2
         end
      else
         break
      end
   end

   for index, ent in ipairs(ents) do
      if ent ~= player and ent:GetTeamNumber() ~= self:GetTeamNumber() then
         local toEnemy = GetNormalizedVector(ent:GetModelOrigin() - eyePos)
         local health = ent:GetHealth()

         self:DoDamage(kDragonBreathDamages, ent, ent:GetModelOrigin(), toEnemy )

         -- Only light on fire if we successfully damaged them
         if ent:GetHealth() ~= health and HasMixin(ent, "Fire") then
            ent:SetOnFire(player, self)
         end

         if ent.GetEnergy and ent.SetEnergy then
            ent:SetEnergy(ent:GetEnergy() - 0.27 * kDragonBreathDamages)
         end

         if Server and ent:isa("Alien") then
            ent:CancelEnzyme()
         end
      end
   end
end

local function ShootFlame(self, player)
   local viewAngles = player:GetViewAngles()
   local viewCoords = viewAngles:GetCoords()

   viewCoords.origin = self:GetAttachPointOrigin("Head_Tongue_02") + viewCoords.zAxis * 2
   local endPoint = viewCoords.origin + viewCoords.zAxis * 3--self:GetRange()

   local trace = Shared.TraceRay(viewCoords.origin, endPoint, CollisionRep.Damage, PhysicsMask.Flame, EntityFilterAll())

   local range = (trace.endPoint - viewCoords.origin):GetLength()
   if range < 0 then
      range = range * (-1)
   end

   if trace.endPoint ~= endPoint and trace.entity == nil then
      local angles = Angles(0,0,0)
      angles.yaw = GetYawFromVector(trace.normal)
      angles.pitch = GetPitchFromVector(trace.normal) + (math.pi/2)

      local normalCoords = angles:GetCoords()
      normalCoords.origin = trace.endPoint
      range = range - 3
   end

   ApplyConeDamage(self, player)


end

local spikesMixinOnSecondaryAttack = SpikesMixin.OnSecondaryAttack
function SpikesMixin:OnSecondaryAttack(player)
   spikesMixinOnSecondaryAttack(self, player)

   -- if (DefIsVirtual(player)) then
   --    if self.lastAttackApplyTime == nil
   --    or self.lastAttackApplyTime + kFireRate < Shared.GetTime() then
   --       ShootFlame(self, player)
   --       self.lastAttackApplyTime  = Shared.GetTime()
   --    end
   --    player:TriggerEffects("dragonbreath_muzzle")
   -- end
end

local spikesMixinOnSecondaryAttackEnd = SpikesMixin.OnSecondaryAttackEnd
function SpikesMixin:OnSecondaryAttackEnd(player)
   spikesMixinOnSecondaryAttackEnd(self, player)
end

GetEffectManager():AddEffectData("FlamerModEffects", {
    dragonbreath_muzzle = {
        flamerMuzzleEffects =
        {
            {viewmodel_cinematic = "cinematics/marine/flamethrower/flame_1p.cinematic", attach_point = "Head_Tongue_02"},
           {weapon_cinematic = "cinematics/marine/flamethrower/flame.cinematic", attach_point = "Head_Tongue_02"},
        },
    },
})

local kSpread = Math.Radians(20)
local kSpikeSize = 0.03

-- Almost as ns2, only increase the spread to make it less OP
local function FireSpikes(self)

   local player = self:GetParent()    
   local viewAngles = player:GetViewAngles()
   viewAngles.roll = NetworkRandom() * math.pi * 2
   local shootCoords = viewAngles:GetCoords()
   
   -- Filter ourself out of the trace so that we don't hit ourselves.
   local filter = EntityFilterOneAndIsa(player, "Babbler")
   local range = kSpikesRange
   
   local numSpikes = kSpikesPerShot
   local startPoint = player:GetEyePos()
   
   local viewCoords = player:GetViewCoords()
   
   self.spiked = true
   self.silenced = GetHasSilenceUpgrade(player) and GetVeilLevel(player:GetTeamNumber()) > 0
   
   for spike = 1, numSpikes do

      -- Calculate spread for each shot, in case they differ
      -- FADED / Defense added
      local spread = kSpread
      if (player and not DefIsVirtual(player)) then
         spread = Math.Radians(4) -- Players are not affected
      end
      --

      local spreadDirection = CalculateSpread(viewCoords, spread, NetworkRandom) 

      local endPoint = startPoint + spreadDirection * range
      startPoint = player:GetEyePos()
      
      local trace = Shared.TraceRay(startPoint, endPoint, CollisionRep.Damage, PhysicsMask.Bullets, filter)
      if not trace.entity then
         local extents = GetDirectedExtentsForDiameter(spreadDirection, kSpikeSize)
         trace = Shared.TraceBox(extents, startPoint, endPoint, CollisionRep.Damage, PhysicsMask.Bullets, filter)
      end
      
      local distToTarget = (trace.endPoint - startPoint):GetLength()
      
      if trace.fraction < 1 then

         -- Have damage increase to reward close combat
         local damageDistScalar = Clamp(1 - (distToTarget / kSpikeMinDamageRange), 0, 1)
         local damage = kSpikeMinDamage + damageDistScalar * (kSpikeMaxDamage - kSpikeMinDamage)
         local direction = (trace.endPoint - startPoint):GetUnit()
         self:DoDamage(damage, trace.entity, trace.endPoint - direction * kHitEffectOffset, direction, trace.surface, true, math.random() < 0.75)
         
      end
      
   end
   
end

function SpikesMixin:OnTag(tagName)

   PROFILE("SpikesMixin:OnTag")

   if self.shootingSpikes and tagName == "shoot" then

      local player = self:GetParent()
      if player and player:GetEnergy() > self:GetSecondaryEnergyCost() then

         FireSpikes(self)
         self:GetParent():DeductAbilityEnergy(self:GetSecondaryEnergyCost())
         self.secondaryAttacking = true

      else
         self.shootingSpikes = false
         self.secondaryAttacking = false
      end

   end

end
